import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-invoice-dialog',
  templateUrl: './search-invoice-dialog.component.html',
  styleUrls: ['./search-invoice-dialog.component.scss']
})
export class SearchInvoiceDialogComponent implements OnInit {

  @Output() searchInvoiceCls = new EventEmitter<string>()

  @Output() invoiceAddInputChild = new EventEmitter<string>()

  @Input() searchInvoiceVisible: any

  @Input() invoiceDatas

  invoiceDisplayDatas: any

  invoice_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.invoiceData()
  }

  invoiceData() {
    this.invoiceDisplayDatas = [...this.invoiceDatas]
  }

  searchInvoiceClose() {
    this.invoice_search_title = ''
    this.invoiceDisplayDatas = [...this.invoiceDatas]    
    this.searchInvoiceCls.emit()
  }

  searchInvoice() {
    let invoiceSearchLower = this.invoice_search_title.toLowerCase()

    const filterFunc = (item: { invoice_code: string }) => {
      return (item.invoice_code.toLowerCase().indexOf(invoiceSearchLower) !== -1)
    }

    const data = this.invoiceDatas.filter((item: { invoice_code: string; }) => filterFunc(item))

    this.invoiceDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(invoiceSearchLower.length == 0) {
      this.invoiceDisplayDatas = [...this.invoiceDatas]
    }
    
  }
  
  invoiceAddInput(invoice_data) {
    this.invoiceAddInputChild.emit(invoice_data)

    this.searchInvoiceClose()
  }

}
