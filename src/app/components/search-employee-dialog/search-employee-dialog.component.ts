import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-employee-dialog',
  templateUrl: './search-employee-dialog.component.html',
  styleUrls: ['./search-employee-dialog.component.scss']
})
export class SearchEmployeeDialogComponent implements OnInit {

  @Output() searchEmployeeCls = new EventEmitter<string>()

  @Output() employeeAddInputChild = new EventEmitter<string>()

  @Input() searchEmployeeVisible: any

  @Input() employeeDatas

  employeeDisplayDatas: any

  employee_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.employeeData()
  }

  employeeData() {
    this.employeeDisplayDatas = [...this.employeeDatas]
  }

  searchEmployeeClose() {
    this.employee_search_title = ''
    this.employeeDisplayDatas = [...this.employeeDatas]    
    this.searchEmployeeCls.emit()
  }

  searchEmployee() {
    let employeeSearchLower = this.employee_search_title.toLowerCase()

    const filterFunc = (item: { employee_identification_no: string; employee_firstname: string; employee_lastname: string }) => {
      return (item.employee_identification_no.toLowerCase().indexOf(employeeSearchLower) !== -1 || item.employee_firstname.toLowerCase().indexOf(employeeSearchLower) !== -1 || item.employee_lastname.toLowerCase().indexOf(employeeSearchLower) !== -1
      )
    }

    const data = this.employeeDatas.filter((item: { employee_identification_no: string; employee_firstname: string; employee_lastname: string }) => filterFunc(item))

    this.employeeDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(employeeSearchLower.length == 0) {
      this.employeeDisplayDatas = [...this.employeeDatas]
    }
    
  }
  
  employeeAddInput(employee_data) {
    this.employeeAddInputChild.emit(employee_data)

    this.searchEmployeeClose()
  }

}
