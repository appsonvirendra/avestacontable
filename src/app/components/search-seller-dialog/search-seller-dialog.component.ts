import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-seller-dialog',
  templateUrl: './search-seller-dialog.component.html',
  styleUrls: ['./search-seller-dialog.component.scss']
})
export class SearchSellerDialogComponent implements OnInit {

  @Output() searchSellerCls = new EventEmitter<string>()

  @Output() sellerAddInputChild = new EventEmitter<string>()

  @Input() searchSellerVisible: any

  @Input() sellerDatas

  sellerDisplayDatas: any

  seller_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.sellerData()
  }

  sellerData() {
    this.sellerDisplayDatas = [...this.sellerDatas]
  }

  searchSellerClose() {
    this.seller_search_title = ''
    this.sellerDisplayDatas = [...this.sellerDatas]    
    this.searchSellerCls.emit()
  }

  searchSeller() {
    let sellerSearchLower = this.seller_search_title.toLowerCase()

    const filterFunc = (item: { seller_name: string; seller_email: string; seller_mobile: string }) => {
      return (item.seller_name.toLowerCase().indexOf(sellerSearchLower) !== -1 || item.seller_email.toLowerCase().indexOf(sellerSearchLower) !== -1 || item.seller_mobile.toLowerCase().indexOf(sellerSearchLower) !== -1
      )
    }

    const data = this.sellerDatas.filter((item: { seller_name: string; seller_email: string; seller_mobile: string }) => filterFunc(item))

    this.sellerDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(sellerSearchLower.length == 0) {
      this.sellerDisplayDatas = [...this.sellerDatas]
    }
    
  }
  
  sellerAddInput(seller_data) {
    this.sellerAddInputChild.emit(seller_data)

    this.searchSellerClose()
  }

}
