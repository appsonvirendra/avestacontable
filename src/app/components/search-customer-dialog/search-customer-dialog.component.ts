import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-customer-dialog',
  templateUrl: './search-customer-dialog.component.html',
  styleUrls: ['./search-customer-dialog.component.scss']
})
export class SearchCustomerDialogComponent implements OnInit {

  @Output() searchCustomerCls = new EventEmitter<string>()

  @Output() customerAddInputChild = new EventEmitter<string>()

  @Input() searchCustomerVisible: any

  @Input() customerDatas

  customerDisplayDatas: any

  customer_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.customerData()
  }

  customerData() {
    this.customerDisplayDatas = [...this.customerDatas]
  }

  searchCustomerClose() {
    this.customer_search_title = ''
    this.customerDisplayDatas = [...this.customerDatas]    
    this.searchCustomerCls.emit()
  }

  searchCustomer() {
    let customerSearchLower = this.customer_search_title.toLowerCase()

    const filterFunc = (item: { customer_name: string; customer_email: string; customer_mobile: string }) => {
      return (item.customer_name.toLowerCase().indexOf(customerSearchLower) !== -1 || item.customer_email.toLowerCase().indexOf(customerSearchLower) !== -1 || 
      item.customer_mobile.toLowerCase().indexOf(customerSearchLower) !== -1
      )
    }

    const data = this.customerDatas.filter((item: { customer_name: string; customer_email: string; customer_mobile: string }) => filterFunc(item))

    this.customerDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(customerSearchLower.length == 0) {
      this.customerDisplayDatas = [...this.customerDatas]
    }
    
  }
  
  customerAddInput(customer_data) {
    this.customerAddInputChild.emit(customer_data)

    this.searchCustomerClose()
  }
  
}
