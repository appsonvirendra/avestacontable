import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbuttonComponent } from './backbutton/backbutton.component';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

// mdi icon set up
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry, MatIconModule } from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchSupplierDialogComponent } from './search-supplier-dialog/search-supplier-dialog.component';
import { SearchUserDialogComponent } from './search-user-dialog/search-user-dialog.component';
import { SearchCustomerDialogComponent } from './search-customer-dialog/search-customer-dialog.component';
import { SearchProductDialogComponent } from './search-product-dialog/search-product-dialog.component';
import { SearchProductDialogNormalComponent } from './search-product-dialog-normal/search-product-dialog-normal.component';
import { SearchEmployeeDialogComponent } from './search-employee-dialog/search-employee-dialog.component';
import { SearchInvoiceDialogComponent } from './search-invoice-dialog/search-invoice-dialog.component';
import { SearchAccountDialogComponent } from './search-account-dialog/search-account-dialog.component';
import { SearchBankaccountDialogComponent } from './search-bankaccount-dialog/search-bankaccount-dialog.component';
import { SearchTaxDialogComponent } from './search-tax-dialog/search-tax-dialog.component';
import { SearchSellerDialogComponent } from './search-seller-dialog/search-seller-dialog.component';
import { CurrencySignComponent } from './currency-sign/currency-sign.component';


@NgModule({
  declarations: [
    BackbuttonComponent,
    SearchSupplierDialogComponent,
    SearchUserDialogComponent,
    SearchCustomerDialogComponent,
    SearchProductDialogComponent,
    SearchProductDialogNormalComponent,
    SearchEmployeeDialogComponent,
    SearchInvoiceDialogComponent,
    SearchAccountDialogComponent,
    SearchBankaccountDialogComponent,
    SearchTaxDialogComponent,
    SearchSellerDialogComponent,
    CurrencySignComponent   
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatIconModule,
    FormsModule,
    ReactiveFormsModule        
  ],
  exports: [
    BackbuttonComponent,
    SearchSupplierDialogComponent,
    SearchUserDialogComponent,
    SearchCustomerDialogComponent,
    SearchProductDialogComponent,
    SearchProductDialogNormalComponent,
    SearchEmployeeDialogComponent,
    SearchInvoiceDialogComponent,
    SearchAccountDialogComponent,
    SearchBankaccountDialogComponent,
    SearchTaxDialogComponent,
    SearchSellerDialogComponent,
    CurrencySignComponent           
  ]
})
export class ComponentsModule { 
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
  }    
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}