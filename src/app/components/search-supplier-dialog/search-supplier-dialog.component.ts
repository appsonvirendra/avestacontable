import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-supplier-dialog',
  templateUrl: './search-supplier-dialog.component.html',
  styleUrls: ['./search-supplier-dialog.component.scss']
})
export class SearchSupplierDialogComponent implements OnInit {

  @Output() searchSupplierCls = new EventEmitter<string>()

  @Output() supplierAddInputChild = new EventEmitter<string>()

  @Input() searchSupplierVisible: any

  @Input() supplierDatas

  supplierDisplayDatas: any

  supplier_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.supplierData()
  }

  supplierData() {
    this.supplierDisplayDatas = [...this.supplierDatas]
  }

  searchSupplierClose() {
    this.supplier_search_title = ''
    this.supplierDisplayDatas = [...this.supplierDatas]    
    this.searchSupplierCls.emit()
  }

  searchSupplier() {
    let supplierSearchLower = this.supplier_search_title.toLowerCase()

    const filterFunc = (item: { supplier_name: string; email: string; phone: string }) => {
      return (item.supplier_name.toLowerCase().indexOf(supplierSearchLower) !== -1 || item.email.toLowerCase().indexOf(supplierSearchLower) !== -1 || item.phone.toLowerCase().indexOf(supplierSearchLower) !== -1
      )
    }

    const data = this.supplierDatas.filter((item: { supplier_name: string; email: string; phone: string }) => filterFunc(item))

    this.supplierDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(supplierSearchLower.length == 0) {
      this.supplierDisplayDatas = [...this.supplierDatas]
    }
    
  }
  
  supplierAddInput(supplier_data) {
    this.supplierAddInputChild.emit(supplier_data)

    this.searchSupplierClose()
  }

}
