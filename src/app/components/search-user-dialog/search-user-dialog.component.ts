import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-user-dialog',
  templateUrl: './search-user-dialog.component.html',
  styleUrls: ['./search-user-dialog.component.scss']
})
export class SearchUserDialogComponent implements OnInit {

  @Output() searchUserCls = new EventEmitter<string>()

  @Output() userAddInputChild = new EventEmitter<string>()

  @Input() searchUserVisible: any

  @Input() userDatas

  userDisplayDatas: any

  user_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {
    this.userData()
  }

  userData() {
    this.userDisplayDatas = [...this.userDatas]
  }

  searchUserClose() {
    this.user_search_title = ''
    this.userDisplayDatas = [...this.userDatas]    
    this.searchUserCls.emit()
  }

  searchUser() {
    let userSearchLower = this.user_search_title.toLowerCase()

    const filterFunc = (item: { user_firstname: string; user_lastname: string; user_email: string; user_branch: string }) => {
      return (
        item.user_firstname.toLowerCase().indexOf(userSearchLower) !== -1 || 
        item.user_lastname.toLowerCase().indexOf(userSearchLower) !== -1 || 
        item.user_email.toLowerCase().indexOf(userSearchLower) !== -1 || 
        item.user_branch.toLowerCase().indexOf(userSearchLower) !== -1
      )
    }

    const data = this.userDatas.filter((item: { user_firstname: string; user_lastname: string; user_email: string; user_branch: string }) => filterFunc(item))

    this.userDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(userSearchLower.length == 0) {
      this.userDisplayDatas = [...this.userDatas]
    }    
  }
  
  userAddInput(user_data) {
    this.userAddInputChild.emit(user_data)

    this.searchUserClose()
  }  

}
