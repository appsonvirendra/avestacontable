import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-tax-dialog',
  templateUrl: './search-tax-dialog.component.html',
  styleUrls: ['./search-tax-dialog.component.scss']
})
export class SearchTaxDialogComponent implements OnInit {

  @Output() searchTaxCls = new EventEmitter<string>()

  @Output() taxAddInputChild = new EventEmitter<string>()

  @Input() searchTaxVisible: any

  @Input() taxDatas

  taxDisplayDatas: any

  taxAddForm: FormGroup

  taxEditForm: FormGroup
  taxEditVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {    
    this.taxAddFormInit()

    this.taxEditFormInit()
  }

  taxAddFormInit() {
    this.taxAddForm = this.fb.group({
      tax_name: [null, [Validators.required]],
      tax_description: [null, [Validators.required]],
      tax_value: [null, [Validators.required]],
    })     
  } 

  searchTaxClose() {
    this.taxAddForm.reset()
    this.searchTaxCls.emit()
  }

  saveTax() {
    for (const i in this.taxAddForm.controls) {
      this.taxAddForm.controls[i].markAsDirty()
      this.taxAddForm.controls[i].updateValueAndValidity()
    }          

    if(this.taxAddForm.valid) {      
      this.taxDatas.push(this.taxAddForm.value)
      this.taxAddForm.reset()
    }
  }  

  removeTax(taxIndex) {
    this.taxDatas.splice(taxIndex, 1)
  }

  // tax edit form
  taxEditFormInit() {
    this.taxEditForm = this.fb.group({
      tax_name: [null, [Validators.required]],
      tax_description: [null, [Validators.required]],
      tax_value: [null, [Validators.required]],
    })     
  } 

  taxEditOpen() {
    this.taxEditVisible = true
  }

  taxEditClose() {
    this.taxEditForm.reset()
    this.taxEditVisible = false
  }  

  saveTaxEdit() {
    for (const i in this.taxEditForm.controls) {
      this.taxEditForm.controls[i].markAsDirty()
      this.taxEditForm.controls[i].updateValueAndValidity()
    }          

    if(this.taxEditForm.valid) {      
      this.taxEditClose()
    }
  }  


}
