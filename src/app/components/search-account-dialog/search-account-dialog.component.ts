import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-account-dialog',
  templateUrl: './search-account-dialog.component.html',
  styleUrls: ['./search-account-dialog.component.scss']
})
export class SearchAccountDialogComponent implements OnInit {

  @Output() searchAccountCls = new EventEmitter<string>()

  @Output() accountAddInputChild = new EventEmitter<string>()

  @Input() searchAccountVisible: any

  @Input() accountDatas

  accountDisplayDatas: any

  account_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.accountData()
  }

  accountData() {
    this.accountDisplayDatas = [...this.accountDatas]
  }

  searchAccountClose() {
    this.account_search_title = ''
    this.accountDisplayDatas = [...this.accountDatas]    
    this.searchAccountCls.emit()
  }

  searchAccount() {
    let accountSearchLower = this.account_search_title.toLowerCase()

    const filterFunc = (item: { account_description: string }) => {
      return (item.account_description.toLowerCase().indexOf(accountSearchLower) !== -1)
    }

    const data = this.accountDatas.filter((item: { account_description: string }) => filterFunc(item))

    this.accountDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(accountSearchLower.length == 0) {
      this.accountDisplayDatas = [...this.accountDatas]
    }
    
  }
  
  accountAddInput(account_data) {
    this.accountAddInputChild.emit(account_data)

    this.searchAccountClose()
  }

}
