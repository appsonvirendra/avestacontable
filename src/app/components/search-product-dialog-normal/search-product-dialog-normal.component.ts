import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CategoriesService } from 'src/app/pages/settings/inventory/categories/categories.service';

@Component({
  selector: 'app-search-product-dialog-normal',
  templateUrl: './search-product-dialog-normal.component.html',
  styleUrls: ['./search-product-dialog-normal.component.scss']
})
export class SearchProductDialogNormalComponent implements OnInit {

  @Output() searchProductNormalCls = new EventEmitter<string>()

  @Output() productNormalAddInputChild = new EventEmitter<string>()

  @Input() searchProductNormalVisible: any

  @Input() saleDatas

  productNormalDisplayDatas: any

  productNormal_search_title = ''
  // product search by category
  product_search_category: any = '0'  
  
  sortName: string | null = null
  sortValue: string | null = null  

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  cateLoader: boolean = false
  categoriesDatas: any

  constructor(
    private translate: TranslateService,
    private message: NzMessageService,
    private _categoriesService: CategoriesService
  ) { 

  }

  ngOnInit() {    
    this.productNormalData()
    
    this.populateGetCategoriesList()
  }

  populateGetCategoriesList() {
    this.cateLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._categoriesService.getCategoriesList(params).subscribe((response) => {
      this.categoriesDatas = response
      this.openMessageBar(this.translate.instant('CATEGORIES_LIST_RECEIVED_SUCCESSFUL'))

      this.cateLoader = false
    }, (error) => {
      this.cateLoader = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCategoriesList')
    })     
  }  

  productNormalData() {
    this.productNormalDisplayDatas = [...this.saleDatas]
  }

  searchProductNormalClose() {
    this.productNormal_search_title = ''
    this.product_search_category = '0'
    this.productNormalDisplayDatas = [...this.saleDatas]    
    this.searchProductNormalCls.emit()
  }

  searchProductNormal() {
    let productNormalSearchLower = this.productNormal_search_title.toLowerCase()
    let productSearchCat = this.product_search_category.toString()

    const filterFunc = (item: { product_code: string; product_name: string, category_id: string }) => {

      if(productSearchCat == '0') {
        return (item.product_code.toLowerCase().indexOf(productNormalSearchLower) !== -1 ||      item.product_name.toLowerCase().indexOf(productNormalSearchLower) !== -1
        )
      } else {
        return ((item.product_code.toLowerCase().indexOf(productNormalSearchLower) !== -1 ||      item.product_name.toLowerCase().indexOf(productNormalSearchLower) !== -1) &&
        item.category_id == productSearchCat
        )        
      }      

    }

    const data = this.saleDatas.filter((item: { product_code: string; product_name: string; category_id: string }) => filterFunc(item))

    this.productNormalDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(productNormalSearchLower.length == 0 && productSearchCat == '0') {
      this.productNormalDisplayDatas = [...this.saleDatas]
    }
    
  }
  
  productNormalAddInput(productNormal_data) {
    this.productNormalAddInputChild.emit(productNormal_data)

    this.searchProductNormalClose()
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}
