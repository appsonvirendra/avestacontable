import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CategoriesService } from 'src/app/pages/settings/inventory/categories/categories.service';

import { DiscountsService } from 'src/app/pages/settings/other-settings/discounts/discounts.service';

@Component({
  selector: 'app-search-product-dialog',
  templateUrl: './search-product-dialog.component.html',
  styleUrls: ['./search-product-dialog.component.scss']
})
export class SearchProductDialogComponent implements OnInit {

  @Output() searchProductCls = new EventEmitter<string>()

  @Output() productAddInputChild = new EventEmitter<string>()

  @Input() searchProductVisible: any

  @Input() saleDatas

  productDisplayDatas: any

  product_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  // product search by category
  product_search_category: any = '0'

  product_table_price_isv: any = []
  product_table_cost_price: any = []
  product_table_sale_price: any = []
  product_table_quantity: any = []
  product_table_discount: any = []
  product_table_discount_percent: any = []

  added_product:any = {}

  successErrorHandle: any

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  cateLoader: boolean = false
  categoriesDatas: any
  
  discountListLoader: boolean = false
  discountsDatas: any  

  constructor(
    private translate: TranslateService,
    private message: NzMessageService,
    private _categoriesService: CategoriesService,
    private _discountsService: DiscountsService    
  ) { 

  }

  ngOnInit() {        
    this.productData()
    
    this.populateGetCategoriesList()
    this.populateGetDiscountsList()    
  }

  populateGetCategoriesList() {
    this.cateLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._categoriesService.getCategoriesList(params).subscribe((response) => {
      this.categoriesDatas = response
      this.openMessageBar(this.translate.instant('CATEGORIES_LIST_RECEIVED_SUCCESSFUL'))

      this.cateLoader = false
    }, (error) => {
      this.cateLoader = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCategoriesList')
    })     
  }    

  populateGetDiscountsList() {
    this.discountListLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._discountsService.getDiscountsList(params).subscribe((response) => {
      this.discountsDatas = response
      this.openMessageBar(this.translate.instant('DISCOUNT_LIST_RECEIVED_SUCCESSFUL'))

      this.discountListLoader = false      

      this.productCostPriceInit()
    }, (error) => {
      this.discountListLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getDiscountsList')    
    })     
  }    

  productData() {
    this.productDisplayDatas = [...this.saleDatas]
  }

  searchProductClose() {
    this.product_search_title = ''
    this.productDisplayDatas = [...this.saleDatas] 
    this.successErrorHandle = ''   
    this.searchProductCls.emit()
  }

  searchProduct() {
    let productSearchLower = this.product_search_title.toLowerCase()
    let productSearchCat = this.product_search_category.toString()

    const filterFunc = (item: { product_code: string; product_name: string, category_id: string }) => {

      if(productSearchCat == '0') {
        return ((item.product_code.toLowerCase().indexOf(productSearchLower) !== -1 ||      
        item.product_name.toLowerCase().indexOf(productSearchLower) !== -1)
        )
      } else {
        return ((item.product_code.toLowerCase().indexOf(productSearchLower) !== -1 ||      
        item.product_name.toLowerCase().indexOf(productSearchLower) !== -1) &&
        item.category_id == productSearchCat
        )        
      }
      
    }

    const data = this.saleDatas.filter((item: { product_code: string; product_name: string; category_id: string }) => filterFunc(item))

    this.productDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(productSearchLower.length == 0 && productSearchCat == '0') {
      this.productDisplayDatas = [...this.saleDatas]
    }    
  }

  productCostPriceInit() {
    for(let i = 0; i < this.productDisplayDatas.length; i++) {

      let itemPriceInt = parseInt(this.productDisplayDatas[i].sale_price1)

      let calcIsv = itemPriceInt * 0.15
      let isvTotal = itemPriceInt + calcIsv
      
      this.product_table_price_isv[i] = isvTotal
      this.product_table_cost_price[i] = itemPriceInt
      this.product_table_sale_price[i] = itemPriceInt
      this.product_table_quantity[i] = '1'
      this.product_table_discount[i] = this.productDisplayDatas[i].item_discount
      this.product_table_discount_percent[i] = '0'
    }    
  }
  
  productAddInput(product_data, index) {

    let priceValue, total, discount, tax, totalAfterDiscount, finalTotal

    if(!this.product_table_cost_price[index] || this.product_table_cost_price[index] == '0') {
      this.successErrorHandle = '2'
      return
    } else {
      this.successErrorHandle = ''
    }

    if(!this.product_table_sale_price[index] || this.product_table_sale_price[index] == '0') {
      this.successErrorHandle = '3'
      return
    } else {
      this.successErrorHandle = ''
    }    

    if(!this.product_table_quantity[index] || this.product_table_quantity[index] == '0') {
      this.successErrorHandle = '4'
      return
    } else {
      this.successErrorHandle = ''
    }

    total = this.product_table_sale_price[index] * this.product_table_quantity[index]

    // discount calculation 
    if(this.product_table_discount_percent[index] > 0) {
      discount = total * (this.product_table_discount_percent[index] / 100)
      totalAfterDiscount = total - discount

      tax = totalAfterDiscount * 0.15

      finalTotal = totalAfterDiscount + tax      
    } else {
      tax = total * 0.15

      finalTotal = total + tax  
    }       

    this.added_product = {
      product_id: product_data.product_id,
      product_code: product_data.product_code,
      product_name: product_data.product_name,
      description: product_data.description,
      cost_price: parseInt(this.product_table_cost_price[index]),
      product_price: parseInt(priceValue),
      product_tax: 15,
      product_quantity: parseInt(this.product_table_quantity[index]),
      product_total_price: finalTotal,
      product_discount: this.product_table_discount_percent[index],
      product_existance: 0
    }

    this.productAddInputChild.emit(this.added_product)
    this.successErrorHandle = '1'
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}