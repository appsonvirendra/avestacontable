import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-bankaccount-dialog',
  templateUrl: './search-bankaccount-dialog.component.html',
  styleUrls: ['./search-bankaccount-dialog.component.scss']
})
export class SearchBankaccountDialogComponent implements OnInit {

  @Output() searchBankaccountCls = new EventEmitter<string>()

  @Output() bankaccountAddInputChild = new EventEmitter<string>()

  @Input() searchBankaccountVisible: any

  @Input() bankaccountDatas

  bankaccountDisplayDatas: any

  bankaccount_search_title = ''
  search_bank_id: any = '0'
  
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {    
    this.bankaccountData()
  }

  bankaccountData() {
    this.bankaccountDisplayDatas = [...this.bankaccountDatas]
  }

  searchBankaccountClose() {
    this.bankaccount_search_title = ''
    this.search_bank_id = '0'
    this.bankaccountDisplayDatas = [...this.bankaccountDatas]    
    this.searchBankaccountCls.emit()
  }

  searchBankaccount() {
    let bankaccountSearchLower = this.bankaccount_search_title.toLowerCase()

    let SearchBankId = this.search_bank_id.toString()

    const filterFunc = (item: { bankaccount_no: string; bank_name: string, bank_id: string }) => {

      if(SearchBankId == '0') {
        return (item.bankaccount_no.toLowerCase().indexOf(bankaccountSearchLower) !== -1 ||
        item.bank_name.toLowerCase().indexOf(bankaccountSearchLower) !== -1
        )
      } else {
        return ((item.bankaccount_no.toLowerCase().indexOf(bankaccountSearchLower) !== -1 ||
        item.bank_name.toLowerCase().indexOf(bankaccountSearchLower) !== -1) && 
        item.bank_id == SearchBankId
        )      
      } 

    }

    const data = this.bankaccountDatas.filter((item: { bankaccount_no: string; bank_name: string; bank_id: string }) => filterFunc(item))

    this.bankaccountDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )

    if(bankaccountSearchLower.length == 0 && SearchBankId == '0') {
      this.bankaccountDisplayDatas = [...this.bankaccountDatas]
    }    
    
  }
  
  bankaccountAddInput(bankaccount_data) {
    this.bankaccountAddInputChild.emit(bankaccount_data)

    this.searchBankaccountClose()
  }

}
