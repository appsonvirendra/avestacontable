import { Component } from '@angular/core';

import {Router} from "@angular/router"
import { AuthenticationService } from './services/authentication.service';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private translate: TranslateService
    ) { 
      // translate.setDefaultLang('es')
    }

  ngOnInit() {
    if (!!localStorage.getItem('companyRes') === false && !!localStorage.getItem('posRes') === false) {
      this.router.navigate(['/auth'])      
      // this.router.navigate(['/user/customers/info_customer'], { queryParams: { customer_id: '1' } })
      // this.router.navigate(['/user/customers/list'])
      // this.router.navigate(['/user/suppliers/new'])
      // this.router.navigate(['/user/suppliers/info_supplier'], { queryParams: { supplier_id: '1' } })
      // this.router.navigate(['/user/products/items/list'])
      // this.router.navigate(['/user/products/purchase/info'], { queryParams: { purchase_id: '1' } })
      // this.router.navigate(['/user/income/invoice/list'])
      // this.router.navigate(['/user/income/invoice/info'], { queryParams: { invoice_id: '1' } })
      // this.router.navigate(['/user/income/returns/info'], { queryParams: { return_id: '1' } })
      // this.router.navigate(['/user/reports/accountants/department-expenses'])
    } /*else {
      this.router.navigate(['/pos'])      
    }*/

    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }
  }

  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
  }  
}
