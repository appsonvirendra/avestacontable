import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutfrontendComponent } from './layout/layoutfrontend/layoutfrontend.component';
import { LayoutuserpanelComponent } from './layout/layoutuserpanel/layoutuserpanel.component';

import { AuthGuardService } from './services/auth-guard.service';

import { AuthGuardPosService } from './services/auth-guard-pos.service';
import { LayoutpospanelComponent } from './layout/layoutpospanel/layoutpospanel.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutfrontendComponent,    
    children: [
      { 
        path: '', 
        pathMatch: 'full', 
        redirectTo: '/auth'
      },
      { 
        path: 'auth', 
        loadChildren: () => import('./pages/login/login.module').then(l => l.LoginModule)
      },   
    ]
  },
  // {
  //   path: 'user/start',
  //   canActivate: [AuthGuardService], 
  //   loadChildren: () => import('./pages/start/start.module').then(start => start.StartModule)     
  // }
  {
    path: 'user',
    component: LayoutuserpanelComponent,    
    children: [    
      { 
        path: '', 
        pathMatch: 'full', 
        redirectTo: '/user/welcome'
      },        
      { 
        path: 'start',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/start/start.module').then(start => start.StartModule) 
      },
      { 
        path: 'customers',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/customers/customers.module').then(customers => customers.CustomersModule) 
      },
      { 
        path: 'suppliers',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/suppliers/suppliers.module').then(suppliers => suppliers.SuppliersModule) 
      },
      { 
        path: 'products',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/products/products.module').then(products => products.ProductsModule) 
      },
      { 
        path: 'income',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/income/income.module').then(income => income.IncomeModule) 
      },
      { 
        path: 'expenses',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/expenses/expenses.module').then(expenses => expenses.ExpensesModule) 
      },
      { 
        path: 'accounting',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/accounting/accounting.module').then(accounting => accounting.AccountingModule) 
      },
      { 
        path: 'banks',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/banks/banks.module').then(banks => banks.BanksModule) 
      },
      { 
        path: 'employees',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/employees/employees.module').then(employees => employees.EmployeesModule) 
      },
      { 
        path: 'settings',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/settings/settings.module').then(settings => settings.SettingsModule) 
      },
      { 
        path: 'reports',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/reports/reports.module').then(reports => reports.ReportsModule) 
      },
      { 
        path: 'profile',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/profile/profile.module').then(profile => profile.ProfileModule) 
      },                                    
      { 
        path: 'welcome',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) 
      },  
      { 
        path: 'form',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/form/form.module').then(f => f.FormModule)
      }  
    ]
  },
  {
    path: 'pos',
    component: LayoutpospanelComponent,    
    children: [      
      { 
        path: '',
        canActivate: [AuthGuardPosService], 
        loadChildren: () => import('./pages/pos/pos.module').then(pos => pos.PosModule) 
      }
    ]
  }        
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
