import { Injectable } from '@angular/core';


@Injectable()
export class JwtService {

  getToken(): String {
    return window.localStorage['avtjwtToken'];
  }

  saveToken(token: String) {
    window.localStorage['avtjwtToken'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('avtjwtToken');
  }

}
