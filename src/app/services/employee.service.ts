import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import * as apiUrl  from 'src/config/defaultConfig';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { 
    
  }

  allTodos() {
    return this.http.get(apiUrl.url.All_TODOS_URL);
  }

}
