import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationPosService } from './authentication-pos.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardPosService implements CanActivate {

  constructor(
    private authPosService: AuthenticationPosService,
    private router: Router
    ) { 

    }

  canActivate(): boolean {
    if (this.authPosService.loggedIn() || this.authPosService.loggedInPos()) {
      return true
    } else {
      this.router.navigate(['/auth'])
    }
  }

}
