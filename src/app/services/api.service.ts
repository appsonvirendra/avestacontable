import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable ,  throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';

import * as apiUrl  from 'src/config/defaultConfig';

@Injectable()
export class ApiService {

  storeUrl: string = apiUrl.url.API_URL

  constructor(
    private http: HttpClient
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  private setHeaders(): HttpHeaders {
    let headersConfig = {
      'Content-Type': 'application/json'
    }

    return new HttpHeaders(headersConfig);
  }  

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${this.storeUrl}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${this.storeUrl}${path}`,
      JSON.stringify(body), 
      { headers: this.setHeaders() },
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${this.storeUrl}${path}`,
      JSON.stringify(body), 
      { headers: this.setHeaders() },
    ).pipe(catchError(this.formatErrors));
  } 

  delete(path): Observable<any> {
    return this.http.delete(
      `${this.storeUrl}${path}`, 
    ).pipe(catchError(this.formatErrors));
  }

}
