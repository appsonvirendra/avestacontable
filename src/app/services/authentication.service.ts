import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticationState = new BehaviorSubject(false)

  constructor( ) { 
  }

  login() {
    return this.authenticationState.next(true)
  }

  logout() { 
    this.authenticationState.next(false)
    return window.location.reload()
  }

  isAuthenticated() {    
    return this.authenticationState.value
  }

  loggedIn() {    
    return !!localStorage.getItem('companyRes')
  }


}
