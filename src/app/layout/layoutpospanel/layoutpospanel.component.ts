import { Component, OnInit, HostListener } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { JwtService } from 'src/app/services/jwt.service';

@Component({
  selector: 'app-layoutpospanel',
  templateUrl: './layoutpospanel.component.html',
  styleUrls: ['./layoutpospanel.component.scss']
})
export class LayoutpospanelComponent implements OnInit {

  todayDate : Date = new Date();
    // openShift dialog form
  openShiftForm: FormGroup  
  openShiftVisible = false

  innerWidth: any
  posDrawerVisible = false
  posDrawerPlacement = 'left'

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth
  }  

  companyLocalStorage: any

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private translate: TranslateService,
    private _jwtService: JwtService
  ) { 

  }

  ngOnInit() {
    this.openShiftFormInit()
    this.innerWidth = window.innerWidth

    this.companyLocalStorage = !!localStorage.getItem('companyRes')
  }
    // open Shift form
  openShiftFormInit() {
    this.openShiftForm = this.fb.group({
      initial_base: [null]
    })      
  }  

  openShiftOpen() {
    this.openShiftVisible = true
  }

  openShiftClose() {
    this.openShiftVisible = false
    this.openShiftForm.reset()
  }      

  openShiftSave() {    
    for (const i in this.openShiftForm.controls) {
      this.openShiftForm.controls[i].markAsDirty()
      this.openShiftForm.controls[i].updateValueAndValidity()
    }          
    if(this.openShiftForm.valid) {
      this.openShiftClose()
    }
  }

  logout() {
    // Remove token from localstorage
    this._jwtService.destroyToken()
    
    if(!!localStorage.getItem('companyRes')) {
      localStorage.removeItem('companyRes')      
    } else {
      localStorage.removeItem('posRes')
    }
    localStorage.removeItem('companyBranchId')
    this.authService.logout()
  }

  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
    window.location.reload()
  }  

  posDrawerOpen() {
    this.posDrawerVisible = true
  }

  posDrawerClose() {
    this.posDrawerVisible = false
  }

}