import { Component, OnInit, HostListener } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup } from '@angular/forms';

import { JwtService } from 'src/app/services/jwt.service';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-layoutuserpanel',
  templateUrl: './layoutuserpanel.component.html',
  styleUrls: ['./layoutuserpanel.component.scss']
})
export class LayoutuserpanelComponent implements OnInit {
  isCollapsed = false

  innerWidth: any
  drawerVisible = false
  drawerPlacement = 'left'

  customerRouteArray: any = ['/user/customers/list', '/user/customers/new']

  customerRouteActive: any

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth
  }  

  openMap: { [name: string]: boolean } = {
    sub1: false,
    sub2: false,
    sub3: false,
    sub4: false,
    sub5: false
  }  

  selectBranchForm: FormGroup

  branchDatas: any
  loader: boolean = false

  selected_branch: any

  companyData = JSON.parse(localStorage.getItem('companyRes')) 
  
  companyBranchId = localStorage.getItem('companyBranchId')

  constructor(
    private authService: AuthenticationService,
    private translate: TranslateService,
    private router: Router,
    private fb: FormBuilder,
    private _jwtService: JwtService,
    private _branchService: BranchService,
    private message: NzMessageService
  ) {  

  }  
  
  drawerOpen() {
    this.drawerVisible = true
  }

  drawerClose() {
    this.drawerVisible = false
  }  

  ngOnInit() {
    this.innerWidth = window.innerWidth
    this.selectBranchFormInit()

    this.populateGetBranchList()
  }
  
  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response
      this.loader = false      

      // branch id store in local storage and asign to select box
      if(!!localStorage.getItem('companyBranchId')) {
        this.selected_branch = localStorage.getItem('companyBranchId')
      } else {
        localStorage.setItem('companyBranchId', this.branchDatas[0].branch_id)
        this.selected_branch = this.branchDatas[0].branch_id
      }
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  } 

  companyBranchChange() {
    localStorage.setItem('companyBranchId', this.selected_branch)
    window.location.reload()
  }

  findCurrentRoute(){
    for(let i = 0; i < this.customerRouteArray.length; i++) {
      if(this.router.url == this.customerRouteArray[i]) {
        this.customerRouteActive = true
        return
      } else {
        this.customerRouteActive = false
      }
    }
  }

  openHandler(value: string) {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)
  }

  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
    window.location.reload()
  }    

  selectBranchFormInit() {
    this.selectBranchForm = this.fb.group({
      select_branch: ['1']
    })  
  } 
  
  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}
