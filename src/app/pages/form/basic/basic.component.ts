import { Component, OnInit } from '@angular/core';

import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  allTodosDatas: any;

  constructor(private _employeeService: EmployeeService) { 

  }

  ngOnInit() {
    this.getAllTodos()
  }

  getAllTodos() {
    this._employeeService.allTodos().subscribe((response) => {
      this.allTodosDatas = response
      // console.log('this.allTodosDatas --', this.allTodosDatas)
    }, (error) => {
      console.log('error --', error)
    })    
  }  

}
