import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { BasicComponent } from './basic/basic.component';
import { AdvanceComponent } from './advance/advance.component';


@NgModule({
  declarations: [BasicComponent, AdvanceComponent],
  imports: [
    CommonModule,
    FormRoutingModule
  ],
  exports: [BasicComponent]
})
export class FormModule { }
