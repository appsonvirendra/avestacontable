import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-suppliers-table',
  templateUrl: './suppliers-table.component.html',
  styleUrls: ['./suppliers-table.component.scss']
})
export class SuppliersTableComponent implements OnInit {

  @Input() suppliersData: any

  search_title = ''

  suppliersDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.SuppliersData()
  }

  SuppliersData() {
    this.suppliersDisplayData = [...this.suppliersData]
  }

  searchSuppliersbyname() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { suppliers_name: string; suppliers_mail: string}) => {
      return (
        item.suppliers_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.suppliers_mail.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.suppliersData.filter((item: { suppliers_name: string; suppliers_mail: string }) => filterFunc(item))

    this.suppliersDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.suppliersDisplayData = [...this.suppliersData]
    }
  }

}
