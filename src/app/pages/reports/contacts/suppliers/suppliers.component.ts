import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

  suppliersDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.suppliersData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.suppliersData()
  }
  
  submitForm() {
    this.suppliersDatas = []
  }
  
  suppliersData() {
    this.suppliersDatas = [
      {
        suppliers_id: '1',
        suppliers_name: 'perkins',
        suppliers_telephone: '---',
        suppliers_cellular: '---',
        suppliers_fax: '---',
        suppliers_mail: '---',
      },
      {
        suppliers_id: '1',
        suppliers_name: 'perkins',
        suppliers_telephone: '---',
        suppliers_cellular: '---',
        suppliers_fax: '08011973003131',
        suppliers_mail: 'albertolanza1967@gmail.com',
      }
    ]
  }

}