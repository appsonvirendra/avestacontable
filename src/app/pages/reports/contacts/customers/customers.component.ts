import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  customersDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.customersData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.customersData()
  }
  
  submitForm() {
    this.customersDatas = []
  }
  
  customersData() {
    this.customersDatas = [
      {
        customers_id: '1',
        customers_registration_date: '0808-1983-00032',
        customers_identification_card: '---',
        customers_name: '---',
        customers_last_name: '---',
        customers_business: '---',
        customers_rtn: '---',
        customers_sex: '---',
        customers_city: 'Tegucigalpa',
        customers_address: '---',
        customers_telephone: '---',
        customers_cellular: '---',
        customers_mail: '---',
        customers_seller: '---',
      },
       {
        customers_id: '1',
        customers_registration_date: '0808-1983-00032',
        customers_identification_card: '---',
        customers_name: 'Jorge Galel',
        customers_last_name: '---',
        customers_business: '---',
        customers_rtn: '---',
        customers_sex: 'Male',
        customers_city: 'Tegucigalpa',
        customers_address: '---',
        customers_telephone: '3177-6548',
        customers_cellular: '---',
        customers_mail: '---',
        customers_seller: '---',
      }
    ]
  }

}
