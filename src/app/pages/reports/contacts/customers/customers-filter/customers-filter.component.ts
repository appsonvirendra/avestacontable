import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-customers-filter',
  templateUrl: './customers-filter.component.html',
  styleUrls: ['./customers-filter.component.scss']
})
export class CustomersFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      registration_date_from: [null],
      registration_date_untill: [null],
      state: ['0'],
      branch: ['0'],
      type: ['0'] 
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
     registration_date_from: [null],
     registration_date_untill: [null],
     state: ['0'],
     branch: ['0'],
     type: ['0']    
    }) 
  }

}
