import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-customers-table',
  templateUrl: './customers-table.component.html',
  styleUrls: ['./customers-table.component.scss']
})
export class CustomersTableComponent implements OnInit {

  @Input() customersData: any

  search_title = ''

  customersDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.CustomersData()
  }

  CustomersData() {
    this.customersDisplayData = [...this.customersData]
  }

  searchcustomersbyname() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { customers_name: string; customers_city: string}) => {
      return (
        item.customers_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.customers_city.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.customersData.filter((item: { customers_name: string; customers_city: string }) => filterFunc(item))

    this.customersDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.customersDisplayData = [...this.customersData]
    }
  }

}
