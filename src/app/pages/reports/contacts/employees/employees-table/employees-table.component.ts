import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-employees-table',
  templateUrl: './employees-table.component.html',
  styleUrls: ['./employees-table.component.scss']
})
export class EmployeesTableComponent implements OnInit {

  @Input() employeesData: any

  search_title = ''

  employeesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.EmployeesData()
  }

  EmployeesData() {
    this.employeesDisplayData = [...this.employeesData]
  }

  searchemployeesbyidentificationcard() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { employees_identification_card: string; employees_name: string}) => {
      return (
        item.employees_identification_card.toLowerCase().indexOf(searchLower) !== -1 || 
        item.employees_name.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.employeesData.filter((item: { employees_identification_card: string; employees_name: string }) => filterFunc(item))

    this.employeesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.employeesDisplayData = [...this.employeesData]
    }
  }

}
