import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  employeesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.employeesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.employeesData()
  }
  
  submitForm() {
    this.employeesDatas = []
  }
  
  employeesData() {
    this.employeesDatas = [
      {
        employees_id: '1',
        employees_identification_card: '0808-1983-00032',
        employees_name: 'Norma Aracely',
        employees_last_name: 'Rhodes Lakes',
        employees_cellular: '3391453',
        employees_user: '---',
        employees_salary: '4,500.00',
        employees_position: 'Counter 1',
        employees_admission_date: '01 / 11 / 2019'
      }
    ]
  }

}
