import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-summarized-item',
  templateUrl: './summarized-item.component.html',
  styleUrls: ['./summarized-item.component.scss']
})
export class SummarizedItemComponent implements OnInit {

  summarizedItemDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.summarizedItemData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.summarizedItemData()
  }
  
  submitForm() {
    this.summarizedItemDatas = []
  }
  
  summarizedItemData() {
    this.summarizedItemDatas = [
      {
        summarized_item_id: '1',
        summarized_item_name: 'Norma Aracely Rodas Lagos',
        summarized_item_total_invoice: '0'
      }, 
      {
        summarized_item_id: '2',
        summarized_item_name: 'Michelle Arely Cuestas Hernandez',
        summarized_item_total_invoice: '0'
      }, 
      {
        summarized_item_id: '3',
        summarized_item_name: 'Hector Obdulio Mercadal Mendoza',
        summarized_item_total_invoice: '0'
      }, 
      {
        summarized_item_id: '4',
        summarized_item_name: 'Manuel Santos',
        summarized_item_total_invoice: '0'
      }
    ]
  }

}
