import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-summarized-item-filter',
  templateUrl: './summarized-item-filter.component.html',
  styleUrls: ['./summarized-item-filter.component.scss']
})
export class SummarizedItemFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null, [Validators.required]],
      date_to: [null, [Validators.required]],
      report_title: ['Partida Resumida', [Validators.required]],
      source: ['1']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {    
    this.filterForm.reset()   
    this.filterForm.patchValue({
      source: ['1']
    })
  }

}
