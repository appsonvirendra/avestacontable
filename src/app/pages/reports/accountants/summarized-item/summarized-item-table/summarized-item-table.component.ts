import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-summarized-item-table',
  templateUrl: './summarized-item-table.component.html',
  styleUrls: ['./summarized-item-table.component.scss']
})
export class SummarizedItemTableComponent implements OnInit {

  @Input() summarizedItemData: any

  search_title = ''

  summarizedItemDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.summarizedItemDisplayData = [...this.summarizedItemData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { summarized_item_name: string; summarized_item_total_invoice: string}) => {
      return (
        item.summarized_item_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.summarized_item_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.summarizedItemData.filter((item: { summarized_item_name: string; summarized_item_total_invoice: string }) => filterFunc(item))

    this.summarizedItemDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.summarizedItemDisplayData = [...this.summarizedItemData]
    }
  }

}
