import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accumulated-income-statement-table',
  templateUrl: './accumulated-income-statement-table.component.html',
  styleUrls: ['./accumulated-income-statement-table.component.scss']
})
export class AccumulatedIncomeStatementTableComponent implements OnInit {

  @Input() accumulatedIncomeStatementData: any

  search_title = ''

  accumulatedIncomeStatementDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.accumulatedIncomeStatementDisplayData = [...this.accumulatedIncomeStatementData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { accumulated_income_statement_name: string; accumulated_income_statement_total_invoice: string}) => {
      return (
        item.accumulated_income_statement_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.accumulated_income_statement_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.accumulatedIncomeStatementData.filter((item: { accumulated_income_statement_name: string; accumulated_income_statement_total_invoice: string }) => filterFunc(item))

    this.accumulatedIncomeStatementDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.accumulatedIncomeStatementDisplayData = [...this.accumulatedIncomeStatementData]
    }
  }

}
