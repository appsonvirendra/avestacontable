import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-accumulated-income-statement',
  templateUrl: './accumulated-income-statement.component.html',
  styleUrls: ['./accumulated-income-statement.component.scss']
})
export class AccumulatedIncomeStatementComponent implements OnInit {

  accumulatedIncomeStatementDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accumulatedIncomeStatementData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.accumulatedIncomeStatementData()
  }
  
  submitForm() {
    this.accumulatedIncomeStatementDatas = []
  }
  
  accumulatedIncomeStatementData() {
    this.accumulatedIncomeStatementDatas = [
      {
        accumulated_Income_statement_id: '1',
        accumulated_Income_statement_name: 'Norma Aracely Rodas Lagos',
        accumulated_Income_statement_total_invoice: '0'
      }, 
      {
        accumulated_Income_statement_id: '2',
        accumulated_Income_statement_name: 'Michelle Arely Cuestas Hernandez',
        accumulated_Income_statement_total_invoice: '0'
      }, 
      {
        accumulated_Income_statement_id: '3',
        accumulated_Income_statement_name: 'Hector Obdulio Mercadal Mendoza',
        accumulated_Income_statement_total_invoice: '0'
      }, 
      {
        accumulated_Income_statement_id: '4',
        accumulated_Income_statement_name: 'Manuel Santos',
        accumulated_Income_statement_total_invoice: '0'
      }
    ]
  }

}
