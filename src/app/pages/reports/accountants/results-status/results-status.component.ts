import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-results-status',
  templateUrl: './results-status.component.html',
  styleUrls: ['./results-status.component.scss']
})
export class ResultsStatusComponent implements OnInit {

  resultsstatusDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.resultsstatusData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.resultsstatusData()
  }
  
  submitForm() {
    this.resultsstatusDatas = []
  }
  
  resultsstatusData() {
    this.resultsstatusDatas = [
      {
        resultsstatus_id: '1',
        resultsstatus_name: 'Norma Aracely Rodas Lagos',
        resultsstatus_total_invoice: '0'
      }, 
      {
        resultsstatus_id: '2',
        resultsstatus_name: 'Michelle Arely Cuestas Hernandez',
        resultsstatus_total_invoice: '0'
      }, 
      {
        resultsstatus_id: '3',
        resultsstatus_name: 'Hector Obdulio Mercadal Mendoza',
        resultsstatus_total_invoice: '0'
      }, 
      {
        resultsstatus_id: '4',
        resultsstatus_name: 'Manuel Santos',
        resultsstatus_total_invoice: '0'
      }
    ]
  }

}
