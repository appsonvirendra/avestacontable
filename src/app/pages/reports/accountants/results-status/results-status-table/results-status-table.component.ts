import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-results-status-table',
  templateUrl: './results-status-table.component.html',
  styleUrls: ['./results-status-table.component.scss']
})
export class ResultsStatusTableComponent implements OnInit {

  @Input() resultsstatusData: any

  search_title = ''

  resultsstatusDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.resultsstatusDisplayData = [...this.resultsstatusData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { resultsstatus_name: string; resultsstatus_total_invoice: string}) => {
      return (
        item.resultsstatus_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.resultsstatus_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.resultsstatusData.filter((item: { resultsstatus_name: string; resultsstatus_total_invoice: string }) => filterFunc(item))

    this.resultsstatusDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.resultsstatusDisplayData = [...this.resultsstatusData]
    }
  }

}
