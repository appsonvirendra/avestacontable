import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-results-status-filter',
  templateUrl: './results-status-filter.component.html',
  styleUrls: ['./results-status-filter.component.scss']
})
export class ResultsStatusFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null, [Validators.required]],
      date_to: [null, [Validators.required]],
      include_account: [null],
      report_title: ['Estado de Resultados', [Validators.required]],
      show_historical: [null],
      cost_center: ['0']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {    
    this.filterForm.reset()   
    this.filterForm.patchValue({
      cost_center: ['0']
    })
  }

}
