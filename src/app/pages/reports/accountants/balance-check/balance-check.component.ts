import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-balance-check',
  templateUrl: './balance-check.component.html',
  styleUrls: ['./balance-check.component.scss']
})
export class BalanceCheckComponent implements OnInit {

  balancecheckDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.balancecheckData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.balancecheckData()
  }
  
  submitForm() {
    this.balancecheckDatas = []
  }
  
  balancecheckData() {
    this.balancecheckDatas = [
      {
        balancecheck_id: '1',
        balancecheck_name: 'Norma Aracely Rodas Lagos',
        balancecheck_total_invoice: '0'
      }, 
      {
        balancecheck_id: '2',
        balancecheck_name: 'Michelle Arely Cuestas Hernandez',
        balancecheck_total_invoice: '0'
      }, 
      {
        balancecheck_id: '3',
        balancecheck_name: 'Hector Obdulio Mercadal Mendoza',
        balancecheck_total_invoice: '0'
      }, 
      {
        balancecheck_id: '4',
        balancecheck_name: 'Manuel Santos',
        balancecheck_total_invoice: '0'
      }
    ]
  }

}
