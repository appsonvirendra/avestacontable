import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-balance-check-table',
  templateUrl: './balance-check-table.component.html',
  styleUrls: ['./balance-check-table.component.scss']
})
export class BalanceCheckTableComponent implements OnInit {

  @Input() balancecheckData: any

  search_title = ''

  balancecheckDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.balancecheckDisplayData = [...this.balancecheckData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { balancecheck_name: string; balancecheck_total_invoice: string}) => {
      return (
        item.balancecheck_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.balancecheck_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.balancecheckData.filter((item: { balancecheck_name: string; balancecheck_total_invoice: string }) => filterFunc(item))

    this.balancecheckDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.balancecheckDisplayData = [...this.balancecheckData]
    }
  }  

}
