import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-department-expenses',
  templateUrl: './department-expenses.component.html',
  styleUrls: ['./department-expenses.component.scss']
})
export class DepartmentExpensesComponent implements OnInit {

  departmentExpensesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.departmentExpensesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.departmentExpensesData()
  }
  
  submitForm() {
    this.departmentExpensesDatas = []
  }
  
  departmentExpensesData() {
    this.departmentExpensesDatas = [
      {
        department_expenses_id: '1',
        department_expenses_name: 'Norma Aracely Rodas Lagos',
        department_expenses_total_invoice: '0'
      }, 
      {
        department_expenses_id: '2',
        department_expenses_name: 'Michelle Arely Cuestas Hernandez',
        department_expenses_total_invoice: '0'
      }, 
      {
        department_expenses_id: '3',
        department_expenses_name: 'Hector Obdulio Mercadal Mendoza',
        department_expenses_total_invoice: '0'
      }, 
      {
        department_expenses_id: '4',
        department_expenses_name: 'Manuel Santos',
        department_expenses_total_invoice: '0'
      }
    ]
  }

}
