import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-department-expenses-table',
  templateUrl: './department-expenses-table.component.html',
  styleUrls: ['./department-expenses-table.component.scss']
})
export class DepartmentExpensesTableComponent implements OnInit {

  @Input() departmentExpensesData: any

  search_title = ''

  departmentExpensesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.departmentExpensesDisplayData = [...this.departmentExpensesData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { department_expenses_name: string; department_expenses_total_invoice: string}) => {
      return (
        item.department_expenses_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.department_expenses_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.departmentExpensesData.filter((item: { department_expenses_name: string; department_expenses_total_invoice: string }) => filterFunc(item))

    this.departmentExpensesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.departmentExpensesDisplayData = [...this.departmentExpensesData]
    }
  }

}
