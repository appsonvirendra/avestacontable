import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-department-expenses-filter',
  templateUrl: './department-expenses-filter.component.html',
  styleUrls: ['./department-expenses-filter.component.scss']
})
export class DepartmentExpensesFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date: [null, [Validators.required]],
      report_title: ['Gastos por Departamento', [Validators.required]],
      cost_center: ['0']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {    
    this.filterForm.reset()   
    this.filterForm.patchValue({
      cost_center: ['0']
    })
  }

}
