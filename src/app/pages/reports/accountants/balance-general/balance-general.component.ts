import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-balance-general',
  templateUrl: './balance-general.component.html',
  styleUrls: ['./balance-general.component.scss']
})
export class BalanceGeneralComponent implements OnInit {

  balancegeneralDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.balancegeneralData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.balancegeneralData()
  }
  
  submitForm() {
    this.balancegeneralDatas = []
  }
  
  balancegeneralData() {
    this.balancegeneralDatas = [
      {
        balancegeneral_id: '1',
        balancegeneral_name: 'Norma Aracely Rodas Lagos',
        balancegeneral_total_invoice: '0'
      }, 
      {
        balancegeneral_id: '2',
        balancegeneral_name: 'Michelle Arely Cuestas Hernandez',
        balancegeneral_total_invoice: '0'
      }, 
      {
        balancegeneral_id: '3',
        balancegeneral_name: 'Hector Obdulio Mercadal Mendoza',
        balancegeneral_total_invoice: '0'
      }, 
      {
        balancegeneral_id: '4',
        balancegeneral_name: 'Manuel Santos',
        balancegeneral_total_invoice: '0'
      }
    ]
  }

}
