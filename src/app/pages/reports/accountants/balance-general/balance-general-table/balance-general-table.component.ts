import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-balance-general-table',
  templateUrl: './balance-general-table.component.html',
  styleUrls: ['./balance-general-table.component.scss']
})
export class BalanceGeneralTableComponent implements OnInit {

  @Input() balancegeneralData: any

  search_title = ''

  balancegeneralDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()
  }

  monthlySalesData() {
    this.balancegeneralDisplayData = [...this.balancegeneralData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { balancegeneral_name: string; balancegeneral_total_invoice: string}) => {
      return (
        item.balancegeneral_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.balancegeneral_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.balancegeneralData.filter((item: { balancegeneral_name: string; balancegeneral_total_invoice: string }) => filterFunc(item))

    this.balancegeneralDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.balancegeneralDisplayData = [...this.balancegeneralData]
    }
  }  


}
