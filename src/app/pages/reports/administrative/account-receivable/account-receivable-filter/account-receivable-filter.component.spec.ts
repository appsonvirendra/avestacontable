import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountReceivableFilterComponent } from './account-receivable-filter.component';

describe('AccountReceivableFilterComponent', () => {
  let component: AccountReceivableFilterComponent;
  let fixture: ComponentFixture<AccountReceivableFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountReceivableFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountReceivableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
