import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountReceivableTableComponent } from './account-receivable-table.component';

describe('AccountReceivableTableComponent', () => {
  let component: AccountReceivableTableComponent;
  let fixture: ComponentFixture<AccountReceivableTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountReceivableTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountReceivableTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
