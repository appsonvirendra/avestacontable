import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-account-receivable-table',
  templateUrl: './account-receivable-table.component.html',
  styleUrls: ['./account-receivable-table.component.scss']
})
export class AccountReceivableTableComponent implements OnInit {

  @Input() accountreceivableData: any

  search_title = ''

  accountreceivableDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.accountReceivableData()
  }

  accountReceivableData() {
    this.accountreceivableDisplayData = [...this.accountreceivableData]
  }

  searchaccountreceivablebyamount() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { accountreceivable_amount: string}) => {
      return (
        item.accountreceivable_amount.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.accountreceivableData.filter((item: { accountreceivable_amount: string }) => filterFunc(item))

    this.accountreceivableDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.accountreceivableDisplayData = [...this.accountreceivableData]
    }
  }  

}
