import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-receivable',
  templateUrl: './account-receivable.component.html',
  styleUrls: ['./account-receivable.component.scss']
})
export class AccountReceivableComponent implements OnInit {

  accountreceivableDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accountreceivableData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.accountreceivableData()
  }
  
  submitForm() {
    this.accountreceivableDatas = []
  }
  
  accountreceivableData() {
    this.accountreceivableDatas = [
      {
        accountreceivable_id: '1',
        accountreceivable_client: ' John Brown 1',
        accountreceivable_amount: '0'
      }
    
    ]
  }

}

