import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsServicesListTableComponent } from './products-services-list-table.component';

describe('ProductsServicesListTableComponent', () => {
  let component: ProductsServicesListTableComponent;
  let fixture: ComponentFixture<ProductsServicesListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsServicesListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsServicesListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
