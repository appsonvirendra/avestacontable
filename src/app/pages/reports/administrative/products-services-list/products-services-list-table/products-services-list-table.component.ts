import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-products-services-list-table',
  templateUrl: './products-services-list-table.component.html',
  styleUrls: ['./products-services-list-table.component.scss']
})
export class ProductsServicesListTableComponent implements OnInit {

  @Input() productservicelistData: any

  search_title = ''

  productservicelistDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.productServiceData()
  }

  productServiceData() {
    this.productservicelistDisplayData = [...this.productservicelistData]
  }

  searchproductservicebycode() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { productservicelist_code: string; productservicelist_type: string}) => {
      return (
        item.productservicelist_code.toLowerCase().indexOf(searchLower) !== -1 || 
        item.productservicelist_type.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.productservicelistData.filter((item: { productservicelist_code: string; productservicelist_type: string }) => filterFunc(item))

    this.productservicelistDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.productservicelistDisplayData = [...this.productservicelistData]
    }
  }  

}
