import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-products-services-list-filter',
  templateUrl: './products-services-list-filter.component.html',
  styleUrls: ['./products-services-list-filter.component.scss']
})
export class ProductsServicesListFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      category: ['0'],
      type: ['0'],
      currency: ['0'],
      state: ['0'],
      exempt_tax: ['0']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      category: ['0'],
      type: ['0'],
      currency: ['0'], 
      state: ['0'],
      exempt_tax: ['0']  
    }) 
  }

}
