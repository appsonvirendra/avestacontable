import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsServicesListFilterComponent } from './products-services-list-filter.component';

describe('ProductsServicesListFilterComponent', () => {
  let component: ProductsServicesListFilterComponent;
  let fixture: ComponentFixture<ProductsServicesListFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsServicesListFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsServicesListFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
