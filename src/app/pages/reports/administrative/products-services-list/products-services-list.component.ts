import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-products-services-list',
  templateUrl: './products-services-list.component.html',
  styleUrls: ['./products-services-list.component.scss']
})
export class ProductsServicesListComponent implements OnInit {

  productservicelistDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.productservicelistData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.productservicelistData()
  }
  
  submitForm() {
    this.productservicelistDatas = []
  }
  
  productservicelistData() {
    this.productservicelistDatas = [
      {
        salesbyuser_id: '1',
        productservicelist_code: '0001',
        productservicelist_product: 'Item 1',
        productservicelist_description: 'Item 1',
        productservicelist_category: 'general',
        productservicelist_provider: 'supplier test',
        productservicelist_cost: 'L. 0.00',
        productservicelist_sale_price: 'L. 80.00',
        productservicelist_type: 'Product',
        productservicelist_currency: 'Lempiras',
        productservicelist_exempt_tax: 'Not'

      }
    
    ]
  }

}

