import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-work-orders-by-product',
  templateUrl: './work-orders-by-product.component.html',
  styleUrls: ['./work-orders-by-product.component.scss']
})
export class WorkOrdersByProductComponent implements OnInit {

  workordersbyproductDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.workordersbyproductData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.workordersbyproductData()
  }
  
  submitForm() {
    this.workordersbyproductDatas = []
  }
  
  workordersbyproductData() {
    this.workordersbyproductDatas = [
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 1',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      }, 
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 2',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 3',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 4',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 5',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 6',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 7',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 8',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 9',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      },
      {
        workordersbyproduct_id: '1',
        workordersbyproduct_product: 'Gatorade de 10',
        workordersbyproduct_quantity: '0',
        workordersbyproduct_total_ordered: '0.00'
      }
    ]
  }

}
