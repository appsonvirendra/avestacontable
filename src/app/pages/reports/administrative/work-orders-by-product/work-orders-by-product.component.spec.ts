import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrdersByProductComponent } from './work-orders-by-product.component';

describe('WorkOrdersByProductComponent', () => {
  let component: WorkOrdersByProductComponent;
  let fixture: ComponentFixture<WorkOrdersByProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrdersByProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrdersByProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
