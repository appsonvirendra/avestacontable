import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrdersByProductTableComponent } from './work-orders-by-product-table.component';

describe('WorkOrdersByProductTableComponent', () => {
  let component: WorkOrdersByProductTableComponent;
  let fixture: ComponentFixture<WorkOrdersByProductTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrdersByProductTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrdersByProductTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
