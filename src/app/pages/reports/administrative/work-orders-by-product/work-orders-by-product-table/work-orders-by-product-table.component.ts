import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-work-orders-by-product-table',
  templateUrl: './work-orders-by-product-table.component.html',
  styleUrls: ['./work-orders-by-product-table.component.scss']
})
export class WorkOrdersByProductTableComponent implements OnInit {

  @Input() workordersbyproductData: any

  search_title = ''

  workordersbyproductDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.workordersbyProductData()

    this.chartDataInit()
  }

  workordersbyProductData() {
    this.workordersbyproductDisplayData = [...this.workordersbyproductData]
  }

  searchworkorderbyquantity() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { workordersbyproduct_quantity: string}) => {
      return (
        item.workordersbyproduct_quantity.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.workordersbyproductData.filter((item: { workordersbyproduct_quantity: string }) => filterFunc(item))

    this.workordersbyproductDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.workordersbyproductDisplayData = [...this.workordersbyproductData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Gatorade de 1", 
          "Gatorade de 2", 
          "Gatorade de 3", 
          "Gatorade de 4", 
          "Gatorade de 5", 
          "Gatorade de 6",
          "Gatorade de 7",
          "Gatorade de 8",
          "Gatorade de 9",
          "Gatorade de 10"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            0, 
            600, 
            0, 
            0, 
            0, 
            800, 
            0, 
            0, 
            0,
            0,
            10
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }     

}
