import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-work-orders-by-product-filter',
  templateUrl: './work-orders-by-product-filter.component.html',
  styleUrls: ['./work-orders-by-product-filter.component.scss']
})
export class WorkOrdersByProductFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []
  
  // search seller dialog
  searchSellerVisible = false
  sellerDatas = []  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.customerData()
    this.sellerData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      payment_type: ['0'],
      payment_method: ['0'],
      branch: ['0'],
      type_of_product: [null],
      to_show: ['0'],
      customer_id: [null],
      customer_name: [null],
      seller_id: [null],
      seller_name: [null]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      payment_type: ['0'],
      payment_method: ['0'],
      branch: ['0'],
      type_of_product: [null],
      to_show: ['0'],
      customer_id: [null],
      customer_name: [null],
      seller_id: [null],
      seller_name: [null]
    }) 
  }

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }               
    ]
  }

  customerAddInput($event) {
    this.filterForm.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  } 
  
  // search seller dialog
  searchSellerOpen() {
    this.searchSellerVisible = true
  }

  searchSellerClose() {
    this.searchSellerVisible = false
  }   

  sellerData() {
    this.sellerDatas = [
      {
        seller_id: '1',
        seller_name: 'John Brown 1',
        seller_phone: "",
        seller_mobile: "",
        seller_email: "",
        seller_type: "1"
      },
      {
        seller_id: '2',
        seller_name: 'John Brown 2',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '3',
        seller_name: 'John Brown 3',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '4',
        seller_name: 'John Brown 4',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      }               
    ]
  }

  sellerAddInput($event) {
    this.filterForm.patchValue({
      seller_id: $event.seller_id,
      seller_name: $event.seller_name
    })

    this.searchSellerClose()    
  }  

}
