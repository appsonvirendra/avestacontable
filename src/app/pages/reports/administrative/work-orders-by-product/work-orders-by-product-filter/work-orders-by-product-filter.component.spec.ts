import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrdersByProductFilterComponent } from './work-orders-by-product-filter.component';

describe('WorkOrdersByProductFilterComponent', () => {
  let component: WorkOrdersByProductFilterComponent;
  let fixture: ComponentFixture<WorkOrdersByProductFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrdersByProductFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrdersByProductFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
