import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-inventories-filter',
  templateUrl: './inventories-filter.component.html',
  styleUrls: ['./inventories-filter.component.scss']
})
export class InventoriesFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      category: ['0'],
      currency: ['0'],
      state: ['0'],
      cellar: ['0'],
      show_non_existent_product: ['0'],
      only_see_products_with_minimum_stock: ['1']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
       category: ['0'],
       currency: ['0'],
       state: ['0'],
       cellar: ['0'],
       show_non_existent_product: ['0'],
       only_see_products_with_minimum_stock: ['1']  
    }) 
  }

}
