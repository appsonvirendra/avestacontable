import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoriesFilterComponent } from './inventories-filter.component';

describe('InventoriesFilterComponent', () => {
  let component: InventoriesFilterComponent;
  let fixture: ComponentFixture<InventoriesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoriesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoriesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
