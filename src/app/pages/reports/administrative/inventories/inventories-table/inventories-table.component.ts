import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inventories-table',
  templateUrl: './inventories-table.component.html',
  styleUrls: ['./inventories-table.component.scss']
})
export class InventoriesTableComponent implements OnInit {

  @Input() inventoriesData: any

  search_title = ''

  inventoriesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.InventoriesData()
  }

  InventoriesData() {
    this.inventoriesDisplayData = [...this.inventoriesData]
  }

  searchinventoriesbycode() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { inventories_code: string; inventories_product: string}) => {
      return (
        item.inventories_code.toLowerCase().indexOf(searchLower) !== -1 || 
        item.inventories_product.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.inventoriesData.filter((item: { inventories_code: string; inventories_product: string }) => filterFunc(item))

    this.inventoriesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.inventoriesDisplayData = [...this.inventoriesData]
    }
  } 

}
