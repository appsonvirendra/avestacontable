import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoriesTableComponent } from './inventories-table.component';

describe('InventoriesTableComponent', () => {
  let component: InventoriesTableComponent;
  let fixture: ComponentFixture<InventoriesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoriesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoriesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
