import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.scss']
})
export class InventoriesComponent implements OnInit {

  inventoriesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.inventoriesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.inventoriesData()
  }
  
  submitForm() {
    this.inventoriesDatas = []
  }
  
  inventoriesData() {
    this.inventoriesDatas = [
      {
        inventories_id: '1',
        inventories_code: '0001',
        inventories_product: 'Item 1',
        inventories_description: 'Item 1',
        inventories_category: 'general',
        inventories_provider: 'supplier test',
        inventories_unit_cost: 'L. 0.00',
        inventories_sale_price: 'L. 0.00',
        inventories_stock_min: '0',
        inventories_existence: '0.00',
        inventories_cost_inv: 'L. 0.00',
        inventories_sale_inv: 'L. 0.00',
      },
      {
        inventories_id: '1',
        inventories_code: '0002',
        inventories_product: 'Item 2',
        inventories_description: 'Item 1',
        inventories_category: 'general',
        inventories_provider: 'supplier test',
        inventories_unit_cost: 'L. 0.00',
        inventories_sale_price: 'L. 0.00',
        inventories_stock_min: '0',
        inventories_existence: '0.00',
        inventories_cost_inv: 'L. 0.00',
        inventories_sale_inv: 'L. 0.00',
      }
    
    ]
  }

}

