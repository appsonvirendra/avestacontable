import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss']
})
export class PurchasesComponent implements OnInit {

  purchasesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.purchasesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.purchasesData()
  }
  
  submitForm() {
    this.purchasesDatas = []
  }
  
  purchasesData() {
    this.purchasesDatas = [
      {
        purchases_id: '1',
        purchases_code: '000-001-01-001',
        purchases_cai: '---',
        purchases_date: '16/05/2020',
        purchases_notes: '---',
        purchases_provider: 'Test 1',
        purchases_subtotal: '0.00',
        purchases_discount: '0.00',
        purchases_tax: '0.00',
        purchases_total: '0.00',
        purchases_balance: '0.00',
        purchases_payment_method: '---',
        purchases_tipe_of_pay: 'Credit',
        purchases_state: '0'
      },
       {
        purchases_id: '1',
        purchases_code: '000-001-01-002',
        purchases_cai: '---',
        purchases_date: '17/05/2020',
        purchases_notes: '---',
        purchases_provider: 'Test 2',
        purchases_subtotal: '0.00',
        purchases_discount: '0.00',
        purchases_tax: '0.00',
        purchases_total: '0.00',
        purchases_balance: '0.00',
        purchases_payment_method: '---',
        purchases_tipe_of_pay: 'Credit',
        purchases_state: '1'
      },
       {
        purchases_id: '1',
        purchases_code: '000-001-01-003',
        purchases_cai: '---',
        purchases_date: '19/05/2020',
        purchases_notes: '---',
        purchases_provider: 'Test 3',
        purchases_subtotal: '0.00',
        purchases_discount: '0.00',
        purchases_tax: '0.00',
        purchases_total: '0.00',
        purchases_balance: '0.00',
        purchases_payment_method: '---',
        purchases_tipe_of_pay: 'Credit',
        purchases_state: '2'
      }
    ]
  }

}

