import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-purchases-table',
  templateUrl: './purchases-table.component.html',
  styleUrls: ['./purchases-table.component.scss']
})
export class PurchasesTableComponent implements OnInit {

  @Input() purchasesData: any

  purchasesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.PurchasesData()
  }

  PurchasesData() {
    this.purchasesDisplayData = [...this.purchasesData]
  }
 

}

