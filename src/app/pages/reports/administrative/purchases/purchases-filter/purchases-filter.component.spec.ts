import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesFilterComponent } from './purchases-filter.component';

describe('PurchasesFilterComponent', () => {
  let component: PurchasesFilterComponent;
  let fixture: ComponentFixture<PurchasesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
