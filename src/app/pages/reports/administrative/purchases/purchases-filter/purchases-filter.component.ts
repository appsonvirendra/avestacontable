import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
 selector: 'app-purchases-filter',
  templateUrl: './purchases-filter.component.html',
  styleUrls: ['./purchases-filter.component.scss']
})
export class PurchasesFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = [] 
  
  // search seller dialog
  searchSellerVisible = false
  sellerDatas = []
  
  // search user dialog
  searchUserVisible = false
  userDatas = []  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.supplierData()
    this.sellerData()
    this.userData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      payment_type: ['0'],
      payment_method: ['0'],
      branch: ['0'],
      state: ['0'],
      supplier_id: [null],
      supplier_name: [null],
      customer_id: [null],
      customer_name: [null],
      seller_id: [null],
      seller_name: [null],
      user_id: [null],
      user_name: [null]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      payment_type: ['0'],
      payment_method: ['0'],
      branch: ['0'],
      to_show: ['0'],
      supplier_id: [null],
      supplier_name: [null],
      customer_id: [null],
      customer_name: [null],
      seller_id: [null],
      seller_name: [null],
      user_id: [null],
      user_name: [null]      
    }) 
  }

  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.filterForm.patchValue({
      supplier_id: $event.supplier_id,
      supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  } 
  
  // search seller dialog
  searchSellerOpen() {
    this.searchSellerVisible = true
  }

  searchSellerClose() {
    this.searchSellerVisible = false
  }   

  sellerData() {
    this.sellerDatas = [
      {
        seller_id: '1',
        seller_name: 'John Brown 1',
        seller_phone: "",
        seller_mobile: "",
        seller_email: "",
        seller_type: "1"
      },
      {
        seller_id: '2',
        seller_name: 'John Brown 2',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '3',
        seller_name: 'John Brown 3',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '4',
        seller_name: 'John Brown 4',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      }               
    ]
  }

  sellerAddInput($event) {
    this.filterForm.patchValue({
      seller_id: $event.seller_id,
      seller_name: $event.seller_name
    })

    this.searchSellerClose()    
  }

  // search user dialog
  searchUserOpen() {
    this.searchUserVisible = true
  }

  searchUserClose() {
    this.searchUserVisible = false
  }   

  userData() {
    this.userDatas = [
      {
        user_id: '1',
        user_firstname: 'John',
        user_lastname: 'brawn',
        user_email: 'john@gmail.com',
        user_branch: 'Sucursal 1'
      }, 
      {
        user_id: '2',
        user_firstname: 'Rony 2',
        user_lastname: 'brawn',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'
      },
      {
        user_id: '3',
        user_firstname: 'Rony 3',
        user_lastname: 'wan',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'        
      },
      {
        user_id: '4',
        user_firstname: 'Rony 4',
        user_lastname: 'wan',
        user_email: 'rony4@gmail.com',
        user_branch: 'Sucursal 1'   
      }               
    ]
  }

  userAddInput($event) {
    this.filterForm.patchValue({
      user_id: $event.user_id,
      user_name: $event.user_firstname +' '+ $event.user_lastname
    })

    this.searchUserClose()    
  }  

}

