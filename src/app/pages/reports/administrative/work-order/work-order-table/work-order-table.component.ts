import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-work-order-table',
  templateUrl: './work-order-table.component.html',
  styleUrls: ['./work-order-table.component.scss']
})
export class WorkOrderTableComponent implements OnInit {

  @Input() workorderData: any

  search_title = ''

  workorderDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.workOrderData()
  }

  workOrderData() {
    this.workorderDisplayData = [...this.workorderData]
  }

  searchWorkorderbycode() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { workorder_code: string; }) => {
      return (
        item.workorder_code.toLowerCase().indexOf(searchLower) !== -1 
      )
    }

    const data = this.workorderData.filter((item: { workorder_code: string; }) => filterFunc(item))

    this.workorderDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.workorderDisplayData = [...this.workorderData]
    }
  } 

}
