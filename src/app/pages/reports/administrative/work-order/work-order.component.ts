import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-work-order',
  templateUrl: './work-order.component.html',
  styleUrls: ['./work-order.component.scss']
})
export class WorkOrderComponent implements OnInit {

  workorderDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.workorderData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.workorderData()
  }
  
  submitForm() {
    this.workorderDatas = []
  }
  
  workorderData() {
    this.workorderDatas = [
      {
        workorder: '1',
        workorder_code: '000-001-01-00000001',
        workorder_date: '16/05/2020',
        workorder_customer: 'Test 1',
        workorder_subtotal: '0.00',
        workorder_discount: '0.00',
        workorder_tax: '0.00',
        workorder_total: '0.00',
        workorder_state: '0'
      },
      {
        workorder: '1',
        workorder_code: '000-001-01-00000002',
        workorder_date: '16/05/2020',
        workorder_customer: 'Test 2',
        workorder_subtotal: '0.00',
        workorder_discount: '0.00',
        workorder_tax: '0.00',
        workorder_total: '0.00',
        workorder_state: '0'
      }
    ]
  }

}
