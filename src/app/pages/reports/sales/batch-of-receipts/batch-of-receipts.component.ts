
import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-batch-of-receipts',
  templateUrl: './batch-of-receipts.component.html',
  styleUrls: ['./batch-of-receipts.component.scss']
})
export class BatchOfReceiptsComponent implements OnInit {

  batchofreceiptsDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.batchofreceiptsData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.batchofreceiptsData()
  }
  
  submitForm() {
    this.batchofreceiptsDatas = []
  }
  
  batchofreceiptsData() {
    this.batchofreceiptsDatas = [
      {
        batchofreceipts_id: '1',
        batchofreceipts_cod_receipt: '000-001-01-00000001',
        batchofreceipts_invoice_code: '000-001-01-00000178',
        batchofreceipts_date: 'Jun / 01/2018',
        batchofreceipts_customer: 'Honduran Naval Force',
        batchofreceipts_previous_balance: 'L. 0.00',
        batchofreceipts_amount: 'L. 0.00',
        batchofreceipts_new_balance: 'L. 0.00',
        batchofreceipts_state: '1'
      },
      {
        batchofreceipts_id: '1',
        batchofreceipts_cod_receipt: '000-001-01-00000002',
        batchofreceipts_invoice_code: '000-001-01-00000180',
        batchofreceipts_date: 'Jun / 02/2018',
        batchofreceipts_customer: 'Test 1',
        batchofreceipts_previous_balance: 'L. 0.00',
        batchofreceipts_amount: 'L. 0.00',
        batchofreceipts_new_balance: 'L. 0.00',
        batchofreceipts_state: '0'
      }
    ]
  }

}