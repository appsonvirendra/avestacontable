import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfReceiptsComponent } from './batch-of-receipts.component';

describe('BatchOfReceiptsComponent', () => {
  let component: BatchOfReceiptsComponent;
  let fixture: ComponentFixture<BatchOfReceiptsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfReceiptsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfReceiptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
