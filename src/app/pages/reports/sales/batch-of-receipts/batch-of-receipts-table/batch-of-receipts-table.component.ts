import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-batch-of-receipts-table',
  templateUrl: './batch-of-receipts-table.component.html',
  styleUrls: ['./batch-of-receipts-table.component.scss']
})
export class BatchOfReceiptsTableComponent implements OnInit {

  @Input() batchofreceiptsData: any

  search_title = ''

  batchofreceiptDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.batchOfReceiptData()
  }

  batchOfReceiptData() {
    this.batchofreceiptDisplayData = [...this.batchofreceiptsData]
  }

  searchbycodereceipt() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { batchofreceipts_cod_receipt: string; batchofreceipts_invoice_code: string}) => {
      return (
        item.batchofreceipts_cod_receipt.toLowerCase().indexOf(searchLower) !== -1 || 
        item.batchofreceipts_invoice_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.batchofreceiptsData.filter((item: { batchofreceipts_cod_receipt: string; batchofreceipts_invoice_code: string }) => filterFunc(item))

    this.batchofreceiptDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.batchofreceiptDisplayData = [...this.batchofreceiptsData]
    }
  }  

}

