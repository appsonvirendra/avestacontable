import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfReceiptsTableComponent } from './batch-of-receipts-table.component';

describe('BatchOfReceiptsTableComponent', () => {
  let component: BatchOfReceiptsTableComponent;
  let fixture: ComponentFixture<BatchOfReceiptsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfReceiptsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfReceiptsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
