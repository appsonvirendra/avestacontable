import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfReceiptsFilterComponent } from './batch-of-receipts-filter.component';

describe('BatchOfReceiptsFilterComponent', () => {
  let component: BatchOfReceiptsFilterComponent;
  let fixture: ComponentFixture<BatchOfReceiptsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfReceiptsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfReceiptsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
