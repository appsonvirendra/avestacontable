import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
 selector: 'app-batch-of-receipts-filter',
  templateUrl: './batch-of-receipts-filter.component.html',
  styleUrls: ['./batch-of-receipts-filter.component.scss']
})
export class BatchOfReceiptsFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []

  
  // search user dialog
  searchUserVisible = false
  userDatas = []  

    // invoice search dialog
  searchInvoiceVisible = false
  invoiceDatas = []  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.customerData()
    this.invoiceData()
    this.userData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      branch: ['0'],
      state: ['0'],
      customer_id: [null],
      invoice_total: [null],
      customer_name: [null],
      invoice_id: [null],
      seller_name: [null],
      user_id: [null],
      user_name: [null]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      payment_type: ['0'],
      payment_method: ['0'],
      branch: ['0'],
      to_show: ['0'],
      customer_id: [null],
      customer_name: [null],
      invoice_total: [null],
      invoice_id: [null],
      seller_name: [null],
      user_id: [null],
      user_name: [null]      
    }) 
  }

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }               
    ]
  }

  customerAddInput($event) {
    this.filterForm.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  } 
  
 
  // search invoice dialog
  searchInvoiceOpen() {
    this.searchInvoiceVisible = true
  }

  searchInvoiceClose() {
    this.searchInvoiceVisible = false
  }   

  invoiceData() {
    this.invoiceDatas = [
      {
        invoice_id: '1',
        invoice_code: '000-001-01-001',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '2',
        invoice_code: '000-001-01-002',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        invoice_provider_name: 'Provider 2',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '0'
      },
      {
        invoice_id: '3',
        invoice_code: '000-001-01-003',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        invoice_provider_name: 'Provider 3',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '4',
        invoice_code: '000-001-01-004',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }               
    ]
  }

  invoiceAddInput($event) {
    this.filterForm.patchValue({
      invoice_id: $event.invoice_id,
      invoice_code: $event.invoice_code,
      invoice_total: $event.invoice_total
    })

    this.searchInvoiceClose()    
  }

  // search user dialog
  searchUserOpen() {
    this.searchUserVisible = true
  }

  searchUserClose() {
    this.searchUserVisible = false
  }   

  userData() {
    this.userDatas = [
      {
        user_id: '1',
        user_firstname: 'John',
        user_lastname: 'brawn',
        user_email: 'john@gmail.com',
        user_branch: 'Sucursal 1'
      }, 
      {
        user_id: '2',
        user_firstname: 'Rony 2',
        user_lastname: 'brawn',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'
      },
      {
        user_id: '3',
        user_firstname: 'Rony 3',
        user_lastname: 'wan',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'        
      },
      {
        user_id: '4',
        user_firstname: 'Rony 4',
        user_lastname: 'wan',
        user_email: 'rony4@gmail.com',
        user_branch: 'Sucursal 1'   
      }               
    ]
  }

  userAddInput($event) {
    this.filterForm.patchValue({
      user_id: $event.user_id,
      user_name: $event.user_firstname +' '+ $event.user_lastname
    })

    this.searchUserClose()    
  }  

}