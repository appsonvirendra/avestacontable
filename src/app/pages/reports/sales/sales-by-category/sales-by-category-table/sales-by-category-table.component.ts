import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-sales-by-category-table',
  templateUrl: './sales-by-category-table.component.html',
  styleUrls: ['./sales-by-category-table.component.scss']
})
export class SalesByCategoryTableComponent implements OnInit {

  @Input() salesbycategoryData: any

  search_title = ''

  salesbycategoryDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()

    this.chartDataInit()
  }

  monthlySalesData() {
    this.salesbycategoryDisplayData = [...this.salesbycategoryData]
  }

  searchSalesbycategory() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { salesbycategory_name: string; salesbycategory_quantity: string}) => {
      return (
        item.salesbycategory_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.salesbycategory_quantity.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.salesbycategoryData.filter((item: { salesbycategory_name: string; salesbycategory_quantity: string }) => filterFunc(item))

    this.salesbycategoryDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.salesbycategoryDisplayData = [...this.salesbycategoryData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "REPUESTOS", 
          "General"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            0
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }

}
