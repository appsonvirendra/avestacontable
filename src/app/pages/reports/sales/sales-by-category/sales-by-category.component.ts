import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sales-by-category',
  templateUrl: './sales-by-category.component.html',
  styleUrls: ['./sales-by-category.component.scss']
})
export class SalesByCategoryComponent implements OnInit {

  salesbycategoryDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.salesbycategoryData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.salesbycategoryData()
  }
  
  submitForm() {
    this.salesbycategoryDatas = []
  }
  
  salesbycategoryData() {
    this.salesbycategoryDatas = [
      {
        salesbycategory_id: '1',
        salesbycategory_name: 'REPUESTOS',
        salesbycategory_quantity: '0',
        salesbycategory_total_invoice: '0'
      }, 
      {
        salesbycategory_id: '1',
        salesbycategory_name: 'General',
        salesbycategory_quantity: '0',
        salesbycategory_total_invoice: '0'
      }
    ]
  }

}
