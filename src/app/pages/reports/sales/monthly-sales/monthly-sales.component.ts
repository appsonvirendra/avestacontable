import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-monthly-sales',
  templateUrl: './monthly-sales.component.html',
  styleUrls: ['./monthly-sales.component.scss']
})
export class MonthlySalesComponent implements OnInit {

  monthlysalesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.monthlysalesData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.monthlysalesData()
  }
  
  submitForm() {
    this.monthlysalesDatas = []
  }
  
  monthlysalesData() {
    this.monthlysalesDatas = [
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-01-01',
        monthlysales_month: 'Enero',
        monthlysales_amount: '0'
      }, 
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-02-02',
        monthlysales_month: 'Febrero',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-03-04',
        monthlysales_month: 'Marzo',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-04-05',
        monthlysales_month: 'Abril',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-05-06',
        monthlysales_month: 'Mayo',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-06-07',
        monthlysales_month: 'Junio',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-07-08',
        monthlysales_month: 'Julio',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-08-09',
        monthlysales_month: 'Agosto',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-09-10',
        monthlysales_month: 'Septiembre',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-10-11',
        monthlysales_month: 'Octubre',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-11-12',
        monthlysales_month: 'Noviembre',
        monthlysales_amount: '0'
      },
      {
        monthlysales_id: '1',
        monthlysales_date: '2019-12-12',
        monthlysales_month: 'Diciembre',
        monthlysales_amount: '0'
      }
    ]
  }

}
