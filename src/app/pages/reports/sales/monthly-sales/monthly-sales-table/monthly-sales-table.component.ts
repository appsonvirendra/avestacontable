import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-monthly-sales-table',
  templateUrl: './monthly-sales-table.component.html',
  styleUrls: ['./monthly-sales-table.component.scss']
})
export class MonthlySalesTableComponent implements OnInit {

  @Input() monthlysalesData: any

  search_title = ''

  monthlysalesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()

    this.chartDataInit()
  }

  monthlySalesData() {
    this.monthlysalesDisplayData = [...this.monthlysalesData]
  }

  searchMonthlysales() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { monthlysales_month: string}) => {
      return (
        item.monthlysales_month.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.monthlysalesData.filter((item: { monthlysales_month: string }) => filterFunc(item))

    this.monthlysalesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.monthlysalesDisplayData = [...this.monthlysalesData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Enero", 
          "Febrero", 
          "Marzo", 
          "Abril", 
          "Mayo", 
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            200, 
            0, 
            400, 
            0, 
            0, 
            1000, 
            0, 
            2000, 
            0,
            0,
            10
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }     


}
