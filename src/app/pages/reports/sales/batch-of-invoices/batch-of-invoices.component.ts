import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-batch-of-invoices',
  templateUrl: './batch-of-invoices.component.html',
  styleUrls: ['./batch-of-invoices.component.scss']
})
export class BatchOfInvoicesComponent implements OnInit {

  batchofinvoiceDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.batchofinvoiceData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.batchofinvoiceData()
  }
  
  submitForm() {
    this.batchofinvoiceDatas = []
  }
  
  batchofinvoiceData() {
    this.batchofinvoiceDatas = [
      {
        batchofinvoice_id: '1',
        batchofinvoice_code: '000-001-01-001',
        batchofinvoice_cai: '4D42E1-3AF99E-034BB5-8DD95E-F12272-04/ VENCE 27/05/2020',
        batchofinvoice_rtn: '---',
        batchofinvoice_date: '16/05/2020',
        batchofinvoice_customer: 'Test 1',
        batchofinvoice_subtotal: '0.00',
        batchofinvoice_discount: '0.00',
        batchofinvoice_tax: '0.00',
        batchofinvoice_total: '0.00',
        batchofinvoice_balance: '0.00',
        batchofinvoice_payment_method: 'Credit card',
        batchofinvoice_tipe_of_pay: 'Credit',
        batchofinvoice_state: '1'
      }
    ]
  }

}

