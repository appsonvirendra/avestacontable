import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfInvoicesComponent } from './batch-of-invoices.component';

describe('BatchOfInvoicesComponent', () => {
  let component: BatchOfInvoicesComponent;
  let fixture: ComponentFixture<BatchOfInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
