import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-batch-of-invoice-table',
  templateUrl: './batch-of-invoice-table.component.html',
  styleUrls: ['./batch-of-invoice-table.component.scss']
})
export class BatchOfInvoiceTableComponent implements OnInit {

  @Input() batchofinvoiceData: any

  batchofinvoiceDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.batchOfInvoiceData()
  }

  batchOfInvoiceData() {
    this.batchofinvoiceDisplayData = [...this.batchofinvoiceData]
  }
 

}

