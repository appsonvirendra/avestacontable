import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfInvoiceTableComponent } from './batch-of-invoice-table.component';

describe('BatchOfInvoiceTableComponent', () => {
  let component: BatchOfInvoiceTableComponent;
  let fixture: ComponentFixture<BatchOfInvoiceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfInvoiceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
