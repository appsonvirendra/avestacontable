import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchOfInvoicesFilterComponent } from './batch-of-invoices-filter.component';

describe('BatchOfInvoicesFilterComponent', () => {
  let component: BatchOfInvoicesFilterComponent;
  let fixture: ComponentFixture<BatchOfInvoicesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchOfInvoicesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchOfInvoicesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
