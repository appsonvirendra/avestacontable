import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sales-by-product',
  templateUrl: './sales-by-product.component.html',
  styleUrls: ['./sales-by-product.component.scss']
})
export class SalesByProductComponent implements OnInit {

  salesbyproductDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.salesbyproductData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.salesbyproductData()
  }
  
  submitForm() {
    this.salesbyproductDatas = []
  }
  
  salesbyproductData() {
    this.salesbyproductDatas = [
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 1',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      }, 
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 2',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 3',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 4',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 5',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 6',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 7',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 8',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 9',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      },
      {
        salesbyproduct_id: '1',
        salesbyproduct_name: 'Gatorade de 10',
        salesbyproduct_exempt_tax: 'No',
        salesbyproduct_quantity: '100',
        salesbyproduct_total_invoice: '1000'
      }
    ]
  }

}
