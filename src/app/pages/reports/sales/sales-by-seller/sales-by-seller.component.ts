import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sales-by-seller',
  templateUrl: './sales-by-seller.component.html',
  styleUrls: ['./sales-by-seller.component.scss']
})
export class SalesBySellerComponent implements OnInit {

  salesbysellerDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.salesbysellerData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.salesbysellerData()
  }
  
  submitForm() {
    this.salesbysellerDatas = []
  }
  
  salesbysellerData() {
    this.salesbysellerDatas = [
      {
        salesbyseller_id: '1',
        salesbyseller_name: 'Norma Aracely Rodas Lagos',
        salesbyseller_total_invoice: '0'
      }, 
      {
        salesbyseller_id: '2',
        salesbyseller_name: 'Michelle Arely Cuestas Hernandez',
        salesbyseller_total_invoice: '0'
      }, 
      {
        salesbyseller_id: '3',
        salesbyseller_name: 'Hector Obdulio Mercadal Mendoza',
        salesbyseller_total_invoice: '0'
      }, 
      {
        salesbyseller_id: '4',
        salesbyseller_name: 'Manuel Santos',
        salesbyseller_total_invoice: '0'
      }
    ]
  }

}
