import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-sales-by-seller-table',
  templateUrl: './sales-by-seller-table.component.html',
  styleUrls: ['./sales-by-seller-table.component.scss']
})
export class SalesBySellerTableComponent implements OnInit {

  @Input() salesbysellerData: any

  search_title = ''

  salesbysellerDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()

    this.chartDataInit()
  }

  monthlySalesData() {
    this.salesbysellerDisplayData = [...this.salesbysellerData]
  }

  searchSalesbyseller() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { salesbyseller_name: string; salesbyseller_total_invoice: string}) => {
      return (
        item.salesbyseller_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.salesbyseller_total_invoice.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.salesbysellerData.filter((item: { salesbyseller_name: string; salesbyseller_total_invoice: string }) => filterFunc(item))

    this.salesbysellerDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.salesbysellerDisplayData = [...this.salesbysellerData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Norma Aracely Rodas Lagos", 
          "Michelle Arely Cuestas Hernandez",
          "Hector Obdulio Mercadal Mendoza",
          "Manuel Santos"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            0,
            0,
            0
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }

}
