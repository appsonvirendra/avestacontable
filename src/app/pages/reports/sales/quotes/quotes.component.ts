import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent implements OnInit {

  quotesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.quotesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.quotesData()
  }
  
  submitForm() {
    this.quotesDatas = []
  }
  
  quotesData() {
    this.quotesDatas = [
      {
        quotes_id: '1',
        quotes_code: '000-001-01-00000001',
        quotes_date: 'Jul 18, 2020',
        quotes_customer: 'Military Industry',
        quotes_subtotal: '2,538,582.93',
        quotes_discount: '0.00',
        quotes_tax: '380,787.44',
        quotes_total: '2,919,370.37',
        quotes_state: '1'
      },
      {
        quotes_id: '1',
        quotes_code: '000-001-01-00000002',
        quotes_date: 'Jul 18, 2020',
        quotes_customer: 'Military Industry',
        quotes_subtotal: '2,178,983.06',
        quotes_discount: '0.00',
        quotes_tax: '326,847.46',
        quotes_total: '2,505,830.52',
        quotes_state: '0'
      }
    ]
  }

}