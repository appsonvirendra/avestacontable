import { Component, OnInit, Input } from '@angular/core';


@Component({
    selector: 'app-quotes-table',
  templateUrl: './quotes-table.component.html',
  styleUrls: ['./quotes-table.component.scss']
})
export class QuotesTableComponent implements OnInit {

  @Input() quotesData: any

  search_title = ''

  quotesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.QuotesData()
  }

  QuotesData() {
    this.quotesDisplayData = [...this.quotesData]
  }

  searchquotesbycode() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { quotes_code: string; quotes_subtotal: string}) => {
      return (
        item.quotes_code.toLowerCase().indexOf(searchLower) !== -1 || 
        item.quotes_subtotal.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.quotesData.filter((item: { quotes_code: string; quotes_subtotal: string }) => filterFunc(item))

    this.quotesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.quotesDisplayData = [...this.quotesData]
    }
  }  

}

