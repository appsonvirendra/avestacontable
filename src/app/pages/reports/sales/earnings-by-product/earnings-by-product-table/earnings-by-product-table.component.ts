import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-earnings-by-product-table',
  templateUrl: './earnings-by-product-table.component.html',
  styleUrls: ['./earnings-by-product-table.component.scss']
})
export class EarningsByProductTableComponent implements OnInit {

  @Input() earningbyproductData: any

  search_title = ''

  earningsbyproductDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.monthlySalesData()

    this.chartDataInit()
  }

  monthlySalesData() {
    this.earningsbyproductDisplayData = [...this.earningbyproductData]
  }

  searchEarningbyproduct() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { earningbyproduct_product: string}) => {
      return (
        item.earningbyproduct_product.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.earningbyproductData.filter((item: { earningbyproduct_product: string }) => filterFunc(item))

    this.earningsbyproductDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.earningsbyproductDisplayData = [...this.earningbyproductData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Gatorade de 1", 
          "Gatorade de 2", 
          "Gatorade de 3", 
          "Gatorade de 4", 
          "Gatorade de 5", 
          "Gatorade de 6",
          "Gatorade de 7",
          "Gatorade de 8",
          "Gatorade de 9",
          "Gatorade de 10"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            200, 
            0, 
            400, 
            0, 
            0, 
            1000, 
            0, 
            2000, 
            0,
            0,
            10
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }     

}
