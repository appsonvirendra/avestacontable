import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsByProductTableComponent } from './earnings-by-product-table.component';

describe('EarningsByProductTableComponent', () => {
  let component: EarningsByProductTableComponent;
  let fixture: ComponentFixture<EarningsByProductTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsByProductTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsByProductTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
