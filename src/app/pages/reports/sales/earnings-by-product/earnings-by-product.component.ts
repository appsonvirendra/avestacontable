import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-earnings-by-product',
  templateUrl: './earnings-by-product.component.html',
  styleUrls: ['./earnings-by-product.component.scss']
})
export class EarningsByProductComponent implements OnInit {

  earningbyproductDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.earningbyproductData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.earningbyproductData()
  }
  
  submitForm() {
    this.earningbyproductDatas = []
  }
  
  earningbyproductData() {
    this.earningbyproductDatas = [
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 1',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      }, 
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 2',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 3',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 4',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 5',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 6',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 7',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      }, 
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 8',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '0.00',
        earningbyproduct_total_sales: '0.00',
        earningbyproduct_unit_cost: '0.00',
        earningbyproduct_total_costs: '0.00',
        earningbyproduct_l_gain: '0.00',
        earningbyproduct_gain_percentage: '1'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 9',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '600.00',
        earningbyproduct_total_sales: '4,000.00',
        earningbyproduct_unit_cost: '450.00',
        earningbyproduct_total_costs: '1,000.00',
        earningbyproduct_l_gain: '4,000.00',
        earningbyproduct_gain_percentage: '0'
      },
      {
        earningbyproduct_id: '1',
        earningbyproduct_product: 'Gatorade de 10',
        earningbyproduct_quantity: '0',
        earningbyproduct_tax: '700.00',
        earningbyproduct_total_sales: '1,834.40',
        earningbyproduct_unit_cost: '2,468.98',
        earningbyproduct_total_costs: '2,932.50',
        earningbyproduct_l_gain: '1,834.40',
        earningbyproduct_gain_percentage: '0'
      }
    ]
  }

}
