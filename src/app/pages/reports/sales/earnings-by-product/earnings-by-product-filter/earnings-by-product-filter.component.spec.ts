import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsByProductFilterComponent } from './earnings-by-product-filter.component';

describe('EarningsByProductFilterComponent', () => {
  let component: EarningsByProductFilterComponent;
  let fixture: ComponentFixture<EarningsByProductFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsByProductFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsByProductFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
