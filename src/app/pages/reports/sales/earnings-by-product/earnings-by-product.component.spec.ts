import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsByProductComponent } from './earnings-by-product.component';

describe('EarningsByProductComponent', () => {
  let component: EarningsByProductComponent;
  let fixture: ComponentFixture<EarningsByProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsByProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsByProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
