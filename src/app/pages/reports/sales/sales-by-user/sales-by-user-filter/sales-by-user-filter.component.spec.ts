import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByUserFilterComponent } from './sales-by-user-filter.component';

describe('SalesByUserFilterComponent', () => {
  let component: SalesByUserFilterComponent;
  let fixture: ComponentFixture<SalesByUserFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByUserFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByUserFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
