import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sales-by-user',
  templateUrl: './sales-by-user.component.html',
  styleUrls: ['./sales-by-user.component.scss']
})
export class SalesByUserComponent implements OnInit {

  salesbyuserDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.salesbyuserData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.salesbyuserData()
  }
  
  submitForm() {
    this.salesbyuserDatas = []
  }
  
  salesbyuserData() {
    this.salesbyuserDatas = [
      {
        salesbyuser_id: '1',
        salesbyuser_name: 'ventas@inversioneslogisticas.com',
        salesbyuser_total_charged: '0'
      }
    
    ]
  }

}

