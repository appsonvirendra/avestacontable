import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-sales-by-user-table',
  templateUrl: './sales-by-user-table.component.html',
  styleUrls: ['./sales-by-user-table.component.scss']
})
export class SalesByUserTableComponent implements OnInit {

  @Input() salesbyuserData: any

  search_title = ''

  salesbyuserDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.salesByUserData()

    this.chartDataInit()
  }

  salesByUserData() {
    this.salesbyuserDisplayData = [...this.salesbyuserData]
  }

  searchSalesbyuser() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { salesbyuser_name: string; salesbyuser_total_charged: string}) => {
      return (
        item.salesbyuser_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.salesbyuser_total_charged.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.salesbyuserData.filter((item: { salesbyuser_name: string; salesbyuser_total_charged: string }) => filterFunc(item))

    this.salesbyuserDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.salesbyuserDisplayData = [...this.salesbyuserData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "ventas@inversioneslogisticas.com"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }

}
