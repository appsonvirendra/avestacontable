import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByUserTableComponent } from './sales-by-user-table.component';

describe('SalesByUserTableComponent', () => {
  let component: SalesByUserTableComponent;
  let fixture: ComponentFixture<SalesByUserTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByUserTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByUserTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
