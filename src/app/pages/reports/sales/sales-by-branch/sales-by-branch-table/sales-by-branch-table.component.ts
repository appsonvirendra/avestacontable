import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-sales-by-branch-table',
  templateUrl: './sales-by-branch-table.component.html',
  styleUrls: ['./sales-by-branch-table.component.scss']
})
export class SalesByBranchTableComponent implements OnInit {

  @Input() salesbybranchData: any

  search_title = ''

  salesbybranchDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.salesByUserData()

    this.chartDataInit()
  }

  salesByUserData() {
    this.salesbybranchDisplayData = [...this.salesbybranchData]
  }

  searchSalesbybranch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { salesbybranch_name: string; salesbybranch_total_charged: string}) => {
      return (
        item.salesbybranch_name.toLowerCase().indexOf(searchLower) !== -1 || 
        item.salesbybranch_total_charged.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.salesbybranchData.filter((item: { salesbybranch_name: string; salesbybranch_total_charged: string }) => filterFunc(item))

    this.salesbybranchDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.salesbybranchDisplayData = [...this.salesbybranchData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Sucursal 1"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }

}
