import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByBranchTableComponent } from './sales-by-branch-table.component';

describe('SalesByBranchTableComponent', () => {
  let component: SalesByBranchTableComponent;
  let fixture: ComponentFixture<SalesByBranchTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByBranchTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByBranchTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
