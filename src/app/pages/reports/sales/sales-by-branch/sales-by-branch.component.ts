import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
 selector: 'app-sales-by-branch',
  templateUrl: './sales-by-branch.component.html',
  styleUrls: ['./sales-by-branch.component.scss']
})
export class SalesByBranchComponent implements OnInit {

  salesbybranchDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.salesbybranchData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.salesbybranchData()
  }
  
  submitForm() {
    this.salesbybranchDatas = []
  }
  
  salesbybranchData() {
    this.salesbybranchDatas = [
      {
        salesbyuser_id: '1',
        salesbybranch_name: 'Branch 1',
        salesbybranch_total_charged: '0.00'
      }
    
    ]
  }

}

