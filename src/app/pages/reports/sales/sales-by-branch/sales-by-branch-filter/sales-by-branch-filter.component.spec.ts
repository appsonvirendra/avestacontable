import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByBranchFilterComponent } from './sales-by-branch-filter.component';

describe('SalesByBranchFilterComponent', () => {
  let component: SalesByBranchFilterComponent;
  let fixture: ComponentFixture<SalesByBranchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByBranchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByBranchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
