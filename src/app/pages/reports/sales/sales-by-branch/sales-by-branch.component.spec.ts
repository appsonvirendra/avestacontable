import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByBranchComponent } from './sales-by-branch.component';

describe('SalesByBranchComponent', () => {
  let component: SalesByBranchComponent;
  let fixture: ComponentFixture<SalesByBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
