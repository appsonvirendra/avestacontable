import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-quotes-by-product-table',
  templateUrl: './quotes-by-product-table.component.html',
  styleUrls: ['./quotes-by-product-table.component.scss']
})
export class QuotesByProductTableComponent implements OnInit {

  @Input() quotesbyproductData: any

  search_title = ''

  quotesbyproductDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.QuotesData()

    this.chartDataInit()
  }

  QuotesData() {
    this.quotesbyproductDisplayData = [...this.quotesbyproductData]
  }

  searchquotesbyproduct() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { quotesbyproduct_total_quoted: string}) => {
      return (
        item.quotesbyproduct_total_quoted.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.quotesbyproductData.filter((item: { quotesbyproduct_total_quoted: string }) => filterFunc(item))

    this.quotesbyproductDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.quotesbyproductDisplayData = [...this.quotesbyproductData]
    }
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "Gatorade de 1", 
          "Gatorade de 2", 
          "Gatorade de 3", 
          "Gatorade de 4", 
          "Gatorade de 5", 
          "Gatorade de 6",
          "Gatorade de 7",
          "Gatorade de 8",
          "Gatorade de 9",
          "Gatorade de 10"
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            200, 
            0, 
            400, 
            0, 
            0, 
            1000, 
            0, 
            2000, 
            0,
            0,
            10
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }      
  }     

}
