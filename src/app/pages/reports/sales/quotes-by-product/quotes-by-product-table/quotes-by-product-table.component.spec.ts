import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotesByProductTableComponent } from './quotes-by-product-table.component';

describe('QuotesByProductTableComponent', () => {
  let component: QuotesByProductTableComponent;
  let fixture: ComponentFixture<QuotesByProductTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotesByProductTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesByProductTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
