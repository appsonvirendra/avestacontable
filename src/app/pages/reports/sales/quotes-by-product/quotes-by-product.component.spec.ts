import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotesByProductComponent } from './quotes-by-product.component';

describe('QuotesByProductComponent', () => {
  let component: QuotesByProductComponent;
  let fixture: ComponentFixture<QuotesByProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotesByProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesByProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
