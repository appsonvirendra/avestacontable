import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-quotes-by-product',
  templateUrl: './quotes-by-product.component.html',
  styleUrls: ['./quotes-by-product.component.scss']
})
export class QuotesByProductComponent implements OnInit {

  quotesbyproductDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.quotesbyproductData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.quotesbyproductData()
  }
  
  submitForm() {
    this.quotesbyproductDatas = []
  }
  
  quotesbyproductData() {
    this.quotesbyproductDatas = [
      {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 1',
        quotesbyproduct_quantity: '31408.00',
        quotesbyproduct_total_quoted: '2,212,216.20'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 2',
        quotesbyproduct_quantity: '1500.00',
        quotesbyproduct_total_quoted: '468,260.85'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 3',
        quotesbyproduct_quantity: '2000.00',
        quotesbyproduct_total_quoted: '717,826.10'
      },
     {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 4',
        quotesbyproduct_quantity: '2774.00',
        quotesbyproduct_total_quoted: '747,773.86'
      },
      {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 5',
        quotesbyproduct_quantity: '7852.00',
        quotesbyproduct_total_quoted: '592,826.00'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 6',
        quotesbyproduct_quantity: '1.00',
        quotesbyproduct_total_quoted: '98,000.00'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 7',
        quotesbyproduct_quantity: '1.00',
        quotesbyproduct_total_quoted: '120,000.00'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 8',
        quotesbyproduct_quantity: '1764.00',
        quotesbyproduct_total_quoted: '123,480.00'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 9',
        quotesbyproduct_quantity: '0',
        quotesbyproduct_total_quoted: '0.00'
      },
       {
        quotesbyproduct_id: '1',
        quotesbyproduct_name: 'Gatorade de 10',
        quotesbyproduct_quantity: '0',
        quotesbyproduct_total_quoted: '0.00'
      }
    
   
    ]
  }

}
