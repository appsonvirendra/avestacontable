import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotesByProductFilterComponent } from './quotes-by-product-filter.component';

describe('QuotesByProductFilterComponent', () => {
  let component: QuotesByProductFilterComponent;
  let fixture: ComponentFixture<QuotesByProductFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotesByProductFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesByProductFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
