import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-daily-sales-table',
  templateUrl: './daily-sales-table.component.html',
  styleUrls: ['./daily-sales-table.component.scss']
})
export class DailySalesTableComponent implements OnInit {

  @Input() dailysalesData: any

  search_title = ''

  dailysalesDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  

  chartData: any

  lineChart: typeof Highcharts = Highcharts
  lineChartOptions: any

  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.dailySalesData()

    this.chartDataInit()
  }

  dailySalesData() {
    this.dailysalesDisplayData = [...this.dailysalesData]
  }

  searchDailysales() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { dailysales_date: string}) => {
      return (
        item.dailysales_date.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.dailysalesData.filter((item: { dailysales_date: string }) => filterFunc(item))

    this.dailysalesDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.dailysalesDisplayData = [...this.dailysalesData]
    }    
  }  

  chartDataInit() {
    this.chartData = [{
      name: "February 2",
      x: 978287400000,
      y: 60
    }]

    // console.log(Date.parse(this.currentDate))
    
    /*this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
            month: '%e. %b',
            year: '%b'
        },
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          data: this.chartData
        }
      ],
      credits: {
        enabled: false
      }
    }*/
    
    this.lineChartOptions = {   
      chart: {        
        type: "line",
      },
      title: {
        text: ""
      },
      subtitle: {
        enabled: false
      },
      xAxis:{
        categories:[
          "01-01-2019", 
          "02-01-2019", 
          "03-01-2019", 
          "04-01-2019", 
          "05-01-2019", 
          "06-01-2019",
          "07-01-2019",
          "08-01-2019",
          "09-01-2019",
          "10-01-2019",
          "11-01-2019",
          "12-01-2019",
          "13-01-2019",
          "14-01-2019",
          "15-01-2019",
          "16-01-2019",
          "17-01-2019",
          "18-01-2019",
          "19-01-2019",
          "20-01-2019",
          "21-01-2019",
          "22-01-2019",
          "23-01-2019",
          "24-01-2019",
          "25-01-2019",
          "26-01-2019",
          "27-01-2019",
          "28-01-2019",
          "29-01-2019",
          "30-01-2019",
          "31-01-2019",
        ]
      },
      yAxis: {          
        title:{
          text:""
        },
        labels: {
          formatter: function() {
            return this.value
          }
        }       
      },
      tooltip: {
        valueSuffix:""
      },
      colors: [
        '#90ED7D'
      ],    
      series: [
        {
          showInLegend: false,
          name: 'Ventas',
          data: [
            0, 
            200, 
            0, 
            400, 
            0, 
            0, 
            1000, 
            0, 
            2000, 
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            10
          ]
        }
      ],
      credits: {
        enabled: false
      }
    }
        
  }     

  /*lineChart: typeof Highcharts = Highcharts;
  lineChartOptions: any = {   
    chart: {        
      type: "line",
    },
    title: {
      text: ""
    },
    subtitle: {
      enabled: false
    },
    xAxis:{
      categories:[
        "01-01-2019", 
        "02-01-2019", 
        "03-01-2019", 
        "04-01-2019", 
        "05-01-2019", 
        "06-01-2019",
        "07-01-2019",
        "08-01-2019",
        "09-01-2019",
        "10-01-2019",
        "11-01-2019",
        "12-01-2019",
        "13-01-2019",
        "14-01-2019",
        "15-01-2019",
        "16-01-2019",
        "17-01-2019",
        "18-01-2019",
        "19-01-2019",
        "20-01-2019",
        "21-01-2019",
        "22-01-2019",
        "23-01-2019",
        "24-01-2019",
        "25-01-2019",
        "26-01-2019",
        "27-01-2019",
        "28-01-2019",
        "29-01-2019",
        "30-01-2019",
        "31-01-2019",
      ]
    },
    yAxis: {          
      title:{
        text:""
      },
      labels: {
        formatter: function() {
          return this.value
        }
      }       
    },
    tooltip: {
      valueSuffix:""
    },
    colors: [
      '#90ED7D'
    ],    
    series: [
      {
        showInLegend: false,
        data: [
          0, 
          200, 
          0, 
          400, 
          0, 
          0, 
          1000, 
          0, 
          2000, 
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          10
        ]
      }
    ],
    credits: {
      enabled: false
    }
  };*/  

}
