import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-daily-sales-filter',
  templateUrl: './daily-sales-filter.component.html',
  styleUrls: ['./daily-sales-filter.component.scss']
})
export class DailySalesFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      daily_sales_year: [null, [Validators.required]],
      daily_sales_month: [null, [Validators.required]],
      daily_sales_branch: ['0']
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm.reset()
    this.filterForm.patchValue({
      daily_sales_branch: ['0']
    })    
  }

}
