import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-daily-sales',
  templateUrl: './daily-sales.component.html',
  styleUrls: ['./daily-sales.component.scss']
})
export class DailySalesComponent implements OnInit {

  dailysalesDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.dailysalesData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.dailysalesData()
  }
  
  submitForm() {
    this.dailysalesDatas = []
  }
  
  dailysalesData() {
    this.dailysalesDatas = [
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-01',
        dailysales_amount: '0'
      }, 
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-02',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-04',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-05',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-06',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-07',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-08',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-09',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-10',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-11',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-12',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-13',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-14',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-15',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-16',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-17',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-18',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-19',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-20',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-21',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-22',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-23',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-24',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-25',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-26',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-27',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-28',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-29',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-30',
        dailysales_amount: '0'
      },
      {
        dailysales_id: '1',
        dailysales_date: '2019-01-31',
        dailysales_amount: '0'
      } 
    ]
  }

}
