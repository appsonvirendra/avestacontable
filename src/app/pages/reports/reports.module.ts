import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// basic load start
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
// basic load end

import { ReportsRoutingModule } from './reports-routing.module';

import { ReportsComponent } from './reports.component';
import { DailySalesComponent } from './sales/daily-sales/daily-sales.component';
import { MonthlySalesComponent } from './sales/monthly-sales/monthly-sales.component';
import { SalesByProductComponent } from './sales/sales-by-product/sales-by-product.component';
import { DailySalesFilterComponent } from './sales/daily-sales/daily-sales-filter/daily-sales-filter.component';
import { DailySalesTableComponent } from './sales/daily-sales/daily-sales-table/daily-sales-table.component';

import { HighchartsChartModule } from 'highcharts-angular';
import { MonthlySalesFilterComponent } from './sales/monthly-sales/monthly-sales-filter/monthly-sales-filter.component';
import { MonthlySalesTableComponent } from './sales/monthly-sales/monthly-sales-table/monthly-sales-table.component';
import { SalesByProductFilterComponent } from './sales/sales-by-product/sales-by-product-filter/sales-by-product-filter.component';
import { SalesByProductTableComponent } from './sales/sales-by-product/sales-by-product-table/sales-by-product-table.component';
import { SalesByCategoryComponent } from './sales/sales-by-category/sales-by-category.component';
import { SalesByCategoryFilterComponent } from './sales/sales-by-category/sales-by-category-filter/sales-by-category-filter.component';
import { SalesByCategoryTableComponent } from './sales/sales-by-category/sales-by-category-table/sales-by-category-table.component';
import { SalesBySellerComponent } from './sales/sales-by-seller/sales-by-seller.component';
import { SalesBySellerFilterComponent } from './sales/sales-by-seller/sales-by-seller-filter/sales-by-seller-filter.component';
import { SalesBySellerTableComponent } from './sales/sales-by-seller/sales-by-seller-table/sales-by-seller-table.component';
import { BalanceGeneralComponent } from './accountants/balance-general/balance-general.component';
import { BalanceGeneralFilterComponent } from './accountants/balance-general/balance-general-filter/balance-general-filter.component';
import { BalanceGeneralTableComponent } from './accountants/balance-general/balance-general-table/balance-general-table.component';
import { BalanceCheckComponent } from './accountants/balance-check/balance-check.component';
import { BalanceCheckFilterComponent } from './accountants/balance-check/balance-check-filter/balance-check-filter.component';
import { BalanceCheckTableComponent } from './accountants/balance-check/balance-check-table/balance-check-table.component';
import { ResultsStatusComponent } from './accountants/results-status/results-status.component';
import { ResultsStatusFilterComponent } from './accountants/results-status/results-status-filter/results-status-filter.component';
import { ResultsStatusTableComponent } from './accountants/results-status/results-status-table/results-status-table.component';
import { AccumulatedIncomeStatementComponent } from './accountants/accumulated-income-statement/accumulated-income-statement.component';
import { AccumulatedIncomeStatementFilterComponent } from './accountants/accumulated-income-statement/accumulated-income-statement-filter/accumulated-income-statement-filter.component';
import { AccumulatedIncomeStatementTableComponent } from './accountants/accumulated-income-statement/accumulated-income-statement-table/accumulated-income-statement-table.component';
import { SummarizedItemComponent } from './accountants/summarized-item/summarized-item.component';
import { SummarizedItemFilterComponent } from './accountants/summarized-item/summarized-item-filter/summarized-item-filter.component';
import { SummarizedItemTableComponent } from './accountants/summarized-item/summarized-item-table/summarized-item-table.component';
import { DepartmentExpensesComponent } from './accountants/department-expenses/department-expenses.component';
import { DepartmentExpensesFilterComponent } from './accountants/department-expenses/department-expenses-filter/department-expenses-filter.component';
import { DepartmentExpensesTableComponent } from './accountants/department-expenses/department-expenses-table/department-expenses-table.component';
import { BatchOfInvoicesComponent } from './sales/batch-of-invoices/batch-of-invoices.component';
import { BatchOfInvoicesFilterComponent } from './sales/batch-of-invoices/batch-of-invoices-filter/batch-of-invoices-filter.component';
import { BatchOfInvoiceTableComponent } from './sales/batch-of-invoices/batch-of-invoice-table/batch-of-invoice-table.component';
import { BatchOfReceiptsComponent } from './sales/batch-of-receipts/batch-of-receipts.component';
import { BatchOfReceiptsTableComponent } from './sales/batch-of-receipts/batch-of-receipts-table/batch-of-receipts-table.component';
import { BatchOfReceiptsFilterComponent } from './sales/batch-of-receipts/batch-of-receipts-filter/batch-of-receipts-filter.component';
import { EarningsByProductComponent } from './sales/earnings-by-product/earnings-by-product.component';
import { EarningsByProductTableComponent } from './sales/earnings-by-product/earnings-by-product-table/earnings-by-product-table.component';
import { EarningsByProductFilterComponent } from './sales/earnings-by-product/earnings-by-product-filter/earnings-by-product-filter.component';
import { SalesByUserComponent } from './sales/sales-by-user/sales-by-user.component';
import { SalesByUserFilterComponent } from './sales/sales-by-user/sales-by-user-filter/sales-by-user-filter.component';
import { SalesByUserTableComponent } from './sales/sales-by-user/sales-by-user-table/sales-by-user-table.component';
import { SalesByBranchComponent } from './sales/sales-by-branch/sales-by-branch.component';
import { SalesByBranchFilterComponent } from './sales/sales-by-branch/sales-by-branch-filter/sales-by-branch-filter.component';
import { SalesByBranchTableComponent } from './sales/sales-by-branch/sales-by-branch-table/sales-by-branch-table.component';
import { QuotesComponent } from './sales/quotes/quotes.component';
import { QuotesFilterComponent } from './sales/quotes/quotes-filter/quotes-filter.component';
import { QuotesTableComponent } from './sales/quotes/quotes-table/quotes-table.component';
import { QuotesByProductComponent } from './sales/quotes-by-product/quotes-by-product.component';
import { QuotesByProductTableComponent } from './sales/quotes-by-product/quotes-by-product-table/quotes-by-product-table.component';
import { QuotesByProductFilterComponent } from './sales/quotes-by-product/quotes-by-product-filter/quotes-by-product-filter.component';
import { ProductsServicesListComponent } from './administrative/products-services-list/products-services-list.component';
import { ProductsServicesListFilterComponent } from './administrative/products-services-list/products-services-list-filter/products-services-list-filter.component';
import { ProductsServicesListTableComponent } from './administrative/products-services-list/products-services-list-table/products-services-list-table.component';
import { InventoriesComponent } from './administrative/inventories/inventories.component';
import { InventoriesTableComponent } from './administrative/inventories/inventories-table/inventories-table.component';
import { InventoriesFilterComponent } from './administrative/inventories/inventories-filter/inventories-filter.component';
import { AccountReceivableComponent } from './administrative/account-receivable/account-receivable.component';
import { AccountReceivableFilterComponent } from './administrative/account-receivable/account-receivable-filter/account-receivable-filter.component';
import { AccountReceivableTableComponent } from './administrative/account-receivable/account-receivable-table/account-receivable-table.component';
import { PurchasesComponent } from './administrative/purchases/purchases.component';
import { PurchasesFilterComponent } from './administrative/purchases/purchases-filter/purchases-filter.component';
import { PurchasesTableComponent } from './administrative/purchases/purchases-table/purchases-table.component';
import { WorkOrderComponent } from './administrative/work-order/work-order.component';
import { WorkOrderFilterComponent } from './administrative/work-order/work-order-filter/work-order-filter.component';
import { WorkOrderTableComponent } from './administrative/work-order/work-order-table/work-order-table.component';
import { WorkOrdersByProductComponent } from './administrative/work-orders-by-product/work-orders-by-product.component';
import { WorkOrdersByProductFilterComponent } from './administrative/work-orders-by-product/work-orders-by-product-filter/work-orders-by-product-filter.component';
import { WorkOrdersByProductTableComponent } from './administrative/work-orders-by-product/work-orders-by-product-table/work-orders-by-product-table.component';
import { EmployeesComponent } from './contacts/employees/employees.component';
import { EmployeesFilterComponent } from './contacts/employees/employees-filter/employees-filter.component';
import { EmployeesTableComponent } from './contacts/employees/employees-table/employees-table.component';
import { CustomersComponent } from './contacts/customers/customers.component';
import { CustomersFilterComponent } from './contacts/customers/customers-filter/customers-filter.component';
import { CustomersTableComponent } from './contacts/customers/customers-table/customers-table.component';
import { SuppliersComponent } from './contacts/suppliers/suppliers.component';
import { SuppliersTableComponent } from './contacts/suppliers/suppliers-table/suppliers-table.component';
import { SuppliersFilterComponent } from './contacts/suppliers/suppliers-filter/suppliers-filter.component';



@NgModule({
  declarations: [ReportsComponent, DailySalesComponent, MonthlySalesComponent, SalesByProductComponent, DailySalesFilterComponent, DailySalesTableComponent, MonthlySalesFilterComponent, MonthlySalesTableComponent, SalesByProductFilterComponent, SalesByProductTableComponent, SalesByCategoryComponent, SalesByCategoryFilterComponent, SalesByCategoryTableComponent, SalesBySellerComponent, SalesBySellerFilterComponent, SalesBySellerTableComponent, BalanceGeneralComponent, BalanceGeneralFilterComponent, BalanceGeneralTableComponent, BalanceCheckComponent, BalanceCheckFilterComponent, BalanceCheckTableComponent, ResultsStatusComponent, ResultsStatusFilterComponent, ResultsStatusTableComponent, AccumulatedIncomeStatementComponent, AccumulatedIncomeStatementFilterComponent, AccumulatedIncomeStatementTableComponent, SummarizedItemComponent, SummarizedItemFilterComponent, SummarizedItemTableComponent, DepartmentExpensesComponent, DepartmentExpensesFilterComponent, DepartmentExpensesTableComponent, BatchOfInvoicesComponent, BatchOfInvoicesFilterComponent, BatchOfInvoiceTableComponent, BatchOfReceiptsComponent, BatchOfReceiptsTableComponent, BatchOfReceiptsFilterComponent, EarningsByProductComponent, EarningsByProductTableComponent, EarningsByProductFilterComponent, SalesByUserComponent, SalesByUserFilterComponent, SalesByUserTableComponent, SalesByBranchComponent, SalesByBranchFilterComponent, SalesByBranchTableComponent, QuotesComponent, QuotesFilterComponent, QuotesTableComponent, QuotesByProductComponent, QuotesByProductTableComponent, QuotesByProductFilterComponent, ProductsServicesListComponent, ProductsServicesListFilterComponent, ProductsServicesListTableComponent, InventoriesComponent, InventoriesTableComponent, InventoriesFilterComponent, AccountReceivableComponent, AccountReceivableFilterComponent, AccountReceivableTableComponent, PurchasesComponent, PurchasesFilterComponent, PurchasesTableComponent, WorkOrderComponent, WorkOrderFilterComponent, WorkOrderTableComponent, WorkOrdersByProductComponent, WorkOrdersByProductFilterComponent, WorkOrdersByProductTableComponent, EmployeesComponent, EmployeesFilterComponent, EmployeesTableComponent, CustomersComponent, CustomersFilterComponent, CustomersTableComponent, SuppliersComponent, SuppliersTableComponent, SuppliersFilterComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    HighchartsChartModule    
  ]
})
export class ReportsModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
