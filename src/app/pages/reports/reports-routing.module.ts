import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { DailySalesComponent } from './sales/daily-sales/daily-sales.component';
import { MonthlySalesComponent } from './sales/monthly-sales/monthly-sales.component';
import { SalesByProductComponent } from './sales/sales-by-product/sales-by-product.component';
import { SalesByCategoryComponent } from './sales/sales-by-category/sales-by-category.component';
import { SalesBySellerComponent } from './sales/sales-by-seller/sales-by-seller.component';
import { BalanceGeneralComponent } from './accountants/balance-general/balance-general.component';
import { BalanceCheckComponent } from './accountants/balance-check/balance-check.component';
import { ResultsStatusComponent } from './accountants/results-status/results-status.component';
import { AccumulatedIncomeStatementComponent } from './accountants/accumulated-income-statement/accumulated-income-statement.component';
import { SummarizedItemComponent } from './accountants/summarized-item/summarized-item.component';
import { DepartmentExpensesComponent } from './accountants/department-expenses/department-expenses.component';
import { BatchOfInvoicesComponent } from './sales/batch-of-invoices/batch-of-invoices.component';
import { BatchOfReceiptsComponent } from './sales/batch-of-receipts/batch-of-receipts.component';
import { EarningsByProductComponent } from './sales/earnings-by-product/earnings-by-product.component';
import { SalesByUserComponent } from './sales/sales-by-user/sales-by-user.component';
import { SalesByBranchComponent } from './sales/sales-by-branch/sales-by-branch.component';
import { QuotesComponent } from './sales/quotes/quotes.component';
import { QuotesByProductComponent } from './sales/quotes-by-product/quotes-by-product.component';
import { ProductsServicesListComponent } from './administrative/products-services-list/products-services-list.component';
import { InventoriesComponent } from './administrative/inventories/inventories.component';
import { AccountReceivableComponent } from './administrative/account-receivable/account-receivable.component';
import { PurchasesComponent } from './administrative/purchases/purchases.component';
import { WorkOrderComponent } from './administrative/work-order/work-order.component';
import { WorkOrdersByProductComponent } from './administrative/work-orders-by-product/work-orders-by-product.component';
import { EmployeesComponent } from './contacts/employees/employees.component';
import { CustomersComponent } from './contacts/customers/customers.component';
import { SuppliersComponent } from './contacts/suppliers/suppliers.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent    
  },
  {
    path: 'sales/daily-sales',
    component: DailySalesComponent    
  },
  {
    path: 'sales/monthly-sales',
    component: MonthlySalesComponent    
  },
  {
    path: 'sales/sales-by-product',
    component: SalesByProductComponent
  },
  {
    path: 'sales/sales-by-category',
    component: SalesByCategoryComponent
  },
   {
    path: 'sales/sales-by-seller',
    component: SalesBySellerComponent
  },
  {
    path: 'sales/batch-of-invoices',
    component: BatchOfInvoicesComponent
  },
  {
    path: 'sales/batch-of-receipts',
    component: BatchOfReceiptsComponent
  },
   {
    path: 'sales/batch-of-receipts',
    component: BatchOfReceiptsComponent
  },
  {
    path: 'sales/earnings-by-product',
    component: EarningsByProductComponent
  },
  {
    path: 'sales/sales-by-user',
    component: SalesByUserComponent
  },
  {
    path: 'sales/sales-by-branch',
    component: SalesByBranchComponent
  },
  {
    path: 'sales/quotes',
    component: QuotesComponent
  },
  {
    path: 'sales/quotes-by-product',
    component: QuotesByProductComponent
  },
  {
    path: 'administrative/products-services-list',
    component: ProductsServicesListComponent
  },
  {
    path: 'administrative/inventories',
    component: InventoriesComponent
  },
  {
    path: 'administrative/purchases',
    component: PurchasesComponent
  },
  {
    path: 'administrative/account-receivable',
    component: AccountReceivableComponent
  },
  {
    path: 'administrative/work-order',
    component: WorkOrderComponent
  },
   {
    path: 'administrative/work-orders-by-product',
    component: WorkOrdersByProductComponent
  },
  {
    path: 'contacts/employees',
    component: EmployeesComponent
  },
   {
    path: 'contacts/customers',
    component: CustomersComponent
  },
  {
    path: 'contacts/suppliers',
    component: SuppliersComponent
  },
  {
    path: 'accountants/balance-general',
    component: BalanceGeneralComponent
  },
  {
    path: 'accountants/checking-balance',
    component: BalanceCheckComponent
  },
  {
    path: 'accountants/status-results',
    component: ResultsStatusComponent
  },
  {
    path: 'accountants/accumulated-income-statement',
    component: AccumulatedIncomeStatementComponent
  },
  {
    path: 'accountants/summarized-item',
    component: SummarizedItemComponent
  },
  {
    path: 'accountants/department-expenses',
    component: DepartmentExpensesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
