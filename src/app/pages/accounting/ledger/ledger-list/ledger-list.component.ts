import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ledger-list',
  templateUrl: './ledger-list.component.html',
  styleUrls: ['./ledger-list.component.scss']
})
export class LedgerListComponent implements OnInit {

  ledgerDatas: any

  activeCollapse = true  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.ledgerData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.ledgerDatas = []
  }
  
  ledgerData() {
    this.ledgerDatas = [
      {
        ledger_id: '1',
        seat_id: '1',
        ledger_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        ledger_provider_name: 'Provider 1',
        ledger_total: '104',
        ledger_balance: '104',
        ledger_date: '2018-12-23',
        ledger_state: '0'
      }, 
      {
        ledger_id: '2',   
        seat_id: '2',     
        ledger_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        ledger_provider_name: 'Provider 2',
        ledger_total: '104',
        ledger_balance: '104',
        ledger_date: '2018-12-23',
        ledger_state: '0'
      },
      {
        ledger_id: '3',
        seat_id: '3',
        ledger_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        ledger_provider_name: 'Provider 3',
        ledger_total: '104',
        ledger_balance: '104',
        ledger_date: '2018-12-23',
        ledger_state: '1'
      }, 
      {
        ledger_id: '4',
        seat_id: '4',
        ledger_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        ledger_provider_name: 'Provider 1',
        ledger_total: '104',
        ledger_balance: '104',
        ledger_date: '2018-12-23',
        ledger_state: '0'
      }                      
    ]
  }

}
