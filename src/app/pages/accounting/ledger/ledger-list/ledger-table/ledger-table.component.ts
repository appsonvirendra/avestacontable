import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ledger-table',
  templateUrl: './ledger-table.component.html',
  styleUrls: ['./ledger-table.component.scss']
})
export class LedgerTableComponent implements OnInit {

  @Input() ledgerData: any

  constructor() { }

  ngOnInit() {
  }

}
