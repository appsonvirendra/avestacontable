import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ledger-filter',
  templateUrl: './ledger-filter.component.html',
  styleUrls: ['./ledger-filter.component.scss']
})
export class LedgerFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  // search account dialog
  searchAccountVisible = false
  accountDatas = []

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.accountData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      account_id: [null],
      account_description: [null],
      cost_center: ['0']
    })  
  }

  searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      account_id: [null],
      account_description: [null],
      cost_center: ['0']
    }) 
  }

  printLedger() {
    
  }

  // search account dialog
  searchAccountOpen() {
    this.searchAccountVisible = true
  }

  searchAccountClose() {
    this.searchAccountVisible = false
  }   

  accountData() {
    this.accountDatas = [
      {
        account_id: '1',
        account_code: '1-01-01-01',
        account_description: "CAJA GENERAL",
        belonging_to: 'Activo',
        account_is: 'Cuenta de Grupo'
      }           
    ]
  }

  accountAddInput($event) {
    this.filterForm.patchValue({
      account_id: $event.account_id,
      account_description: $event.account_description,
    })   

    this.searchAccountClose()    
  }  

}
