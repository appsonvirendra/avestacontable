import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export interface TreeNodeInterface {
  key: number;
  code: number;
  account: string;
  level?: number;
  expand?: boolean;
  children?: TreeNodeInterface[];
  parent?: TreeNodeInterface;
}

@Component({
  selector: 'app-categories-table',
  templateUrl: './categories-table.component.html',
  styleUrls: ['./categories-table.component.scss']
})
export class CategoriesTableComponent implements OnInit {
  listOfMapData: TreeNodeInterface[] = [
    {
      key: 1,
      code: 200000,
      account: 'Passives',
      children: [
          {
          key: 11,
          code: 210000,
          account: 'Current liabilities',
          children: [
            {
              key: 111,
              code: 211000,
              account: 'Debts to pay',
              children: [
                {
                  key: 1111,
                  code:211100,
                  account: 'Advances and advances received'
                },
              ]
            }
          ]
        }
      ]
    },
   
  ];
  mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};
  
  collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
    if ($event === false) {
      if (data.children) {
        data.children.forEach(d => {
          const target = array.find(a => a.key === d.key)!;
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }
  
  convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
    const stack: TreeNodeInterface[] = [];
    const array: TreeNodeInterface[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node = stack.pop()!;
      this.visitNode(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          stack.push({ ...node.children[i], level: node.level! + 1, expand: false, parent: node });
        }
      }
    }

    return array;
  }
  visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }
  @Input() categoriesData: any

  // edit Categories Form
  editCategoriesForm: FormGroup  
  editCategoriesVisible = false

   // add Categories Form
  addCategoriesForm: FormGroup  
  addCategoriesVisible = false

  // search
  search_title = ''

  sortName: string | null = null
  sortValue: string | null = null
  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit(): void {
    this.listOfMapData.forEach(item => {
      this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
    });
    this.editCategoriesFormInit()
    this.addCategoriesFormInit()
  }

  //edit Categories Form
  editCategoriesFormInit() {
    this.editCategoriesForm = this.fb.group({
      categories_code: [null, [Validators.required]],
      categories_name: [null, [Validators.required]],
      categories_description: [null]
    })      
  }  

  editCategoriesOpen() {
    this.editCategoriesVisible = true
  }

  editCategoriesClose() {
    this.editCategoriesForm.reset()
    this.editCategoriesVisible = false
  }      

  editCategoriesSave() {    
    for (const i in this.editCategoriesForm.controls) {
      this.editCategoriesForm.controls[i].markAsDirty()
      this.editCategoriesForm.controls[i].updateValueAndValidity()
    }          
    if(this.editCategoriesForm.valid) {
      this.editCategoriesClose()
    }
  }

    //add Categories Form
  addCategoriesFormInit() {
    this.addCategoriesForm = this.fb.group({
      categories_code: [null, [Validators.required]],
      categories_name: [null, [Validators.required]],
      categories_description: [null]
    })      
  }  

  addCategoriesOpen() {
    this.addCategoriesVisible = true
  }

  addCategoriesClose() {
    this.addCategoriesForm.reset()
    this.addCategoriesVisible = false
  }      

  addCategoriesSave() {    
    for (const i in this.addCategoriesForm.controls) {
      this.addCategoriesForm.controls[i].markAsDirty()
      this.addCategoriesForm.controls[i].updateValueAndValidity()
    }          
    if(this.addCategoriesForm.valid) {
      this.addCategoriesClose()
    }
  }

  keyUpSearch() {   
  } 

}

