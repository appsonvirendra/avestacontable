import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-link',
  templateUrl: './account-link.component.html',
  styleUrls: ['./account-link.component.scss']
})
export class AccountLinkComponent implements OnInit {

  accountlinkDatas: any

  activeCollapse = true  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accountlinkData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.accountlinkDatas = []
  }
  
  accountlinkData() {
    this.accountlinkDatas = [
      {
        accountlink_id: '1',
        seat_id: '1',
        accountlink_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        accountlink_provider_name: 'Provider 1',
        accountlink_total: '104',
        accountlink_balance: '104',
        accountlink_date: '2018-12-23',
        accountlink_state: '0'
      }, 
      {
        accountlink_id: '2',   
        seat_id: '2',     
        accountlink_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        accountlink_provider_name: 'Provider 2',
        accountlink_total: '104',
        accountlink_balance: '104',
        accountlink_date: '2018-12-23',
        accountlink_state: '0'
      },
      {
        accountlink_id: '3',
        seat_id: '3',
        accountlink_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        accountlink_provider_name: 'Provider 3',
        accountlink_total: '104',
        accountlink_balance: '104',
        accountlink_date: '2018-12-23',
        accountlink_state: '1'
      }, 
      {
        accountlink_id: '4',
        seat_id: '4',
        accountlink_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        accountlink_provider_name: 'Provider 1',
        accountlink_total: '104',
        accountlink_balance: '104',
        accountlink_date: '2018-12-23',
        accountlink_state: '0'
      }                      
    ]
  }  

}
