import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-account-link-filter',
  templateUrl: './account-link-filter.component.html',
  styleUrls: ['./account-link-filter.component.scss']
})
export class AccountLinkFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  link_module: any = '0'
  activate_account_link: any

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
  }

  searchFilter() {
    this.searchEvent.emit(this.link_module)
  }

}
