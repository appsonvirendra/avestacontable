import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-account-link-table',
  templateUrl: './account-link-table.component.html',
  styleUrls: ['./account-link-table.component.scss']
})
export class AccountLinkTableComponent implements OnInit {

  @Input() accountlinkData: any

  cost_center: any = []
  account_type: any = []
  
  account_id: any = []
  account_code: any = []
  account_description: any = []

  // search account dialog
  searchAccountVisible = false
  accountDatas = []

  constructor() { }

  ngOnInit() {
    this.accountData()
  }

  // search account dialog
  searchAccountOpen() {
    this.searchAccountVisible = true
  }

  searchAccountClose() {
    this.searchAccountVisible = false
  }   

  accountData() {
    this.accountDatas = [
      {
        account_id: '1',
        account_code: '1-01-01-01',
        account_description: "CAJA GENERAL",
        belonging_to: 'Activo',
        account_is: 'Cuenta de Grupo'
      }           
    ]
  }

  accountAddInput($event) {
    this.account_id = $event.account_id
    this.account_code = $event.account_code
    this.account_description = $event.account_description

    this.searchAccountClose()    
  }  

  saveAccountLink() {

  }

  clearFilter() {
    
  }

}
