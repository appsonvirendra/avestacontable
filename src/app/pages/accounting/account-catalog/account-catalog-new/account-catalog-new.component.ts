import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-account-catalog-new',
  templateUrl: './account-catalog-new.component.html',
  styleUrls: ['./account-catalog-new.component.scss']
})
export class AccountCatalogNewComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  accountCodingVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      accountcatalog_code: [null, [Validators.required]],
      account_is: [null, [Validators.required]],
      description: [null, [Validators.required]],
      required_cost_center: [null, [Validators.required]],
      belonging_to: [null, [Validators.required]]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm.reset() 
  }  
  
  accountCodingOpen() {
    this.accountCodingVisible = true
  }

  accountCodingClose() {
    this.accountCodingVisible = false
  }

}
