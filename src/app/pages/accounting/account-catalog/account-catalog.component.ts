import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-catalog',
  templateUrl: './account-catalog.component.html',
  styleUrls: ['./account-catalog.component.scss']
})
export class AccountCatalogComponent implements OnInit {

  accountcatalogDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accountcatalogData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.accountcatalogDatas = []
  }
  
  accountcatalogData() {
    this.accountcatalogDatas = [
      {
        accountcatalog_id: '1',
        accountcatalog_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        accountcatalog_provider_name: 'Provider 1',
        accountcatalog_total: '104',
        accountcatalog_balance: '104',
        accountcatalog_date: '2018-12-23',
        accountcatalog_state: '2'
      }, 
      {
        accountcatalog_id: '2',        
        accountcatalog_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        accountcatalog_provider_name: 'Provider 2',
        accountcatalog_total: '104',
        accountcatalog_balance: '104',
        accountcatalog_date: '2018-12-23',
        accountcatalog_state: '2'
      },
      {
        accountcatalog_id: '3',
        accountcatalog_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        accountcatalog_provider_name: 'Provider 3',
        accountcatalog_total: '104',
        accountcatalog_balance: '104',
        accountcatalog_date: '2018-12-23',
        accountcatalog_state: '1'
      }, 
      {
        accountcatalog_id: '4',
        accountcatalog_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        accountcatalog_provider_name: 'Provider 1',
        accountcatalog_total: '104',
        accountcatalog_balance: '104',
        accountcatalog_date: '2018-12-23',
        accountcatalog_state: '0'
      }                      
    ]
  }

}
