import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export interface TreeNodeInterface {
  key: number;
  code: number;
  account: string;
  account_is: string;
  level?: number;
  expand?: boolean;
  belonging?: string;
  children?: TreeNodeInterface[];
  parent?: TreeNodeInterface;
}

@Component({
  selector: 'app-account-catalog-table',
  templateUrl: './account-catalog-table.component.html',
  styleUrls: ['./account-catalog-table.component.scss']
})
export class AccountCatalogTableComponent implements OnInit {
  listOfMapData: TreeNodeInterface[] = [
    {
      key: 1,
      code: 1,
      account: 'ACTIVO',
      belonging: 'Activo',
      account_is: 'Cuenta de Grupo',
      children: [
          {
          key: 11,
          code: 1.1,
          account: 'ACTIVO',
          belonging: 'Activo',
          account_is: 'Cuenta de Grupo',
          children: [
            {
              key: 111,
              code: 1.11,
              account: 'ACTIVO',
              belonging: 'Activo',
              account_is: 'Cuenta de Grupo',
              children: [
                {
                  key: 1111,
                  code:1.111,
                  account: 'ACTIVO',
                  account_is: 'Cuenta de Grupo',
                  belonging: 'Activo'
                },
              ]
            }
          ]
        }
      ]
    },
   
  ];
  mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};
  
  collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
    if ($event === false) {
      if (data.children) {
        data.children.forEach(d => {
          const target = array.find(a => a.key === d.key)!;
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }
  
  convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
    const stack: TreeNodeInterface[] = [];
    const array: TreeNodeInterface[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node = stack.pop()!;
      this.visitNode(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          stack.push({ ...node.children[i], level: node.level! + 1, expand: false, parent: node });
        }
      }
    }

    return array;
  }
  visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }
  @Input() accountcatalogData: any

  // edit dialog form
  editAccountForm: FormGroup  
  editAccountVisible = false

  // search
  search_title = ''

  accountcatalogDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null
  
  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit(): void {
    this.listOfMapData.forEach(item => {
      this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
    });
    this.editAccountFormInit()
  }

  // send invoice mail form
  editAccountFormInit() {
    this.editAccountForm = this.fb.group({
      accountcatalog_code: [null, [Validators.required]],
      description: [null, [Validators.required]],
      belonging_to: [null, [Validators.required]],
      account_is: [null, [Validators.required]],
      required_cost_department: [null, [Validators.required]]
    })      
  }  

  editAccountOpen() {
    this.editAccountVisible = true
  }

  editAccountClose() {
    this.editAccountForm.reset()
    this.editAccountVisible = false
  }      

  editAccountSave() {    
    for (const i in this.editAccountForm.controls) {
      this.editAccountForm.controls[i].markAsDirty()
      this.editAccountForm.controls[i].updateValueAndValidity()
    }          
    if(this.editAccountForm.valid) {
      this.editAccountClose()
    }
  }

  keyUpSearch() {
    
  }

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
