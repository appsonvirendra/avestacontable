import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDebitNotesComponent } from './edit-debit-notes.component';

describe('EditDebitNotesComponent', () => {
  let component: EditDebitNotesComponent;
  let fixture: ComponentFixture<EditDebitNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDebitNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDebitNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
