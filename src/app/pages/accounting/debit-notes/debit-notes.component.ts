import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-debit-notes',
  templateUrl: './debit-notes.component.html',
  styleUrls: ['./debit-notes.component.scss']
})
export class DebitNotesComponent implements OnInit {

  debitDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.debitData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.debitDatas = []
  }
  
  debitData() {
    this.debitDatas = [
      {
        debit_id: '1',
        debit_code: '000-001-01-001',
        customer_id: '11',
        provider_firstname: 'Roy',
        provider_lastname: 'King',
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }, 
      {
        debit_id: '2',
        debit_code: '000-001-01-002',
        customer_id: '22',
        provider_firstname: 'Jennifer',
        provider_lastname: 'Hart',        
        returns_provider_name: 'Provider 2',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      },
      {
        debit_id: '3',
        debit_code: '000-001-01-003',
        customer_id: '33',
        provider_firstname: 'Olivia',
        provider_lastname: 'Carr',        
        returns_provider_name: 'Provider 3',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }, 
      {
        debit_id: '4',
        debit_code: '000-001-01-004',
        customer_id: '33',
        provider_firstname: 'Jean',
        provider_lastname: 'Shaw',        
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }                      
    ]
  }

}
