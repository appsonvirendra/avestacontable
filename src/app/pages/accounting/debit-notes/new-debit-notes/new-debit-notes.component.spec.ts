import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDebitNotesComponent } from './new-debit-notes.component';

describe('NewDebitNotesComponent', () => {
  let component: NewDebitNotesComponent;
  let fixture: ComponentFixture<NewDebitNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDebitNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDebitNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
