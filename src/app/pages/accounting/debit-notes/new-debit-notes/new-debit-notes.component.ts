import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface ItemnewdebitData {
  id: string;
  item: string;
  value: string;
  observations: string;
  tax: string;
  quantity: number;
  total: number;
}
interface ItemSaleInvoiceData {
  id: string;
  total: string;
  paid_out: string;
  payable: string;
}

interface ItemmoneybackData {
  id: string;
  amount: string;
  Observations: string;
}

interface ItemData {
  id: string;
  name: string;
  age: string;
}

@Component({
  selector: 'app-new-debit-notes',
  templateUrl: './new-debit-notes.component.html',
  styleUrls: ['./new-debit-notes.component.scss']
})
export class NewDebitNotesComponent implements OnInit {

 referenceguideAddform: FormGroup
 senderDialogVisible = false
 addSenderForm: FormGroup

 reasonDialogVisible = false
 addReasonForm: FormGroup

  myBlog: string = "";

  newdebitnotesAddform: FormGroup
  i = 0;
  currentDate:any = new Date()
  supplierAddDialogVisible = false
  addSupplierAddForm: FormGroup

  // New development
  itemSelect: any = []

  itemSelectSupplier: any = []
  
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.newdebitnotesFormInit()
    this.addSupplierAddFormInit()
    this.addnewdebitRow()
    this.addSenderFormInit()
    this.addReasonFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  newdebitnotesFormInit() {
    this.newdebitnotesAddform = this.fb.group({
      supplier: [null],
      correlative_of_the_voucher: [null],
      sender: [null],
      proof_issue_date: [null],
      date_of_issue: [null],
      reason: [null],
      original: [null],
      numeration: [null],
      copy: [null],
      item: [null],
      reference: [null],
      price: [null],
      discount: [null],
      tax: [null],
      description: [null],
      amount: [null],
      sales_invoice: ['0']

    })      
  }


  
  saveCredit() {
    for (const i in this.newdebitnotesAddform.controls) {
      this.newdebitnotesAddform.controls[i].markAsDirty()
      this.newdebitnotesAddform.controls[i].updateValueAndValidity()
    }
    if(this.newdebitnotesAddform.valid) {
      console.log('this.newdebitnotesAddform.value', this.newdebitnotesAddform.value)
    }
  }
        // reason form
     addReasonFormInit() {
    this.addReasonForm = this.fb.group({
      reason: [null]
    })   
  }

  openReasonDialog() {
    this.reasonDialogVisible = true
  }

  closeReasonDialog() {
    this.reasonDialogVisible = false
  }

  saveReason() {
    for (const i in this.addReasonForm.controls) {
      this.addReasonForm.controls[i].markAsDirty()
      this.addReasonForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addReasonForm.value --', this.addReasonForm.value)
    if(this.addReasonForm.value.category_name != null && this.addReasonForm.value.category_name != '')  {
      this.reasonDialogVisible = false
      this.addReasonFormInit()
    }    
  }
  
      // sender form
     addSenderFormInit() {
    this.addSenderForm = this.fb.group({
      company_name: [null],
      c_a_i: [null],
      address: [null],
      email: [null],
      telephone: [null],
      expires: [null],
      image: [null],
      site_url: [null],
      rtn_tdi: [null],
    })   
  }

  openSenderDialog() {
    this.senderDialogVisible = true
  }

  closeSenderDialog() {
    this.senderDialogVisible = false
  }

  saveSender() {
    for (const i in this.addSenderForm.controls) {
      this.addSenderForm.controls[i].markAsDirty()
      this.addSenderForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addSenderForm.value --', this.addSenderForm.value)
    if(this.addSenderForm.value.category_name != null && this.addSenderForm.value.category_name != '')  {
      this.senderDialogVisible = false
      this.addSenderFormInit()
    }    
  } 
  

      // Supplier Add form
     addSupplierAddFormInit() {
    this.addSupplierAddForm = this.fb.group({
      supplier_name: [null],
      supplier_identification: [null],
      supplier_phone: [null],
      supplier_mobile: [null],
      supplier_fax: [null],
      supplier_email: [null],
      supplier_direction: [null]
    })   
  }

  openNewDebitNotesDialog() {
    this.supplierAddDialogVisible = true
  }

  closeSupplierAddDialog() {
    this.supplierAddDialogVisible = false
  }

  saveSupplierAdd() {
    for (const i in this.addSupplierAddForm.controls) {
      this.addSupplierAddForm.controls[i].markAsDirty()
      this.addSupplierAddForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addSupplierAddForm.value --', this.addSupplierAddForm.value)
    if(this.addSupplierAddForm.value.category_name != null && this.addSupplierAddForm.value.category_name != '')  {
      this.supplierAddDialogVisible = false
      this.addSupplierAddFormInit()
    }    
  } 
// add line Begin
   
  listOfnewdebitData: ItemnewdebitData[] = [];


  addnewdebitRow(): void {
    this.listOfnewdebitData = [
      ...this.listOfnewdebitData,
      {
        id: `${this.i+1}`,
        item: ``,
        value: '',
        tax: '',
        observations: '',
        quantity: 0,
        total: 0
      }
    ];
    this.i++;
  }

  deletenewdebitRow(index): void {
    this.listOfnewdebitData.splice(index, 1)
  } 
// add line End

// add money back Begin
   
  listOfmoneybackData: ItemmoneybackData[] = [];


  addmoneybackRow(): void {
    this.listOfmoneybackData = [
      ...this.listOfmoneybackData,
      {
        id: `${this.i+1}`,
        amount: '',
        Observations: ''
      }
    ];
    this.i++;
  }

  deletemoneybackRow(index): void {
    this.listOfmoneybackData.splice(index, 1)
  } 
// add money back End

// add money back begin

j = 0;
  editId: string | null = null;
  listOfSupplierInvoiceData: ItemSaleInvoiceData[] = [];


  addsupplierinvoiceRow(): void {
    if(this.listOfSupplierInvoiceData.length < 1) {
      this.listOfSupplierInvoiceData = [
        ...this.listOfSupplierInvoiceData,
        {
          id: `${this.j}`,
          total: '',
          paid_out: '',
          payable: ''
        }
      ];
      this.j++;
    }
  }

  deletesupplierinvoiceRow(index): void {
    this.listOfSupplierInvoiceData.splice(index, 1)
    this.itemSelectSupplier = []
  }

  // add money back end

  // New development
  updateDebitItem(itemData, itemIndex) {

    let itemValue

    if(itemData == '1') {
      itemValue = {
        id: '1',
        item: 'item 1',
        value: '1244',
        tax: '0',
        observations: '',
        quantity: 1,
        total: 10     
      }      
    } else {
      itemValue = {
        id: '2',
        item: 'item 2',
        value: '1245',
        tax: '0',
        observations: '',
        quantity: 1,
        total: 20      
      }  
    }

    this.listOfnewdebitData[itemIndex] = itemValue
  }

  updateSupplierItem(supplierData, supplierIndex) {

    let supplierValue

    if(supplierData == '1') {
      supplierValue = {
        id: '1',
        total: 0,
        paid_out: 0,
        payable: 0   
      }      
    }

    this.listOfSupplierInvoiceData[supplierIndex] = supplierValue
  }  


}
