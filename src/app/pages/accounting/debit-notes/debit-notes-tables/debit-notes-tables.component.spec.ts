import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebitNotesTablesComponent } from './debit-notes-tables.component';

describe('DebitNotesTablesComponent', () => {
  let component: DebitNotesTablesComponent;
  let fixture: ComponentFixture<DebitNotesTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebitNotesTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebitNotesTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
