import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-debit-notes-tables',
  templateUrl: './debit-notes-tables.component.html',
  styleUrls: ['./debit-notes-tables.component.scss']
})
export class DebitNotesTablesComponent implements OnInit {
// modal loading
isVisible = false;
isConfirmLoading = false;

  @Input() debitDatas: any
  
  // search
  search_title = ''
  debitDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }
  // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
    this.debitData()
  }

  debitData() {
    this.debitDisplayDatas = [...this.debitDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { debit_code: string; }) => {
      return (
        item.debit_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.debitDatas.filter((item: { debit_code: string; }) => filterFunc(item))

    this.debitDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.debitDisplayDatas = [...this.debitDatas]
    }    
  }

}

