import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-info-debit-notes',
  templateUrl: './info-debit-notes.component.html',
  styleUrls: ['./info-debit-notes.component.scss']
})
export class InfoDebitNotesComponent implements OnInit {

  inforeference_id: any
  isVisible = false;
  infodebitnotesDatas: any
  infodebitDatas: any
  generatePaymentDatas: any  

  // create receipt form
  createReceiptForm: FormGroup  
  createReceiptVisible = false
  
  // send inforeference mail form
  sendInfoReferenceMailForm: FormGroup  
  sendInfoReferenceMailVisible = false  

  generatePaymentCollapse = false

  currentDate:any = new Date()

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder    
  ) { 

  }
  // generate payment view receipt  
  generatePayViewVisible = false  
  ngOnInit() {
    this.debitnotesInfo()
    this.languageTranslate()
    this.getQueryParams()

    this.infodebitnotesInfo()
    this.generatePaymentInfo()
    
    this.createReceiptFormInit()
    this.sendInfoReferenceMailFormInit()    
  }
  listOfData = [
    {
      key: '1',
      qty: '50',
      code: 'SW92751',
      description: 'Dulce de Leche en coco',
      unit_price: '1,300.00',
      total: '300.00'
    },
    {
      key: '2',
      qty: '50',
      code: 'SW92751',
      description: 'Pastel selva negra',
      unit_price: '1,300.00',
      total: '1,495.00'
    },
    {
      key: '3',
      qty: '50',
      code: 'SW92751',
      description: 'Vasito de leche ',
      unit_price: '1,300.00',
      total: '1,300.00'
    }
  ];
debitnotesInfo() {
    this.infodebitDatas = 
      {

        "debit_date_of_issue": "01/01/2020",
        "debit_sender": "Inversiones Logisticas H & M S. de R.L."
      }
  } 
  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.inforeference_id = params.inforeference_id
    })    
  }  

  infodebitnotesInfo() {
    this.infodebitnotesDatas = 
      {
        "inforeference_order_no": "000-001-01-00000351",
        "inforeference_date": "2019-12-27 13:25:02",
        "inforeference_discount": "0",
        "inforeference_customer_id": "1",
        "inforeference_customer_name": "John brawn",
        "inforeference_exchange_rate": "123",
        "inforeference_receive_product": null,
        "inforeference_notes": null,
        "inforeference_exempt_amount": 9,
        "inforeference_taxed_amount": 7,
        "inforeference_isv_amount": "0",
        "inforeference_final_discount": "0",
        "inforeference_total_to_pay": 9,
        "inforeference_status": "2",
        "infodebitnotes_product_item": [
          {
            "item_qty": "50",
            "item_code": "SW92751",
            "item_description": "Dulce de Leche en coco",
            "unit_price": "1,300.00",
            "total": "300.00"
          },
          {
            "item_qty": "50",
            "item_code": "SW92751",
            "item_description": "Pastel selva negra",
            "unit_price": "1,300.00",
            "total": "1,495.00"
          },
          {
            "item_qty": "50",
            "item_code": "SW92751",
            "item_description": "Vasito de leche",
            "unit_price": "1,300.00",
            "total": "1,300.00"
          }
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
        "g_p_receipt_status": "1"
      }           
    ]
  }

  // create receipt form
  createReceiptFormInit() {
    this.createReceiptForm = this.fb.group({
      receipt_date_of_payment: [this.currentDate, [Validators.required]],
      receipt_payment_received: [null, [Validators.required]],
      receipt_bank_account: [null, [Validators.required]],
      receipt_method_of_payment: ['1', [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  createReceiptOpen() {
    this.createReceiptVisible = true
  }

  createReceiptClose() {
    this.createReceiptForm.reset()
    this.createReceiptVisible = false
  }      

  createReceiptSave() {        
    for (const i in this.createReceiptForm.controls) {
      this.createReceiptForm.controls[i].markAsDirty()
      this.createReceiptForm.controls[i].updateValueAndValidity()
    }    
    // console.log('this.createReceiptForm.value', this.createReceiptForm.value)
    if(this.createReceiptForm.valid) {
      this.createReceiptClose()
    }
  }  
  
  // send inforeference mail form
  sendInfoReferenceMailFormInit() {
    this.sendInfoReferenceMailForm = this.fb.group({
      send_inforeference_email1: [null, [Validators.required]],
      send_inforeference_email2: [null],
      send_inforeference_email3: [null]
    })      
  }  

  sendInfoReferenceMailOpen() {
    this.sendInfoReferenceMailVisible = true
  }

  sendInfoReferenceMailClose() {
    this.sendInfoReferenceMailForm.reset()
    this.sendInfoReferenceMailVisible = false
  }      

  sendInfoReferenceMailSave() {    
    for (const i in this.sendInfoReferenceMailForm.controls) {
      this.sendInfoReferenceMailForm.controls[i].markAsDirty()
      this.sendInfoReferenceMailForm.controls[i].updateValueAndValidity()
    }          
    if(this.sendInfoReferenceMailForm.valid) {
      this.sendInfoReferenceMailClose()
    }
  } 
  // generate payment view receipt start  
  generatePayViewOpen() {
    this.generatePayViewVisible = true
  }

  generatePayViewClose() {
    this.generatePayViewVisible = false
  }      

  generatePayViewPrint() {
    this.generatePayViewClose()
  }
  // generate payment collapse
  generatePyamentCollapse() {
    this.generatePaymentCollapse = !this.generatePaymentCollapse
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}



