import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoDebitNotesComponent } from './info-debit-notes.component';

describe('InfoDebitNotesComponent', () => {
  let component: InfoDebitNotesComponent;
  let fixture: ComponentFixture<InfoDebitNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoDebitNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoDebitNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
