import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountCatalogComponent } from './account-catalog/account-catalog.component';
import { SeatListComponent } from './seat/seat-list/seat-list.component';
import { NewSeatComponent } from './seat/new-seat/new-seat.component';
import { InfoSeatComponent } from './seat/info-seat/info-seat.component';
import { DiaryBookListComponent } from './diary-book/diary-book-list/diary-book-list.component';
import { LedgerListComponent } from './ledger/ledger-list/ledger-list.component';
import { AccountLinkComponent } from './account-link/account-link.component';
import { PeriodClosingComponent } from './period-closing/period-closing.component';
import { EditSeatComponent } from './seat/edit-seat/edit-seat.component';
import { DebitNotesComponent } from './debit-notes/debit-notes.component';
import { NewDebitNotesComponent } from './debit-notes/new-debit-notes/new-debit-notes.component';
import { EditDebitNotesComponent } from './debit-notes/edit-debit-notes/edit-debit-notes.component';
import { InfoDebitNotesComponent } from './debit-notes/info-debit-notes/info-debit-notes.component';
import { CategoriesComponent } from './categories/categories.component';



const routes: Routes = [
  {
    path: 'account-catalog/list',
    component: AccountCatalogComponent    
  },
  {
    path: 'seat/list',
    component: SeatListComponent    
  },
  {
    path: 'seat/new',
    component: NewSeatComponent
  },
  {
    path: 'seat/info',
    component: InfoSeatComponent  
  },
  {
    path: 'seat/edit',
    component: EditSeatComponent  
  },
  {
    path: 'diary-book/list',
    component: DiaryBookListComponent  
  },
  {
    path: 'ledger/list',
    component: LedgerListComponent
  },
  {
    path: 'account-link',
    component: AccountLinkComponent
  },
  {
    path: 'period-closing',
    component: PeriodClosingComponent
  },
   {
    path: 'debit-notes/list',
    component: DebitNotesComponent
  },
   {
   path: 'debit-notes/new',
   component:NewDebitNotesComponent
  },
  {
   path: 'debit-notes/edit',
   component:EditDebitNotesComponent
  },
  {
   path: 'debit-notes/info',
   component:InfoDebitNotesComponent
  },
  {
   path: 'categories',
   component:CategoriesComponent
  }       
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }
