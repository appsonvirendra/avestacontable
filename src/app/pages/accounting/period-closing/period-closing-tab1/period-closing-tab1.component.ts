import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-period-closing-tab1',
  templateUrl: './period-closing-tab1.component.html',
  styleUrls: ['./period-closing-tab1.component.scss']
})
export class PeriodClosingTab1Component implements OnInit {

  monthlyPeriodAddForm: FormGroup
  
  monthlyPeriodCloseDatas: any

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.monthlyPeriodAddFormInit()

    this.monthlyPeriodCloseData()
  }

  monthlyPeriodCloseData() {
    this.monthlyPeriodCloseDatas = [
      // {
      //   monthly_period_close_id: '1',
      //   month: 'Enero',
      //   year: '2018'
      // }, 
    ]
  }  

  monthlyPeriodAddFormInit() {
    this.monthlyPeriodAddForm = this.fb.group({
      select_year: [null, [Validators.required]],
      select_month: [null, [Validators.required]]
    })      
  }
  
  monthlyPeriodAddFormSave() {
    for (const i in this.monthlyPeriodAddForm.controls) {
      this.monthlyPeriodAddForm.controls[i].markAsDirty()
      this.monthlyPeriodAddForm.controls[i].updateValueAndValidity()
    }
    if(this.monthlyPeriodAddForm.valid) {
      // console.log('this.monthlyPeriodAddForm.value', this.monthlyPeriodAddForm.value)
    }
  }    

}
