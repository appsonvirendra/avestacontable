import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-period-closing-tab2',
  templateUrl: './period-closing-tab2.component.html',
  styleUrls: ['./period-closing-tab2.component.scss']
})
export class PeriodClosingTab2Component implements OnInit {

  yearlyPeriodAddForm: FormGroup
  
  yearlyPeriodCloseDatas: any

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.yearlyPeriodAddFormInit()

    this.yearlyPeriodCloseData()
  }

  yearlyPeriodCloseData() {
    this.yearlyPeriodCloseDatas = [
      // {
      //   yearly_period_close_id: '1',
      //   year: '2018'
      // }, 
    ]
  }  

  yearlyPeriodAddFormInit() {
    this.yearlyPeriodAddForm = this.fb.group({
      select_year: [null, [Validators.required]]
    })      
  }
  
  yearlyPeriodAddFormSave() {
    for (const i in this.yearlyPeriodAddForm.controls) {
      this.yearlyPeriodAddForm.controls[i].markAsDirty()
      this.yearlyPeriodAddForm.controls[i].updateValueAndValidity()
    }
    if(this.yearlyPeriodAddForm.valid) {
      // console.log('this.yearlyPeriodAddForm.value', this.yearlyPeriodAddForm.value)
    }
  }

}
