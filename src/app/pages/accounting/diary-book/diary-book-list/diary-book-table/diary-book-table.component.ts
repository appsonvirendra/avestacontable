import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-diary-book-table',
  templateUrl: './diary-book-table.component.html',
  styleUrls: ['./diary-book-table.component.scss']
})
export class DiaryBookTableComponent implements OnInit {

  @Input() diarybookData: any

  constructor() { }

  ngOnInit() {
  }

}
