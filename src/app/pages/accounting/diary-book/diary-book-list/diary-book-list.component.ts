import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-diary-book-list',
  templateUrl: './diary-book-list.component.html',
  styleUrls: ['./diary-book-list.component.scss']
})
export class DiaryBookListComponent implements OnInit {

  diarybookDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.diarybookData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.diarybookDatas = []
  }
  
  diarybookData() {
    this.diarybookDatas = [
      {
        diarybook_id: '1',
        diarybook_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        diarybook_provider_name: 'Provider 1',
        diarybook_total: '104',
        diarybook_balance: '104',
        diarybook_date: '2018-12-23',
        diarybook_state: '0'
      }, 
      {
        diarybook_id: '2',        
        diarybook_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        diarybook_provider_name: 'Provider 2',
        diarybook_total: '104',
        diarybook_balance: '104',
        diarybook_date: '2018-12-23',
        diarybook_state: '0'
      },
      {
        diarybook_id: '3',
        diarybook_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        diarybook_provider_name: 'Provider 3',
        diarybook_total: '104',
        diarybook_balance: '104',
        diarybook_date: '2018-12-23',
        diarybook_state: '1'
      }, 
      {
        diarybook_id: '4',
        diarybook_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        diarybook_provider_name: 'Provider 1',
        diarybook_total: '104',
        diarybook_balance: '104',
        diarybook_date: '2018-12-23',
        diarybook_state: '0'
      }                      
    ]
  }

}
