import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-diary-book-filter',
  templateUrl: './diary-book-filter.component.html',
  styleUrls: ['./diary-book-filter.component.scss']
})
export class DiaryBookFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      voucher: [null],
      cost_center: ['0']
    })  
  }

  searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      voucher: [null],
      cost_center: ['0']
    }) 
  }

  printDiaryBook() {
    
  }

}
