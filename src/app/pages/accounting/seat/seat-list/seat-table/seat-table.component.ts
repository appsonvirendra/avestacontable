import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-seat-table',
  templateUrl: './seat-table.component.html',
  styleUrls: ['./seat-table.component.scss']
})
export class SeatTableComponent implements OnInit {
  // modal loading
  isVisible = false;
  isConfirmLoading = false;
  @Input() seatData: any

  constructor() { }
 
    // modal confirm
    deleteConfirmOpen(): void {
      this.isVisible = true;
    }
    handleOk(): void {
      this.isConfirmLoading = true;
      setTimeout(() => {
        this.isVisible = false;
        this.isConfirmLoading = false;
      }, 3000);
    }
    handleCancel(): void {
      this.isVisible = false;
    }
  ngOnInit() {
  }

}
