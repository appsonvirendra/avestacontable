import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-seat-list',
  templateUrl: './seat-list.component.html',
  styleUrls: ['./seat-list.component.scss']
})
export class SeatListComponent implements OnInit {

  seatDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.seatData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.seatDatas = []
  }
  
  seatData() {
    this.seatDatas = [
      {
        seat_id: '1',
        seat_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        seat_provider_name: 'Provider 1',
        seat_total: '104',
        seat_balance: '104',
        seat_date: '2018-12-23',
        seat_state: '0'
      }, 
      {
        seat_id: '2',        
        seat_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        seat_provider_name: 'Provider 2',
        seat_total: '104',
        seat_balance: '104',
        seat_date: '2018-12-23',
        seat_state: '0'
      },
      {
        seat_id: '3',
        seat_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        seat_provider_name: 'Provider 3',
        seat_total: '104',
        seat_balance: '104',
        seat_date: '2018-12-23',
        seat_state: '1'
      }, 
      {
        seat_id: '4',
        seat_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        seat_provider_name: 'Provider 1',
        seat_total: '104',
        seat_balance: '104',
        seat_date: '2018-12-23',
        seat_state: '0'
      }                      
    ]
  }

}
