import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-seat',
  templateUrl: './new-seat.component.html',
  styleUrls: ['./new-seat.component.scss']
})
export class NewSeatComponent implements OnInit {

  seatAddForm: FormGroup

  currentDate:any = new Date()  

  // seat table data 
  seatTableDatas = []  

  // search account dialog
  searchAccountVisible = false
  accountDatas = []


  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.seatAddFormInit()

    this.seatTableData()  
    
    this.accountData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  seatAddFormInit() {
    this.seatAddForm = this.fb.group({
      date: [null, [Validators.required]],
      description: [null, [Validators.required]],
      voucher: [null, [Validators.required]],
      source: [null, [Validators.required]],
      cost_center: [null, [Validators.required]],
      seat_file: [null],
      commentary: [null],
      debit: [null],
      credit: [null],
      seatTableDatas: []
    })      
  }
  
  saveSeat() {
    for (const i in this.seatAddForm.controls) {
      this.seatAddForm.controls[i].markAsDirty()
      this.seatAddForm.controls[i].updateValueAndValidity()
    }
    if(this.seatAddForm.valid) {
      console.log('this.seatAddForm.value', this.seatAddForm.value)
    }
  }

  seatTableData() {
    this.seatTableDatas = []    
  }

  seatTableDataRemove(item_index) {
    this.seatTableDatas.splice(item_index, 1)
    this.seatAddForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  

  // search account dialog
  searchAccountOpen() {
    this.searchAccountVisible = true
  }

  searchAccountClose() {
    this.searchAccountVisible = false
  }   

  accountData() {
    this.accountDatas = [
      {
        account_id: '1',
        account_code: '1-01-01-01',
        account_description: "CAJA GENERAL",
        belonging_to: 'Activo',
        account_is: 'Cuenta de Grupo'
      }           
    ]
  }

  accountAddInput($event) {
    this.seatTableDatas.push(
      $event
    )

    this.seatAddForm.patchValue({
      seatTableDatas: $event
    })     

    this.searchAccountClose()    
  }  

}
