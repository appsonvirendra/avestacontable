import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';

import { AccountingRoutingModule } from './accounting-routing.module';
import { AccountCatalogComponent } from './account-catalog/account-catalog.component';
import { AccountCatalogNewComponent } from './account-catalog/account-catalog-new/account-catalog-new.component';
import { AccountCatalogTableComponent } from './account-catalog/account-catalog-table/account-catalog-table.component';
import { SeatListComponent } from './seat/seat-list/seat-list.component';
import { SeatFilterComponent } from './seat/seat-list/seat-filter/seat-filter.component';
import { SeatTableComponent } from './seat/seat-list/seat-table/seat-table.component';
import { NewSeatComponent } from './seat/new-seat/new-seat.component';
import { InfoSeatComponent } from './seat/info-seat/info-seat.component';
import { DiaryBookListComponent } from './diary-book/diary-book-list/diary-book-list.component';
import { DiaryBookFilterComponent } from './diary-book/diary-book-list/diary-book-filter/diary-book-filter.component';
import { DiaryBookTableComponent } from './diary-book/diary-book-list/diary-book-table/diary-book-table.component';
import { LedgerListComponent } from './ledger/ledger-list/ledger-list.component';
import { LedgerFilterComponent } from './ledger/ledger-list/ledger-filter/ledger-filter.component';
import { LedgerTableComponent } from './ledger/ledger-list/ledger-table/ledger-table.component';
import { AccountLinkComponent } from './account-link/account-link.component';
import { AccountLinkFilterComponent } from './account-link/account-link-filter/account-link-filter.component';
import { AccountLinkTableComponent } from './account-link/account-link-table/account-link-table.component';
import { PeriodClosingComponent } from './period-closing/period-closing.component';
import { PeriodClosingTab1Component } from './period-closing/period-closing-tab1/period-closing-tab1.component';
import { PeriodClosingTab2Component } from './period-closing/period-closing-tab2/period-closing-tab2.component';
import { EditSeatComponent } from './seat/edit-seat/edit-seat.component';
import { DebitNotesComponent } from './debit-notes/debit-notes.component';
import { DebitNotesTablesComponent } from './debit-notes/debit-notes-tables/debit-notes-tables.component';
import { EditDebitNotesComponent } from './debit-notes/edit-debit-notes/edit-debit-notes.component';
import { InfoDebitNotesComponent } from './debit-notes/info-debit-notes/info-debit-notes.component';
import { NewDebitNotesComponent } from './debit-notes/new-debit-notes/new-debit-notes.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoriesTableComponent } from './categories/categories-table/categories-table.component';



@NgModule({
  declarations: [AccountCatalogComponent, AccountCatalogNewComponent, AccountCatalogTableComponent, SeatListComponent, SeatFilterComponent, SeatTableComponent, NewSeatComponent, InfoSeatComponent, DiaryBookListComponent, DiaryBookFilterComponent, DiaryBookTableComponent, LedgerListComponent, LedgerFilterComponent, LedgerTableComponent, AccountLinkComponent, AccountLinkFilterComponent, AccountLinkTableComponent, PeriodClosingComponent, PeriodClosingTab1Component, PeriodClosingTab2Component, EditSeatComponent, DebitNotesComponent, DebitNotesTablesComponent, EditDebitNotesComponent, InfoDebitNotesComponent, NewDebitNotesComponent, CategoriesComponent, CategoriesTableComponent],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule    
  ]
})
export class AccountingModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
