import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SuppliersService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getSupplierList(bodyparams) {
    return this.apiService.post(`/supplierList`, bodyparams)
      .pipe(map(data => data.data))
  }  

  postSupplier(bodyparams) {
    return this.apiService.post(`/supplierNew`, bodyparams)
      .pipe(map(data => data))
  } 
  
  getSingleSupplier(bodyparams) {
    return this.apiService.post(`/supplierData`, bodyparams)
      .pipe(map(data => data.data[0]))
  }
  
  putSupplier(supplier_id, bodyparams) {
    return this.apiService.post(`/supplierUpdate/`+supplier_id, bodyparams)
      .pipe(map(data => data))
  }
  
  deleteSupplier(supplier_id) {
    return this.apiService.post(`/supplierDelete/`+supplier_id)
      .pipe(map(data => data))
  }    


}
