import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-supplierinfotab1',
  templateUrl: './supplierinfotab1.component.html',
  styleUrls: ['./supplierinfotab1.component.scss']
})
export class Supplierinfotab1Component implements OnInit {

  @Input() supplierInfoData: any

  constructor() { }

  ngOnInit() {
  }

}
