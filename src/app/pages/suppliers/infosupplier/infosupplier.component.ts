import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-infosupplier',
  templateUrl: './infosupplier.component.html',
  styleUrls: ['./infosupplier.component.scss']
})
export class InfosupplierComponent implements OnInit {

  supplier_id: any

  active_tab: any

  supplierInfoDatas: any

  supplierAccountDatas: any

  supplierFirstLastName: any

  supplierUnPaidBills: any

  loader = true

  btnLoading: any

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private _suppliersService: SuppliersService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  getQueryParams() {
    this.route.queryParams.subscribe(params => {
      this.supplier_id = params.supplier_id
      this.active_tab = params.active_tab
      this.populateGetSingleSupplier()
    })    
  }

  populateGetSingleSupplier() {
    this.loader = true
    let params = {
      supplier_id: this.supplier_id
    }
    this._suppliersService.getSingleSupplier(params).subscribe((response) => {
      this.loader = false      
      this.supplierInfoDatas = response
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleSupplier')
    })     
  }    
  
  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }     

}
