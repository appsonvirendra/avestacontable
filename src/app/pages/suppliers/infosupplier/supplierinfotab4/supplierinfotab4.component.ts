import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-supplierinfotab4',
  templateUrl: './supplierinfotab4.component.html',
  styleUrls: ['./supplierinfotab4.component.scss']
})
export class Supplierinfotab4Component implements OnInit {

  @Input() supplierInfoData: any

  supplierEditform: FormGroup

  @Output("populateGetSingleSupplier") populateGetSingleSupplier: EventEmitter<any> = new EventEmitter()

  btnLoading: boolean = false

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private _suppliersService: SuppliersService,    
    private message: NzMessageService
  ) { 

  }

  ngOnInit() {
    this.formVariablesInit()
  }

  formVariablesInit() {
    this.supplierEditform = this.fb.group({
      supplier_id: [this.supplierInfoData.supplier_id],
      supplier_name: [this.supplierInfoData.supplier_name],
      identification: [this.supplierInfoData.identification],
      phone: [this.supplierInfoData.phone],
      mobile: [this.supplierInfoData.mobile],
      fax: [this.supplierInfoData.fax],
      email: [this.supplierInfoData.email],
      direction: [this.supplierInfoData.direction]
    })      
  }  

  updateSupplier(formData) {
    this.btnLoading = true
    let params = {
      supplier_name: formData.supplier_name, 
      identification: formData.identification, 
      phone: formData.phone,
      mobile: formData.mobile,
      fax: formData.fax,
      email: formData.email,
      direction: formData.direction
    }

    this._suppliersService.putSupplier(formData.supplier_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        this.populateGetSingleSupplier.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateSupplier')
    })
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}
