import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-supplierinfotab2',
  templateUrl: './supplierinfotab2.component.html',
  styleUrls: ['./supplierinfotab2.component.scss']
})
export class Supplierinfotab2Component implements OnInit {

  supplierAccountStatusFilter: FormGroup

  @Input() supplierInfoData: any
  @Input() supplierAccountData: any

  currentDate:any = new Date()  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.supplierAccount()
    this.formVariablesInit()
  }

  formVariablesInit() {
    this.supplierAccountStatusFilter = this.fb.group({
      date_from: [null],
      date_to: [null]
    })      
  }  
  
  supplierAccount() {
    // console.log('this.supplierInfoData', this.supplierInfoData)
    // console.log('this.supplierAccountData', this.supplierAccountData)
  }

  supplierReportFilter() {
    // console.log('this.supplierAccountStatusFilter', this.supplierAccountStatusFilter.value)
  }

  supplierReportPrint() {

  }

  clearFilter() {
    this.supplierAccountStatusFilter = this.fb.group({
      date_from: [null],
      date_to: [null]
    })
  }  

}
