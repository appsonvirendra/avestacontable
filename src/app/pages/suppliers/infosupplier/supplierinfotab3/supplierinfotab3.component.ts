import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-supplierinfotab3',
  templateUrl: './supplierinfotab3.component.html',
  styleUrls: ['./supplierinfotab3.component.scss']
})
export class Supplierinfotab3Component implements OnInit {

  @Input() supplierUnPaidBills: any

  constructor() { }

  ngOnInit() {
  }

}
