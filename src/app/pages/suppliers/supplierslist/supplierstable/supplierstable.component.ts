import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-supplierstable',
  templateUrl: './supplierstable.component.html',
  styleUrls: ['./supplierstable.component.scss']
})
export class SupplierstableComponent implements OnInit {

  @Input() supplierDatas: any

  // search
  search_title = ''
  supplierDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  deleteConfirmVisible = false

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false  

  deleteSupplierId: any

  @Output("populateGetSupplierList") populateGetSupplierList: EventEmitter<any> = new EventEmitter();

  constructor(
    private translate: TranslateService,
    private _suppliersService: SuppliersService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.supplierData()
  }

  supplierData() {
    this.supplierDisplayDatas = [...this.supplierDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { supplier_name: string; phone: string; identification: string; email: string }) => {
      return (
        item.supplier_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.phone.toLowerCase().indexOf(searchLower) !== -1 ||
        item.identification.toLowerCase().indexOf(searchLower) !== -1 ||
        item.email.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.supplierDatas.filter((item: { supplier_name: string; phone: string; identification: string; email: string }) => filterFunc(item))

    this.supplierDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.supplierDisplayDatas = [...this.supplierDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteSupplierId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteSupplier() {         
    this.deleteBtnLoading = true

    this._suppliersService.deleteSupplier(this.deleteSupplierId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get supplier list api
        this.populateGetSupplierList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteSupplier')
      this.deleteConfirmClose()
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
