import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';

import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-supplierslist',
  templateUrl: './supplierslist.component.html',
  styleUrls: ['./supplierslist.component.scss']
})
export class SupplierslistComponent implements OnInit {

  supplierDatas: any

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')  

  branchDatas: any

  constructor(
    private translate: TranslateService,
    private _suppliersService: SuppliersService,
    private message: NzMessageService,
    private _branchService: BranchService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    if(this.companyBranchId) {
      this.populateGetSupplierList()
    } else {
      this.populateGetBranchList()
    }
  }

  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response

      // branch id store in local storage and asign to select box
      if(!!localStorage.getItem('companyBranchId')) {
        this.companyBranchId = localStorage.getItem('companyBranchId')
      } else {
        localStorage.setItem('companyBranchId', this.branchDatas[0].branch_id)
        this.companyBranchId = this.branchDatas[0].branch_id
      }      

      this.populateGetSupplierList()
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })     
  }   

  populateGetSupplierList() {
    this.loader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._suppliersService.getSupplierList(params).subscribe((response) => {
      this.supplierDatas = response
      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSupplierList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }   

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
