import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup } from '@angular/forms';

import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newsupplier',
  templateUrl: './newsupplier.component.html',
  styleUrls: ['./newsupplier.component.scss']
})
export class NewsupplierComponent implements OnInit {

  supplierAddform: FormGroup

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  btnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _suppliersService: SuppliersService,
    private message: NzMessageService,
    private _router: Router    
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.supplierAddform = this.fb.group({
      supplier_name: [null],
      identification: [null],
      phone: [null],
      mobile: [null],
      fax: [null],
      email: [null],
      direction: [null]
    })      
  } 

  insertSupplier() {
    this.btnLoading = true
    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      supplier_name: this.supplierAddform.value.supplier_name, 
      identification: this.supplierAddform.value.identification, 
      phone: this.supplierAddform.value.phone,
      mobile: this.supplierAddform.value.mobile,
      fax: this.supplierAddform.value.fax,
      email: this.supplierAddform.value.email,
      direction: this.supplierAddform.value.direction
    }

    this._suppliersService.postSupplier(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to supplier list page
        this.gotoSupplierList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertSupplier')
    })       
  }

  gotoSupplierList() {
    this._router.navigate(['/user/suppliers/list'], {})
  }  

  clearFilter() {
    this.supplierAddform.reset()  
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}
