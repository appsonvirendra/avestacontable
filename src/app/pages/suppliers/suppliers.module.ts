import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuppliersRoutingModule } from './suppliers-routing.module';
import { SupplierslistComponent } from './supplierslist/supplierslist.component';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SupplierstableComponent } from './supplierslist/supplierstable/supplierstable.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsupplierComponent } from './newsupplier/newsupplier.component';
import { InfosupplierComponent } from './infosupplier/infosupplier.component';
import { Supplierinfotab1Component } from './infosupplier/supplierinfotab1/supplierinfotab1.component';
import { Supplierinfotab2Component } from './infosupplier/supplierinfotab2/supplierinfotab2.component';
import { Supplierinfotab3Component } from './infosupplier/supplierinfotab3/supplierinfotab3.component';
import { Supplierinfotab4Component } from './infosupplier/supplierinfotab4/supplierinfotab4.component';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  declarations: [SupplierslistComponent, SupplierstableComponent, NewsupplierComponent, InfosupplierComponent, Supplierinfotab1Component, Supplierinfotab2Component, Supplierinfotab3Component, Supplierinfotab4Component],
  imports: [
    CommonModule,
    SuppliersRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule        
  ]
})
export class SuppliersModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
