import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierslistComponent } from './supplierslist/supplierslist.component';
import { NewsupplierComponent } from './newsupplier/newsupplier.component';
import { InfosupplierComponent } from './infosupplier/infosupplier.component';


const routes: Routes = [
  {
    path: 'list',
    component: SupplierslistComponent
  },
  {
    path: 'new',
    component: NewsupplierComponent
  },
  {
    path: 'info_supplier',
    component: InfosupplierComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuppliersRoutingModule { }
