import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-pos-shifts',
  templateUrl: './pos-shifts.component.html',
  styleUrls: ['./pos-shifts.component.scss']
})
export class PosShiftsComponent implements OnInit {


  todayDate : Date = new Date();
  
  // openShift dialog form
  openShiftForm: FormGroup  
  openShiftVisible = false

   // closeShift dialog form
  closeShiftForm: FormGroup  
  closeShiftVisible = false

  constructor(
     private fb: FormBuilder,
     private translate: TranslateService
  ) { }

  ngOnInit() {
  this.openShiftFormInit()
  this.closeShiftFormInit()
  this.languageTranslate()
  }
  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  } 
      // open Shift form
  openShiftFormInit() {
    this.openShiftForm = this.fb.group({
      initial_base: [null]
    })      
  }  

  openShiftOpen() {
    this.openShiftVisible = true
  }

  openShiftClose() {
    this.openShiftVisible = false
    this.openShiftForm.reset()
  }      

  openShiftSave() {    
    for (const i in this.openShiftForm.controls) {
      this.openShiftForm.controls[i].markAsDirty()
      this.openShiftForm.controls[i].updateValueAndValidity()
    }          
    if(this.openShiftForm.valid) {
      this.openShiftClose()
    }
  }
     // close Shift form
  closeShiftFormInit() {
    this.closeShiftForm = this.fb.group({
      observations: [null],
      real_value_in_box: [null]
    })      
  }  

  closeShiftOpen() {
    this.closeShiftVisible = true
  }

  closeShiftClose() {
    this.closeShiftVisible = false
  }      

  closeShiftSave() {    
    for (const i in this.closeShiftForm.controls) {
      this.closeShiftForm.controls[i].markAsDirty()
      this.closeShiftForm.controls[i].updateValueAndValidity()
    }          
    if(this.closeShiftForm.valid) {
      this.closeShiftClose()
    }
  }
  shifts = [
       {
      key: '1',
      name: 'Cierre N°',
      date:'Martes,26 de mayo de 2020 04:49 pm'
    },
      {
      key: '2',
      name: 'Cierre N°',
      date:'Viernes,8 de mayo de 2020 04:31 pm'
    },
      {
      key: '3',
      name: 'Cierre N°',
      date:'Viernes,30 de mayo de 2020 12:07 pm'
    },
     {
      key: '4',
      name: 'Cierre N°',
      date:'Viernes,30 de mayo de 2020 12:05 pm'
    },
     {
      key: '5',
      name: 'Cierre N°',
      date:'Miercoles,11 de mayo de 2020 10:39 am'
    },
     {
      key: '6',
      name: 'Cierre N°',
      date:'Miercoles,30 de mayo de 2020 12:07 pm'
    },
    {
      key: '7',
      name: 'Cierre N°',
      date:'Miercoles,30 de mayo de 2020 12:07 pm'
    },
    {
      key: '8',
      name: 'Cierre N°',
      date:'Miercoles,30 de mayo de 2020 12:07 pm'
    }

  ];

}
