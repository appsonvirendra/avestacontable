import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosShiftsComponent } from './pos-shifts.component';

describe('PosShiftsComponent', () => {
  let component: PosShiftsComponent;
  let fixture: ComponentFixture<PosShiftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosShiftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosShiftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
