import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosComponent } from './pos.component';
import { PosInvoicesComponent } from './pos-invoices/pos-invoices.component';
import { PosConfigurationComponent } from './pos-configuration/pos-configuration.component';
import { PosShiftsComponent } from './pos-shifts/pos-shifts.component';


const routes: Routes = [
  {
    path: '',
    component: PosComponent    
  },
  {
    path: 'invoices',
    component: PosInvoicesComponent    
  },
  {
    path: 'configuration',
    component: PosConfigurationComponent    
  },
  {
    path: 'shifts',
    component: PosShiftsComponent    
  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosRoutingModule { }
