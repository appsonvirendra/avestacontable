import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent implements OnInit {

  // modal loading
  isVisible = false;
  isConfirmLoading = false;

  productDatas = []
  currenttab:any= 0
  productAddInvoiceDatas: any = []
  
  productDisplayDatas = []

   nofity_add_remove = 0

    salePriceCollapse = false

  // sale prive dialog
  priceIsv1Visible = false
  priceIsv1Form: FormGroup  
  saleDialogNumber: any

    // add item form
  itemAddForm: FormGroup
  itemAddFormVisible = false

    // supplier dialog
  supplierDialogVisible = false
  supplierForm: FormGroup 

    // category dialog
  categoryDialogVisible = false
  addCategoryForm: FormGroup

  product_search_title = ''
  
  sortName: string | null = null
  sortValue: string | null = null  

  // customer add form
  customerAddForm: FormGroup  
  customerAddVisible = false
  
  // invoice detail form
  invoiceDetailForm: FormGroup  
  invoiceDetailVisible = false

  totalquantityAmount = 0

  // total payment form
  totalPaymentForm: FormGroup  
  totalPaymentVisible = false  

  subtotal = 0
  subtotalDiscount = 0
  isvTax: any = 0

  // Product detail dialog subtotal
  subTotalDialog = 0
  dialogDiscount = 0
  subTotalDialogWithDiscount = 0
  
  // update_price: any
  // update_quantity: any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

    // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
    this.totalquantity()
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 30);
    this.productAddInvoiceDatas=[]
    this.totalquantity()

    // Remove border and quantity from display products
    this.productDisplayDatas.forEach(element => {
      element.isBorder = false
      element.item_quantity_added = 0
    })

    // SubTotal, total zero
    this.subtotal = 0
    this.subtotalDiscount = 0
    this.isvTax = 0

  }
  handleCancel(): void {
    this.isVisible = false;
  }

  ngOnInit() {
    this.languageTranslate()
    this.supplierFormInit()
    this.productList()
    this.itemAddFormInit()
    this.customerAddFormInit()
    this.invoiceDetailFormInit()
    this.totalPaymentFormInit()
    this.addCategoryFormInit()
    this.priceIsv1FormInit()
  }
  newwindow(t){
  this.currenttab=t;
  }
  backfunction(t){
  this.currenttab=0;
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  searchProduct() {
    let productSearchLower = this.product_search_title.toLowerCase()

    const filterFunc = (item: { name: string; code: string }) => {
      return (item.name.toLowerCase().indexOf(productSearchLower) !== -1 ||
      item.code.toLowerCase().indexOf(productSearchLower) !== -1
      )
    }

    const data = this.productDatas.filter((item: { name: string; code: string }) => filterFunc(item))

    this.productDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 

    if(productSearchLower.length == 0) {
      this.productDisplayDatas = [...this.productDatas]
    }
    
  }  

  productList() {
    for(let i = 0; i < 20; i++) {
      this.productDatas.push(
        {
          id: i,
          item_quantity: 1,
          item_quantity_added: 0,
          quantity: 1,
          code: 'c'+i,
          name: 'Item'+ ' ' +i,
          price: 10+i,
          total: 10+i,
          tax: 0.15,
          discount: 0, 
          discountPercent: 0 
        }
      )
    }

    this.productDisplayDatas = [...this.productDatas]
  }
  totalquantity(){
   
   // console.log("data");
   let ttitems  = 0;
   this.productAddInvoiceDatas.forEach(element => {
       // console.log("totalquantityAmount",element);
      // console.log("totalquantityAmount",element.bill_object_item_quantity);

     ttitems  += element.bill_object_item_quantity
    })
    this.totalquantityAmount = ttitems    
  }

  productAddInvoice(data) {
    let getproductData = data  
    
    // Add border and quantity in display products
    if (this.productDisplayDatas.some((item) => item.id == getproductData.id)) {
      let borderAddIndex = this.productDisplayDatas.map(item => item.id).indexOf(getproductData.id)
      
      this.productDisplayDatas[borderAddIndex].isBorder = true          
      this.productDisplayDatas[borderAddIndex].item_quantity_added = this.productDisplayDatas[borderAddIndex].item_quantity_added + this.productDisplayDatas[borderAddIndex].item_quantity       
    }

    if (this.productAddInvoiceDatas.some((item) => item.bill_object_id == getproductData.id)) {
      
      let targetIdx = this.productAddInvoiceDatas.map(item => item.bill_object_id).indexOf(getproductData.id)

      this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity = this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity + getproductData.item_quantity


      this.productAddInvoiceDatas[targetIdx].bill_object_total = this.productAddInvoiceDatas[targetIdx].bill_object_total + getproductData.price

      let taxAdd = getproductData.price * getproductData.tax

      this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax = this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax + taxAdd    

    } else {      

      let taxAdd = getproductData.price * getproductData.tax

      let billObject = {
        bill_object_id: getproductData.id,
        bill_object_item_quantity: getproductData.item_quantity,
        bill_object_quantity: getproductData.quantity,
        bill_object_code: getproductData.code,
        bill_object_name: getproductData.name,
        bill_object_price: getproductData.price,
        bill_object_total: getproductData.total,
        bill_object_cal_tax: taxAdd,
        bill_object_tax: getproductData.tax,
        bill_object_discount: getproductData.discount,
        bill_object_discount_percent: getproductData.discountPercent
      }
      this.productAddInvoiceDatas.push(billObject)
    }

    this.subtotalCalc()
    this.taxCalc()
    this.totalquantity()
  }

  addQuantity(billItem) {
    let getproductItem = billItem  

    // Add quantity in display products
    if (this.productDisplayDatas.some((item) => item.id == billItem.bill_object_id)) {
      let borderAddIndex = this.productDisplayDatas.map(item => item.id).indexOf(billItem.bill_object_id)
            
      this.productDisplayDatas[borderAddIndex].item_quantity_added = this.productDisplayDatas[borderAddIndex].item_quantity_added + this.productDisplayDatas[borderAddIndex].item_quantity       
    }    

    let targetIdx = this.productAddInvoiceDatas.map(item => item.bill_object_id).indexOf(getproductItem.bill_object_id)

    this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity = this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity + 1


    this.productAddInvoiceDatas[targetIdx].bill_object_total = this.productAddInvoiceDatas[targetIdx].bill_object_total + getproductItem.bill_object_price

    let taxAdd = getproductItem.bill_object_price * getproductItem.bill_object_tax

    this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax = this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax + taxAdd    

    // Add discount to the dialog
    this.dialogDiscount = ((this.invoiceDetailForm.value.update_price * this.invoiceDetailForm.value.update_quantity) * this.invoiceDetailForm.value.update_discount_percent) / 100

    // Add discount in the added product
    this.productAddInvoiceDatas[targetIdx].bill_object_discount = ((this.productAddInvoiceDatas[targetIdx].bill_object_price * this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity) * this.productAddInvoiceDatas[targetIdx].bill_object_discount_percent) / 100    

    this.subtotalCalc()
    this.taxCalc()
    this.totalquantity()    
  }   
  
  removeQuantity(billItem) {
    let getproductItem = billItem  

    if(getproductItem.bill_object_item_quantity > 1) {

      // Remove quantity in display products
      if (this.productDisplayDatas.some((item) => item.id == billItem.bill_object_id)) {
        let borderAddIndex = this.productDisplayDatas.map(item => item.id).indexOf(billItem.bill_object_id)
              
        this.productDisplayDatas[borderAddIndex].item_quantity_added = this.productDisplayDatas[borderAddIndex].item_quantity_added - this.productDisplayDatas[borderAddIndex].item_quantity       
      }         

      let targetIdx = this.productAddInvoiceDatas.map(item => item.bill_object_id).indexOf(getproductItem.bill_object_id)
  
      this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity = this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity - 1
  
  
      this.productAddInvoiceDatas[targetIdx].bill_object_total = this.productAddInvoiceDatas[targetIdx].bill_object_total - getproductItem.bill_object_price
  
      let taxAdd = getproductItem.bill_object_price * getproductItem.bill_object_tax
  
      this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax = this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax - taxAdd    

      // Remove discount in the added product
      this.productAddInvoiceDatas[targetIdx].bill_object_discount = ((this.productAddInvoiceDatas[targetIdx].bill_object_price * this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity) * this.productAddInvoiceDatas[targetIdx].bill_object_discount_percent) / 100          
  
      this.subtotalCalc()
      this.taxCalc()
      this.totalquantity()    
    }
  }   

  subtotalCalc() {
    let subtotalSum = 0
    let subtotalDiscountSum = 0

    this.productAddInvoiceDatas.forEach(element => {
      subtotalSum += element.bill_object_total
      subtotalDiscountSum += element.bill_object_discount
    })

    this.subtotal = subtotalSum    
    this.subtotalDiscount = subtotalDiscountSum    
  }

  taxCalc() {
    let taxSum = 0

    this.productAddInvoiceDatas.forEach(element => {
      taxSum += element.bill_object_cal_tax
    })

    this.isvTax = taxSum        
  }

  productRemoveInvoice(index, objectId) {
    
    // Remove border and quantity from display products
    if (this.productDisplayDatas.some((item) => item.id == objectId)) {
      let borderRemoveIndex = this.productDisplayDatas.map(item => item.id).indexOf(objectId)
      this.productDisplayDatas[borderRemoveIndex].isBorder = false       
      this.productDisplayDatas[borderRemoveIndex].item_quantity_added = 0
    }    

    this.productAddInvoiceDatas.splice(index, 1)
    this.subtotalCalc()
    this.taxCalc()
    this.totalquantity()
    
  }

  showSalePriceCollapse() {
    this.salePriceCollapse = !this.salePriceCollapse
  }
  
    // edit item form
  itemAddFormInit() {
    this.itemAddForm = this.fb.group({
      item_type: ['0'],
      item_name: [null, [Validators.required]],
      item_code: [null, [Validators.required]],
      item_category: [null, [Validators.required]],
      item_provider_id: [null, [Validators.required]],
      item_cost_price: [{value: '0', disabled: true}],
      item_image: [null],
      item_description: [null],
      item_currency: ['1'],
      item_discount: ['0'],
      item_sale_price1: [null, [Validators.required]],
      item_sale_price2: [null],
      item_sale_price3: [null],
      item_sales_tax1: ['1'],
      item_sales_tax2: ['1'],
      item_tax_when_buy1: ['1'],
      item_tax_when_buy2: ['1'],
      item_wineries: [null],
      item_current_existence: [{value: '0', disabled: true}],
      item_minimum_existency: [null, [Validators.required]],
      item_low_mail: [false],
      item_notify_in_stock: ['0']
    })
  }

  itemEditFormOpen() {
    this.itemAddFormVisible = true
  }

  itemAddFormClose() {
    this.itemAddFormVisible = false
  }    

  itemAddSave() {
    for (const i in this.itemAddForm.controls) {
      this.itemAddForm.controls[i].markAsDirty()
      this.itemAddForm.controls[i].updateValueAndValidity()
    }
  }

    // category dialog
  addCategoryFormInit() {
    this.addCategoryForm = this.fb.group({
      category_name: [null, [Validators.required]],
      category_description: [null],
    })   
  }
  
  openCategoryDialog() {
    this.categoryDialogVisible = true
  }

  closeCategoryDialog() {
    this.categoryDialogVisible = false
  }

  saveCategory() {
    for (const i in this.addCategoryForm.controls) {
      this.addCategoryForm.controls[i].markAsDirty()
      this.addCategoryForm.controls[i].updateValueAndValidity()
    }  
    if(this.addCategoryForm.valid)  {
      this.categoryDialogVisible = false
      this.addCategoryFormInit()
    }    
  }

    // sales and price dialog
  priceIsv1open(saleDialogNumber) {
    this.saleDialogNumber = saleDialogNumber
    this.priceIsv1Visible = true
  }

  priceIsv1close() {
    this.priceIsv1Visible = false
  }    

  priceIsv1FormInit() {
    this.priceIsv1Form = this.fb.group({
      final_price: [null],
      tax_to_calculate: ['1'],
    })  
  }
  
  priceIsv1calculate() {
    // console.log('this.priceIsv1Form.value --', this.priceIsv1Form.value)

    /*if(this.saleDialogNumber == '1') {
      this.itemEditForm.patchValue({
        item_sale_price1: '666'
      })
    }

    if(this.saleDialogNumber == '2') {
      this.itemEditForm.patchValue({
        item_sale_price2: '667'
      })
    }    

    if(this.saleDialogNumber == '3') {
      this.itemEditForm.patchValue({
        item_sale_price3: '668'
      })
    }*/

    this.priceIsv1FormInit()
    this.priceIsv1Visible = false    

  }

    // supplier form
  supplierFormInit() {
    this.supplierForm = this.fb.group({
      supplier_name: [null, [Validators.required]],
      supplier_phone: [null],
      supplier_mobile: [null],
      supplier_fax: [null],
      supplier_email: [null],
      supplier_direction: [null]
    })   
  }

  supplierDialogOpen() {
    this.supplierDialogVisible = true
  }

  supplierDialogClose() {
    this.supplierDialogVisible = false
  }

  supplierAdd() {
    for (const i in this.supplierForm.controls) {
      this.supplierForm.controls[i].markAsDirty()
      this.supplierForm.controls[i].updateValueAndValidity()
    }  
    if(this.supplierForm.valid)  {
      this.supplierDialogVisible = false
      this.supplierFormInit()
    }    
  }

    notify_add_remove(zero_one) {    
    if(zero_one) {
      this.nofity_add_remove += 1
    } else {
      if(this.itemAddForm.value.item_notify_in_stock > 0) {
        this.nofity_add_remove -= 1
      } 
    }
    
    this.itemAddForm.patchValue({
      item_notify_in_stock: this.nofity_add_remove
    })
        
  }

  // customer add dialog form
  customerAddFormInit() {
    this.customerAddForm = this.fb.group({
     customer_type: ['1'],
      customer_tax_exempt: ['no'],
      customer_assign_seller: [null],
      customer_identification_card: [null],
      customer_firstname: [null, [Validators.required]],
      customer_lastname: [null, [Validators.required]],
      customer_birthdate: [null],
      customer_sex: [null],
      customer_company: [null, [Validators.required]],
      customer_identification: [null],
      customer_identification1: [null],
      customer_direction: [null],
      customer_country: [null],
      customer_city: [null],
      customer_phone: [null],
      customer_mobile: [null],
      customer_fax: [null],
      customer_postal_code: [null],
      customer_email: [null],
      customer_name1: [null],
      customer_lastname1: [null],
      customer_mobile1: [null],
      customer_email1: [null],
      customer_name2: [null],
      customer_lastname2: [null],
      customer_mobile2: [null],
      customer_email2: [null],
      customer_name3: [null],
      customer_lastname3: [null],
      customer_mobile3: [null],
      customer_email3: [null]
    })      
  }  

  customerAddOpen() {
    this.customerAddVisible = true
  }

  customerAddClose() {
    this.customerAddForm.reset()
    this.customerAddForm.patchValue({
      customer_type: ['1']
    })    
    this.customerAddVisible = false
  }      

  customerAddSave() {    
    for (const i in this.customerAddForm.controls) {
      this.customerAddForm.controls[i].markAsDirty()
      this.customerAddForm.controls[i].updateValueAndValidity()
    }          
    if(this.customerAddForm.valid) {
      this.customerAddClose()
    }
  }  

  // invoice detail dialog form
  invoiceDetailFormInit() {
    this.invoiceDetailForm = this.fb.group({
      update_id: [null],
      update_code: [null],
      update_name: [null],
      update_price: [null, [Validators.required]],
      update_quantity: [null, [Validators.required]],
      update_tax: ['1'],
      update_total: [null],
      description: [null],
      update_discount: ['0'],
      update_discount_percent: ['0']
    })      
  }  

  invoiceDetailOpen(invoiceDetailData) {
    this.invoiceDetailVisible = true
    this.invoiceDetailForm.patchValue({
      update_id: invoiceDetailData.bill_object_id,
      update_code: invoiceDetailData.bill_object_code,
      update_name: invoiceDetailData.bill_object_name,
      update_price: invoiceDetailData.bill_object_price,
      update_quantity: invoiceDetailData.bill_object_item_quantity,
      update_tax: invoiceDetailData.bill_object_cal_tax,
      update_discount: invoiceDetailData.bill_object_discount,
      update_discount_percent: invoiceDetailData.bill_object_discount_percent,
    })    

    // Update total and discount of dialog 
    this.dialogSubTotalUpdate()
  }


  invoiceDetailClose() {
    this.invoiceDetailForm.patchValue({
      discount: ['0'],
    })    
    this.invoiceDetailVisible = false
  }      

  invoiceDetailUpdate() {    
    for (const i in this.invoiceDetailForm.controls) {
      this.invoiceDetailForm.controls[i].markAsDirty()
      this.invoiceDetailForm.controls[i].updateValueAndValidity()
    }          
    if(this.invoiceDetailForm.valid) {

      if(this.invoiceDetailForm.value.update_price > 0 && this.invoiceDetailForm.value.update_quantity > 0) {
        // invoice price and quantity update
        this.updateInvoiceList()
  
        // product price update
        this.updateProductPrice()
  
        this.invoiceDetailClose()
      }

    }
  }
  
  // Dialog sub total update
  dialogSubTotalUpdate() {
    if(this.invoiceDetailForm.value.update_price) {
      this.subTotalDialog = this.invoiceDetailForm.value.update_price * this.invoiceDetailForm.value.update_quantity
    }
    this.dialogDiscountTotalUpdate()
  }

  // Dialog discount total update
  dialogDiscountTotalUpdate() {
    if(this.invoiceDetailForm.value.update_price) {
      this.dialogDiscount = ((this.invoiceDetailForm.value.update_price * this.invoiceDetailForm.value.update_quantity) * this.invoiceDetailForm.value.update_discount_percent) / 100
    }
  }  

  updateInvoiceList() {
    let invoiceUpdateValue = this.invoiceDetailForm.value

    let targetIdx = this.productAddInvoiceDatas.map(item => item.bill_object_id).indexOf(invoiceUpdateValue.update_id)

    this.productAddInvoiceDatas[targetIdx].bill_object_item_quantity = invoiceUpdateValue.update_quantity

    this.productAddInvoiceDatas[targetIdx].bill_object_price = invoiceUpdateValue.update_price

    this.productAddInvoiceDatas[targetIdx].bill_object_total = invoiceUpdateValue.update_quantity * invoiceUpdateValue.update_price

    let taxAdd = this.productAddInvoiceDatas[targetIdx].bill_object_total * 0.15

    this.productAddInvoiceDatas[targetIdx].bill_object_cal_tax = taxAdd

    // Add discount
    this.productAddInvoiceDatas[targetIdx].bill_object_discount = this.dialogDiscount

    // Add discount percent
    this.productAddInvoiceDatas[targetIdx].bill_object_discount_percent = invoiceUpdateValue.update_discount_percent    

    this.subtotalCalc()
    this.taxCalc()
  }

  updateProductPrice() {

    let invoiceUpdateValue = this.invoiceDetailForm.value

    let targetIdx = this.productDatas.map(item => item.id).indexOf(invoiceUpdateValue.update_id)

    if(targetIdx != -1) {
      this.productDatas[targetIdx].price = invoiceUpdateValue.update_price
      this.productDatas[targetIdx].total = invoiceUpdateValue.update_price
    }

  }

  // total payment dialog form
  totalPaymentFormInit() {
    this.totalPaymentForm = this.fb.group({
      cash: [null],
      notes:[null],
      debit_card:[null],
      credit_card:[null],
      transference:[null],
      card:[null]
    })      
  }  

  totalPaymentOpen() {
    this.totalPaymentVisible = true
  }

  totalPaymentClose() {
    this.totalPaymentForm.reset()
    this.totalPaymentVisible = false
    this.currenttab=0;
  }      

  totalPaymentSave() {    
    for (const i in this.totalPaymentForm.controls) {
      this.totalPaymentForm.controls[i].markAsDirty()
      this.totalPaymentForm.controls[i].updateValueAndValidity()
    }          
    if(this.totalPaymentForm.valid) {
      this.totalPaymentClose()
    }
  }   

}