import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// basic load start
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
// basic load end

import { PosRoutingModule } from './pos-routing.module';
import { PosComponent } from './pos.component';
import { PosInvoicesComponent } from './pos-invoices/pos-invoices.component';
import { PosConfigurationComponent } from './pos-configuration/pos-configuration.component';
import { PosShiftsComponent } from './pos-shifts/pos-shifts.component';


@NgModule({
  declarations: [PosComponent, PosInvoicesComponent, PosConfigurationComponent, PosShiftsComponent],
  imports: [
    CommonModule,
    PosRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class PosModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
