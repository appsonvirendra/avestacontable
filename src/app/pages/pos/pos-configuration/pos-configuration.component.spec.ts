import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosConfigurationComponent } from './pos-configuration.component';

describe('PosConfigurationComponent', () => {
  let component: PosConfigurationComponent;
  let fixture: ComponentFixture<PosConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
