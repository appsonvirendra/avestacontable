import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-pos-configuration',
  templateUrl: './pos-configuration.component.html',
  styleUrls: ['./pos-configuration.component.scss']
})
export class PosConfigurationComponent implements OnInit {

  configurationform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.configurationFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  configurationFormInit() {
    this.configurationform = this.fb.group({
      branch: ['1'],
      cellar: ['1']
    })    
  }

  configurationSave() {
    for (const i in this.configurationform.controls) {
      this.configurationform.controls[i].markAsDirty()
      this.configurationform.controls[i].updateValueAndValidity()
    }
    if(this.configurationform.valid) {
      // console.log('this.configurationform.value', this.configurationform.value)
    }    
  }

}
