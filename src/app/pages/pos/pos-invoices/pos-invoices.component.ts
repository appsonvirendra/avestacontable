import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-pos-invoices',
  templateUrl: './pos-invoices.component.html',
  styleUrls: ['./pos-invoices.component.scss']
})
export class PosInvoicesComponent implements OnInit {

  invoicesDatas: any = []

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.invoicesData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  invoicesData() {
    this.invoicesDatas = [
      // {
      //   code: '1',
      //   customer_id: '1',
      //   customer_name: 'John',
      //   date: '2020-01-20',
      //   total: '0'
      // }      
    ]
  }

}
