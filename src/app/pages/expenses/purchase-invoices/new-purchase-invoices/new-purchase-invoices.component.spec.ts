import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPurchaseInvoicesComponent } from './new-purchase-invoices.component';

describe('NewPurchaseInvoicesComponent', () => {
  let component: NewPurchaseInvoicesComponent;
  let fixture: ComponentFixture<NewPurchaseInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPurchaseInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPurchaseInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
