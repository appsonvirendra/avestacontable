import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface ItemnewpurchaseData {
  id: string;
  item: string;
  reference: string;
  quantity: string;
  price: string;
  total: string;
}

@Component({
  selector: 'app-new-purchase-invoices',
  templateUrl: './new-purchase-invoices.component.html',
  styleUrls: ['./new-purchase-invoices.component.scss']
})
export class NewPurchaseInvoicesComponent implements OnInit {

  // item Add Form
  itemAddForm: FormGroup

  itemAddFormVisible = false 

  // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = []  

  // category dialog
  categoryDialogVisible = false
  addCategoryForm: FormGroup  

  salePriceCollapse = false

  // sale prive dialog
  priceIsv1Visible = false
  priceIsv1Form: FormGroup  
  saleDialogNumber: any

  // supplier dialog
  supplierDialogVisible = false
  supplierForm: FormGroup

  nofity_add_remove = 0

// item view visible
  itemViewVisible = false 

 // search seller dialog
  searchSellerVisible = false
  sellerDatas = []

  myBlog: string = "";

  newpurchaseinvoices: FormGroup
  i = 0;
  currentDate:any = new Date()


  itemSelectSales: any = []
  
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.newpurchaseinvoicesFormInit()
    this.addnewpurchaseinvoicesRow()
    this.sellerData()
    this.itemAddFormInit()
    this.addCategoryFormInit()
    this.priceIsv1FormInit()
    this.supplierFormInit()
    this.supplierData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  newpurchaseinvoicesFormInit() {
    this.newpurchaseinvoices = this.fb.group({
      seller_id: [null],
      seller_name: [null],
      supplier_id: [null],
      supplier_name: [null],
      creation_date: [null],
      expiration_date: [null],
      currency: ['0'],
      discount: ['0'],
      cellar: ['3'],
      item: [null]

    })      
  }

  
  savePurchase() {
    for (const i in this.newpurchaseinvoices.controls) {
      this.newpurchaseinvoices.controls[i].markAsDirty()
      this.newpurchaseinvoices.controls[i].updateValueAndValidity()
    }
    if(this.newpurchaseinvoices.valid) {
      console.log('this.newpurchaseinvoices.value', this.newpurchaseinvoices.value)
    }
  }
    
// add line Begin
   
  listOfnewpurchaseinvoiceData: ItemnewpurchaseData[] = [];


  addnewpurchaseinvoicesRow(): void {
    this.listOfnewpurchaseinvoiceData = [
      ...this.listOfnewpurchaseinvoiceData,
      {
        id: `${this.i+1}`,
        item: 'Item 1',
        reference: ``,
        quantity: '1',
        price: '0.00',
        total: '0.00'
      }
    ];
    this.i++;
  }

  deletenewpurchaseinvoicesRow(index): void {
    this.listOfnewpurchaseinvoiceData.splice(index, 1)
  } 
// add line End


  // search seller dialog
  searchSellerOpen() {
    this.searchSellerVisible = true
  }

  searchSellerClose() {
    this.searchSellerVisible = false
  }   

  sellerData() {
    this.sellerDatas = [
      {
        seller_id: '1',
        seller_name: 'John Brown 1',
        seller_phone: "",
        seller_mobile: "",
        seller_email: "",
        seller_type: "1"
      },
      {
        seller_id: '2',
        seller_name: 'John Brown 2',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '3',
        seller_name: 'John Brown 3',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '4',
        seller_name: 'John Brown 4',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      }               
    ]
  }

  sellerAddInput($event) {
    this.newpurchaseinvoices.patchValue({
      seller_id: $event.seller_id,
      seller_name: $event.seller_name
    })

    this.searchSellerClose()    
  }

  // item Add Form
  itemAddFormInit() {
    this.itemAddForm = this.fb.group({
      item_type: ['0'],
      item_name: [null, [Validators.required]],
      item_code: [null, [Validators.required]],
      item_category: [null, [Validators.required]],
      item_provider_id: [null, [Validators.required]],
      item_cost_price: [{value: '0', disabled: true}],
      item_image: [null],
      item_description: [null],
      item_currency: ['1'],
      item_discount: ['0'],
      item_sale_price1: [null, [Validators.required]],
      item_sale_price2: [null],
      item_sale_price3: [null],
      item_sales_tax1: ['1'],
      item_sales_tax2: ['1'],
      item_tax_when_buy1: ['1'],
      item_tax_when_buy2: ['1'],
      item_wineries: [null],
      item_current_existence: [{value: '0', disabled: true}],
      item_minimum_existency: [null, [Validators.required]],
      item_low_mail: [false],
      item_notify_in_stock: ['0']
    })
  }

  itemAddFormOpen() {
    this.itemAddFormVisible = true
  }

  itemAddFormClose() {
    this.itemAddFormVisible = false
  }    

  itemAddSave() {
    for (const i in this.itemAddForm.controls) {
      this.itemAddForm.controls[i].markAsDirty()
      this.itemAddForm.controls[i].updateValueAndValidity()
    }
    // console.log('this.itemAddForm.value', this.itemAddForm.value)
  }

  // category dialog
  addCategoryFormInit() {
    this.addCategoryForm = this.fb.group({
      category_name: [null, [Validators.required]],
      category_description: [null],
    })   
  }
  
  openCategoryDialog() {
    this.categoryDialogVisible = true
  }

  closeCategoryDialog() {
    this.categoryDialogVisible = false
  }

  saveCategory() {
    for (const i in this.addCategoryForm.controls) {
      this.addCategoryForm.controls[i].markAsDirty()
      this.addCategoryForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addCategoryForm.value --', this.addCategoryForm.value)
    if(this.addCategoryForm.valid)  {
      this.categoryDialogVisible = false
      this.addCategoryFormInit()
    }    
  }
  
  showSalePriceCollapse() {
    this.salePriceCollapse = !this.salePriceCollapse
  }
  
  // sales and price dialog
  priceIsv1open(saleDialogNumber) {
    this.saleDialogNumber = saleDialogNumber
    this.priceIsv1Visible = true
  }

  priceIsv1close() {
    this.priceIsv1Visible = false
  }    

  priceIsv1FormInit() {
    this.priceIsv1Form = this.fb.group({
      final_price: [null],
      tax_to_calculate: ['1'],
    })  
  }
  
  priceIsv1calculate() {
    // console.log('this.priceIsv1Form.value --', this.priceIsv1Form.value)

    /*if(this.saleDialogNumber == '1') {
      this.itemAddForm.patchValue({
        item_sale_price1: '666'
      })
    }

    if(this.saleDialogNumber == '2') {
      this.itemAddForm.patchValue({
        item_sale_price2: '667'
      })
    }    

    if(this.saleDialogNumber == '3') {
      this.itemAddForm.patchValue({
        item_sale_price3: '668'
      })
    }*/

    this.priceIsv1FormInit()
    this.priceIsv1Visible = false    

  }
  
  // supplier form
  supplierFormInit() {
    this.supplierForm = this.fb.group({
      supplier_name: [null, [Validators.required]],
      supplier_phone: [null],
      supplier_mobile: [null],
      supplier_fax: [null],
      supplier_email: [null],
      supplier_direction: [null]
    })   
  }

  supplierDialogOpen() {
    this.supplierDialogVisible = true
  }

  supplierDialogClose() {
    this.supplierDialogVisible = false
  }

  supplierAdd() {
    for (const i in this.supplierForm.controls) {
      this.supplierForm.controls[i].markAsDirty()
      this.supplierForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.supplierForm.value --', this.supplierForm.value)
    if(this.supplierForm.valid)  {
      this.supplierDialogVisible = false
      this.supplierFormInit()
    }    
  }

  notify_add_remove(zero_one) {    
    if(zero_one) {
      this.nofity_add_remove += 1
    } else {
      if(this.itemAddForm.value.item_notify_in_stock > 0) {
        this.nofity_add_remove -= 1
      } 
    }
    
    this.itemAddForm.patchValue({
      item_notify_in_stock: this.nofity_add_remove
    })
        
  }
  itemViewOpen() {
    this.itemViewVisible = true
  }

  itemViewClose() {
    this.itemViewVisible = false
  }
    // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.newpurchaseinvoices.patchValue({
      supplier_id: $event.supplier_id,
      supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  }

}

