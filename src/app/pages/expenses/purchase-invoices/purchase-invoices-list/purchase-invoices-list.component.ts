import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-purchase-invoices-list',
  templateUrl: './purchase-invoices-list.component.html',
  styleUrls: ['./purchase-invoices-list.component.scss']
})
export class PurchaseInvoicesListComponent implements OnInit {

  purchaseinvoicesDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.paymentsmadeData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.purchaseinvoicesDatas = []
  }
  
  paymentsmadeData() {
    this.purchaseinvoicesDatas = [
      {
        purchase_invoices_code: '',
        purchase_invoices_supplier: '',
        purchase_invoices_balance: '',
        purchase_invoices_expiration_date: '',
        purchase_invoices_total: '',
        purchase_invoices_date: '',
        purchase_invoices_state: ''
      }                      
    ]
  }

}
