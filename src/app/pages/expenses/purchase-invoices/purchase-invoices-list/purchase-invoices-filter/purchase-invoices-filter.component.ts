import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-purchase-invoices-filter',
  templateUrl: './purchase-invoices-filter.component.html',
  styleUrls: ['./purchase-invoices-filter.component.scss']
})
export class PurchaseInvoicesFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      state: ['0'],
      branch: ['0'],
      supplier: ['0'],
      user: [null],
      code: [null]
    })  
  }

  searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      state: ['0'],
      branch: ['0'],
      supplier: ['0'],
      user: [null],
      code: [null]
    }) 
  }

}
