import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseInvoicesFilterComponent } from './purchase-invoices-filter.component';

describe('PurchaseInvoicesFilterComponent', () => {
  let component: PurchaseInvoicesFilterComponent;
  let fixture: ComponentFixture<PurchaseInvoicesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseInvoicesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseInvoicesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
