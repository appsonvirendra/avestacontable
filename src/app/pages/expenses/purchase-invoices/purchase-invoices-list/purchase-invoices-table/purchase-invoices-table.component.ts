import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-purchase-invoices-table',
  templateUrl: './purchase-invoices-table.component.html',
  styleUrls: ['./purchase-invoices-table.component.scss']
})
export class PurchaseInvoicesTableComponent implements OnInit {

  @Input() purchaseinvoicesDatas: any

  constructor() { }
  ngOnInit() {
  }

}
