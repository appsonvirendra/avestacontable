import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseInvoicesTableComponent } from './purchase-invoices-table.component';

describe('PurchaseInvoicesTableComponent', () => {
  let component: PurchaseInvoicesTableComponent;
  let fixture: ComponentFixture<PurchaseInvoicesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseInvoicesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseInvoicesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
