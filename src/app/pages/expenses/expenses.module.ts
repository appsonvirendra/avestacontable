import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// basic load start
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
// basic load end

import { ExpensesRoutingModule } from './expenses-routing.module';
import { PaymentsMadeListComponent } from './payments-made/payments-made-list/payments-made-list.component';
import { PaymentsMadeFilterComponent } from './payments-made/payments-made-list/payments-made-filter/payments-made-filter.component';
import { PaymentsMadeTableComponent } from './payments-made/payments-made-list/payments-made-table/payments-made-table.component';
import { NewPaymentsMadeComponent } from './payments-made/new-payments-made/new-payments-made.component';
import { PurchaseInvoicesListComponent } from './purchase-invoices/purchase-invoices-list/purchase-invoices-list.component';
import { PurchaseInvoicesFilterComponent } from './purchase-invoices/purchase-invoices-list/purchase-invoices-filter/purchase-invoices-filter.component';
import { PurchaseInvoicesTableComponent } from './purchase-invoices/purchase-invoices-list/purchase-invoices-table/purchase-invoices-table.component';
import { NewPurchaseInvoicesComponent } from './purchase-invoices/new-purchase-invoices/new-purchase-invoices.component';



@NgModule({
  declarations: [PaymentsMadeListComponent, PaymentsMadeFilterComponent, PaymentsMadeTableComponent, NewPaymentsMadeComponent, PurchaseInvoicesListComponent, PurchaseInvoicesFilterComponent, PurchaseInvoicesTableComponent, NewPurchaseInvoicesComponent],
  imports: [
    CommonModule,
    ExpensesRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule 
  ]
})
export class ExpensesModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}