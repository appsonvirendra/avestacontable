import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentsMadeListComponent } from './payments-made/payments-made-list/payments-made-list.component';
import { NewPaymentsMadeComponent } from './payments-made/new-payments-made/new-payments-made.component';
import { PurchaseInvoicesListComponent } from './purchase-invoices/purchase-invoices-list/purchase-invoices-list.component';
import { NewPurchaseInvoicesComponent } from './purchase-invoices/new-purchase-invoices/new-purchase-invoices.component';


const routes: Routes = [
 {
    path: 'payments-made/payment',
    component: PaymentsMadeListComponent    
  },
  {
    path: 'payments-made/payment/create',
    component: NewPaymentsMadeComponent    
  },
  {
    path: 'purchase-invoices/bill',
    component: PurchaseInvoicesListComponent    
  },
  {
    path: 'purchase-invoices/bill/create',
    component: NewPurchaseInvoicesComponent    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpensesRoutingModule { }
