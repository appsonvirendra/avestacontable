import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payments-made-filter',
  templateUrl: './payments-made-filter.component.html',
  styleUrls: ['./payments-made-filter.component.scss']
})
export class PaymentsMadeFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      state: ['0'],
      branch: ['0'],
      supplier: ['0'],
      invoice_code: [null],
      code: [null]
    })  
  }

  searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      state: ['0'],
      branch: ['0'],
      supplier: ['0'],
      invoice_code: [null],
      code: [null]
    }) 
  }

}