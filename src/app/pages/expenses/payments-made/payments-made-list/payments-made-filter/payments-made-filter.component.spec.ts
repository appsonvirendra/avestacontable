import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsMadeFilterComponent } from './payments-made-filter.component';

describe('PaymentsMadeFilterComponent', () => {
  let component: PaymentsMadeFilterComponent;
  let fixture: ComponentFixture<PaymentsMadeFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsMadeFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsMadeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
