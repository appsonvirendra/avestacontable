import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsMadeTableComponent } from './payments-made-table.component';

describe('PaymentsMadeTableComponent', () => {
  let component: PaymentsMadeTableComponent;
  let fixture: ComponentFixture<PaymentsMadeTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsMadeTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsMadeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
