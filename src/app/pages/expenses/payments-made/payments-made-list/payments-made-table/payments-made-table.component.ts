import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payments-made-table',
  templateUrl: './payments-made-table.component.html',
  styleUrls: ['./payments-made-table.component.scss']
})
export class PaymentsMadeTableComponent implements OnInit {

  @Input() paymentsmadeDatas: any

  constructor() { }
  ngOnInit() {
  }

}
