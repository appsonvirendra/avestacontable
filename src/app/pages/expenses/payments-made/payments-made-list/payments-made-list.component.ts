import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-payments-made-list',
  templateUrl: './payments-made-list.component.html',
  styleUrls: ['./payments-made-list.component.scss']
})
export class PaymentsMadeListComponent implements OnInit {

  paymentsmadeDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.paymentsmadeData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.paymentsmadeDatas = []
  }
  
  paymentsmadeData() {
    this.paymentsmadeDatas = [
      {
        payment_made_code: '',
        payment_made_supplier: '',
        payment_made_document: '',
        payment_made_bank: '',
        payment_made_total: '',
        payment_made_date: '',
        payment_made_state: ''
      }                      
    ]
  }

}
