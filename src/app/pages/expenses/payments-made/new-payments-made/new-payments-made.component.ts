import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Person {
  key: string;
  invoice_no: string;
  total: string;
  paid_out: string;
  balance: string;
  with_holdings: string;
  amount_to_be_paid: string;
}

interface ItemnewcreditData {
  id: string;
  item: string;
  value: string;
  tax: string;
  quantity: string;
  total: string;
}

@Component({
  selector: 'app-new-payments-made',
  templateUrl: './new-payments-made.component.html',
  styleUrls: ['./new-payments-made.component.scss']
})
export class NewPaymentsMadeComponent implements OnInit {

 
 type: string = "";
 typeuser: string = "no";

 categoryDialogVisible = false
 addCategoryForm: FormGroup

 // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = [] 

  myBlog: string = "";

  newpaymentmade: FormGroup
  i = 0;
  currentDate:any = new Date()


  itemSelectSales: any = []
  
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.newpaymentmadeFormInit()
    this.addnewpaymentmadeRow()
    this.supplierData()
    this.addCategoryFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  newpaymentmadeFormInit() {
    this.newpaymentmade = this.fb.group({
      reference_no: [null],
      supplier_id: [null],
      supplier_name: [null],
      creation_date: [null],
      branch: ['1'],
      payment_method: ['1'],
      currency: ['0'],
      observations: [null],
      internal_note: [null]

    })      
  }

  
  savePayments() {
    for (const i in this.newpaymentmade.controls) {
      this.newpaymentmade.controls[i].markAsDirty()
      this.newpaymentmade.controls[i].updateValueAndValidity()
    }
    if(this.newpaymentmade.valid) {
      console.log('this.newpaymentmade.value', this.newpaymentmade.value)
    }
  }
    
// add line Begin
   
  listOfnewpaymentsmadeData: ItemnewcreditData[] = [];


  addnewpaymentmadeRow(): void {
    this.listOfnewpaymentsmadeData = [
      ...this.listOfnewpaymentsmadeData,
      {
        id: `${this.i+1}`,
        item: ``,
        value: '',
        tax: '',
        quantity: '',
        total: ''
      }
    ];
    this.i++;
  }

  deletenewpaymentmadeRow(index): void {
    this.listOfnewpaymentsmadeData.splice(index, 1)
  } 
// add line End


  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.newpaymentmade.patchValue({
      supplier_id: $event.supplier_id,
      supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  }

  checkUser(type){

         // console.log("selected user type", type)
         this.type=type;
         // console.log("assing type", this.type)

  }
    check(typeuser){

         // console.log("selected user type", type)
         this.typeuser=typeuser;
         // console.log("assing type", this.type)

  }


    listOfData: Person[] = [
    {
      key: '1',
      invoice_no: '',
      total: '',
      paid_out: '',
      balance: '',
      with_holdings: '',
      amount_to_be_paid: ''
    }
  ];

        // Category Form
     addCategoryFormInit() {
    this.addCategoryForm = this.fb.group({
      code: [null],
      name: [null, [Validators.required]],
      description: [null],
      belonging_to: [null]
    })   
  }

  openCategoryDialog() {
    this.categoryDialogVisible = true
  }

  closeCategoryDialog() {
    this.categoryDialogVisible = false
  }

  saveCategory() {
    for (const i in this.addCategoryForm.controls) {
      this.addCategoryForm.controls[i].markAsDirty()
      this.addCategoryForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addCategoryForm.value --', this.addCategoryForm.value)
    if(this.addCategoryForm.value.category_name != null && this.addCategoryForm.value.category_name != '')  {
      this.categoryDialogVisible = false
      this.addCategoryFormInit()
    }    
  }


}
