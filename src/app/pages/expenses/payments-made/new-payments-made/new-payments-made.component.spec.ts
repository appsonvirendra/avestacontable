import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPaymentsMadeComponent } from './new-payments-made.component';

describe('NewPaymentsMadeComponent', () => {
  let component: NewPaymentsMadeComponent;
  let fixture: ComponentFixture<NewPaymentsMadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPaymentsMadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPaymentsMadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
