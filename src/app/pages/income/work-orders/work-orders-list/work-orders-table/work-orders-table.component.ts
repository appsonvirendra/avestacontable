import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-work-orders-table',
  templateUrl: './work-orders-table.component.html',
  styleUrls: ['./work-orders-table.component.scss']
})
export class WorkOrdersTableComponent implements OnInit {
 
   // modal loading
   isVisible = false;
   isConfirmLoading = false;
  @Input() workordersData: any

    // search
    search_title = ''

    workDisplayDatas: any
  
    sortName: string | null = null
    sortValue: string | null = null  

  constructor() { }
   
  // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
    this.workData()
  }
  workData() {
    this.workDisplayDatas = [...this.workordersData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { workorders_code: string; }) => {
      return (
        item.workorders_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.workordersData.filter((item: { workorders_code: string; }) => filterFunc(item))

    this.workDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.workDisplayDatas = [...this.workordersData]
    }    
  }  

}
