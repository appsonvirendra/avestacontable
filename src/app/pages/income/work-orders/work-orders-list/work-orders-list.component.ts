import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-work-orders-list',
  templateUrl: './work-orders-list.component.html',
  styleUrls: ['./work-orders-list.component.scss']
})
export class WorkOrdersListComponent implements OnInit {

  workordersDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.workordersData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.workordersDatas = []
  }
  
  workordersData() {
    this.workordersDatas = [
      {
        workorders_id: '1',
        workorders_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        workorders_provider_name: 'Provider 1',
        workorders_total: '104',
        workorders_balance: '104',
        workorders_date: '2018-12-23',
        workorders_state: '2'
      }, 
      {
        workorders_id: '2',        
        workorders_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        workorders_provider_name: 'Provider 2',
        workorders_total: '104',
        workorders_balance: '104',
        workorders_date: '2018-12-23',
        workorders_state: '2'
      },
      {
        workorders_id: '3',
        workorders_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        workorders_provider_name: 'Provider 3',
        workorders_total: '104',
        workorders_balance: '104',
        workorders_date: '2018-12-23',
        workorders_state: '1'
      }, 
      {
        workorders_id: '4',
        workorders_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        workorders_provider_name: 'Provider 1',
        workorders_total: '104',
        workorders_balance: '104',
        workorders_date: '2018-12-23',
        workorders_state: '0'
      }                      
    ]
  }

}
