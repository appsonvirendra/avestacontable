import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentLinksListComponent } from './payment-links-list.component';

describe('PaymentLinksListComponent', () => {
  let component: PaymentLinksListComponent;
  let fixture: ComponentFixture<PaymentLinksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentLinksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentLinksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
