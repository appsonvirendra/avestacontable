import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
   selector: 'app-payment-links-list',
  templateUrl: './payment-links-list.component.html',
  styleUrls: ['./payment-links-list.component.scss']
})
export class PaymentLinksListComponent implements OnInit {


  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }

}

