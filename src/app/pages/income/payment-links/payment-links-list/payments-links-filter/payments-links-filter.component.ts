import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payments-links-filter',
  templateUrl: './payments-links-filter.component.html',
  styleUrls: ['./payments-links-filter.component.scss']
})
export class PaymentsLinksFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      search_by_code: [null],
      customers: ['0'],
      everyone: ['0'],
      date: [null]
    })  
  }

  /* searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  } 
  */

 /*  clearFilter() {
    this.filterForm = this.fb.group({
      search_by_code: [null],
      customers: ['0'],
      everyone: ['0'],
      date: [null]
    }) 
  }

  */

}

