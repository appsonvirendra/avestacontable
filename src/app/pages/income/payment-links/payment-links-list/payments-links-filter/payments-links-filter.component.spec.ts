import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsLinksFilterComponent } from './payments-links-filter.component';

describe('PaymentsLinksFilterComponent', () => {
  let component: PaymentsLinksFilterComponent;
  let fixture: ComponentFixture<PaymentsLinksFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsLinksFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsLinksFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
