import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPaymentLinksComponent } from './new-payment-links.component';

describe('NewPaymentLinksComponent', () => {
  let component: NewPaymentLinksComponent;
  let fixture: ComponentFixture<NewPaymentLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPaymentLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPaymentLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
