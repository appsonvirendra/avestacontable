import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface ItemnewpurchaseData {
  id: string;
  reference: string;
  quantity: string;
  price: string;
  total: number;
}

@Component({
  selector: 'app-new-payment-links',
  templateUrl: './new-payment-links.component.html',
  styleUrls: ['./new-payment-links.component.scss']
})
export class NewPaymentLinksComponent implements OnInit {

  // item Add Form
  itemAddForm: FormGroup

  itemAddFormVisible = false 
  
   // customer search dialog
  searchCustomerVisible = false
  customerDatas = []

  // category dialog
  categoryDialogVisible = false
  addCategoryForm: FormGroup  

  salePriceCollapse = false

  // sale prive dialog
  priceIsv1Visible = false
  priceIsv1Form: FormGroup  
  saleDialogNumber: any

  // supplier dialog
  supplierDialogVisible = false
  supplierForm: FormGroup

  nofity_add_remove = 0

// item view visible
  itemViewVisible = false 

 // search seller dialog
  searchSellerVisible = false
  sellerDatas = []

  myBlog: string = "";

  newpaymentlinks: FormGroup
  i = 0;
  currentDate:any = new Date()


  itemSelectSales: any = []
  
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.newpaymentlinksFormInit()
    this.addnewpaymentlinksRow()
    this.sellerData()
    this.itemAddFormInit()
    this.addCategoryFormInit()
    this.priceIsv1FormInit()
    this.supplierFormInit()
    this.customerData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  newpaymentlinksFormInit() {
    this.newpaymentlinks = this.fb.group({
      customer_id: [null],
      customer_name: [null],
      creation_date: [null, [Validators.required]],
      seller: ['0'],
      discount: ['0'],
      cellar:  [null, [Validators.required]],
      item: [null]

    })      
  }

  
  savePaymentlink() {
    for (const i in this.newpaymentlinks.controls) {
      this.newpaymentlinks.controls[i].markAsDirty()
      this.newpaymentlinks.controls[i].updateValueAndValidity()
    }
    if(this.newpaymentlinks.valid) {
      console.log('this.newpaymentlinks.value', this.newpaymentlinks.value)
    }
  }
    
// add line Begin
   
  listOfnewpaymentlinksData: ItemnewpurchaseData[] = [];


  addnewpaymentlinksRow(): void {
    this.listOfnewpaymentlinksData = [
      ...this.listOfnewpaymentlinksData,
      {
        id: `${this.i+1}`,
        reference: ``,
        quantity: '0',
        price: '0',
        total: 0.00
      }
    ];
    this.i++;
  }

  deletenewpaymentlinksRow(index): void {
    this.listOfnewpaymentlinksData.splice(index, 1)
  } 
// add line End


  // search seller dialog
  searchSellerOpen() {
    this.searchSellerVisible = true
  }

  searchSellerClose() {
    this.searchSellerVisible = false
  }   

  sellerData() {
    this.sellerDatas = [
      {
        seller_id: '1',
        seller_name: 'John Brown 1',
        seller_phone: "",
        seller_mobile: "",
        seller_email: "",
        seller_type: "1"
      },
      {
        seller_id: '2',
        seller_name: 'John Brown 2',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '3',
        seller_name: 'John Brown 3',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '4',
        seller_name: 'John Brown 4',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      }               
    ]
  }

  sellerAddInput($event) {
    this.newpaymentlinks.patchValue({
      seller_id: $event.seller_id,
      seller_name: $event.seller_name
    })

    this.searchSellerClose()    
  }

  // item Add Form
  itemAddFormInit() {
    this.itemAddForm = this.fb.group({
      item_type: ['0'],
      item_name: [null, [Validators.required]],
      item_code: [null, [Validators.required]],
      item_category: [null, [Validators.required]],
      item_cost_price: [{value: '0', disabled: true}],
      item_image: [null],
      item_description: [null],
      item_provider_id: [null, [Validators.required]],
      item_currency: ['1'],
      item_discount: ['0'],
      item_sale_price1: [null, [Validators.required]],
      item_sale_price2: [null],
      item_sale_price3: [null],
      item_sales_tax1: ['1'],
      item_sales_tax2: ['1'],
      item_tax_when_buy1: ['1'],
      item_tax_when_buy2: ['1'],
      item_wineries: [null],
      item_current_existence: [{value: '0', disabled: true}],
      item_minimum_existency: [null, [Validators.required]],
      item_low_mail: [false],
      item_notify_in_stock: ['0']
    })
  }

  itemAddFormOpen() {
    this.itemAddFormVisible = true
  }

  itemAddFormClose() {
    this.itemAddFormVisible = false
  }    

  itemAddSave() {
    for (const i in this.itemAddForm.controls) {
      this.itemAddForm.controls[i].markAsDirty()
      this.itemAddForm.controls[i].updateValueAndValidity()
    }
    // console.log('this.itemAddForm.value', this.itemAddForm.value)
  }

  // category dialog
  addCategoryFormInit() {
    this.addCategoryForm = this.fb.group({
      category_name: [null, [Validators.required]],
      category_description: [null],
    })   
  }
  
  openCategoryDialog() {
    this.categoryDialogVisible = true
  }

  closeCategoryDialog() {
    this.categoryDialogVisible = false
  }

  saveCategory() {
    for (const i in this.addCategoryForm.controls) {
      this.addCategoryForm.controls[i].markAsDirty()
      this.addCategoryForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addCategoryForm.value --', this.addCategoryForm.value)
    if(this.addCategoryForm.valid)  {
      this.categoryDialogVisible = false
      this.addCategoryFormInit()
    }    
  }
  
  showSalePriceCollapse() {
    this.salePriceCollapse = !this.salePriceCollapse
  }
  
  // sales and price dialog
  priceIsv1open(saleDialogNumber) {
    this.saleDialogNumber = saleDialogNumber
    this.priceIsv1Visible = true
  }

  priceIsv1close() {
    this.priceIsv1Visible = false
  }    

  priceIsv1FormInit() {
    this.priceIsv1Form = this.fb.group({
      final_price: [null],
      tax_to_calculate: ['1'],
    })  
  }
  
  priceIsv1calculate() {
    // console.log('this.priceIsv1Form.value --', this.priceIsv1Form.value)

    /*if(this.saleDialogNumber == '1') {
      this.itemAddForm.patchValue({
        item_sale_price1: '666'
      })
    }

    if(this.saleDialogNumber == '2') {
      this.itemAddForm.patchValue({
        item_sale_price2: '667'
      })
    }    

    if(this.saleDialogNumber == '3') {
      this.itemAddForm.patchValue({
        item_sale_price3: '668'
      })
    }*/

    this.priceIsv1FormInit()
    this.priceIsv1Visible = false    

  }
  
  // supplier form
  supplierFormInit() {
    this.supplierForm = this.fb.group({
      supplier_name: [null, [Validators.required]],
      supplier_phone: [null],
      supplier_mobile: [null],
      supplier_fax: [null],
      supplier_email: [null],
      supplier_direction: [null]
    })   
  }

  supplierDialogOpen() {
    this.supplierDialogVisible = true
  }

  supplierDialogClose() {
    this.supplierDialogVisible = false
  }

  supplierAdd() {
    for (const i in this.supplierForm.controls) {
      this.supplierForm.controls[i].markAsDirty()
      this.supplierForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.supplierForm.value --', this.supplierForm.value)
    if(this.supplierForm.valid)  {
      this.supplierDialogVisible = false
      this.supplierFormInit()
    }    
  }

  notify_add_remove(zero_one) {    
    if(zero_one) {
      this.nofity_add_remove += 1
    } else {
      if(this.itemAddForm.value.item_notify_in_stock > 0) {
        this.nofity_add_remove -= 1
      } 
    }
    
    this.itemAddForm.patchValue({
      item_notify_in_stock: this.nofity_add_remove
    })
        
  }
  itemViewOpen() {
    this.itemViewVisible = true
  }

  itemViewClose() {
    this.itemViewVisible = false
  }

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }             
    ]
  }

  customerAddInput($event) {
    this.newpaymentlinks.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  }

}

