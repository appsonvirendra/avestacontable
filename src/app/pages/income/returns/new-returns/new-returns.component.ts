import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-returns',
  templateUrl: './new-returns.component.html',
  styleUrls: ['./new-returns.component.scss']
})
export class NewReturnsComponent implements OnInit {

  returnsAddForm: FormGroup

  currentDate:any = new Date()  
  
  // product search dialog
  searchProductVisible = false
  saleDatas = []  

  // invoice product data 
  invoiceProductDatas = []  

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []

  // invoice search dialog
  searchInvoiceVisible = false
  invoiceDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.returnsAddFormInit()

    this.productData()
    this.invoiceProductData()  
    
    this.customerData()

    this.invoiceData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  returnsAddFormInit() {
    this.returnsAddForm = this.fb.group({
      date: [this.currentDate, [Validators.required]],
      invoice_id: [null],
      invoice_code: [null],
      invoice_total: [null, [Validators.required]],
      identification_no: [null, [Validators.required]],
      customer_id: [null],
      customer_name: [null, [Validators.required]],
      received: [null],
      notes: [null],
      exempt_amount: ['0'],
      isv_amount: ['0'],
      final_discount: ['0'],
      total_to_pay: ['0'],
      product_item: []
    })      
  }
  
  saveReturns() {
    for (const i in this.returnsAddForm.controls) {
      this.returnsAddForm.controls[i].markAsDirty()
      this.returnsAddForm.controls[i].updateValueAndValidity()
    }
    if(this.returnsAddForm.valid) {
      console.log('this.returnsAddForm.value', this.returnsAddForm.value)
    }
  }
  
  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }   

  productData() {
    this.saleDatas = [    
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      }
    ]
  }

  productTotalPriceSum() {
    let totalValueSum = 0

    this.invoiceProductDatas.forEach(function (value) {
      totalValueSum += value.item_total
    })

    this.returnsAddForm.patchValue({
      exempt_amount: totalValueSum,
      total_to_pay: totalValueSum,
      product_item: this.invoiceProductDatas
    })    
  }

  productAddInput($event) {
    this.invoiceProductDatas.push(
      $event
    )
    
    this.productTotalPriceSum()
  }

  invoiceProductData() {
    this.invoiceProductDatas = []    
  }

  invoiceProductRemove(item_index) {
    this.invoiceProductDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }  

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }             
    ]
  }

  customerAddInput($event) {
    this.returnsAddForm.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  }  

  // search invoice dialog
  searchInvoiceOpen() {
    this.searchInvoiceVisible = true
  }

  searchInvoiceClose() {
    this.searchInvoiceVisible = false
  }   

  invoiceData() {
    this.invoiceDatas = [
      {
        invoice_id: '1',
        invoice_code: '000-001-01-001',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '2',
        invoice_code: '000-001-01-002',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        invoice_provider_name: 'Provider 2',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '0'
      },
      {
        invoice_id: '3',
        invoice_code: '000-001-01-003',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        invoice_provider_name: 'Provider 3',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '4',
        invoice_code: '000-001-01-004',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }               
    ]
  }

  invoiceAddInput($event) {
    this.returnsAddForm.patchValue({
      invoice_id: $event.invoice_id,
      invoice_code: $event.invoice_code,
      invoice_total: $event.invoice_total
    })

    this.searchInvoiceClose()    
  }  

}
