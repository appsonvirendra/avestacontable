import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-info-returns',
   templateUrl: './info-returns.component.html',
   styleUrls: ['./info-returns.component.scss']
})
export class InfoReturnsComponent implements OnInit {

  return_id: any

  returnDatas: any

  generatePaymentDatas: any  

  // create receipt form
  createReceiptForm: FormGroup  
  createReceiptVisible = false
  
  // send invoice mail form
  //sendInvoiceMailForm: FormGroup  
  //sendInvoiceMailVisible = false  

  generatePaymentCollapse = false

  currentDate:any = new Date()

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()

    this.returnInfo()
    this.generatePaymentInfo()
    
    this.createReceiptFormInit()
    //this.sendInvoiceMailFormInit()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.return_id = params.return_id
    })    
  }  

  returnInfo() {
    this.returnDatas = 
      {
        "return_order_no": "000-111-2222-0033",
        "return_date": "2019-12-27 13:25:02",
        "return_discount": "0",
        "return_customer_id": "1",
        "return_customer_name": "John brawn",
        "return_exchange_rate": "123",
        "return_receive_product": null,
        "return_notes": null,
        "return_exempt_amount": 9,
        "return_taxed_amount": 7,
        "return_isv_amount": "0",
        "return_final_discount": "0",
        "return_total_to_pay": 9,
        "return_status": "2",
        "return_product_item": [
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "0",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "1",
            "item_total": 0,
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "3",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "3",
            "item_total": 9,
            "item_cellar": "Bodega Principal"
          }
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
        "g_p_receipt_status": "1"
      }           
    ]
  }

  // create receipt form
  createReceiptFormInit() {
    this.createReceiptForm = this.fb.group({
      receipt_date_of_payment: [this.currentDate, [Validators.required]],
      receipt_payment_received: [null, [Validators.required]],
      receipt_bank_account: [null, [Validators.required]],
      receipt_method_of_payment: ['1', [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  createReceiptOpen() {
    this.createReceiptVisible = true
  }

  createReceiptClose() {
    this.createReceiptForm.reset()
    this.createReceiptVisible = false
  }      

  createReceiptSave() {        
    for (const i in this.createReceiptForm.controls) {
      this.createReceiptForm.controls[i].markAsDirty()
      this.createReceiptForm.controls[i].updateValueAndValidity()
    }    
    // console.log('this.createReceiptForm.value', this.createReceiptForm.value)
    if(this.createReceiptForm.valid) {
      this.createReceiptClose()
    }
  }  
  
  // send invoice mail form
  // sendInvoiceMailFormInit() {
  //   this.sendInvoiceMailForm = this.fb.group({
  //     send_invoice_email1: [null, [Validators.required]],
  //     send_invoice_email2: [null],
  //     send_invoice_email3: [null]
  //   })      
  // }  

  // sendInvoiceMailOpen() {
  //   this.sendInvoiceMailVisible = true
  // }

  // sendInvoiceMailClose() {
  //   this.sendInvoiceMailForm.reset()
  //   this.sendInvoiceMailVisible = false
  // }      

  // sendInvoiceMailSave() {    
  //   for (const i in this.sendInvoiceMailForm.controls) {
  //     this.sendInvoiceMailForm.controls[i].markAsDirty()
  //     this.sendInvoiceMailForm.controls[i].updateValueAndValidity()
  //   }          
  //   if(this.sendInvoiceMailForm.valid) {
  //     this.sendInvoiceMailClose()
  //   }
  // }   

  // generate payment collapse
  generatePyamentCollapse() {
    this.generatePaymentCollapse = !this.generatePaymentCollapse
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}

