import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-returns-table',
  templateUrl: './returns-table.component.html',
  styleUrls: ['./returns-table.component.scss']
})
export class ReturnsTableComponent implements OnInit {
// modal loading
isVisible = false;
isConfirmLoading = false;

  @Input() returnsDatas: any
  
  // search
  search_title = ''
  returnsDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }
  // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
    this.returnsData()
  }

  returnsData() {
    this.returnsDisplayDatas = [...this.returnsDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { returns_code: string; }) => {
      return (
        item.returns_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.returnsDatas.filter((item: { returns_code: string; }) => filterFunc(item))

    this.returnsDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.returnsDisplayDatas = [...this.returnsDatas]
    }    
  }

}
