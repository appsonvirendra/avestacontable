import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-returns-list',
  templateUrl: './returns-list.component.html',
  styleUrls: ['./returns-list.component.scss']
})
export class ReturnsListComponent implements OnInit {

  returnsDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.returnsData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.returnsDatas = []
  }
  
  returnsData() {
    this.returnsDatas = [
      {
        returns_id: '1',
        returns_code: '000-001-01-001',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        returns_date: '2018-12-23',
        returns_state: '1'
      }, 
      {
        returns_id: '2',
        returns_code: '000-001-01-002',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        returns_provider_name: 'Provider 2',
        returns_total: '104',
        returns_balance: '104',
        returns_date: '2018-12-23',
        returns_state: '0'
      },
      {
        returns_id: '3',
        returns_code: '000-001-01-003',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        returns_provider_name: 'Provider 3',
        returns_total: '104',
        returns_balance: '104',
        returns_date: '2018-12-23',
        returns_state: '1'
      }, 
      {
        returns_id: '4',
        returns_code: '000-001-01-004',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        returns_date: '2018-12-23',
        returns_state: '1'
      }                      
    ]
  }

}
