import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-info-invoice',
  templateUrl: './info-invoice.component.html',
  styleUrls: ['./info-invoice.component.scss']
})
export class InfoInvoiceComponent implements OnInit {

  invoice_id: any

  invoiceDatas: any

  generatePaymentDatas: any  

  // create receipt form
  createReceiptForm: FormGroup  
  createReceiptVisible = false
  
  // send invoice mail form
  sendInvoiceMailForm: FormGroup  
  sendInvoiceMailVisible = false 

  // payment view receipt  
  PayViewVisible = false   

  generatePaymentCollapse = false

  currentDate:any = new Date()

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()

    this.invoiceInfo()
    this.generatePaymentInfo()
    
    this.createReceiptFormInit()
    this.sendInvoiceMailFormInit()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.invoice_id = params.invoice_id
    })    
  }  

  invoiceInfo() {
    this.invoiceDatas = 
      {
        "invoice_order_no": "000-111-2222-0033",
        "invoice_date": "2019-12-27 13:25:02",
        "invoice_discount": "0",
        "invoice_rtn": "08019017795401",
        "invoice_attended": "Hector Obdulio Mercadal Mendoza",
        "invoice_customer_id": "1",
        "invoice_direction": "Residencial el Sauce villas Palmeras Casa",
        "invoice_telephone": "50431860490",
        "invoice_customer_name": "Pegasus Medeval Logistica S.A. R.L",
        "invoice_exchange_rate": "123",
        "invoice_receive_product": null,
        "invoice_notes": null,
        "invoice_exempt_amount": 9,
        "invoice_taxed_amount": 7,
        "invoice_isv_amount": "0",
        "invoice_final_discount": "0",
        "invoice_total_to_pay": 9,
        "invoice_status": "2",
        "invoice_product_item": [
          {
            "item_id": "1",
            "item_code": "SW92751",
            "item_name": "Item 1",
            "item_description": "Mochila Swisswin negra",
            "item_cost_price": "0",
            "item_price": "1300",
            "item_discount": "100",
            "item_quantity": "1.00",
            "item_total": "1300",
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "SW92751",
            "item_name": "Item 1",
            "item_description": "Mochila Swisswin negra",
            "item_cost_price": "3",
            "item_price": "1300",
            "item_discount": "20",
            "item_quantity": "1.00",
            "item_total": "1300",
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "SW92751",
            "item_name": "Item 1",
            "item_description": "Mochila Swisswin negra",
            "item_cost_price": "3",
            "item_price": "1300",
            "item_discount": "0",
            "item_quantity": "1.00",
            "item_total": "1300",
            "item_cellar": "Bodega Principal"
            }   
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
        "g_p_receipt_status": "1"
      }           
    ]
  }
     listOfData = [
    {
      key: '1',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '100.00',
      total: '1,300.00'
    },
    {
      key: '2',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '20.00',
      total: '1,300.00'
    },
    {
      key: '3',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '0.00',
      total: '1,300.00'
    }
  ];

   // payment view receipt start  
  PayViewOpen() {
    this.PayViewVisible = true
  }

  PayViewClose() {
    this.PayViewVisible = false
  }      

  PayViewPrint() {
    this.PayViewClose()
  }

  // create receipt form
  createReceiptFormInit() {
    this.createReceiptForm = this.fb.group({
      receipt_date_of_payment: [this.currentDate, [Validators.required]],
      receipt_payment_received: [null, [Validators.required]],
      receipt_bank_account: [null, [Validators.required]],
      receipt_method_of_payment: ['1', [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  createReceiptOpen() {
    this.createReceiptVisible = true
  }

  createReceiptClose() {
    this.createReceiptForm.reset()
    this.createReceiptVisible = false
  }      

  createReceiptSave() {        
    for (const i in this.createReceiptForm.controls) {
      this.createReceiptForm.controls[i].markAsDirty()
      this.createReceiptForm.controls[i].updateValueAndValidity()
    }    
    // console.log('this.createReceiptForm.value', this.createReceiptForm.value)
    if(this.createReceiptForm.valid) {
      this.createReceiptClose()
    }
  }  
  
  // send invoice mail form
  sendInvoiceMailFormInit() {
    this.sendInvoiceMailForm = this.fb.group({
      send_invoice_email1: [null, [Validators.required]],
      send_invoice_email2: [null],
      send_invoice_email3: [null]
    })      
  }  

  sendInvoiceMailOpen() {
    this.sendInvoiceMailVisible = true
  }

  sendInvoiceMailClose() {
    this.sendInvoiceMailForm.reset()
    this.sendInvoiceMailVisible = false
  }      

  sendInvoiceMailSave() {    
    for (const i in this.sendInvoiceMailForm.controls) {
      this.sendInvoiceMailForm.controls[i].markAsDirty()
      this.sendInvoiceMailForm.controls[i].updateValueAndValidity()
    }          
    if(this.sendInvoiceMailForm.valid) {
      this.sendInvoiceMailClose()
    }
  }   

  // generate payment collapse
  generatePyamentCollapse() {
    this.generatePaymentCollapse = !this.generatePaymentCollapse
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
