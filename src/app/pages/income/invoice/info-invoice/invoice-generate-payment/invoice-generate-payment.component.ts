import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invoice-generate-payment',
  templateUrl: './invoice-generate-payment.component.html',
  styleUrls: ['./invoice-generate-payment.component.scss']
})
export class InvoiceGeneratePaymentComponent implements OnInit {

 invoiceDatas: any


  @Input() generatePaymentData: any

  // generate payment edit form
  generatePayEditForm: FormGroup  
  generatePayEditVisible = false
  
  // generate payment send mail form
  generatePayMailForm: FormGroup  
  generatePayMailVisible = false
  
  // generate payment view receipt  
  generatePayViewVisible = false  

  deleteConfirm2Visible = false

  currentDate:any = new Date()

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
  this.invoiceInfo()
    this.generatePayEditFormInit()    

    this.generatePayMailFormInit()
  }
   listOfData = [
    {
      key: '1',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '100.00',
      total: '1,300.00'
    },
    {
      key: '2',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '20.00',
      total: '1,300.00'
    },
    {
      key: '3',
      qty: '1.00',
      code: 'SW92751',
      description: 'Mochila Swisswin negra',
      unitprice: '1,300.00',
      desc_and_reb_granted: '0.00',
      total: '1,300.00'
    }
  ];
   invoiceInfo() {
    this.invoiceDatas = 
      {
        "invoice_order_no": "000-111-2222-0033",
        "invoice_date": "2019-12-27 13:25:02",
        "invoice_discount": "0",
        "invoice_customer_id": "1",
        "invoice_customer_name": "Pegasus Medeval Logistica S.A. R.L",
        "invoice_rtn": "08019017795401",
        "invoice_direction": "Residencial el Sauce villas Palmeras Casa",
        "invoice_telephone": "50431860490",
        "invoice_attended": "Hector Obdulio Mercadal Mendoza",
        "invoice_exchange_rate": "123",
        "invoice_receive_product": null,
        "invoice_notes": null,
        "invoice_exempt_amount": 9,
        "invoice_taxed_amount": 7,
        "invoice_isv_amount": "0",
        "invoice_final_discount": "0",
        "invoice_total_to_pay": 9,
        "invoice_status": "2"
      }
  }  

  // generate payment edit form start  
  generatePayEditFormInit() {
    this.generatePayEditForm = this.fb.group({
      receipt_date_of_payment: [this.currentDate, [Validators.required]],
      receipt_payment_received: [null, [Validators.required]],
      receipt_bank_account: [null, [Validators.required]],
      amount_in_letters: [null, [Validators.required]],
      receipt_method_of_payment: ['1', [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  generatePayEditOpen() {
    this.generatePayEditVisible = true
  }

  generatePayEditClose() {
    this.generatePayEditForm.reset()
    this.generatePayEditVisible = false
  }      

  generatePayEditSave() {    
    for (const i in this.generatePayEditForm.controls) {
      this.generatePayEditForm.controls[i].markAsDirty()
      this.generatePayEditForm.controls[i].updateValueAndValidity()
    }          
    // console.log('this.generatePayEditForm.value --', this.generatePayEditForm.value)
    if(this.generatePayEditForm.valid) {
      this.generatePayEditClose()
    }
  }  

  // generate payment send mail form start  
  generatePayMailFormInit() {
    this.generatePayMailForm = this.fb.group({
      send_receipt_email1: [null, [Validators.required]],
      send_receipt_email2: [null],
      send_receipt_email3: [null]
    })      
  }  

  generatePayMailOpen() {
    this.generatePayMailVisible = true
  }

  generatePayMailClose() {
    this.generatePayMailForm.reset()
    this.generatePayMailVisible = false
  }      

  generatePayMailSave() {    
    for (const i in this.generatePayMailForm.controls) {
      this.generatePayMailForm.controls[i].markAsDirty()
      this.generatePayMailForm.controls[i].updateValueAndValidity()
    }          
    // console.log('this.generatePayMailForm.value --', this.generatePayMailForm.value)
    if(this.generatePayMailForm.valid) {
      this.generatePayMailClose()
    }
  } 

  // generate payment view receipt start  
  generatePayViewOpen() {
    this.generatePayViewVisible = true
  }

  generatePayViewClose() {
    this.generatePayViewVisible = false
  }      

  generatePayViewPrint() {
    this.generatePayViewClose()
  }

  // delete confirm 2
  deleteConfirm2Open() {
    this.deleteConfirm2Visible = true
  }

  deleteConfirm2Close() {
    this.deleteConfirm2Visible = false
  }  

}
