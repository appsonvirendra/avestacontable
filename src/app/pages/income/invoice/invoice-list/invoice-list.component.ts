import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

  invoiceDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.invoiceData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.invoiceDatas = []
  }
  
  invoiceData() {
    this.invoiceDatas = [
      {
        invoice_id: '1',
        invoice_number: '000-001-01-001',
        customer_id: '11',
        invoice_provider_name: 'Provider 1',
        invoice_creation_date: '2018-12-23',
        invoice_expiration_date: '2018-12-23',
        invoice_total: '10',
        invoice_paid_out: '',
        invoice_to_pay: '10',
        invoice_state: '1'
      }, 
      {
        invoice_id: '1',
        invoice_number: '000-001-01-002',
        customer_id: '11',
        invoice_provider_name: 'Provider 2',
        invoice_creation_date: '2018-12-24',
        invoice_expiration_date: '2018-12-24',
        invoice_total: '20',
        invoice_paid_out: '',
        invoice_to_pay: '20',
        invoice_state: '1'
      },
      {
        invoice_id: '1',
        invoice_number: '000-001-01-003',
        customer_id: '11',
        invoice_provider_name: 'Provider 3',
        invoice_creation_date: '2018-12-25',
        invoice_expiration_date: '2018-12-25',
        invoice_total: '30',
        invoice_paid_out: '',
        invoice_to_pay: '30',
        invoice_state: '1'
      }, 
      {
        invoice_id: '1',
        invoice_number: '000-001-01-004',
        customer_id: '11',
        invoice_provider_name: 'Provider 4',
        invoice_creation_date: '2018-12-26',
        invoice_expiration_date: '2018-12-26',
        invoice_total: '10',
        invoice_paid_out: '',
        invoice_to_pay: '10',
        invoice_state: '1'
      }                       
    ]
  }  

}
