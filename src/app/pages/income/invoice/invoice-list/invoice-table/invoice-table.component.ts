import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-invoice-table',
  templateUrl: './invoice-table.component.html',
  styleUrls: ['./invoice-table.component.scss']
})
export class InvoiceTableComponent implements OnInit {

  @Input() invoiceDatas: any

  // search
  search_title = ''  
  invoiceDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null 
  
  deleteConfirmVisible = false

  constructor() { }

  ngOnInit() {
    this.invoiceData()
  }

  invoiceData() {
    this.invoiceDisplayDatas = [...this.invoiceDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { invoice_code: string; invoice_provider_name: string; invoice_date: string; }) => {
      return (
        item.invoice_code.toLowerCase().indexOf(searchLower) !== -1 ||
        item.invoice_provider_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.invoice_date.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.invoiceDatas.filter((item: { invoice_code: string; invoice_provider_name: string; invoice_date: string; }) => filterFunc(item))

    this.invoiceDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.invoiceDisplayDatas = [...this.invoiceDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }    

}
