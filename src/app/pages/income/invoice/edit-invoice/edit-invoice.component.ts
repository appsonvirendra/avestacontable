import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.scss']
})
export class EditInvoiceComponent implements OnInit {

  invoiceEditform: FormGroup
  
  // Variables for current date and expiration date select
  currentDate:any = new Date()

  datePlus: any = new Date()
  datePlus1: any = new Date()
  datePlus15: any = new Date()
  datePlus30: any = new Date()
  datePlus45: any = new Date()
  datePlus60: any = new Date()
  datePlus90: any = new Date()
  datePlus120: any = new Date()

  expirationDisable: boolean = false
  expirationShow: boolean = false

  // exchange rate
  exchangeRateForm: FormGroup  
  exchangeRateVisible = false
  
  // product search dialog
  searchProductVisible = false
  saleDatas = []  

  // invoice product data 
  invoiceProductDatas = []  

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []
  
  // employee search dialog
  searchEmployeeVisible = false
  employeeDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.invoiceFormInit()
    this.exchangeRateFormInit()
    this.productData()
    this.invoiceProductData()  
    this.customerData()
    this.employeeData()

    this.asignDate()
  }

  asignDate() {
    this.datePlus.setDate( this.currentDate.getDate() + 1 )
    this.datePlus1.setDate( this.currentDate.getDate() + 1 )
    this.datePlus15.setDate( this.currentDate.getDate() + 15 )
    this.datePlus30.setDate( this.currentDate.getDate() + 30 )
    this.datePlus45.setDate( this.currentDate.getDate() + 45 )
    this.datePlus60.setDate( this.currentDate.getDate() + 60 )
    this.datePlus90.setDate( this.currentDate.getDate() + 90 )
    this.datePlus120.setDate( this.currentDate.getDate() + 120 )    
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  invoiceFormInit() {
    this.invoiceEditform = this.fb.group({
      date: [this.currentDate, [Validators.required]],
      term: ['1'],
      expiration_date: [this.datePlus1, [Validators.required]],
      payment_method: ['1'],
      discount: ['0'],
      employee_id: [null],
      original: [null],
      employee_name: [null],
      identification_no: [null, [Validators.required]],
      customer_id: [null],
      customer_name: [null, [Validators.required]],
      exchange_rate: [null, [Validators.required]],
      notes: [null],
      exempt_amount: ['0'],
      isv_amount: ['0'],
      final_discount: ['0'],
      total_to_pay: ['0'],
      product_item: [],
      no_exempt_purchase_order: [null],
      no_identification_sag: [null],
      no_proof_of_registration_exonerated: [null]
    })      
  }

  termChange(termSelectedValue) {
    // Expiration after 1 day for counted option
    if(termSelectedValue == '1') {
      this.expirationDisable = false
      this.expirationShow = false
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus
      })
    }    

    // Expiration after 1 day for manual option
    if(termSelectedValue == 'plus1') {
      this.expirationDisable = false
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus1
      })
    }
    
    // Expiration after 15 day
    if(termSelectedValue == 'plus15') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus15
      })
    } 
    
    // Expiration after 30 day
    if(termSelectedValue == 'plus30') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus30
      })
    } 
    
    // Expiration after 45 day
    if(termSelectedValue == 'plus45') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus45
      })
    } 
    
    // Expiration after 60 day
    if(termSelectedValue == 'plus60') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus60
      })
    } 
    
    // Expiration after 90 day
    if(termSelectedValue == 'plus90') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus90
      })
    } 
    
    // Expiration after 120 day
    if(termSelectedValue == 'plus120') {
      this.expirationDisable = true
      this.expirationShow = true
      this.invoiceEditform.patchValue({
        expiration_date: this.datePlus120
      })
    }     
  }
  
  saveInvoice() {
    for (const i in this.invoiceEditform.controls) {
      this.invoiceEditform.controls[i].markAsDirty()
      this.invoiceEditform.controls[i].updateValueAndValidity()
    }
    if(this.invoiceEditform.valid) {
      console.log('this.invoiceEditform.value', this.invoiceEditform.value)
    }
  }
  
  // exchange rate form
  exchangeRateFormInit() {
    this.exchangeRateForm = this.fb.group({
      exchange_rate: [null]
    })      
  }  

  exchangeRateOpen() {
    this.exchangeRateVisible = true
  }

  exchangeRateClose() {
    this.exchangeRateFormInit()
    this.exchangeRateVisible = false
  }      

  exchangeRateCalculate() {    
    this.invoiceEditform.patchValue({
      exchange_rate: this.exchangeRateForm.value.exchange_rate
    })    

    this.exchangeRateClose()
  }
  
  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }   

  productData() {
    this.saleDatas = [    
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: 20,
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: 30,
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: 20,
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: 40,
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      }
    ]
  }

  productTotalPriceSum() {
    let totalValueSum = 0

    this.invoiceProductDatas.forEach(function (value) {
      totalValueSum += value.item_total
    })

    this.invoiceEditform.patchValue({
      exempt_amount: totalValueSum,
      exonerated_amount: totalValueSum,
      total_to_pay: totalValueSum,
      product_item: this.invoiceProductDatas
    })    
  }

  productAddInput($event) {
    this.invoiceProductDatas.push(
      $event
    )
    
    this.productTotalPriceSum()
  }

  invoiceProductData() {
    this.invoiceProductDatas = [    
      /*{
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      },
      {
        item_id: '2',
        item_code: '011',
        item_name: 'Item 2',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      },
      {
        item_id: '3',
        item_code: '011',
        item_name: 'Item 3',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      }*/            
    ]    
  }

  invoiceProductRemove(item_index) {
    this.invoiceProductDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }  

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_identification_no: '121',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_identification_no: '122',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_identification_no: '123',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_identification_no: '124',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }             
    ]
  }

  customerAddInput($event) {
    this.invoiceEditform.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name,
      identification_no: $event.customer_identification_no
    })

    this.searchCustomerClose()    
  }  

  // search employee dialog
  searchEmployeeOpen() {
    this.searchEmployeeVisible = true
  }

  searchEmployeeClose() {
    this.searchEmployeeVisible = false
  }   

  employeeData() {
    this.employeeDatas = [
      {
        employee_id: '1',
        employee_identification_no: '1112211',
        employee_firstname: 'Jeffrey',
        employee_lastname: 'Wilson',
        employee_phone: "",
        employee_mobile: "",
        employee_email: "jeffrey@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '2',
        employee_identification_no: '1112222',
        employee_firstname: 'Richard',
        employee_lastname: 'Peterson',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "richard@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '3',
        employee_identification_no: '1112233',
        employee_firstname: 'Dennis',
        employee_lastname: 'Beck',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "dennis@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '4',
        employee_identification_no: '1112244',
        employee_firstname: 'Brian',
        employee_lastname: 'Holmes',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "brian@gmail.com",
        employee_type: "1"
      }               
    ]
  }

  employeeAddInput($event) {
    this.invoiceEditform.patchValue({
      employee_id: $event.employee_id,
      employee_name: $event.employee_firstname +' '+ $event.employee_lastname
    })

    this.searchEmployeeClose()    
  }  

}