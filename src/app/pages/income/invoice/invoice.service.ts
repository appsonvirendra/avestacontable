import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getInvoiceList(bodyparams) {
    return this.apiService.get(`/posList`, bodyparams)
      .pipe(map(data => data.data))
  }  

}
