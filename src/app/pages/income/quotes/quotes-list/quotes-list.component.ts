import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-quotes-list',
  templateUrl: './quotes-list.component.html',
  styleUrls: ['./quotes-list.component.scss']
})
export class QuotesListComponent implements OnInit {

  quotationDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.quotationData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.quotationDatas = []
  }
  
  quotationData() {
    this.quotationDatas = [
      {
        quotation_id: '1',
        quotation_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        quotation_provider_name: 'Provider 1',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '2'
      }, 
      {
        quotation_id: '2',        
        quotation_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        quotation_provider_name: 'Provider 2',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '2'
      },
      {
        quotation_id: '3',
        quotation_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        quotation_provider_name: 'Provider 3',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '1'
      }, 
      {
        quotation_id: '4',
        quotation_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        quotation_provider_name: 'Provider 1',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '0'
      }                      
    ]
  }

}
