import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quotes-table',
  templateUrl: './quotes-table.component.html',
  styleUrls: ['./quotes-table.component.scss']
})
export class QuotesTableComponent implements OnInit {
  // modal loading
  isVisible = false;
  isConfirmLoading = false;
  @Input() quotationDatas: any

  constructor() { }
  // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
  }

}
