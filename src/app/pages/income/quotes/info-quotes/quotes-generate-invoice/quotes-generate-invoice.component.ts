import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quotes-generate-invoice',
  templateUrl: './quotes-generate-invoice.component.html',
  styleUrls: ['./quotes-generate-invoice.component.scss']
})
export class QuotesGenerateInvoiceComponent implements OnInit {

  quotationAddForm: FormGroup

  currentDate:any = new Date()  

  // exchange rate
  exchangeRateForm: FormGroup  
  exchangeRateVisible = false
  
  // product search dialog
  searchProductVisible = false
  saleDatas = []  

  // invoice product data 
  invoiceProductDatas = []  

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []
  
  // employee search dialog
  searchEmployeeVisible = false
  employeeDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.quotationAddFormInit()

    this.exchangeRateFormInit()

    this.productData()
    this.invoiceProductData()  
    
    this.customerData()
    
    this.employeeData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  quotationAddFormInit() {
    this.quotationAddForm = this.fb.group({
      date: [this.currentDate, [Validators.required]],
      expiry_date: [null, [Validators.required]],
      payment_type: [null, [Validators.required]],
      discount: ['0'],
      employee_id: [null],
      employee_name: [null],
      identification_no: [null, [Validators.required]],
      customer_id: [null],
      customer_name: [null, [Validators.required]],
      exchange_rate: [null, [Validators.required]],
      notes: [null],
      exempt_amount: ['0'],
      isv_amount: ['0'],
      final_discount: ['0'],
      total_to_pay: ['0'],
      product_item: [],
      no_order_purchase_exempt: [null],
      no_identification_sag: [null],
      no_registraion_certificate: [null]
    })      
  }
  
  saveInvoice() {
    for (const i in this.quotationAddForm.controls) {
      this.quotationAddForm.controls[i].markAsDirty()
      this.quotationAddForm.controls[i].updateValueAndValidity()
    }
    if(this.quotationAddForm.valid) {
      console.log('this.quotationAddForm.value', this.quotationAddForm.value)
    }
  }
  
  // exchange rate form
  exchangeRateFormInit() {
    this.exchangeRateForm = this.fb.group({
      exchange_rate: [null]
    })      
  }  

  exchangeRateOpen() {
    this.exchangeRateVisible = true
  }

  exchangeRateClose() {
    this.exchangeRateFormInit()
    this.exchangeRateVisible = false
  }      

  exchangeRateCalculate() {    
    this.quotationAddForm.patchValue({
      exchange_rate: this.exchangeRateForm.value.exchange_rate
    })    

    this.exchangeRateClose()
  }
  
  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }   

  productData() {
    this.saleDatas = [    
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      }
    ]
  }

  productTotalPriceSum() {
    let totalValueSum = 0

    this.invoiceProductDatas.forEach(function (value) {
      totalValueSum += value.item_total
    })

    this.quotationAddForm.patchValue({
      exempt_amount: totalValueSum,
      total_to_pay: totalValueSum,
      product_item: this.invoiceProductDatas
    })    
  }

  productAddInput($event) {
    this.invoiceProductDatas.push(
      $event
    )
    
    this.productTotalPriceSum()
  }

  invoiceProductData() {
    this.invoiceProductDatas = []    
  }

  invoiceProductRemove(item_index) {
    this.invoiceProductDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }  

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }             
    ]
  }

  customerAddInput($event) {
    this.quotationAddForm.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  }  

  // search employee dialog
  searchEmployeeOpen() {
    this.searchEmployeeVisible = true
  }

  searchEmployeeClose() {
    this.searchEmployeeVisible = false
  }   

  employeeData() {
    this.employeeDatas = [
      {
        employee_id: '1',
        employee_identification_no: '1112211',
        employee_firstname: 'Jeffrey',
        employee_lastname: 'Wilson',
        employee_phone: "",
        employee_mobile: "",
        employee_email: "jeffrey@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '2',
        employee_identification_no: '1112222',
        employee_firstname: 'Richard',
        employee_lastname: 'Peterson',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "richard@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '3',
        employee_identification_no: '1112233',
        employee_firstname: 'Dennis',
        employee_lastname: 'Beck',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "dennis@gmail.com",
        employee_type: "1"
      },
      {
        employee_id: '4',
        employee_identification_no: '1112244',
        employee_firstname: 'Brian',
        employee_lastname: 'Holmes',
        employee_phone: "0731525374",
        employee_mobile: "1213141516",
        employee_email: "brian@gmail.com",
        employee_type: "1"
      }               
    ]
  }

  employeeAddInput($event) {
    this.quotationAddForm.patchValue({
      employee_id: $event.employee_id,
      employee_name: $event.employee_firstname +' '+ $event.employee_lastname
    })

    this.searchEmployeeClose()    
  }

}
