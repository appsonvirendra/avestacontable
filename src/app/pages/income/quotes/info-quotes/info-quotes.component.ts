import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-info-quotes',
  templateUrl: './info-quotes.component.html',
  styleUrls: ['./info-quotes.component.scss']
})
export class InfoQuotesComponent implements OnInit {

  quotation_id: any

  quotationDatas: any

  generatePaymentDatas: any
  
  // send invoice mail form
  sendInvoiceMailForm: FormGroup  
  sendInvoiceMailVisible = false  

  currentDate:any = new Date()

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()

    this.quotationInfo()
    this.generatePaymentInfo()
    
    this.sendInvoiceMailFormInit()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.quotation_id = params.quotation_id
    })    
  }  

  quotationInfo() {
    this.quotationDatas = 
      {
        "quotation_id": "1",
        "quotation_order_no": "000-111-2222-0033",
        "quotation_date": "2019-12-27 13:25:02",
        "quotation_discount": "0",
        "quotation_supplier_id": "1",
        "quotation_supplier_name": "John brawn",
        "quotation_exchange_rate": "123",
        "quotation_receive_product": null,
        "quotation_notes": null,
        "quotation_exempt_amount": 9,
        "quotation_taxed_amount": 7,
        "quotation_isv_amount": "0",
        "quotation_final_discount": "0",
        "quotation_total_to_pay": 9,
        "quotation_status": "2",
        "quotation_product_item": [
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "0",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "1",
            "item_total": 0,
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "3",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "3",
            "item_total": 9,
            "item_cellar": "Bodega Principal"
          }
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
      }           
    ]
  }
  
  // send invoice mail form
  sendInvoiceMailFormInit() {
    this.sendInvoiceMailForm = this.fb.group({
      send_invoice_email1: [null, [Validators.required]],
      send_invoice_email2: [null],
      send_invoice_email3: [null]
    })      
  }  

  sendInvoiceMailOpen() {
    this.sendInvoiceMailVisible = true
  }

  sendInvoiceMailClose() {
    this.sendInvoiceMailForm.reset()
    this.sendInvoiceMailVisible = false
  }      

  sendInvoiceMailSave() {    
    for (const i in this.sendInvoiceMailForm.controls) {
      this.sendInvoiceMailForm.controls[i].markAsDirty()
      this.sendInvoiceMailForm.controls[i].updateValueAndValidity()
    }          
    if(this.sendInvoiceMailForm.valid) {
      this.sendInvoiceMailClose()
    }
  }

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
