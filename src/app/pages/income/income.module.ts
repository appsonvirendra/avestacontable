import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';

import { IncomeRoutingModule } from './income-routing.module';
import { InvoiceListComponent } from './invoice/invoice-list/invoice-list.component';
import { NewInvoiceComponent } from './invoice/new-invoice/new-invoice.component';
import { InfoInvoiceComponent } from './invoice/info-invoice/info-invoice.component';
import { InvoiceTableComponent } from './invoice/invoice-list/invoice-table/invoice-table.component';
import { InvoiceGeneratePaymentComponent } from './invoice/info-invoice/invoice-generate-payment/invoice-generate-payment.component';
import { PaymentsReceivedComponent } from './payments-received/payments-received.component';
import { PaymentsReceivedTableComponent } from './payments-received/payments-received-table/payments-received-table.component';
import { ReturnsListComponent } from './returns/returns-list/returns-list.component';
import { ReturnsTableComponent } from './returns/returns-list/returns-table/returns-table.component';
import { NewReturnsComponent } from './returns/new-returns/new-returns.component';
import { InfoReturnsComponent } from './returns/info-returns/info-returns.component';
import { QuotesListComponent } from './quotes/quotes-list/quotes-list.component';
import { QuotesFilterComponent } from './quotes/quotes-list/quotes-filter/quotes-filter.component';
import { QuotesTableComponent } from './quotes/quotes-list/quotes-table/quotes-table.component';
import { NewQuotesComponent } from './quotes/new-quotes/new-quotes.component';
import { InfoQuotesComponent } from './quotes/info-quotes/info-quotes.component';
import { QuotesGenerateInvoiceComponent } from './quotes/info-quotes/quotes-generate-invoice/quotes-generate-invoice.component';
import { WorkOrdersListComponent } from './work-orders/work-orders-list/work-orders-list.component';
import { WorkOrdersFilterComponent } from './work-orders/work-orders-list/work-orders-filter/work-orders-filter.component';
import { WorkOrdersTableComponent } from './work-orders/work-orders-list/work-orders-table/work-orders-table.component';
import { NewWorkOrdersComponent } from './work-orders/new-work-orders/new-work-orders.component';
import { InfoWorkOrdersComponent } from './work-orders/info-work-orders/info-work-orders.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { EditQuotesComponent } from './quotes/edit-quotes/edit-quotes.component';
import { EditReturnComponent } from './returns/edit-return/edit-return.component';
import { EditWorkOrderComponent } from './work-orders/edit-work-order/edit-work-order.component';
import { CreditNotesComponent } from './credit-notes/credit-notes.component';
import { CreditNotesTablesComponent } from './credit-notes/credit-notes-tables/credit-notes-tables.component';
import { NewCreditNotesComponent } from './credit-notes/new-credit-notes/new-credit-notes.component';
import { InfoCreditNotesComponent } from './credit-notes/info-credit-notes/info-credit-notes.component';
import { EditCreditNotesComponent } from './credit-notes/edit-credit-notes/edit-credit-notes.component';
import { PaymentLinksListComponent } from './payment-links/payment-links-list/payment-links-list.component';
import { PaymentsLinksFilterComponent } from './payment-links/payment-links-list/payments-links-filter/payments-links-filter.component';
import { NewPaymentLinksComponent } from './payment-links/new-payment-links/new-payment-links.component';

@NgModule({
  declarations: [InvoiceListComponent, NewInvoiceComponent, InfoInvoiceComponent,InvoiceTableComponent, InvoiceGeneratePaymentComponent, PaymentsReceivedComponent, PaymentsReceivedTableComponent, ReturnsListComponent, ReturnsTableComponent, NewReturnsComponent, InfoReturnsComponent, QuotesListComponent, QuotesFilterComponent, QuotesTableComponent, NewQuotesComponent, InfoQuotesComponent, QuotesGenerateInvoiceComponent, WorkOrdersListComponent, WorkOrdersFilterComponent, WorkOrdersTableComponent, NewWorkOrdersComponent, InfoWorkOrdersComponent, EditInvoiceComponent, EditQuotesComponent, EditReturnComponent, EditWorkOrderComponent, CreditNotesComponent, CreditNotesTablesComponent, NewCreditNotesComponent, InfoCreditNotesComponent, EditCreditNotesComponent, PaymentLinksListComponent, PaymentsLinksFilterComponent, NewPaymentLinksComponent],
  imports: [
    CommonModule,
    IncomeRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule    
  ]
})
export class IncomeModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
