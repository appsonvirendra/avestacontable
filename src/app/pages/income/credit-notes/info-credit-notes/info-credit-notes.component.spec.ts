import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCreditNotesComponent } from './info-credit-notes.component';

describe('InfoCreditNotesComponent', () => {
  let component: InfoCreditNotesComponent;
  let fixture: ComponentFixture<InfoCreditNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoCreditNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCreditNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
