import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-info-credit-notes',
  templateUrl: './info-credit-notes.component.html',
  styleUrls: ['./info-credit-notes.component.scss']
})
export class InfoCreditNotesComponent implements OnInit {

  inforeference_id: any
  isVisible = false;
  infocreditnotesDatas: any
  infocreditDatas: any
  generatePaymentDatas: any  

  // create receipt form
  createReceiptForm: FormGroup  
  createReceiptVisible = false
  
  // send inforeference mail form
  sendInfoReferenceMailForm: FormGroup  
  sendInfoReferenceMailVisible = false  

  generatePaymentCollapse = false

  currentDate:any = new Date()

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder    
  ) { 

  }
  // generate payment view receipt  
  generatePayViewVisible = false  
  ngOnInit() {
    this.creditnotesInfo()
    this.languageTranslate()
    this.getQueryParams()

    this.infocreditnotesInfo()
    this.generatePaymentInfo()
    
    this.createReceiptFormInit()
    this.sendInfoReferenceMailFormInit()    
  }
  listOfData = [
    {
      key: '1',
      no: '1',
      description: 'Item 1',
      total_amount: 'L. 00.00',
      associated_tax: 'L. 00.00'
    },
    {
      key: '2',
      no: '2',
      description: 'Item 2',
      total_amount: 'L. 00.00',
      associated_tax: 'L. 00.00'
    },
    {
      key: '3',
      no: '3',
      description: 'Item 3',
      total_amount: 'L. 00.00',
      associated_tax: 'L. 00.00'
    }
  ];
creditnotesInfo() {
    this.infocreditDatas = 
      {

        "credit_date_of_issue": "01/01/2020",
        "credit_sender": "Inversiones Logisticas H & M S. de R.L."
      }
  } 
  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.inforeference_id = params.inforeference_id
    })    
  }  

  infocreditnotesInfo() {
    this.infocreditnotesDatas = 
      {
        "inforeference_order_no": "000-001-01-00000351",
        "inforeference_date": "2019-12-27 13:25:02",
        "inforeference_discount": "0",
        "inforeference_customer_id": "1",
        "inforeference_customer_name": "John brawn",
        "inforeference_exchange_rate": "123",
        "inforeference_receive_product": null,
        "inforeference_notes": null,
        "inforeference_exempt_amount": 9,
        "inforeference_taxed_amount": 7,
        "inforeference_isv_amount": "0",
        "inforeference_final_discount": "0",
        "inforeference_total_to_pay": 9,
        "inforeference_status": "2",
        "infocreditnotes_product_item": [
          {
            "item_no": "1",
            "item_description": "Item 1",
            "total_amount": "L. 00.00",
            "associated_tax": "L. 00.00"
          },
          {
            "item_no": "2",
            "item_description": "Item 2",
            "total_amount": "L. 00.00",
            "associated_tax": "L. 00.00"
          },
          {
            "item_no": "3",
            "item_description": "Item 3",
            "total_amount": "L. 00.00",
            "associated_tax": "L. 00.00"
          }
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
        "g_p_receipt_status": "1"
      }           
    ]
  }

  // create receipt form
  createReceiptFormInit() {
    this.createReceiptForm = this.fb.group({
      receipt_date_of_payment: [this.currentDate, [Validators.required]],
      receipt_payment_received: [null, [Validators.required]],
      receipt_bank_account: [null, [Validators.required]],
      receipt_method_of_payment: ['1', [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  createReceiptOpen() {
    this.createReceiptVisible = true
  }

  createReceiptClose() {
    this.createReceiptForm.reset()
    this.createReceiptVisible = false
  }      

  createReceiptSave() {        
    for (const i in this.createReceiptForm.controls) {
      this.createReceiptForm.controls[i].markAsDirty()
      this.createReceiptForm.controls[i].updateValueAndValidity()
    }    
    // console.log('this.createReceiptForm.value', this.createReceiptForm.value)
    if(this.createReceiptForm.valid) {
      this.createReceiptClose()
    }
  }  
  
  // send inforeference mail form
  sendInfoReferenceMailFormInit() {
    this.sendInfoReferenceMailForm = this.fb.group({
      send_inforeference_email1: [null, [Validators.required]],
      send_inforeference_email2: [null],
      send_inforeference_email3: [null]
    })      
  }  

  sendInfoReferenceMailOpen() {
    this.sendInfoReferenceMailVisible = true
  }

  sendInfoReferenceMailClose() {
    this.sendInfoReferenceMailForm.reset()
    this.sendInfoReferenceMailVisible = false
  }      

  sendInfoReferenceMailSave() {    
    for (const i in this.sendInfoReferenceMailForm.controls) {
      this.sendInfoReferenceMailForm.controls[i].markAsDirty()
      this.sendInfoReferenceMailForm.controls[i].updateValueAndValidity()
    }          
    if(this.sendInfoReferenceMailForm.valid) {
      this.sendInfoReferenceMailClose()
    }
  } 
  // generate payment view receipt start  
  generatePayViewOpen() {
    this.generatePayViewVisible = true
  }

  generatePayViewClose() {
    this.generatePayViewVisible = false
  }      

  generatePayViewPrint() {
    this.generatePayViewClose()
  }
  // generate payment collapse
  generatePyamentCollapse() {
    this.generatePaymentCollapse = !this.generatePaymentCollapse
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}



