import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface ItemnewcreditData {
  id: string;
  item: string;
  reference: string;
  price: string;
  discount: string;
  discription: string;
  tax: string;
  quantity: number;
  total: number;
}
interface ItemSaleInvoiceData {
  id: string;
  date: string;
  expiration: string;
  observations: string;
  total: string;
  paid_out: string;
  payable: string;
}
interface ItemmoneybackData {
  id: string;
  amount: string;
  Observations: string;
}
interface ItemData {
  id: string;
  name: string;
  age: string;
}

@Component({
  selector: 'app-new-credit-notes',
  templateUrl: './new-credit-notes.component.html',
  styleUrls: ['./new-credit-notes.component.scss']
})
export class NewCreditNotesComponent implements OnInit {

 referenceguideAddform: FormGroup
 senderDialogVisible = false
 addSenderForm: FormGroup

 reasonDialogVisible = false
 addReasonForm: FormGroup

  myBlog: string = "";

  newcreditnotesAddform: FormGroup
  i = 0;
  currentDate:any = new Date()
  customerAddDialogVisible = false
  addCustomerAddForm: FormGroup

  // New development
  itemSelect: any = []

  itemSelectSales: any = []
  
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.newcreditnotesFormInit()
    this.addCustomerAddFormInit()
    this.addnewcreditRow()
    this.addSenderFormInit()
    this.addReasonFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  newcreditnotesFormInit() {
    this.newcreditnotesAddform = this.fb.group({
      proof_issue_date: [null],
      date_of_issue: [null],
      sender: [null],
      correlative_of_the_voucher: [null],
      reason: [null],
      others_specify: [null],
      original: [null],
      client: [null, [Validators.required]],
      customer: [null],
      types_of_credit_notes: [null],
      price_list: [null],
      item: [null],
      reference: [null],
      price: [null],
      discount: [null],
      tax: [null],
      description: [null],
      amount: [null],
      sales_invoice: ['0']

    })      
  }

      // reason form
     addReasonFormInit() {
    this.addReasonForm = this.fb.group({
      reason: [null]
    })   
  }

  openReasonDialog() {
    this.reasonDialogVisible = true
  }

  closeReasonDialog() {
    this.reasonDialogVisible = false
  }

  saveReason() {
    for (const i in this.addReasonForm.controls) {
      this.addReasonForm.controls[i].markAsDirty()
      this.addReasonForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addReasonForm.value --', this.addReasonForm.value)
    if(this.addReasonForm.value.category_name != null && this.addReasonForm.value.category_name != '')  {
      this.reasonDialogVisible = false
      this.addReasonFormInit()
    }    
  }

  
  saveCredit() {
    for (const i in this.newcreditnotesAddform.controls) {
      this.newcreditnotesAddform.controls[i].markAsDirty()
      this.newcreditnotesAddform.controls[i].updateValueAndValidity()
    }
    if(this.newcreditnotesAddform.valid) {
      console.log('this.newcreditnotesAddform.value', this.newcreditnotesAddform.value)
    }
  }
  
      // sender form
     addSenderFormInit() {
    this.addSenderForm = this.fb.group({
      company_name: [null],
      c_a_i: [null],
      address: [null],
      email: [null],
      telephone: [null],
      expires: [null],
      image: [null],
      site_url: [null],
      rtn_tdi: [null],
    })   
  }

  openSenderDialog() {
    this.senderDialogVisible = true
  }

  closeSenderDialog() {
    this.senderDialogVisible = false
  }

  saveSender() {
    for (const i in this.addSenderForm.controls) {
      this.addSenderForm.controls[i].markAsDirty()
      this.addSenderForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addSenderForm.value --', this.addSenderForm.value)
    if(this.addSenderForm.value.category_name != null && this.addSenderForm.value.category_name != '')  {
      this.senderDialogVisible = false
      this.addSenderFormInit()
    }    
  } 
  

      // CustomerAdd form
     addCustomerAddFormInit() {
    this.addCustomerAddForm = this.fb.group({
       customer_type: ['1'],
      customer_tax_exempt: ['no'],
      customer_assign_seller: [null],
      customer_identification_card: [null],
      customer_firstname: [null, [Validators.required]],
      customer_lastname: [null, [Validators.required]],
      customer_birthdate: [null],
      customer_sex: [null],
      customer_company: [null, [Validators.required]],
      customer_identification: [null],
      customer_identification1: [null],
      customer_direction: [null],
      customer_country: [null],
      customer_city: [null],
      customer_phone: [null],
      customer_mobile: [null],
      customer_fax: [null],
      customer_postal_code: [null],
      customer_email: [null],
      customer_name1: [null],
      customer_lastname1: [null],
      customer_mobile1: [null],
      customer_email1: [null],
      customer_name2: [null],
      customer_lastname2: [null],
      customer_mobile2: [null],
      customer_email2: [null],
      customer_name3: [null],
      customer_lastname3: [null],
      customer_mobile3: [null],
      customer_email3: [null]
    })   
  }

  openNewCreditNotesDialog() {
    this.customerAddDialogVisible = true
  }

  closeCustomerAddDialog() {
    this.customerAddDialogVisible = false
  }

  saveCustomerAdd() {
    for (const i in this.addCustomerAddForm.controls) {
      this.addCustomerAddForm.controls[i].markAsDirty()
      this.addCustomerAddForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.addCustomerAddForm.value --', this.addCustomerAddForm.value)
    if(this.addCustomerAddForm.value.category_name != null && this.addCustomerAddForm.value.category_name != '')  {
      this.customerAddDialogVisible = false
      this.addCustomerAddFormInit()
    }    
  } 
// add line Begin
   
  listOfnewcreditData: ItemnewcreditData[] = [];


  addnewcreditRow(): void {
    this.listOfnewcreditData = [
      ...this.listOfnewcreditData,
      {
        id: `${this.i+1}`,
        item: ``,
        reference: '',
        price: '',
        tax: '',
        discount: '',
        discription: '',
        quantity: 0,
        total: 0
      }
    ];
    this.i++;
  }

  deletenewcreditRow(index): void {
    this.listOfnewcreditData.splice(index, 1)
  } 
// add line End

// add money back Begin
   
  listOfmoneybackData: ItemmoneybackData[] = [];


  addmoneybackRow(): void {
    this.listOfmoneybackData = [
      ...this.listOfmoneybackData,
      {
        id: `${this.i+1}`,
        amount: '',
        Observations: ''
      }
    ];
    this.i++;
  }

  deletemoneybackRow(index): void {
    this.listOfmoneybackData.splice(index, 1)
  } 
// add money back End

// add sales invoice begin

j = 0;
  editId: string | null = null;
  listOfSaleInvoiceData: ItemSaleInvoiceData[] = [];


  addsaleinvoiceRow(): void {
    if(this.listOfSaleInvoiceData.length < 1) {
      this.listOfSaleInvoiceData = [
        ...this.listOfSaleInvoiceData,
        {
          id: `${this.j}`,
          date: '',
          expiration: '',
          observations: '',
          total: '',
          paid_out: '',
          payable: ''
        }
      ];
      this.j++;
    }
  }

  deletesaleinvoiceRow(index): void {
    this.listOfSaleInvoiceData.splice(index, 1)
    this.itemSelectSales = []
  }

  // add sales invoice end

  // New development
  updateCreditItem(itemData, itemIndex) {

    let itemValue

    if(itemData == '1') {
      itemValue = {
        id: '1',
        item: 'item 1',
        reference: '1244',
        price: '10',
        discount: '0',
        tax: '0',
        discription: '',
        quantity: 1,
        total: 10      
      }      
    } else {
      itemValue = {
        id: '2',
        item: 'item 2',
        reference: '1245',
        price: '20',
        discount: '0',
        tax: '0',
        discription: '',
        quantity: 1,
        total: 20      
      }  
    }

    this.listOfnewcreditData[itemIndex] = itemValue
  }

  updateSalesItem(salesData, salesIndex) {

    let salesValue

    if(salesData == '1') {
      salesValue = {
        id: '1',
        date: '25/08/2020',
        expiration: '10/10/2020',
        observations: 'liamar a cobrar',
        total: 0,
        paid_out: 0,
        payable: 0   
      }      
    }

    this.listOfSaleInvoiceData[salesIndex] = salesValue
  }  


}
