import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-credit-notes',
  templateUrl: './credit-notes.component.html',
  styleUrls: ['./credit-notes.component.scss']
})
export class CreditNotesComponent implements OnInit {

  creditDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.creditData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.creditDatas = []
  }
  
  creditData() {
    this.creditDatas = [
      {
        credit_id: '1',
        credit_code: '000-001-01-001',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }, 
      {
        credit_id: '2',
        credit_code: '000-001-01-002',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        returns_provider_name: 'Provider 2',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      },
      {
        credit_id: '3',
        credit_code: '000-001-01-003',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        returns_provider_name: 'Provider 3',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }, 
      {
        credit_id: '4',
        credit_code: '000-001-01-004',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        returns_provider_name: 'Provider 1',
        returns_total: '104',
        returns_balance: '104',
        credit_creation: '2018-12-23'
      }                      
    ]
  }

}

