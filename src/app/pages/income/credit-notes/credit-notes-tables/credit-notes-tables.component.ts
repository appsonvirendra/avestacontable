import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-credit-notes-tables',
  templateUrl: './credit-notes-tables.component.html',
  styleUrls: ['./credit-notes-tables.component.scss']
})
export class CreditNotesTablesComponent implements OnInit {
// modal loading
isVisible = false;
isConfirmLoading = false;

  @Input() creditDatas: any
  
  // search
  search_title = ''
  creditDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }
  // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
    this.creditData()
  }

  creditData() {
    this.creditDisplayDatas = [...this.creditDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { credit_code: string; }) => {
      return (
        item.credit_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.creditDatas.filter((item: { credit_code: string; }) => filterFunc(item))

    this.creditDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.creditDisplayDatas = [...this.creditDatas]
    }    
  }

}

