import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditNotesTablesComponent } from './credit-notes-tables.component';

describe('CreditNotesTablesComponent', () => {
  let component: CreditNotesTablesComponent;
  let fixture: ComponentFixture<CreditNotesTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditNotesTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditNotesTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
