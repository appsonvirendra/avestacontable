import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceListComponent } from './invoice/invoice-list/invoice-list.component';
import { NewInvoiceComponent } from './invoice/new-invoice/new-invoice.component';
import { InfoInvoiceComponent } from './invoice/info-invoice/info-invoice.component';
import { PaymentsReceivedComponent } from './payments-received/payments-received.component';
import { ReturnsListComponent } from './returns/returns-list/returns-list.component';
import { NewReturnsComponent } from './returns/new-returns/new-returns.component';
import { InfoReturnsComponent } from './returns/info-returns/info-returns.component';
import { QuotesListComponent } from './quotes/quotes-list/quotes-list.component';
import { NewQuotesComponent } from './quotes/new-quotes/new-quotes.component';
import { InfoQuotesComponent } from './quotes/info-quotes/info-quotes.component';
import { QuotesGenerateInvoiceComponent } from './quotes/info-quotes/quotes-generate-invoice/quotes-generate-invoice.component';
import { WorkOrdersListComponent } from './work-orders/work-orders-list/work-orders-list.component';
import { NewWorkOrdersComponent } from './work-orders/new-work-orders/new-work-orders.component';
import { InfoWorkOrdersComponent } from './work-orders/info-work-orders/info-work-orders.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { EditQuotesComponent } from './quotes/edit-quotes/edit-quotes.component';
import { EditReturnComponent } from './returns/edit-return/edit-return.component';
import { EditWorkOrderComponent } from './work-orders/edit-work-order/edit-work-order.component';
import { CreditNotesComponent } from './credit-notes/credit-notes.component';
import { NewCreditNotesComponent } from './credit-notes/new-credit-notes/new-credit-notes.component';
import { InfoCreditNotesComponent } from './credit-notes/info-credit-notes/info-credit-notes.component';
import { EditCreditNotesComponent } from './credit-notes/edit-credit-notes/edit-credit-notes.component';
import { PaymentLinksListComponent } from './payment-links/payment-links-list/payment-links-list.component';
import { NewPaymentLinksComponent } from './payment-links/new-payment-links/new-payment-links.component';

const routes: Routes = [
  {
    path: 'invoice/list',
    component: InvoiceListComponent    
  },
  {
    path: 'invoice/new',
    component: NewInvoiceComponent    
  },
  {
    path: 'invoice/info',
    component: InfoInvoiceComponent    
  },
  {
    path: 'invoice/edit',
    component: EditInvoiceComponent    
  },  
  {
    path: 'payments-received/list',
    component: PaymentsReceivedComponent    
  },
  {
    path: 'returns/list',
    component: ReturnsListComponent    
  },
  {
    path: 'returns/new',
    component: NewReturnsComponent     
  },
  {
    path: 'returns/info',
    component: InfoReturnsComponent     
  },
  {
    path: 'returns/edit',
    component: EditReturnComponent     
  },
  {
    path: 'quotes/list',
    component: QuotesListComponent    
  },
  {
    path: 'quotes/new',
    component: NewQuotesComponent     
  },
  {
    path: 'quotes/info',
    component: InfoQuotesComponent     
  },
  {
    path: 'quotes/edit',
    component: EditQuotesComponent     
  },
  {
    path: 'quotes/generate-invoice',
    component: QuotesGenerateInvoiceComponent     
  },
  {
    path: 'work-orders/list',
    component: WorkOrdersListComponent    
  },
  {
    path: 'work-orders/new',
    component: NewWorkOrdersComponent
  },
  {
    path: 'work-orders/edit',
    component: EditWorkOrderComponent
  },
  {
    path: 'work-orders/info',
    component: InfoWorkOrdersComponent  
  },
  {
    path: 'credit-notes/list',
    component: CreditNotesComponent  
  },
  {
    path: 'credit-notes/new',
    component: NewCreditNotesComponent  
  },
  {
    path: 'credit-notes/info',
    component: InfoCreditNotesComponent  
  },
  {
    path: 'credit-notes/edit',
    component: EditCreditNotesComponent  
  },
  {
    path: 'online-payment',
    component: PaymentLinksListComponent  
  },
  {
    path: 'online-payment/create',
    component: NewPaymentLinksComponent  
  }              
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncomeRoutingModule { }
