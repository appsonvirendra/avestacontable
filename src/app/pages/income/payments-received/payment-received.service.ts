import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaymentReceivedService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getPaymentReceivedList(bodyparams) {
    return this.apiService.get(`/paymentReceivedList`, bodyparams)
      .pipe(map(data => data.data))
  }  

}
