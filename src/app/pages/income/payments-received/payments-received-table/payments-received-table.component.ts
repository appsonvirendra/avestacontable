import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payments-received-table',
  templateUrl: './payments-received-table.component.html',
  styleUrls: ['./payments-received-table.component.scss']
})
export class PaymentsReceivedTableComponent implements OnInit {

  @Input() paymentReceivedDatas: any

  // search
  search_title = ''
  invoiceDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { }

  ngOnInit() {
    this.invoiceData()
  }

  invoiceData() {
    this.invoiceDisplayDatas = [...this.paymentReceivedDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { invoice_code: string; }) => {
      return (
        item.invoice_code.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.paymentReceivedDatas.filter((item: { invoice_code: string; }) => filterFunc(item))

    this.invoiceDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.invoiceDisplayDatas = [...this.paymentReceivedDatas]
    }    
  }  

}
