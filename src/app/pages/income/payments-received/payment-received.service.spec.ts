import { TestBed } from '@angular/core/testing';

import { PaymentReceivedService } from './payment-received.service';

describe('PaymentReceivedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentReceivedService = TestBed.get(PaymentReceivedService);
    expect(service).toBeTruthy();
  });
});
