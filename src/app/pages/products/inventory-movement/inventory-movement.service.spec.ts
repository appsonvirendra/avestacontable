import { TestBed } from '@angular/core/testing';

import { InventoryMovementService } from './inventory-movement.service';

describe('InventoryMovementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InventoryMovementService = TestBed.get(InventoryMovementService);
    expect(service).toBeTruthy();
  });
});
