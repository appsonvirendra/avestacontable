import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inventory-movement-table',
  templateUrl: './inventory-movement-table.component.html',
  styleUrls: ['./inventory-movement-table.component.scss']
})
export class InventoryMovementTableComponent implements OnInit {

	@Input() inventoryMovementData: any
	@Input() entryTypeShow: any
	@Input() exitTypeShow: any
	@Input() transferTypeShow: any

  constructor() { }

  ngOnInit() {
  }

  inventoryMovementRemove(item_index) {
    this.inventoryMovementData.splice(item_index, 1)
  }

}
