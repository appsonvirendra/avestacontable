import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { SalesService } from 'src/app/pages/products/items/sales.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';
import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { InventoryMovementService } from 'src/app/pages/products/inventory-movement/inventory-movement.service';

@Component({
  selector: 'app-inventory-movement',
  templateUrl: './inventory-movement.component.html',
  styleUrls: ['./inventory-movement.component.scss']
})
export class InventoryMovementComponent implements OnInit {

  inventoryMovementDatas = []

  // inventory movement form
  inventoryMovementForm: FormGroup

  // supplier search dialog
  searchProductNormalVisible = false

  // supplier search dialog
  searchSupplierVisible = false

  loader: boolean = false
  saleDatas: any = []

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  branchDatas: any = []

  suplierLoader: boolean = false
  supplierDatas: any = []

  // Movement type entry required/unrequired and hide/show
  entryTypeRequired: boolean = true
  entryTypeShow: boolean = true
  exitTypeRequired: boolean = false
  exitTypeShow: boolean = false
  transferTypeRequired: boolean = false
  transferTypeShow: boolean = false

  wineryLoader: boolean = false
  wineryDatas: any = []
  defaultWineryData: any = {}  

  inventoryBtnLoading: boolean = false

  base64String:String = null

  movementTypeSelectDisable: boolean = false

  movementDetailsArray: any = []

  saleDataLoader: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _salesService: SalesService,
    private message: NzMessageService,
    private _branchService: BranchService,
    private _jwtService: JwtService,
    private authService: AuthenticationService,
    private _suppliersService: SuppliersService,
    private _wineriesService: WineriesService,    
    private _inventoryMovementService: InventoryMovementService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    if(this.companyBranchId) {
      this.populateGetSalesList()
      this.populateGetSupplierList()
      this.populateGetWineryList()
    } else {
      this.populateGetBranchList()
    }     

    this.inventoryMovementFormInit()
  }

  movementTypeSelection(value) {

    if(value == '1') {
      // Condition for required
      this.entryTypeRequired = true

      this.exitTypeRequired = false
      this.transferTypeRequired = false

      // Timeout for detail section hide/show
      setTimeout(() => {
        this.entryTypeShow = true

        this.exitTypeShow = false
        this.transferTypeShow = false
      }, 1000)          
    } else if(value == '2') {
      // Condition for required
      this.exitTypeRequired = true
      
      this.entryTypeRequired = false
      this.transferTypeRequired = false

      // Timeout for detail section hide/show
      setTimeout(() => {
        this.exitTypeShow = true
        
        this.entryTypeShow = false
        this.transferTypeShow = false
      }, 1000)      

    } else {
      // Condition for required
      this.transferTypeRequired = true
      
      this.entryTypeRequired = false
      this.exitTypeRequired = false

      // Timeout for detail section hide/show
      setTimeout(() => {
          this.transferTypeShow = true
          
          this.entryTypeShow = false
          this.exitTypeShow = false
      }, 1000)      

    } 
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  } 

  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response

      // branch id store in local storage and asign to select box
      if(!!localStorage.getItem('companyBranchId')) {
        this.companyBranchId = localStorage.getItem('companyBranchId')
      } else {
        localStorage.setItem('companyBranchId', this.branchDatas[0].branch_id)
        this.companyBranchId = this.branchDatas[0].branch_id
      }      

      this.loader = false

      // Populate get sales list api and supplier list api
      this.populateGetSalesList()
      this.populateGetSupplierList()      
      this.populateGetWineryList()      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })     
  }  

  populateGetSalesList() {
    this.saleDataLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._salesService.getSalesList(params).subscribe((response) => {
      this.saleDatas = response            
      this.openMessageBar(this.translate.instant('PRODUCT_LIST_RECEIVED_SUCCESSFUL'))

      this.saleDataLoader = false      
    }, (error) => {
      this.saleDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSalesList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetSupplierList() {
    this.suplierLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._suppliersService.getSupplierList(params).subscribe((response) => {
      this.supplierDatas = response
      this.openMessageBar(this.translate.instant('SUPPLIER_LIST_RECEIVED_SUCCESSFUL'))

      this.suplierLoader = false      
    }, (error) => {
      this.suplierLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSupplierList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetWineryList() {
    this.wineryLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      this.wineryDatas = response

      // Asign default winery data
      this.wineryDatas.forEach(element => {
        if(element.default_winery) {
          this.defaultWineryData = element
        }
      })

      this.openMessageBar(this.translate.instant('WINERY_LIST_RECEIVED_SUCCESSFUL'))

      this.wineryLoader = false      
    }, (error) => {
      this.wineryLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }  

  // generate payment edit form start  
  inventoryMovementFormInit() {
    this.inventoryMovementForm = this.fb.group({
      movement_type: ['1'],
      product_id: [null],
      product_name: [null],
      transaction: [null],
      existance: [null],
      quantity: [null],
      supplier_id: [null],
      supplier_name: [null],
      cost_price: [null],
      sale_price: [null],
      observation: [null],
      invoice_number: [null],
      document: [null],
      origin_winery_id: [null],
      destination_winery_id: [null]
    })      
  }
  
  inventoryMovementAdd() {              
    this.inventoryMovementDatas.push(this.inventoryMovementForm.value)

    this.clearFilterType(this.inventoryMovementForm.value.movement_type)    
  }

  clearFilterType(type) {    
    this.movementTypeSelectDisable = true

    // Clear form type 1 for entry, type 2 for exit and type 3 for transfer 
    if(type == '1') {   

      this.inventoryMovementForm.controls['product_id'].reset()
      this.inventoryMovementForm.controls['product_name'].reset()
      this.inventoryMovementForm.controls['transaction'].reset()
      this.inventoryMovementForm.controls['origin_winery_id'].reset()
      this.inventoryMovementForm.controls['existance'].reset()
      this.inventoryMovementForm.controls['quantity'].reset()
      this.inventoryMovementForm.controls['supplier_id'].reset()
      this.inventoryMovementForm.controls['supplier_name'].reset()
      this.inventoryMovementForm.controls['cost_price'].reset()
      this.inventoryMovementForm.controls['sale_price'].reset()
      this.inventoryMovementForm.controls['invoice_number'].reset()
      this.inventoryMovementForm.controls['observation'].reset()      

    } else if(type == '2') {

      this.inventoryMovementForm.controls['product_id'].reset()
      this.inventoryMovementForm.controls['product_name'].reset()
      this.inventoryMovementForm.controls['transaction'].reset()
      this.inventoryMovementForm.controls['origin_winery_id'].reset()
      this.inventoryMovementForm.controls['existance'].reset()
      this.inventoryMovementForm.controls['quantity'].reset()
      this.inventoryMovementForm.controls['observation'].reset()      

    } else {
      
      this.inventoryMovementForm.controls['product_id'].reset()
      this.inventoryMovementForm.controls['product_name'].reset()
      this.inventoryMovementForm.controls['origin_winery_id'].reset()
      this.inventoryMovementForm.controls['existance'].reset()
      this.inventoryMovementForm.controls['quantity'].reset()
      this.inventoryMovementForm.controls['observation'].reset()
      this.inventoryMovementForm.controls['destination_winery_id'].reset()
      
    }
  }  

  clearFilter() {
    this.movementTypeSelectDisable = false

    this.inventoryMovementForm.controls['product_id'].reset()
    this.inventoryMovementForm.controls['product_name'].reset()
    this.inventoryMovementForm.controls['transaction'].reset()
    this.inventoryMovementForm.controls['origin_winery_id'].reset()
    this.inventoryMovementForm.controls['existance'].reset()
    this.inventoryMovementForm.controls['quantity'].reset()
    this.inventoryMovementForm.controls['supplier_id'].reset()
    this.inventoryMovementForm.controls['supplier_name'].reset()
    this.inventoryMovementForm.controls['cost_price'].reset()
    this.inventoryMovementForm.controls['sale_price'].reset()
    this.inventoryMovementForm.controls['invoice_number'].reset()
    this.inventoryMovementForm.controls['observation'].reset()    

    this.inventoryMovementDatas = []
  }

  handleFileSelect(evt) {
    var files = evt.target.files
    var file = files[0]

    if (files && file) {
      var reader = new FileReader()
      reader.onload =this._handleReaderLoaded.bind(this)
      reader.readAsBinaryString(file)
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result
    this.base64String = btoa(binaryString)
  }  

  insertInventoryMovement() {
    this.movementDetailsArray = []

    // Creating movement detail api parameter based on type 1 for entry, 2 for exit, 3 for transfer
    if(this.inventoryMovementForm.value.movement_type == '1') {

      this.inventoryMovementDatas.forEach(value => {
        this.movementDetailsArray.push(
          {
            product_id: value.product_id,
            transaction: value.transaction,
            origin_winery_id: value.origin_winery_id,
            existance: value.existance,
            quantity: value.quantity,
            supplier_id: value.supplier_id,
            cost_price: value.cost_price,
            sale_price: value.sale_price,
            invoice_number: value.invoice_number,
            observation: value.observation,
            document: this.base64String
          }
        )
      }) 

    } else if(this.inventoryMovementForm.value.movement_type == '2') {    

      this.inventoryMovementDatas.forEach(value => {
        this.movementDetailsArray.push(
          {
            product_id: value.product_id,
            transaction: value.transaction,
            origin_winery_id: value.origin_winery_id,
            existance: value.existance,
            quantity: value.quantity,
            observation: value.observation         
          }
        )
      })      

    } else {      

      this.inventoryMovementDatas.forEach(value => {
        this.movementDetailsArray.push(
          {
            product_id: value.product_id,
            origin_winery_id: value.origin_winery_id,
            existance: value.existance,
            quantity: value.quantity,
            observation: value.observation,
            destination_winery_id: value.destination_winery_id            
          }
        )
      })

    }

    this.inventoryBtnLoading = true

    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      movement_type: this.inventoryMovementForm.value.movement_type,
      movementDetails: this.movementDetailsArray
    }

    this._inventoryMovementService.postInventoryMovement(params).subscribe((response) => {
      this.inventoryBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.clearFilter()
    }, (error) => {
      this.inventoryBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertInventoryMovement')
    })
  }
  
  // search product normal dialog
  searchProductNormalOpen() {
    this.searchProductNormalVisible = true
  }

  searchProductNormalClose() {
    this.searchProductNormalVisible = false
  }   

  productNormalAddInput($event) {
    this.inventoryMovementForm.patchValue({
      product_id: $event.product_id,
      product_name: $event.product_name,
      existance: $event.minimum_existence
    })

    this.searchProductNormalClose()    
  }  

  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierAddInput($event) {
    this.inventoryMovementForm.patchValue({
      supplier_id: $event.supplier_id,
      supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  }      

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}
