import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InventoryMovementService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  postInventoryMovement(bodyparams) {
    return this.apiService.post(`/inventoryMovement`, bodyparams)
      .pipe(map(data => data))
  }  

}
