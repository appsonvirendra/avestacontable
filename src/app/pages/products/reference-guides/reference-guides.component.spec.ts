import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceGuidesComponent } from './reference-guides.component';

describe('ReferenceGuidesComponent', () => {
  let component: ReferenceGuidesComponent;
  let fixture: ComponentFixture<ReferenceGuidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceGuidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
