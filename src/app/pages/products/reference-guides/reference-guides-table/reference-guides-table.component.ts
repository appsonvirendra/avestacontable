import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ReferenceGuidesService } from 'src/app/pages/products/reference-guides/reference-guides.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-reference-guides-table',
  templateUrl: './reference-guides-table.component.html',
  styleUrls: ['./reference-guides-table.component.scss']
})
export class ReferenceGuidesTableComponent implements OnInit {

  @Input() referenceGuidesDatas: any

  // search
  search_title = ''  
  referenceguideDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null 
  
  deleteConfirmVisible = false

  deleteReferenceGuideId: any

  deleteBtnLoading: boolean = false

  @Output("populateGetReferenceGuidesList") populateGetReferenceGuidesList: EventEmitter<any> = new EventEmitter()  

  constructor(
    private translate: TranslateService,
    private _referenceGuidesService: ReferenceGuidesService,
    private message: NzMessageService
  ) { 

  }

  ngOnInit() {
    this.invoiceData()
  }

  invoiceData() {
    this.referenceguideDisplayDatas = [...this.referenceGuidesDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { reference_guide_id: string; start_date: string; finish_date: string; total_amount: string }) => {
      return (
        item.reference_guide_id.toLowerCase().indexOf(searchLower) !== -1 ||
        item.start_date.toLowerCase().indexOf(searchLower) !== -1 ||
        item.finish_date.toLowerCase().indexOf(searchLower) !== -1 ||
        item.total_amount.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.referenceGuidesDatas.filter((item: { reference_guide_id: string; start_date: string; finish_date: string; total_amount: string }) => filterFunc(item))

    this.referenceguideDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.referenceguideDisplayDatas = [...this.referenceGuidesDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteReferenceGuideId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }    

  deleteReferenceGuides() {         
    this.deleteBtnLoading = true

    this._referenceGuidesService.deleteReferenceGuides(this.deleteReferenceGuideId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get reference guides list api
        this.populateGetReferenceGuidesList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteReferenceGuides')
      this.deleteConfirmClose()
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}

