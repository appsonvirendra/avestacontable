import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceGuidesTableComponent } from './reference-guides-table.component';

describe('ReferenceGuidesTableComponent', () => {
  let component: ReferenceGuidesTableComponent;
  let fixture: ComponentFixture<ReferenceGuidesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceGuidesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceGuidesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
