import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReferenceGuidesComponent } from './new-reference-guides.component';

describe('NewReferenceGuidesComponent', () => {
  let component: NewReferenceGuidesComponent;
  let fixture: ComponentFixture<NewReferenceGuidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReferenceGuidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReferenceGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
