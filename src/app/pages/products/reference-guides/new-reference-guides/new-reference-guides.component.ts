import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { SenderService } from 'src/app/pages/products/sender/sender.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { ReasonService } from 'src/app/pages/settings/inventory/reason/reason.service';

import { SalesService } from 'src/app/pages/products/items/sales.service';

import { Router, ActivatedRoute } from '@angular/router';

import { ReferenceGuidesService } from 'src/app/pages/products/reference-guides/reference-guides.service';

import { EmployeeService } from 'src/app/pages/employees/employee.service';

import * as _moment from 'moment';
const moment = _moment

@Component({
  selector: 'app-new-reference-guides',
  templateUrl: './new-reference-guides.component.html',
  styleUrls: ['./new-reference-guides.component.scss']
})

export class NewReferenceGuidesComponent implements OnInit {

  reference_guide_id: any
  pageType: any  

  referenceGuidesForm: FormGroup

  saveBtnLoading: boolean = false

  senderDatas: any = []
  senderDataLoader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')  

  branchDatas: any = []  

  senderDialogVisible = false
  senderForm: FormGroup

  reasonDialogVisible = false
  addReasonForm: FormGroup
  currentDate:any = new Date()  
  
  // product search dialog
  searchProductVisible = false

  // product data 
  productDatas = []

  base64String:String = null

  // Format date
  format = 'YYYY-MM-DD'  

  btnLoadingAddSender: boolean = false

  reasonDatas: any = []
  reasonDataLoader: boolean = false  

  btnLoadingAddReason: boolean = false

  loader: boolean = false

  saleDataLoader: boolean = false
  saleDatas: any = []

  referenceGuidesDatas: any
  referenceGuidesDataLoader: any
  referenceGuidesDataVisible: any  

  employeeDatas: any = []
  employeeDataLoader: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _branchService: BranchService,
    private _senderService: SenderService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService,
    private _reasonService: ReasonService,
    private _salesService: SalesService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _referenceGuidesService: ReferenceGuidesService,
    private _employeeService: EmployeeService        
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    if(this.companyBranchId) {
      this.populateGetBranchListForSelect()
      this.populateGetReasonList()
      this.populateGetSenderList()
      this.populateGetEmployeeList()
      this.populateGetSalesList()
    } else {
      this.populateGetBranchList()
    }        
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response

      console.log('this.branchDatas', this.branchDatas)

      // branch id store in local storage and asign to select box
      if(!!localStorage.getItem('companyBranchId')) {
        this.companyBranchId = localStorage.getItem('companyBranchId')
      } else {
        localStorage.setItem('companyBranchId', this.branchDatas[0].branch_id)
        this.companyBranchId = this.branchDatas[0].branch_id
      }      

      this.loader = false

      // Populate get sales, sender, reason list api
      this.populateGetReasonList()            
      this.populateGetSenderList()
      this.populateGetEmployeeList()
      this.populateGetSalesList()
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })     
  }     

  // Branch list api for set data to the select box
  populateGetBranchListForSelect() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response

      console.log('this.branchDatas', this.branchDatas)     

      this.loader = false
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })     
  }  

  populateGetReasonList() {
    this.reasonDataLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._reasonService.getReasonList(params).subscribe((response) => {
      this.reasonDatas = response
      this.openMessageBar(this.translate.instant('REASON_LIST_RECEIVED_SUCCESSFUL'))      

      this.reasonDataLoader = false      
    }, (error) => {
      this.reasonDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getReasonList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetSenderList() {
    this.senderDataLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._senderService.getSenderList(params).subscribe((response) => {
      this.senderDatas = response
      this.openMessageBar(this.translate.instant('SENDER_LIST_RECEIVED_SUCCESSFUL'))

      this.senderDataLoader = false      
    }, (error) => {
      this.senderDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSenderList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }   

  populateGetEmployeeList() {
    this.employeeDataLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._employeeService.getEmployeeList(params).subscribe((response) => {
      this.employeeDatas = response
      this.employeeDatas.forEach(element => {
        element.employee_full_name = element.first_name + ' ' + element.last_name
      })
      this.openMessageBar(this.translate.instant('EMPLOYEE_LIST_RECEIVED_SUCCESSFUL'))      

      this.employeeDataLoader = false      
    }, (error) => {
      this.employeeDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getEmployeeList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetSalesList() {
    this.saleDataLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._salesService.getSalesList(params).subscribe((response) => {
      this.saleDatas = response  
      this.openMessageBar(this.translate.instant('PRODUCT_LIST_RECEIVED_SUCCESSFUL'))

      this.getQueryParams()
      
      this.saleDataLoader = false      
    }, (error) => {
      this.saleDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSalesList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  
  
  // Get current query params
  getQueryParams() {
    this._activatedRoute.queryParams.subscribe(params => { 
      this.reference_guide_id = params.reference_guide_id  
      if(this.reference_guide_id) {
        this.pageType = 'edit'
        this.populateGetReferenceGuides()
      } else {        
        this.pageType = 'new'
        this.referenceGuidesFormInitNew()
        this.referenceGuidesDataVisible = true
      }
    })    
  }  

  populateGetReferenceGuides() {
    this.referenceGuidesDataLoader = true
    let params = {
      reference_guide_id: this.reference_guide_id
    }
    this._referenceGuidesService.getSingleReferenceGuides(params).subscribe((response) => {
      this.referenceGuidesDatas = response

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.referenceGuidesFormInitEdit()

      this.referenceGuidesDataLoader = false     
      this.referenceGuidesDataVisible = true 
    }, (error) => {
      this.referenceGuidesDataLoader = false
      this.referenceGuidesDataVisible = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleReferenceGuides')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  } 
  
  // edit reference guides form
  referenceGuidesFormInitEdit() {
    this.productDatas = this.referenceGuidesDatas.productsDetails
    this.referenceGuidesForm = this.fb.group({
      start_date: [this.referenceGuidesDatas.start_date],
      finish_date: [this.referenceGuidesDatas.finish_date],
      address: [this.referenceGuidesDatas.address],
      RTN_TDI: [this.referenceGuidesDatas.RTN_TDI],
      origin: [this.referenceGuidesDatas.origin],
      destination: [this.referenceGuidesDatas.destination],
      denomination: [this.referenceGuidesDatas.denomination],
      emited_by: [this.referenceGuidesDatas.emited_by],
      sender_company_id: [this.referenceGuidesDatas.sender_company_id],
      reason_id: [this.referenceGuidesDatas.reason_id],
      driver_first_name: [this.referenceGuidesDatas.driver_first_name],
      driver_last_name: [this.referenceGuidesDatas.driver_last_name],
      vehical_brand: [this.referenceGuidesDatas.vehical_brand],
      model: [this.referenceGuidesDatas.model],
      license_plate: [this.referenceGuidesDatas.license_plate],
      driver_license: [this.referenceGuidesDatas.driver_license],  
      transport_RTN_TDI: [this.referenceGuidesDatas.transport_RTN_TDI],
      total_amount: [parseFloat(this.referenceGuidesDatas.total_amount)],        
      products: this.referenceGuidesDatas.productsDetails
    })    
  }  
  
  referenceGuidesFormInitNew() {
    this.referenceGuidesForm = this.fb.group({
      start_date: [this.currentDate],
      finish_date: [this.currentDate],
      address: [null],
      RTN_TDI: [null],
      origin: [null],
      destination: [null],
      denomination: [null],
      emited_by: [null],
      sender_company_id: [null],
      reason_id: [null],
      driver_first_name: [null],
      driver_last_name: [null],
      vehical_brand: [null],
      model: [null],
      license_plate: [null],
      driver_license: [null],        
      transport_RTN_TDI: [null],
      total_amount: [0],        
      products: []     
    })      
  }

  saveReferenceGuides() {

    let params

    if(this.pageType == 'new') {
      params = {
        company_id: this.companyData.id,
        branch_id: this.companyBranchId,        
        start_date: moment(this.referenceGuidesForm.value.start_date).format(this.format),
        finish_date: moment(this.referenceGuidesForm.value.finish_date).format(this.format),
        address: this.referenceGuidesForm.value.address,
        RTN_TDI: this.referenceGuidesForm.value.RTN_TDI,
        origin: this.referenceGuidesForm.value.origin,
        destination: this.referenceGuidesForm.value.destination,
        denomination: this.referenceGuidesForm.value.denomination,
        emited_by: this.referenceGuidesForm.value.emited_by,
        sender_company_id: this.referenceGuidesForm.value.sender_company_id,
        reason_id: this.referenceGuidesForm.value.reason_id,
        driver_first_name: this.referenceGuidesForm.value.driver_first_name,
        driver_last_name: this.referenceGuidesForm.value.driver_last_name,
        vehical_brand: this.referenceGuidesForm.value.vehical_brand,
        model: this.referenceGuidesForm.value.model,
        license_plate: this.referenceGuidesForm.value.license_plate,
        driver_license: this.referenceGuidesForm.value.driver_license,
        transport_RTN_TDI: this.referenceGuidesForm.value.transport_RTN_TDI,
        total_amount: this.referenceGuidesForm.value.total_amount,
        products: this.productDatas
      }      
    } else {
      params = {
        referenceGuideDetails: {
          company_id: this.companyData.id,
          branch_id: this.companyBranchId,        
          start_date: moment(this.referenceGuidesForm.value.start_date).format(this.format),
          finish_date: moment(this.referenceGuidesForm.value.finish_date).format(this.format),
          address: this.referenceGuidesForm.value.address,
          RTN_TDI: this.referenceGuidesForm.value.RTN_TDI,
          origin: this.referenceGuidesForm.value.origin,
          destination: this.referenceGuidesForm.value.destination,
          denomination: this.referenceGuidesForm.value.denomination,
          emited_by: this.referenceGuidesForm.value.emited_by,
          sender_company_id: this.referenceGuidesForm.value.sender_company_id,
          reason_id: this.referenceGuidesForm.value.reason_id,
          driver_first_name: this.referenceGuidesForm.value.driver_first_name,
          driver_last_name: this.referenceGuidesForm.value.driver_last_name,
          vehical_brand: this.referenceGuidesForm.value.vehical_brand,
          model: this.referenceGuidesForm.value.model,
          license_plate: this.referenceGuidesForm.value.license_plate,
          driver_license: this.referenceGuidesForm.value.driver_license,
          transport_RTN_TDI: this.referenceGuidesForm.value.transport_RTN_TDI,
          total_amount: this.referenceGuidesForm.value.total_amount
        },
        products: this.productDatas
      }
    }

    // Call update method or insert method based on page type
    if (this.pageType == 'edit') {
      this.updateReferenceGuides(params)    
    } else {   
      this.insertReferenceGuides(params)
    }     
  }  

  updateReferenceGuides(itemRowData) {
    this.saveBtnLoading = true
    this._referenceGuidesService.putReferenceGuides(this.reference_guide_id, itemRowData).subscribe((response) => {
      this.saveBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to reference guides list page
        this.gotoReferenceGuidesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.saveBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateReferenceGuides')
    })  
  }
  
  insertReferenceGuides(itemRowData) {
    this.saveBtnLoading = true
    this._referenceGuidesService.postReferenceGuides(itemRowData).subscribe((response) => {
      this.saveBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to reference guides list page
        this.gotoReferenceGuidesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.saveBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertReferenceGuides')
    })    
  }  

  productAddInput(data) {
    console.log('data -->', data)
    // Check product code is exist or not
    if (this.productDatas.some((item) => item.product_code == data.product_code)) {
      // Find index and update object on perticular index
      let index = this.productDatas.map(item => item.product_code).indexOf(data.product_code)      
      this.productDatas[index] = data      
    } else {
      this.productDatas.push(
        data
      )      
    }
    
    this.productTotalPriceSum()
  }

  invoiceProductRemove(item_index) {
    this.productDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }  

  productTotalPriceSum() {
    let totalValueSum = 0

    this.productDatas.forEach(function (value) {
      totalValueSum += parseFloat(value.product_total_price)
    })

    this.referenceGuidesForm.patchValue({
      exempt_amount: totalValueSum,
      total_amount: totalValueSum,
      products: this.productDatas
    })    
  }   

  gotoReferenceGuidesList() {
    this._router.navigate(['/user/products/reference-guides/list'], {})
  }  

  // sender form init
  senderFormInit() {
    this.senderForm = this.fb.group({
      company_name: [null],
      RTN_TDI: [null],
      address: [null],
      email: [null],
      telephone: [null],
      CAI: [null],
      expires: [null],
      site_url: [null],
      image: [null]
    })   
  }

  openSenderDialog() {
    this.senderFormInit()
    this.senderDialogVisible = true
  }

  closeSenderDialog() {
    this.senderDialogVisible = false
  }

  handleFileSelect(evt) {
    var files = evt.target.files
    var file = files[0]

    if (files && file) {
      var reader = new FileReader()

      reader.onload = this._handleReaderLoaded.bind(this)

      reader.readAsBinaryString(file)
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result
    this.base64String = btoa(binaryString)
  }  

  insertSender() {
    // Change expires value format
    let changeDateFormat = ''
    if(this.senderForm.value.expires) {
      changeDateFormat = moment(this.senderForm.value.expires).format(this.format)
    }

    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      company_name: this.senderForm.value.company_name,
      RTN_TDI: this.senderForm.value.RTN_TDI,
      address: this.senderForm.value.address,
      email: this.senderForm.value.email,
      telephone: this.senderForm.value.telephone,
      CAI: this.senderForm.value.CAI,
      expires: changeDateFormat,
      site_url: this.senderForm.value.site_url,
      image: this.base64String
    }

    // If image value is null
    if(!this.base64String) {
      delete params.image
    }    

    this.btnLoadingAddSender = true
    this._senderService.postSender(params).subscribe((response) => {
      this.btnLoadingAddSender = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate sender list
        this.populateGetSenderList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.closeSenderDialog()
    }, (error) => {
      this.closeSenderDialog()

      this.btnLoadingAddSender = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertSender')
    })     
  } 

  // reason form
  addReasonFormInit() {
    this.addReasonForm = this.fb.group({
      reason: [null]
    })   
  }

  openReasonDialog() {
    this.reasonDialogVisible = true
    this.addReasonFormInit()
  }

  closeReasonDialog() {
    this.reasonDialogVisible = false
  }

  insertReason() {
    this.btnLoadingAddReason = true
    let params = {
      company_id: this.companyData.id,
      reason: this.addReasonForm.value.reason
    }

    this._reasonService.postReason(params).subscribe((response) => {
      this.btnLoadingAddReason = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate reason list page
        this.populateGetReasonList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.closeReasonDialog()
    }, (error) => {      
      this.closeReasonDialog()

      this.btnLoadingAddReason = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertReason')
    })    
  }
  
  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }    

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
  