import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReferenceGuidesService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getReferenceGuidesList(bodyparams) {
    return this.apiService.get(`/referenceGuidesList`, bodyparams)
      .pipe(map(data => data.data))
  } 
  
  postReferenceGuides(bodyparams) {
    return this.apiService.post(`/referenceGuides`, bodyparams)
      .pipe(map(data => data))
  }  

  getSingleReferenceGuides(bodyparams) {
    return this.apiService.get(`/referenceGuides`, bodyparams)
      .pipe(map(data => data.data[0]))
  } 
  
  putReferenceGuides(reference_guide_id, bodyparams) {
    return this.apiService.put(`/referenceGuides/`+reference_guide_id, bodyparams)
      .pipe(map(data => data))
  } 
  
  deleteReferenceGuides(reference_guide_id) {
    return this.apiService.delete(`/referenceGuides/`+reference_guide_id)
      .pipe(map(data => data))
  }  

}
