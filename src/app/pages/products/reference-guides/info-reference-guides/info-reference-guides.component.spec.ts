import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoReferenceGuidesComponent } from './info-reference-guides.component';

describe('InfoReferenceGuidesComponent', () => {
  let component: InfoReferenceGuidesComponent;
  let fixture: ComponentFixture<InfoReferenceGuidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoReferenceGuidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoReferenceGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
