import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { ReferenceGuidesService } from 'src/app/pages/products/reference-guides/reference-guides.service';

@Component({
  selector: 'app-info-reference-guides',
  templateUrl: './info-reference-guides.component.html',
  styleUrls: ['./info-reference-guides.component.scss']
})
export class InfoReferenceGuidesComponent implements OnInit {

  reference_guide_id: any

  deleteConfirmVisible = false

  referenceGuidesDatas: any
  referenceGuidesDataLoader: any
  referenceGuidesDataVisible: any  

  productQuantitySum = 0
  productTaxSum: any = 0.00

  deleteReferenceGuideId: any

  deleteBtnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService,    
    private _referenceGuidesService: ReferenceGuidesService,
    private _router: Router
  ) { 

  }
  
  ngOnInit() {
    this.languageTranslate()

    this.getQueryParams()

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.reference_guide_id = params.reference_guide_id
      if(this.reference_guide_id) {
        this.populateGetReferenceGuides()
      }
    })    
  } 

  populateGetReferenceGuides() {
    this.referenceGuidesDataLoader = true
    let params = {
      reference_guide_id: this.reference_guide_id
    }
    this._referenceGuidesService.getSingleReferenceGuides(params).subscribe((response) => {
      this.referenceGuidesDatas = response

      // Product quantity and tax sum
      this.referenceGuidesDatas.productsDetails.forEach(value => {
        this.productQuantitySum += value.product_quantity
        this.productTaxSum += parseFloat(value.product_tax)
      })

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.referenceGuidesDataLoader = false     
      this.referenceGuidesDataVisible = true 
    }, (error) => {
      this.referenceGuidesDataLoader = false
      this.referenceGuidesDataVisible = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleReferenceGuides')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }  


  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteReferenceGuideId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteReferenceGuides() {         
    this.deleteBtnLoading = true

    this._referenceGuidesService.deleteReferenceGuides(this.deleteReferenceGuideId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to reference guides list page
        this.gotoReferenceGuidesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteReferenceGuides')
      this.deleteConfirmClose()
    })      
  }    

  gotoReferenceGuidesList() {
    this._router.navigate(['/user/products/reference-guides/list'], {})
  }    

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}

