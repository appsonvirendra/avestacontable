import { TestBed } from '@angular/core/testing';

import { ReferenceGuidesService } from './reference-guides.service';

describe('ReferenceGuidesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReferenceGuidesService = TestBed.get(ReferenceGuidesService);
    expect(service).toBeTruthy();
  });
});
