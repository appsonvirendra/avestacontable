import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemslistComponent } from './items/itemslist/itemslist.component';
import { NewitemComponent } from './items/newitem/newitem.component';
import { PurchaselistComponent } from './purchase/purchaselist/purchaselist.component';
import { NewpurchaseComponent } from './purchase/newpurchase/newpurchase.component';
import { InfopurchaseComponent } from './purchase/infopurchase/infopurchase.component';
import { InventoryMovementComponent } from './inventory-movement/inventory-movement.component';
import { InventoryGeneralComponent } from './inventory-general/inventory-general.component';
import { InventoryFlowComponent } from './inventory-flow/inventory-flow.component';
import { EditpurchaseComponent } from './purchase/editpurchase/editpurchase.component';
import { ReferenceGuidesComponent } from './reference-guides/reference-guides.component';
import { NewReferenceGuidesComponent } from './reference-guides/new-reference-guides/new-reference-guides.component';
import { InfoReferenceGuidesComponent } from './reference-guides/info-reference-guides/info-reference-guides.component';
import { SenderComponent } from './sender/sender.component';
import { NewSenderComponent } from './sender/new-sender/new-sender.component';
import { InfoSenderComponent } from './sender/info-sender/info-sender.component';

const routes: Routes = [
  {
    path: 'items/list',
    component: ItemslistComponent
  },
  {
    path: 'items/:_id',
    component: NewitemComponent
  },
  {
    path: 'purchase/list',
    component: PurchaselistComponent
  },
  {
    path: 'purchase/new',
    component: NewpurchaseComponent
  },
  {
    path: 'purchase/info',
    component: InfopurchaseComponent
  },
  {
    path: 'purchase/edit',
    component: EditpurchaseComponent
  },
  {
    path: 'inventory-movement',
    component: InventoryMovementComponent
  },
  {
    path: 'inventory-general',
    component: InventoryGeneralComponent
  },
  {
    path: 'inventory-flow',
    component: InventoryFlowComponent
  },
  {
    path: 'reference-guides/list',
    component: ReferenceGuidesComponent
  },
  {
    path: 'reference-guides/:_id',
    component: NewReferenceGuidesComponent
  },
  {
    path: 'reference-guides-info',
    component: InfoReferenceGuidesComponent
  },
  {
    path: 'sender/list',
    component: SenderComponent
  },
  {
    path: 'sender/:_id',
    component: NewSenderComponent
  },
  {
    path: 'sender-info',
    component: InfoSenderComponent
  }         
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
