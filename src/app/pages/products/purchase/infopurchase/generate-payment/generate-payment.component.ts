import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-generate-payment',
  templateUrl: './generate-payment.component.html',
  styleUrls: ['./generate-payment.component.scss']
})
export class GeneratePaymentComponent implements OnInit {

  @Input() generatePaymentData: any

  // generate payment edit form
  generatePayEditForm: FormGroup  
  generatePayEditVisible = false
  
  // generate payment send mail form
  generatePayMailForm: FormGroup  
  generatePayMailVisible = false
  
  // generate payment view receipt  
  generatePayViewVisible = false  

  // delete confirm 2
  deleteConfirm2Visible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.generatePayEditFormInit()    

    this.generatePayMailFormInit()
  }

  // generate payment edit form start  
  generatePayEditFormInit() {
    this.generatePayEditForm = this.fb.group({
      receipt_amount_letters: [null, [Validators.required]],
      receipt_way_to_pay: [null, [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  generatePayEditOpen() {
    this.generatePayEditVisible = true
  }

  generatePayEditClose() {
    this.generatePayEditForm.reset()
    this.generatePayEditVisible = false
  }      

  generatePayEditSave() {    
    for (const i in this.generatePayEditForm.controls) {
      this.generatePayEditForm.controls[i].markAsDirty()
      this.generatePayEditForm.controls[i].updateValueAndValidity()
    }          
    // console.log('this.generatePayEditForm.value --', this.generatePayEditForm.value)
    if(this.generatePayEditForm.valid) {
      this.generatePayEditClose()
    }
  }  

  // generate payment send mail form start  
  generatePayMailFormInit() {
    this.generatePayMailForm = this.fb.group({
      send_receipt_email1: [null, [Validators.required]],
      send_receipt_email2: [null],
      send_receipt_email3: [null]
    })      
  }  

  generatePayMailOpen() {
    this.generatePayMailVisible = true
  }

  generatePayMailClose() {
    this.generatePayMailForm.reset()
    this.generatePayMailVisible = false
  }      

  generatePayMailSave() {    
    for (const i in this.generatePayMailForm.controls) {
      this.generatePayMailForm.controls[i].markAsDirty()
      this.generatePayMailForm.controls[i].updateValueAndValidity()
    }          
    // console.log('this.generatePayMailForm.value --', this.generatePayMailForm.value)
    if(this.generatePayMailForm.valid) {
      this.generatePayMailClose()
    }
  } 
  
  // generate payment receipt cancel form start


  // generate payment view receipt start  
  generatePayViewOpen() {
    this.generatePayViewVisible = true
  }

  generatePayViewClose() {
    this.generatePayViewVisible = false
  }      

  generatePayViewPrint() {
    this.generatePayViewClose()
  }    

  // delete confirm 2
  deleteConfirm2Open() {
    this.deleteConfirm2Visible = true
  }

  deleteConfirm2Close() {
    this.deleteConfirm2Visible = false
  }  


}
