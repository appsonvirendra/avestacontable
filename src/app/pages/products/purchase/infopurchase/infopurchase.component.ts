import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-infopurchase',
  templateUrl: './infopurchase.component.html',
  styleUrls: ['./infopurchase.component.scss']
})
export class InfopurchaseComponent implements OnInit {

  purchase_id: any

  purchaseDatas: any

  generatePaymentDatas: any  

  // create receipt form
  createReceiptForm: FormGroup  
  createReceiptVisible = false
  
  // send order mail form
  sendOrderMailForm: FormGroup  
  sendOrderMailVisible = false  

  generatePaymentCollapse = false

  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) { 
    
  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()

    this.purchaseInfo()
    this.generatePaymentInfo()
    
    this.createReceiptFormInit()
    this.sendOrderMailFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.purchase_id = params.purchase_id
    })    
  }

  purchaseInfo() {
    this.purchaseDatas = 
      {
        "purchase_order_no": "000-111-2222-0033",
        "purchase_date": "2019-12-27 13:25:02",
        "purchase_discount": "0",
        "purchase_supplier_id": "1",
        "purchase_supplier_name": "John brawn",
        "purchase_exchange_rate": "123",
        "purchase_receive_product": null,
        "purchase_notes": null,
        "purchase_exempt_amount": 9,
        "purchase_taxed_amount": 7,
        "purchase_isv_amount": "0",
        "purchase_final_discount": "0",
        "purchase_total_to_pay": 9,
        "purchase_status": "2",
        "purchase_product_item": [
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "0",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "1",
            "item_total": 0,
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "3",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "3",
            "item_total": 9,
            "item_cellar": "Bodega Principal"
          }
        ]
      }
  }  
  
  generatePaymentInfo() {      
    this.generatePaymentDatas = [
      {
        "payment_code": "0022-0011",
        "payment_date": "2019-12-27",
        "payment_previous_balance": "124",
        "payment_amount": "3",
        "payment_new_balance": "25000",
      }           
    ]
  }

  // create receipt form
  createReceiptFormInit() {
    this.createReceiptForm = this.fb.group({
      receipt_date: [null, [Validators.required]],
      receipt_amount_payable: [null, [Validators.required]],
      receipt_amount_letters: [null, [Validators.required]],
      receipt_way_to_pay: [null, [Validators.required]],
      receipt_notes: [null]
    })      
  }  

  createReceiptOpen() {
    this.createReceiptVisible = true
  }

  createReceiptClose() {
    this.createReceiptForm.reset()
    this.createReceiptVisible = false
  }      

  createReceiptSave() {        
    for (const i in this.createReceiptForm.controls) {
      this.createReceiptForm.controls[i].markAsDirty()
      this.createReceiptForm.controls[i].updateValueAndValidity()
    }    
    // console.log('this.createReceiptForm.value', this.createReceiptForm.value)
    if(this.createReceiptForm.valid) {
      this.createReceiptClose()
    }
  }
  
  // send order mail form
  sendOrderMailFormInit() {
    this.sendOrderMailForm = this.fb.group({
      send_order_email1: [null, [Validators.required]],
      send_order_email2: [null],
      send_order_email3: [null]
    })      
  }  

  sendOrderMailOpen() {
    this.sendOrderMailVisible = true
  }

  sendOrderMailClose() {
    this.sendOrderMailForm.reset()
    this.sendOrderMailVisible = false
  }      

  sendOrderMailSave() {    
    for (const i in this.sendOrderMailForm.controls) {
      this.sendOrderMailForm.controls[i].markAsDirty()
      this.sendOrderMailForm.controls[i].updateValueAndValidity()
    }          
    if(this.sendOrderMailForm.valid) {
      this.sendOrderMailClose()
    }
  }   

  // generate payment collapse
  generatePyamentCollapse() {
    this.generatePaymentCollapse = !this.generatePaymentCollapse
  }

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
