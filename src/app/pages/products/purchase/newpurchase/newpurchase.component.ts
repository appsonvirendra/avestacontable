import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-newpurchase',
  templateUrl: './newpurchase.component.html',
  styleUrls: ['./newpurchase.component.scss']
})
export class NewpurchaseComponent implements OnInit {

  purchaseAddform: FormGroup

  currentDate:any = new Date()

  // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = []
  
  // exchange rate
  exchangeRateForm: FormGroup  
  exchangeRateVisible = false

  // product search dialog
  searchProductVisible = false
  saleDatas = []  

  // purchase product data 
  purchaseProductDatas = []

  quantityNgModel: any = []

  editId: string | null = null;

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.purchaseFormInit()
    this.supplierData()

    this.exchangeRateFormInit()

    this.productData()
    this.purchaseProductData()
  }

  startEdit(id: string): void {
    this.editId = id;
  }

  stopEdit(): void {
    this.editId = null;
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  purchaseFormInit() {
    this.purchaseAddform = this.fb.group({
      purchase_date: [this.currentDate, [Validators.required]],
      purchase_discount: ['0'],
      purchase_supplier_id: [null],
      purchase_supplier_name: [null, [Validators.required]],
      purchase_supplier_email: [{value: '', disabled: true}],
      purchase_exchange_rate: [null, [Validators.required]],
      purchase_receive_product: [null],
      purchase_notes: [null],
      purchase_exempt_amount: ['0'],
      purchase_isv_amount: ['0'],
      purchase_final_discount: ['0'],
      purchase_total_to_pay: ['0'],
      purchase_product_item: []
    })      
  }
  
  savePurchase() {
    for (const i in this.purchaseAddform.controls) {
      this.purchaseAddform.controls[i].markAsDirty()
      this.purchaseAddform.controls[i].updateValueAndValidity()
    }
    // let dateFormate = new DatePipe('en-us').transform(this.purchaseAddform.value.purchase_date, 'dd/MM/yyyy')
    // console.log('dateFormate --', dateFormate)
    if(this.purchaseAddform.value.purchase_supplier_id != null && this.purchaseAddform.value.purchase_exchange_rate != null) {
      console.log('this.purchaseAddform.value', this.purchaseAddform.value)
    }
  }  

  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.purchaseAddform.patchValue({
      purchase_supplier_id: $event.supplier_id,
      purchase_supplier_name: $event.supplier_name,
      purchase_supplier_email: $event.supplier_email
    })

    this.searchSupplierClose()    
  }  

  // exchange rate form
  exchangeRateFormInit() {
    this.exchangeRateForm = this.fb.group({
      exchange_rate: [null]
    })      
  }  

  exchangeRateOpen() {
    this.exchangeRateVisible = true
  }

  exchangeRateClose() {
    this.exchangeRateFormInit()
    this.exchangeRateVisible = false
  }      

  exchangeRateCalculate() {    
    this.purchaseAddform.patchValue({
      purchase_exchange_rate: this.exchangeRateForm.value.exchange_rate
    })    

    this.exchangeRateClose()
  }

  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }   

  productData() {
    this.saleDatas = [    
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0',
        item_discount_percent: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0',
        item_discount_percent: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0',
        item_discount_percent: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0',
        item_discount_percent: '0'
      }
    ]
  }

  productTotalPriceSum() {
    let totalActualPriceSum = 0

    let totalValueSum = 0

    let discountValueSum = 0

    this.purchaseProductDatas.forEach(function (value) {

      // Sum for actual value
      if(value.item_cost_price) {
        totalActualPriceSum += value.item_cost_price * value.item_quantity
      }      

      // Sum for total value
      if(value.item_total) {
        totalValueSum += value.item_total
      }

      // Sum for total discount
      if(value.item_discount) {
        discountValueSum += value.item_discount
      }      
    })

    this.purchaseAddform.patchValue({
      purchase_exempt_amount: totalActualPriceSum,
      purchase_total_to_pay: totalValueSum,
      purchase_final_discount: discountValueSum,
      purchase_product_item: this.purchaseProductDatas
    })    
  }

  productAddInput(data) {
    let getproductData = data    

    if (this.purchaseProductDatas.some((item) => item.item_id == getproductData.item_id)) {
      let targetIdx = this.purchaseProductDatas.map(item => item.item_id).indexOf(getproductData.item_id)

      this.purchaseProductDatas[targetIdx].item_cost_price = getproductData.item_cost_price
      this.purchaseProductDatas[targetIdx].item_price = getproductData.item_price

      this.quantityNgModel[targetIdx] = this.purchaseProductDatas[targetIdx].item_quantity + getproductData.item_quantity

      this.purchaseProductDatas[targetIdx].item_quantity = this.purchaseProductDatas[targetIdx].item_quantity + getproductData.item_quantity


      this.purchaseProductDatas[targetIdx].item_total = getproductData.item_cost_price * this.purchaseProductDatas[targetIdx].item_quantity  
      
      // Update discount value
      if(this.purchaseProductDatas[targetIdx].item_discount_percent) {

        this.purchaseProductDatas[targetIdx].item_discount = ((this.purchaseProductDatas[targetIdx].item_cost_price * this.purchaseProductDatas[targetIdx].item_quantity) * this.purchaseProductDatas[targetIdx].item_discount_percent) / 100
    
        this.purchaseProductDatas[targetIdx].item_total = (this.purchaseProductDatas[targetIdx].item_cost_price * this.purchaseProductDatas[targetIdx].item_quantity) - this.purchaseProductDatas[targetIdx].item_discount      

      }      
      
    } else {
      this.purchaseProductDatas.push(
        getproductData        
      )      
      this.quantityNgModel[this.purchaseProductDatas.length-1] = getproductData.item_quantity
    }
    
    this.productTotalPriceSum()
    // this.searchProductClose()    
  }

  quantityOnKeyUp(value, i) {
    let valueInt = parseInt(value)
    this.purchaseProductDatas[i].item_quantity = valueInt
    this.purchaseProductDatas[i].item_total = this.purchaseProductDatas[i].item_cost_price * valueInt
    
    // Update discount value
    if(this.purchaseProductDatas[i].item_discount_percent) {

      this.purchaseProductDatas[i].item_discount = ((this.purchaseProductDatas[i].item_cost_price * valueInt) * this.purchaseProductDatas[i].item_discount_percent) / 100
  
      this.purchaseProductDatas[i].item_total = (this.purchaseProductDatas[i].item_cost_price * valueInt) - this.purchaseProductDatas[i].item_discount

    }

    this.productTotalPriceSum()
  }

  costPriceOnKeyUp(value, i) {
    let valueInt = parseInt(value)
    this.purchaseProductDatas[i].item_cost_price = valueInt
    this.purchaseProductDatas[i].item_total = this.purchaseProductDatas[i].item_quantity * valueInt

    // Update discount value
    if(this.purchaseProductDatas[i].item_discount_percent) {

      this.purchaseProductDatas[i].item_discount = ((valueInt * this.purchaseProductDatas[i].item_quantity) * this.purchaseProductDatas[i].item_discount_percent) / 100
  
      this.purchaseProductDatas[i].item_total = (valueInt * this.purchaseProductDatas[i].item_quantity) - this.purchaseProductDatas[i].item_discount      

    }

    this.productTotalPriceSum()
  }
  
  salePriceOnKeyUp(value, i) {
    let valueInt = parseInt(value)
    this.purchaseProductDatas[i].item_price = valueInt
  }
  
  discountOnKeyUp(value, i, id) {
    let valueInt
    if(value) {
      valueInt = parseInt(value)
      this.purchaseProductDatas[i].item_discount_percent = valueInt
  
      this.purchaseProductDatas[i].item_discount = ((this.purchaseProductDatas[i].item_cost_price * this.purchaseProductDatas[i].item_quantity) * valueInt) / 100
  
      this.purchaseProductDatas[i].item_total = (this.purchaseProductDatas[i].item_cost_price * this.purchaseProductDatas[i].item_quantity) - this.purchaseProductDatas[i].item_discount
    } else {
      this.purchaseProductDatas[i].item_discount_percent = 0
      this.purchaseProductDatas[i].item_discount = 0

      this.purchaseProductDatas[i].item_total = this.purchaseProductDatas[i].item_cost_price * this.purchaseProductDatas[i].item_quantity
    }
    this.productTotalPriceSum()

    // Discount value update in dialog
    if (this.saleDatas.some((item) => item.item_id == id)) {
      let targetIdx = this.saleDatas.map(item => item.item_id).indexOf(id)

      this.saleDatas[targetIdx].item_discount_percent = valueInt      
    }    
  }  

  purchaseProductData() {
    this.purchaseProductDatas = [    
      /*{
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      },
      {
        item_id: '2',
        item_code: '011',
        item_name: 'Item 2',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      },
      {
        item_id: '3',
        item_code: '011',
        item_name: 'Item 3',
        item_description: 'Item 1',
        item_quantity: '2',
        item_cost_price: '20',
        item_price: '20',
        item_discount: '1',
        item_total: '20',
        item_cellar: '20'
      }*/            
    ]    
  }

  purchaseProductRemove(item_index) {
    this.purchaseProductDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }

}