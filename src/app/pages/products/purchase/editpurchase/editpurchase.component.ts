import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editpurchase',
  templateUrl: './editpurchase.component.html',
  styleUrls: ['./editpurchase.component.scss']
})
export class EditpurchaseComponent implements OnInit {

  purchase_id: any

  purchaseDatas: any  

  // product search dialog
  searchProductVisible = false
  saleDatas = []  

  // purchase product data 
  purchaseProductDatas = []  

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) { 
    
  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()

    this.purchaseInfo()    

    this.productData()
    this.purchaseProductData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.purchase_id = params.purchase_id
    })    
  }

  purchaseInfo() {
    this.purchaseDatas = 
      {
        "purchase_order_no": "000-111-2222-0033",
        "purchase_date": "2019-12-27 13:25:02",
        "purchase_discount": "0",
        "purchase_supplier_id": "1",
        "purchase_supplier_name": "John brawn",
        "purchase_exchange_rate": "123",
        "purchase_receive_product": null,
        "purchase_notes": null,
        "purchase_exempt_amount": 9,
        "purchase_taxed_amount": 0,
        "purchase_isv_amount": "0",
        "purchase_final_discount": "0",
        "purchase_total_to_pay": 9,
        "purchase_status": "2",
        "purchase_product_item": [
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "0",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "1",
            "item_total": 0,
            "item_cellar": "Bodega Principal"
          },
          {
            "item_id": "1",
            "item_code": "011",
            "item_name": "Item 1",
            "item_description": "Item 1",
            "item_cost_price": "3",
            "item_price": "20",
            "item_discount": "0",
            "item_quantity": "3",
            "item_total": 9,
            "item_cellar": "Bodega Principal"
          }
        ]
      }
  }  

  // search product dialog
  searchProductOpen() {
    this.searchProductVisible = true
  }

  searchProductClose() {
    this.searchProductVisible = false
  }   

  productData() {
    this.saleDatas = [    
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      }
    ]
  } 
  
  productTotalPriceSum() {
    let totalValueSum = 0

    this.purchaseProductDatas.forEach(function (value) {
      totalValueSum += value.item_total
    })
    
    this.purchaseDatas.purchase_exempt_amount = totalValueSum,
    this.purchaseDatas.purchase_total_to_pay = totalValueSum,
    this.purchaseDatas.purchase_product_item = this.purchaseProductDatas
    
  }

  productAddInput($event) {
    this.purchaseProductDatas.push(
      $event
    )
    
    this.productTotalPriceSum()
    // this.searchProductClose()    
  }  

  purchaseProductData() {
    this.purchaseProductDatas = [...this.purchaseDatas.purchase_product_item]
  }

  purchaseProductRemove(item_index) {
    this.purchaseProductDatas.splice(item_index, 1)
    this.productTotalPriceSum()
  }  

}
