import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-purchasefilter',
  templateUrl: './purchasefilter.component.html',
  styleUrls: ['./purchasefilter.component.scss']
})
export class PurchasefilterComponent implements OnInit {

  purchasefilterform: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = []

  // user search dialog
  searchUserVisible = false
  userDatas = []

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.purchaseFilterFormInit()

    this.supplierData()
    this.userData()
  }

  purchaseFilterFormInit() {
    this.purchasefilterform = this.fb.group({
      purchase_invoice_code: [null],
      purchase_supplier_id: [null],
      purchase_supplier_name: [{value: '', disabled: true}],
      purchase_date: [null],
      purchase_state: ['0'],
      purchase_branch: ['0'],
      purchase_user_id: [null],
      purchase_user_name: [{value: '', disabled: true}]
    })  
  }

  searchPurchase() {
    this.searchEvent.emit(this.purchasefilterform.value)
  }

  clearFilter() {
    this.purchasefilterform = this.fb.group({
      purchase_invoice_code: [null],
      purchase_supplier_id: [null],
      purchase_supplier_name: [{value: '', disabled: true}],
      purchase_date: [null],
      purchase_state: ['0'],
      purchase_branch: ['0'],
      purchase_user_id: [null],
      purchase_user_name: [{value: '', disabled: true}]
    }) 
  }  

  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.purchasefilterform.patchValue({
      purchase_supplier_id: $event.supplier_id,
      purchase_supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  }  

  // search user dialog
  searchUserOpen() {
    this.searchUserVisible = true
  }

  searchUserClose() {
    this.searchUserVisible = false
  }   

  userData() {
    this.userDatas = [
      {
        user_id: '1',
        user_firstname: 'John',
        user_lastname: 'brawn',
        user_email: 'john@gmail.com',
        user_branch: 'Sucursal 1'
      }, 
      {
        user_id: '2',
        user_firstname: 'Rony 2',
        user_lastname: 'brawn',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'
      },
      {
        user_id: '3',
        user_firstname: 'Rony 3',
        user_lastname: 'wan',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'        
      },
      {
        user_id: '4',
        user_firstname: 'Rony 4',
        user_lastname: 'wan',
        user_email: 'rony4@gmail.com',
        user_branch: 'Sucursal 1'   
      }                  
    ]
  }
  
  userAddInput($event) {
    this.purchasefilterform.patchValue({
      purchase_user_id: $event.user_id,
      purchase_user_name: $event.user_firstname +' '+ $event.user_lastname
    })

    this.searchUserClose()
  }  

}
