import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-purchaselist',
  templateUrl: './purchaselist.component.html',
  styleUrls: ['./purchaselist.component.scss']
})
export class PurchaselistComponent implements OnInit {

  purchaseDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.purchaseData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.purchaseDatas = []
  }
  
  purchaseData() {
    this.purchaseDatas = [
      {
        purchase_id: '1',
        purchase_code: '000-001-01-001',
        purchase_supplier_name: 'Provider 1',
        purchase_total: '104',
        purchase_balance: '107',
        purchase_date: '2018-12-23',
        purchase_state: '1',
        purchase_active_status: '1'
      }, 
      {
        purchase_id: '2',
        purchase_code: '000-001-01-002',
        purchase_supplier_name: 'Provider 2',
        purchase_total: '105',
        purchase_balance: '108',
        purchase_date: '2018-12-24',
        purchase_state: '0',
        purchase_active_status: '1'
      },
      {
        purchase_id: '3',
        purchase_code: '000-001-01-003',
        purchase_supplier_name: 'Provider 3',
        purchase_total: '106',
        purchase_balance: '109',
        purchase_date: '2018-12-25',
        purchase_state: '1',
        purchase_active_status: '0'
      }, 
      {
        purchase_id: '4',
        purchase_code: '000-001-01-004',
        purchase_supplier_name: 'Provider 4',
        purchase_total: '107',
        purchase_balance: '110',
        purchase_date: '2018-12-26',
        purchase_state: '1',
        purchase_active_status: '1'
      }                       
    ]
  }  

}
