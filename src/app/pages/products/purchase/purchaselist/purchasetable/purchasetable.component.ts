import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-purchasetable',
  templateUrl: './purchasetable.component.html',
  styleUrls: ['./purchasetable.component.scss']
})
export class PurchasetableComponent implements OnInit {

  @Input() purchaseDatas: any

  // search
  search_title = ''

  purchaseDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  deleteConfirmVisible = false

  constructor() { }

  ngOnInit() {
    this.purchaseData()
  }

  purchaseData() {
    this.purchaseDisplayDatas = [...this.purchaseDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { purchase_code: string; purchase_supplier_name: string; purchase_total: string; purchase_balance: string; purchase_date: string; purchase_state: string; }) => {
      return (
        item.purchase_code.toLowerCase().indexOf(searchLower) !== -1 ||
        item.purchase_supplier_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.purchase_total.toLowerCase().indexOf(searchLower) !== -1 ||
        item.purchase_balance.toLowerCase().indexOf(searchLower) !== -1 || 
        item.purchase_date.toLowerCase().indexOf(searchLower) !== -1 || 
        item.purchase_state.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.purchaseDatas.filter((item: { purchase_code: string; purchase_supplier_name: string; purchase_total: string; purchase_balance: string; purchase_date: string; purchase_state: string; }) => filterFunc(item))

    this.purchaseDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.purchaseDisplayDatas = [...this.purchaseDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
