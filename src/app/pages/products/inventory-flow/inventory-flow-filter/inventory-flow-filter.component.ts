import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-inventory-flow-filter',
  templateUrl: './inventory-flow-filter.component.html',
  styleUrls: ['./inventory-flow-filter.component.scss']
})
export class InventoryFlowFilterComponent implements OnInit {

  itemfilterform: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  // supplier search dialog
  searchProductNormalVisible = false
  productNormalDatas = []  

  // user search dialog
  searchUserVisible = false
  userDatas = []  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.itemFilterFormInit()

    this.productNormalData()
    this.userData()
  }

  itemFilterFormInit() {
    this.itemfilterform = this.fb.group({
      item_id: [null],
      item_name: [null],
      transaction_id: [null],
      purchase_user_id: [null],
      purchase_user_name: [null],
      date_from: [null],
      date_to: [null],
      cellar_type: ['0'],
      transaction_type_id: [null]
    })  
  }

  searchItem() {
    this.searchEvent.emit(this.itemfilterform.value)
  }

  clearFilter() {
    this.itemfilterform = this.fb.group({
      item_id: [null],
      item_name: [null],
      transaction_id: [null],
      purchase_user_id: [null],
      purchase_user_name: [null],
      date_from: [null],
      date_to: [null],
      cellar_type: ['0'],
      transaction_type_id: [null]
    }) 
  }

  printItemList() {
  }
  
  // search product normal dialog
  searchProductNormalOpen() {
    this.searchProductNormalVisible = true
  }

  searchProductNormalClose() {
    this.searchProductNormalVisible = false
  }   

  productNormalData() {
    this.productNormalDatas = [
      {
        item_id: '1',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '2',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        item_category_id: '1',
        item_cellar_id: '1',
        item_discount: '0'        
      },
      {
        item_id: '3',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      },
      {
        item_id: '4',
        item_code: '014',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        item_category_id: '2',
        item_cellar_id: '1',
        item_discount: '0'
      }                 
    ]
  }

  productNormalAddInput($event) {
    this.itemfilterform.patchValue({
      item_id: $event.item_id,
      item_name: $event.item_name
    })

    this.searchProductNormalClose()    
  }
  
  // search user dialog
  searchUserOpen() {
    this.searchUserVisible = true
  }

  searchUserClose() {
    this.searchUserVisible = false
  }   

  userData() {
    this.userDatas = [
      {
        user_id: '1',
        user_firstname: 'John',
        user_lastname: 'brawn',
        user_email: 'john@gmail.com',
        user_branch: 'Sucursal 1'
      }, 
      {
        user_id: '2',
        user_firstname: 'Rony 2',
        user_lastname: 'brawn',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'
      },
      {
        user_id: '3',
        user_firstname: 'Rony 3',
        user_lastname: 'wan',
        user_email: 'rony2@gmail.com',
        user_branch: 'Sucursal 1'        
      },
      {
        user_id: '4',
        user_firstname: 'Rony 4',
        user_lastname: 'wan',
        user_email: 'rony4@gmail.com',
        user_branch: 'Sucursal 1'   
      }                  
    ]
  }
  
  userAddInput($event) {
    this.itemfilterform.patchValue({
      purchase_user_id: $event.user_id,
      purchase_user_name: $event.user_firstname +' '+ $event.user_lastname
    })

    this.searchUserClose()
  }  

}
