import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inventory-flow-table',
  templateUrl: './inventory-flow-table.component.html',
  styleUrls: ['./inventory-flow-table.component.scss']
})
export class InventoryFlowTableComponent implements OnInit {

  @Input() itemDatas: any

  constructor() { }

  ngOnInit() {
  }

}
