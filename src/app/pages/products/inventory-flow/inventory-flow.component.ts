import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inventory-flow',
  templateUrl: './inventory-flow.component.html',
  styleUrls: ['./inventory-flow.component.scss']
})
export class InventoryFlowComponent implements OnInit {

  itemDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.itemData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.itemDatas = []
  }
  
  itemData() {
    this.itemDatas = [   
      {
        item_id: '1',
        item_image: '',
        item_code: '011',
        item_name: 'Item 1',
        item_description: 'Item 1',
        item_price: '20',
        item_stocks: '10',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '2',
        item_image: '',
        item_code: '012',
        item_name: 'Item 2',
        item_description: 'Item 2',
        item_price: '30',
        item_stocks: '20',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '3',
        item_image: '',
        item_code: '013',
        item_name: 'Item 3',
        item_description: 'Item 3',
        item_price: '20',
        item_stocks: '10',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '4',
        item_image: '',
        item_code: '04',
        item_name: 'Item 4',
        item_description: 'Item 4',
        item_price: '40',
        item_stocks: '10',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '5',
        item_image: '',
        item_code: '05',
        item_name: 'Item 5',
        item_description: 'Item 5',
        item_price: '20',
        item_stocks: '10',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '6',
        item_image: '',
        item_code: '06',
        item_name: 'Item 6',
        item_description: 'Item 6',
        item_price: '60',
        item_stocks: '160',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '7',
        item_image: '',
        item_code: '07',
        item_name: 'Item 7',
        item_description: 'Item 7',
        item_price: '70',
        item_stocks: '170',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '8',
        item_image: '',
        item_code: '08',
        item_name: 'Item 8',
        item_description: 'Item 8',
        item_price: '80',
        item_stocks: '180',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '9',
        item_image: '',
        item_code: '09',
        item_name: 'Item 9',
        item_description: 'Item 9',
        item_price: '90',
        item_stocks: '190',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '10',
        item_image: '',
        item_code: '010',
        item_name: 'Item 10',
        item_description: 'Item 10',
        item_price: '100',
        item_stocks: '100',
        category_id: '1',
        category_name: 'General'
      },
      {
        item_id: '11',
        item_image: '',
        item_code: '011',
        item_name: 'Item 11',
        item_description: 'Item 11',
        item_price: '110',
        item_stocks: '111',
        category_id: '1',
        category_name: 'General'
      } 
    ]
  }

}
