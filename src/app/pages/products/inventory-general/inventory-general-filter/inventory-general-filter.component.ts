import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-inventory-general-filter',
  templateUrl: './inventory-general-filter.component.html',
  styleUrls: ['./inventory-general-filter.component.scss']
})
export class InventoryGeneralFilterComponent implements OnInit {

  itemfilterform: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  // supplier search dialog
  searchSupplierVisible = false
  supplierDatas = []  

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.itemFilterFormInit()

    this.supplierData()
  }

  itemFilterFormInit() {
    this.itemfilterform = this.fb.group({
      item_code: [null],
      supplier_id: [null],
      supplier_name: [null],
      item_name: [null],
      item_currency: [null],
      item_category: ['0'],
      item_discount: [null],
      cellar_type: ['0']
    })  
  }

  searchItem() {
    this.searchEvent.emit(this.itemfilterform.value)
  }

  clearFilter() {
    this.itemfilterform = this.fb.group({
      item_code: [null],
      supplier_id: [null],
      supplier_name: [null],
      item_name: [null],
      item_currency: [null],
      item_category: ['0'],
      item_discount: [null],
      cellar_type: ['0']
    })  
  }
  
  // search supplier dialog
  searchSupplierOpen() {
    this.searchSupplierVisible = true
  }

  searchSupplierClose() {
    this.searchSupplierVisible = false
  }   

  supplierData() {
    this.supplierDatas = [
      {
        supplier_id: '1',
        supplier_name: 'John brawn',
        supplier_email: 'john@gmail.com',
        supplier_phone: '1122334455',
      }, 
      {
        supplier_id: '2',
        supplier_name: 'Rony 2',
        supplier_email: 'rony2@gmail.com',
        supplier_phone: '6677889911',
      },
      {
        supplier_id: '3',
        supplier_name: 'Rony 3',
        supplier_email: 'rony3@gmail.com',
        supplier_phone: '4455667788',
      },
      {
        supplier_id: '4',
        supplier_name: 'Rony 4',
        supplier_email: 'rony4@gmail.com',
        supplier_phone: '5566778899',
      }                  
    ]
  }

  supplierAddInput($event) {
    this.itemfilterform.patchValue({
      supplier_id: $event.supplier_id,
      supplier_name: $event.supplier_name
    })

    this.searchSupplierClose()    
  }  

  printItemList() {
  }

}
