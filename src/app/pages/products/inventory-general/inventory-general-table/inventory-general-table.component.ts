import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inventory-general-table',
  templateUrl: './inventory-general-table.component.html',
  styleUrls: ['./inventory-general-table.component.scss']
})
export class InventoryGeneralTableComponent implements OnInit {

  @Input() itemDatas: any

  // general info dialog
  generalInfoVisible = false

  date_from: any
  date_to: any

  // search
  search_title = ''

  itemDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  constructor() { 

  }

  ngOnInit() {
    this.itemData()
  }

  generalInfoOpen() {
    this.generalInfoVisible = true
  }

  generalInfoClose() {
    this.generalInfoVisible = false
  }

  generalInfoFilter() {
    // console.log('this.date_from', this.date_from)
    // console.log('this.date_to', this.date_to)
  }
 
  // search
  itemData() {    
    this.itemDisplayDatas = [...this.itemDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { item_name: string; item_description: string; item_price: string; item_stocks: string }) => {
      return (
        item.item_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.item_description.toLowerCase().indexOf(searchLower) !== -1 ||
        item.item_price.toLowerCase().indexOf(searchLower) !== -1 ||
        item.item_stocks.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.itemDatas.filter((item: { item_name: string; item_description: string; item_price: string; item_stocks: string }) => filterFunc(item))

    this.itemDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.itemDisplayDatas = [...this.itemDatas]
    }    
  }  

}
