import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SenderService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getSenderList(bodyparams) {
    return this.apiService.get(`/senderList`, bodyparams)
      .pipe(map(data => data.data))
  } 
  
  postSender(bodyparams) {
    return this.apiService.post(`/sender`, bodyparams)
      .pipe(map(data => data))
  } 
  
  getSingleSender(bodyparams) {
    return this.apiService.get(`/sender`, bodyparams)
      .pipe(map(data => data.data[0]))
  }  
  
  putSender(sender_id, bodyparams) {
    return this.apiService.put(`/sender/`+sender_id, bodyparams)
      .pipe(map(data => data))
  }  

  deleteSender(sender_id) {
    return this.apiService.delete(`/sender/`+sender_id)
      .pipe(map(data => data))
  }  


}
