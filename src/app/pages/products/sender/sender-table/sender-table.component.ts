import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { SenderService } from 'src/app/pages/products/sender/sender.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-sender-table',
  templateUrl: './sender-table.component.html',
  styleUrls: ['./sender-table.component.scss']
})
export class SenderTableComponent implements OnInit {

  // modal loading
  deleteConfirmVisible = false
  deleteBtnLoading = false

  @Input() senderDatas: any
  
  // search
  search_title = ''
  senderDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  deleteSenderId: any

  @Output("populateGetSenderList") populateGetSenderList: EventEmitter<any> = new EventEmitter();

  constructor(
    private translate: TranslateService,
    private _senderService: SenderService,
    private message: NzMessageService    
  ) { 

  }
  
  ngOnInit() {
    this.senderData()
  }

  senderData() {
    this.senderDisplayDatas = [...this.senderDatas]
  }

  // modal confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteSenderId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteSender() {
    this.deleteBtnLoading = true

    this._senderService.deleteSender(this.deleteSenderId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get sender list api
        this.populateGetSenderList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteSender')
      this.deleteConfirmClose()
    }) 
  }  

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { company_name: string; email: string; telephone: string }) => {
      return (
        item.company_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.email.toLowerCase().indexOf(searchLower) !== -1 ||
        item.telephone.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.senderDatas.filter((item: { company_name: string; email: string; telephone: string }) => filterFunc(item))

    this.senderDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.senderDisplayDatas = [...this.senderDatas]
    }    
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}

