import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderTableComponent } from './sender-table.component';

describe('SenderTableComponent', () => {
  let component: SenderTableComponent;
  let fixture: ComponentFixture<SenderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
