import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

// import { DatePipe } from '@angular/common';

import { ActivatedRoute } from '@angular/router';
import { SenderService } from 'src/app/pages/products/sender/sender.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-info-sender',
  templateUrl: './info-sender.component.html',
  styleUrls: ['./info-sender.component.scss']
})
export class InfoSenderComponent implements OnInit {

  sender_id: any

  senderInfoDatas: any

  loader = true

  constructor(
    private translate: TranslateService,
    private _activatedRoute: ActivatedRoute,
    private _senderService: SenderService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService     
  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  getQueryParams() {
    this._activatedRoute.queryParams.subscribe(params => {
      this.sender_id = params.sender_id
      this.populateGetSingleSender()
    })    
  }  

  populateGetSingleSender() {
    this.loader = true
    let params = {
      sender_id: this.sender_id
    }
    this._senderService.getSingleSender(params).subscribe((response) => {
      this.senderInfoDatas = response

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.loader = false   
    }, (error) => {
      this.loader = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleSender')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}

