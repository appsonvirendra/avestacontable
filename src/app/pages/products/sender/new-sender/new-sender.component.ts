import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';

import { SenderService } from 'src/app/pages/products/sender/sender.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

import * as _moment from 'moment';
const moment = _moment

@Component({
  selector: 'app-new-sender',
  templateUrl: './new-sender.component.html',
  styleUrls: ['./new-sender.component.scss']
})
export class NewSenderComponent implements OnInit {

  sender_id: any
  pageType: any  

  senderForm: FormGroup

  btnLoading: boolean = false

  senderDataLoader: any
  senderDataVisible: any  

  companyData = JSON.parse(localStorage.getItem('companyRes'))  

  companyBranchId = localStorage.getItem('companyBranchId')

  senderDatas: any

  base64String:String = null

  imageRequired: boolean = false

  // Format date
  format = 'YYYY-MM-DD'  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _senderService: SenderService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService       
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.getQueryParams()
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  // Get current query params
  getQueryParams() {
    this._activatedRoute.queryParams.subscribe(params => { 
      this.sender_id = params.sender_id  
      if(this.sender_id) {
        this.pageType = 'edit'
        this.populateGetSender()
      } else {        
        this.pageType = 'new'
        this.senderFormInitNew()
        this.senderDataVisible = true
      }
    })    
  }  

  populateGetSender() {
    this.senderDataLoader = true
    let params = {
      sender_id: this.sender_id
    }
    this._senderService.getSingleSender(params).subscribe((response) => {
      this.senderDatas = response

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.senderFormInitEdit()

      this.senderDataLoader = false   
      this.senderDataVisible = true   
    }, (error) => {
      this.senderDataLoader = false
      this.senderDataVisible = true

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleSender')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  senderFormInitEdit() {
    // Required
    if(!this.senderDatas.image) {
      this.imageRequired = true
    }
    
    // Condition for expires
    if(this.senderDatas.expires == '0000-00-00') {
      this.senderDatas.expires = null
    }

    this.senderForm = this.fb.group({
      company_name: [this.senderDatas.company_name],
      RTN_TDI: [this.senderDatas.RTN_TDI],
      address: [this.senderDatas.address],
      email: [this.senderDatas.email],
      telephone: [this.senderDatas.telephone],
      CAI: [this.senderDatas.CAI],
      expires: [this.senderDatas.expires],
      site_url: [this.senderDatas.site_url],
      image: [null]
    })    
  }

  senderFormInitNew() {
    this.senderForm = this.fb.group({
      company_name: [null],
      RTN_TDI: [null],
      address: [null],
      email: [null],
      telephone: [null],
      CAI: [null],
      expires: [null],
      site_url: [null],
      image: [null]
    })      
  }

  handleFileSelect(evt) {
    var files = evt.target.files
    var file = files[0]

    if (files && file) {
      var reader = new FileReader()

      reader.onload =this._handleReaderLoaded.bind(this)

      reader.readAsBinaryString(file)
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result
    this.base64String = btoa(binaryString)
  }  

  saveSender() {
    // Change expires value format
    let changeDateFormat = ''
    if(this.senderForm.value.expires) {
      changeDateFormat = moment(this.senderForm.value.expires).format(this.format)
    }

    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      company_name: this.senderForm.value.company_name,
      RTN_TDI: this.senderForm.value.RTN_TDI,
      address: this.senderForm.value.address,
      email: this.senderForm.value.email,
      telephone: this.senderForm.value.telephone,
      CAI: this.senderForm.value.CAI,
      expires: changeDateFormat,
      site_url: this.senderForm.value.site_url,
      image: this.base64String
    }

    // If image value is null
    if(!this.base64String) {
      delete params.image
    }

    // Call update method or insert method based on page type
    if (this.pageType == 'edit') {
      this.updateSender(params)    
    } else {   
      this.insertSender(params)
    }      
  }

  updateSender(itemRowData) {
    this.btnLoading = true
    this._senderService.putSender(this.sender_id, itemRowData).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to sender list page
        this.gotoSenderList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateSender')
    })    
  }

  insertSender(itemRowData) {
    this.btnLoading = true
    this._senderService.postSender(itemRowData).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to sender list page
        this.gotoSenderList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertSender')
    })    
  }

  gotoSenderList() {
    this._router.navigate(['/user/products/sender/list'], {})
  }  

  clearFilter() {
    this.senderForm.reset()  
  }

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
