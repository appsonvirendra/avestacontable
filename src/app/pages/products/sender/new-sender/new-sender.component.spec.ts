import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSenderComponent } from './new-sender.component';

describe('NewSenderComponent', () => {
  let component: NewSenderComponent;
  let fixture: ComponentFixture<NewSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
