import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { ProductsRoutingModule } from './products-routing.module';
import { ItemslistComponent } from './items/itemslist/itemslist.component';
import { NewitemComponent } from './items/newitem/newitem.component';
import { ItemstableComponent } from './items/itemslist/itemstable/itemstable.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
import { PurchaselistComponent } from './purchase/purchaselist/purchaselist.component';
import { NewpurchaseComponent } from './purchase/newpurchase/newpurchase.component';
import { PurchasefilterComponent } from './purchase/purchaselist/purchasefilter/purchasefilter.component';
import { PurchasetableComponent } from './purchase/purchaselist/purchasetable/purchasetable.component';
import { InfopurchaseComponent } from './purchase/infopurchase/infopurchase.component';
import { GeneratePaymentComponent } from './purchase/infopurchase/generate-payment/generate-payment.component';
import { InventoryMovementComponent } from './inventory-movement/inventory-movement.component';
import { InventoryMovementTableComponent } from './inventory-movement/inventory-movement-table/inventory-movement-table.component';
import { InventoryGeneralComponent } from './inventory-general/inventory-general.component';
import { InventoryGeneralFilterComponent } from './inventory-general/inventory-general-filter/inventory-general-filter.component';
import { InventoryGeneralTableComponent } from './inventory-general/inventory-general-table/inventory-general-table.component';
import { InventoryFlowComponent } from './inventory-flow/inventory-flow.component';
import { InventoryFlowFilterComponent } from './inventory-flow/inventory-flow-filter/inventory-flow-filter.component';
import { InventoryFlowTableComponent } from './inventory-flow/inventory-flow-table/inventory-flow-table.component';
import { EditpurchaseComponent } from './purchase/editpurchase/editpurchase.component';
import { ReferenceGuidesComponent } from './reference-guides/reference-guides.component';
import { ReferenceGuidesTableComponent } from './reference-guides/reference-guides-table/reference-guides-table.component';
import { NewReferenceGuidesComponent } from './reference-guides/new-reference-guides/new-reference-guides.component';
import { InfoReferenceGuidesComponent } from './reference-guides/info-reference-guides/info-reference-guides.component';
import { SenderComponent } from './sender/sender.component';
import { SenderTableComponent } from './sender/sender-table/sender-table.component';
import { NewSenderComponent } from './sender/new-sender/new-sender.component';
import { InfoSenderComponent } from './sender/info-sender/info-sender.component';

@NgModule({
  declarations: [ItemslistComponent, NewitemComponent, ItemstableComponent, PurchaselistComponent, NewpurchaseComponent, PurchasefilterComponent, PurchasetableComponent, InfopurchaseComponent, GeneratePaymentComponent, InventoryMovementComponent, InventoryMovementTableComponent, InventoryGeneralComponent, InventoryGeneralFilterComponent, InventoryGeneralTableComponent, InventoryFlowComponent, InventoryFlowFilterComponent, InventoryFlowTableComponent, EditpurchaseComponent, ReferenceGuidesComponent, ReferenceGuidesTableComponent, NewReferenceGuidesComponent, InfoReferenceGuidesComponent, SenderComponent, SenderTableComponent, NewSenderComponent, InfoSenderComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule        
  ]
})
export class ProductsModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
