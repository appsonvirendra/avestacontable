import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getSalesList(bodyparams) {
    return this.apiService.get(`/sales`, bodyparams)
      .pipe(map(data => data.data))
  }  

  postSale(bodyparams) {
    return this.apiService.post(`/sale`, bodyparams)
      .pipe(map(data => data))
  }  

  getSingleSale(bodyparams) {
    return this.apiService.get(`/sale`, bodyparams)
      .pipe(map(data => data.data[0]))
  }  

  putSale(company_id, branch_id, product_id, bodyparams) {
    return this.apiService.put(`/sale/`+company_id+`/`+branch_id+`/`+product_id, bodyparams)
      .pipe(map(data => data))
  } 
  
  deleteSale(product_id) {
    return this.apiService.delete(`/sale/`+product_id)
      .pipe(map(data => data))
  }   

}
