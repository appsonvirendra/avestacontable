import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup } from '@angular/forms';

import { SalesService } from 'src/app/pages/products/items/sales.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-itemstable',
  templateUrl: './itemstable.component.html',
  styleUrls: ['./itemstable.component.scss']
})
export class ItemstableComponent implements OnInit {

  confirmModalFooter = null

  @Input() itemDatas: any

  // category dialog
  categoryDialogVisible = false
  addCategoryForm: FormGroup  

  salePriceCollapse = false

  // sale prive dialog
  priceIsv1Visible = false
  priceIsv1Form: FormGroup  
  saleDialogNumber: any

  // supplier dialog
  supplierDialogVisible = false
  supplierForm: FormGroup

  nofity_add_remove = 0

  // search
  search_title = ''

  itemDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  // item view visible
  itemViewVisible = false

  deleteConfirmVisible = false

  loader: boolean = false
  saleDatas: any  

  dialogForView: 'View'
  dialogForEdit: 'Edit'

  deleteSaleId: any

  deleteBtnLoading: boolean = false

  @Output("populateGetSalesList") populateGetSalesList: EventEmitter<any> = new EventEmitter();

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _salesService: SalesService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { 

  }

  ngOnInit() {   
    this.itemData()
  }     
  
  showSalePriceCollapse() {
    this.salePriceCollapse = !this.salePriceCollapse
  }

  // search
  itemData() {
    this.itemDisplayDatas = [...this.itemDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { product_name: string; description: string; sale_price1: string }) => {
      return (
        item.product_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.description.toLowerCase().indexOf(searchLower) !== -1 ||
        item.sale_price1.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.itemDatas.filter((item: { product_name: string; description: string; sale_price1: string }) => filterFunc(item))

    this.itemDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.itemDisplayDatas = [...this.itemDatas]
    }    
  }  

  itemViewOpen(product_id_value) {
    this.itemViewVisible = true
    this.populateGetSaleList(product_id_value)
  }

  populateGetSaleList(product_id_value) {
    this.loader = true
    let params = {
      product_id: product_id_value
    }
    this._salesService.getSingleSale(params).subscribe((response) => {
      this.saleDatas = response

      this.openMessageBar(this.translate.instant('ITEM_DETAIL'))

      this.loader = false      
    }, (error) => {
      this.loader = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleSale')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }

  itemViewClose() {
    this.itemViewVisible = false
  }

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteSaleId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteSale() {         
    this.deleteBtnLoading = true

    this._salesService.deleteSale(this.deleteSaleId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get sales list api
        this.populateGetSalesList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteSale')
      this.deleteConfirmClose()
    })      
  }    

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
