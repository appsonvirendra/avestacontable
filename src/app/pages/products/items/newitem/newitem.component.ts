import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup } from '@angular/forms';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { SalesService } from 'src/app/pages/products/items/sales.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { Router, ActivatedRoute } from '@angular/router';

import { CategoriesService } from 'src/app/pages/settings/inventory/categories/categories.service';

import { SuppliersService } from 'src/app/pages/suppliers/suppliers.service';

import { CompanyService } from 'src/app/pages/login/company.service';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';

@Component({
  selector: 'app-newitem',
  templateUrl: './newitem.component.html',
  styleUrls: ['./newitem.component.scss']
})
export class NewitemComponent implements OnInit {

  product_id: any
  pageType: any

  categoriesAddForm: FormGroup
  categoryDialogVisible = false
  
  supplierAddForm: FormGroup
  supplierDialogVisible = false  
  supplierBtnLoading: boolean = false

  itemForm: FormGroup

  nofity_add_remove = 0

  salePriceCollapse = false

  priceIsv1Visible = false

  priceIsv1Form: FormGroup

  saleDialogNumber: any

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')  

  btnLoading: boolean = false
  
  btnLoadingAddCategory: boolean = false
  
  cateLoader: boolean = false
  suplierLoader: boolean = false
  loader: boolean = false

  categoriesDatas: any
  supplierDatas: any
  currencyDatas: any
  utilConstantsDatas: any
  wineryDatas: any

  defaultWineryData: any = {}

  // Item type product required/unrequired and hide/show
  itemTypeRequired: any
  itemTypeShow: any  

  base64String:String = null  

  saleDatas: any

  saleDataLoader: any
  saleDataVisible: any

  selectedWineries: any = []

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _salesService: SalesService,
    private message: NzMessageService,
    private _branchService: BranchService,
    private _jwtService: JwtService,
    private authService: AuthenticationService,
    private _router: Router,
    private _categoriesService: CategoriesService,
    private _companyService: CompanyService,
    private _wineriesService: WineriesService,
    private _suppliersService: SuppliersService,
    private _activatedRoute: ActivatedRoute    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.priceIsv1FormInit()   
    
    this.populateGetCategoriesList()    
    this.populateGetSupplierList()

    this.populateGetCurrencyList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  // Get current query params
  getQueryParams() {
    this._activatedRoute.queryParams.subscribe(params => { 
      this.product_id = params.product_id  
      if(this.product_id) {
        this.pageType = 'edit'
        this.populateGetSale()
      } else {        
        this.pageType = 'new'
        this.itemFormInitNew()
        this.saleDataVisible = true
      }
    })    
  }  

  populateGetSale() {
    this.saleDataLoader = true
    let params = {
      product_id: this.product_id
    }
    this._salesService.getSingleSale(params).subscribe((response) => {
      this.saleDatas = response

      // Item type product change wineries data
      if(this.saleDatas.sale_type == 'P') {
        this.saleDatas.wineries.forEach(value => {
          this.selectedWineries.push(value.wineries_id)
        })
        this.saleDatas.wineries = this.selectedWineries
      }

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.itemFormInitEdit()

      this.saleDataLoader = false     
      this.saleDataVisible = true 
    }, (error) => {
      this.saleDataLoader = false
      this.saleDataVisible = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleSale')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }  

  // edit item form
  itemFormInitEdit() {

    let lowMail

    // Low mail stock value
    if(this.saleDatas.low_mail_stock == 'Y') {
      lowMail = true
    } else {
      lowMail = false
    }    

    this.itemForm = this.fb.group({
      sale_type: [this.saleDatas.sale_type],
      product_name: [this.saleDatas.product_name],
      product_code: [this.saleDatas.product_code],
      category_id: [this.saleDatas.category_id],
      supplier_id: [this.saleDatas.supplier_id],
      image: [null],
      description: [this.saleDatas.description],
      currency_code: [this.saleDatas.currency_code],
      discount: [this.saleDatas.discount],
      sale_price1: [this.saleDatas.sale_price1],
      cost_price:[this.saleDatas.cost_price],
      sales_tax1: [this.saleDatas.sales_tax1],
      purchase_tax1: [this.saleDatas.purchase_tax1],
      initial_warehouse:[this.saleDatas.initial_warehouse],
      initial_amount:[this.saleDatas.initial_amount],
      low_mail_stock: [lowMail],
      notify_in_stock: [this.saleDatas.notify_in_stock],
      wineries: [this.saleDatas.wineries],
      minimum_existence: [this.saleDatas.minimum_existence]
    })

    this.productTypeSelection(this.saleDatas.sale_type)
  }  

  itemFormInitNew() {
    this.itemForm = this.fb.group({
      sale_type: ['S'],
      product_name: [null],
      product_code: [null],
      category_id: [null],
      supplier_id: [null],
      image: [null],
      description: [null],
      currency_code: ['HNL'],
      discount: ['0'],
      sale_price1: [null],
      cost_price:[null],
      sales_tax1: [null],
      purchase_tax1: [null],
      initial_warehouse:[null],
      initial_amount:[null],
      low_mail_stock: [false],
      notify_in_stock: ['0'],
      wineries: [null],
      minimum_existence: [null]
    })      
  }

  populateGetCategoriesList() {
    this.cateLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._categoriesService.getCategoriesList(params).subscribe((response) => {
      this.categoriesDatas = response
      this.openMessageBar(this.translate.instant('CATEGORIES_LIST_RECEIVED_SUCCESSFUL'))

      this.cateLoader = false
    }, (error) => {
      this.cateLoader = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCategoriesList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }
  
  populateGetSupplierList() {
    this.suplierLoader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._suppliersService.getSupplierList(params).subscribe((response) => {
      this.supplierDatas = response
      this.openMessageBar(this.translate.instant('SUPPLIER_LIST_RECEIVED_SUCCESSFUL'))

      this.suplierLoader = false      
    }, (error) => {
      this.suplierLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSupplierList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetCurrencyList() {
    this.loader = true
    this._companyService.getCurrencyList().subscribe((response) => {
      this.currencyDatas = response

      this.openMessageBar(this.translate.instant('CURRENCY_LIST_RECEIVED_SUCCESSFUL'))
   
      this.populateGetUtilConstantsList()
    }, (error) => {
      this.populateGetUtilConstantsList()

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCurrencyList')
    })     
  }  

  populateGetUtilConstantsList() {
    let params = {
      company_id: this.companyData.id,
      constant_code: "TAX_RATE"
    }    
    this._companyService.getUtilConstantsList(params).subscribe((response) => {
      this.utilConstantsDatas = response

      this.utilConstantsDatas.forEach(value => {
        value.constant_name_value = value.constant_name + ' (' + value.value + ')'
      })

      this.openMessageBar(this.translate.instant('UTILS_LIST_RECEIVED_SUCCESSFUL'))

      this.populateGetWineryList()          
    }, (error) => {
      this.populateGetWineryList()
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getUtilConstantsList')
    })     
  }  
  
  populateGetWineryList() {
    let params = {
      company_id: this.companyData.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      this.wineryDatas = response

      // Asign default winery data
      this.wineryDatas.forEach(element => {
        if(element.default_winery) {
          this.defaultWineryData = element
        }
      })

      this.openMessageBar(this.translate.instant('WINERY_LIST_RECEIVED_SUCCESSFUL'))

      this.getQueryParams()

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }  

  productTypeSelection(value) {
    // Condition for required
    if(value == 'P') {
      this.itemTypeRequired = true
    } else {
      this.itemTypeRequired = false
    }    

    // Timeout for detail section hide/show
    setTimeout(() => {
      if(value == 'P') {
        this.itemTypeShow = true
      } else {      
        this.itemTypeShow = false
      }
    }, 300)    
  }

  handleFileSelect(evt) {
    var files = evt.target.files
    var file = files[0]

    if (files && file) {
      var reader = new FileReader()

      reader.onload =this._handleReaderLoaded.bind(this)

      reader.readAsBinaryString(file)
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result
    this.base64String = btoa(binaryString)
  }  

  saveSale() {   
    // Winery null check
    let params, winery, lowMail
    if(this.itemForm.value.wineries != null) {
      winery = this.itemForm.value.wineries.toString()
    } else {
      winery = null
    }

    // Low mail stock value
    if(this.itemForm.value.low_mail_stock == true) {
      lowMail = 'Y'
    } else {
      lowMail = 'N'
    }

    if(this.pageType == 'new') {
      params = {
        company_id: this.companyData.id,
        branch_id: this.companyBranchId,
        sale_type: this.itemForm.value.sale_type, 
        product_name: this.itemForm.value.product_name, 
        product_code: this.itemForm.value.product_code,
        category_id: this.itemForm.value.category_id,
        supplier_id: this.itemForm.value.supplier_id,
        description: this.itemForm.value.description,
        image: this.base64String,
        currency_code: this.itemForm.value.currency_code,
        discount: this.itemForm.value.discount,
        sale_price1: this.itemForm.value.sale_price1,
        cost_price: this.itemForm.value.cost_price,
        sales_tax1: this.itemForm.value.sales_tax1,
        purchase_tax1: this.itemForm.value.purchase_tax1,
        initial_warehouse: this.itemForm.value.initial_warehouse,
        initial_amount: this.itemForm.value.initial_amount,
        low_mail_stock: lowMail,
        wineries: winery,
        minimum_existence: this.itemForm.value.minimum_existence,
        notify_in_stock: this.itemForm.value.notify_in_stock
      }
    } else {
      params = {
        productDetails: {
          company_id: this.companyData.id,
          branch_id: this.companyBranchId,
          sale_type: this.itemForm.value.sale_type, 
          product_name: this.itemForm.value.product_name, 
          product_code: this.itemForm.value.product_code,
          category_id: this.itemForm.value.category_id,
          supplier_id: this.itemForm.value.supplier_id,
          description: this.itemForm.value.description,
          image: this.base64String,
          currency_code: this.itemForm.value.currency_code,
          discount: this.itemForm.value.discount,
          sale_price1: this.itemForm.value.sale_price1,
          cost_price: this.itemForm.value.cost_price,
          sales_tax1: this.itemForm.value.sales_tax1,
          purchase_tax1: this.itemForm.value.purchase_tax1,
          initial_warehouse: this.itemForm.value.initial_warehouse,
          initial_amount: this.itemForm.value.initial_amount,
          low_mail_stock: lowMail,
          minimum_existence: this.itemForm.value.minimum_existence,
          notify_in_stock: this.itemForm.value.notify_in_stock
        },
        wineries: winery
      }

      // Item edit remove image key if encoded string not persent
      if(params.productDetails.image == null) {
        delete params.productDetails.image
      }      
    }

    // Call update sale method or insert sale method based on page type
    if (this.pageType == 'edit') {
      this.updateSale(params)    
    } else {   
      this.insertSale(params)
    }    
   
  }  

  updateSale(itemRowData) {
    this.btnLoading = true
    this._salesService.putSale(this.companyData.id, this.companyBranchId, this.product_id, itemRowData).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to item list page
        this.gotoItemList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateSale')
    })  
  }

  insertSale(itemRowData) {
    this.btnLoading = true
    this._salesService.postSale(itemRowData).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to item list page
        this.gotoItemList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertSale')
    })    
  }

  gotoItemList() {
    this._router.navigate(['/user/products/items/list'], {})
  }  

  notify_add_remove(zero_one) {    
    if(zero_one) {
      this.nofity_add_remove += 1
    } else {
      if(this.itemForm.value.item_notify_in_stock > 0) {
        this.nofity_add_remove -= 1
      } 
    }
    
    this.itemForm.patchValue({
      item_notify_in_stock: this.nofity_add_remove
    })
        
  }

  clearFilter() {
    this.itemForm = this.fb.group({
      sale_type: ['S'],
      product_name: [null],
      product_code: [null],
      category_id: [null],
      supplier_id: [null],
      image: [null],
      description: [null],
      currency_code: ['HNL'],
      discount: ['0'],
      sale_price1: [null],
      cost_price:[null],
      sales_tax1: [null],
      purchase_tax1: [null],
      initial_warehouse:[null],
      initial_amount:[null],
      low_mail_stock: [false],
      notify_in_stock: ['0'],
      wineries: [null],
      minimum_existence: [null]
    })  
  }  

  categoriesAddFormInit() {
    this.categoriesAddForm = this.fb.group({
      category_code: [null],
      category_name: [null],
      category_description: [null]
    })   
  }

  openCategoryDialog() {
    this.categoriesAddFormInit()
    this.categoryDialogVisible = true
  }

  closeCategoryDialog() {
    this.categoryDialogVisible = false
  }

  insertCategories() {
    this.btnLoadingAddCategory = true
    let params = {
      company_id: this.companyData.id,
      category_code: this.categoriesAddForm.value.category_code, 
      category_name: this.categoriesAddForm.value.category_name, 
      category_description: this.categoriesAddForm.value.category_description
    }

    this._categoriesService.postCategories(params).subscribe((response) => {
      this.btnLoadingAddCategory = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        this.closeCategoryDialog()

        // Populate categories list api
        this.populateGetCategoriesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoadingAddCategory = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCategories')
    })       
  }

  // supplier form
  supplierFormInit() {
    this.supplierAddForm = this.fb.group({
      supplier_name: [null],
      identification: [null],
      phone: [null],
      mobile: [null],
      fax: [null],
      email: [null],
      direction: [null]
    })   
  }  

  openSupplierDialog() {
    this.supplierFormInit()
    this.supplierDialogVisible = true
  }

  closeSupplierDialog() {
    this.supplierDialogVisible = false
  } 
  
  insertSupplier() {
    this.supplierBtnLoading = true
    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      supplier_name: this.supplierAddForm.value.supplier_name, 
      identification: this.supplierAddForm.value.identification, 
      phone: this.supplierAddForm.value.phone,
      mobile: this.supplierAddForm.value.mobile,
      fax: this.supplierAddForm.value.fax,
      email: this.supplierAddForm.value.email,
      direction: this.supplierAddForm.value.direction
    }

    this._suppliersService.postSupplier(params).subscribe((response) => {
      this.supplierBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        this.closeSupplierDialog()

        // Populate supplier list api
        this.populateGetSupplierList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.supplierBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertSupplier')
    })       
  }  

  // sales and price dialog
  priceIsv1open(saleDialogNumber) {
    this.saleDialogNumber = saleDialogNumber
    this.priceIsv1Visible = true
  }

  priceIsv1close() {
    this.priceIsv1Visible = false
  }    

  priceIsv1FormInit() {
    this.priceIsv1Form = this.fb.group({
      final_price: [null],
      tax_to_calculate: [null],
    })  
  }

  priceIsv1calculate() {
    let add_tax = this.priceIsv1Form.value.final_price * (this.priceIsv1Form.value.tax_to_calculate/100)

    let calc_sale_price = this.priceIsv1Form.value.final_price + add_tax

    // Update sale price into itemForm
    this.itemForm.patchValue({
      sale_price1: calc_sale_price
    })

    this.priceIsv1FormInit()
    this.priceIsv1Visible = false    
  }

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
