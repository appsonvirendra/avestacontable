import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  validateForm: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.purchaseFormInit()
    
  }
  purchaseFormInit() {
  this.validateForm = this.fb.group({
      identificationcard: [null],
      name: [null],
      user: [null],
      date: [null],
      seat_file: [null],
      mobile: [null],
      phone: [null],
      mail: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confirm: [null, [this.confirmValidator]],
      branch: [null],
      country: ['HN']
    }) 
  }   

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  validateConfirmPassword(): void {
    setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
  }  

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true }
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true }
    }
    return {}
  } 

  submitForm() {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty()
      this.validateForm.controls[i].updateValueAndValidity()
    }
  }  

}
