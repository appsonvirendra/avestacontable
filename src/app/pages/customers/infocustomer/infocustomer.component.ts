import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CustomersService } from 'src/app/pages/customers/customers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-infocustomer',
  templateUrl: './infocustomer.component.html',
  styleUrls: ['./infocustomer.component.scss']
})
export class InfocustomerComponent implements OnInit {

  customer_id: any

  active_tab: any

  customerInfoDatas: any

  customerAccountDatas: any

  customerUnPaidBills: any

  customerIssuedInvoiceBills: any

  quotationDatas: any

  invoiceDatas: any

  purchaseDatas: any

  loader = true

  btnLoading: any  

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private _customersService: CustomersService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()
    
    this.customerInfo()
    
    this.quotationData()
    this.invoiceData()
    this.purchaseData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  getQueryParams() {
    this.route.queryParams.subscribe(params => {
      this.customer_id = params.customer_id
      this.active_tab = params.active_tab
      this.populateGetSingleCustomer()
    })    
  }

  populateGetSingleCustomer() {
    this.loader = true
    let params = {
      customer_id: this.customer_id
    }
    this._customersService.getSingleCustomer(params).subscribe((response) => {
      this.loader = false      
      this.customerInfoDatas = response
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleCustomer')
    })     
  }    

  customerInfo() {      
    this.customerAccountDatas = [
      {
        "invoice_id": "22",
        "invoice_no": "22",
        "invoice_type": "1",
        "date": "2019-11-09",
        "description": "000-001-222-333",
        "should": "25000",
        "to_have": "0",
        "balance": "25000"
      },
      {
        "invoice_id": "22",
        "invoice_no": "22",
        "invoice_type": "1",        
        "date": "2019-11-09",
        "description": "000-001-222-331",
        "should": "0",
        "to_have": "30000",
        "balance": "30000"
      },
      {
        "invoice_id": "22",
        "invoice_no": "22",
        "invoice_type": "2",        
        "date": "2019-11-09",
        "description": "000-001-222-332",
        "should": "50000",
        "to_have": "0",
        "balance": "50000"
      },
      {
        "invoice_id": "22",
        "invoice_no": "22",
        "invoice_type": "2",        
        "date": "2019-11-09",
        "description": "000-001-222-333",
        "should": "40000",
        "to_have": "0",
        "balance": "40000"
      },
      {
        "invoice_id": "22",
        "invoice_no": "22",
        "invoice_type": "2",        
        "date": "2019-11-09",
        "description": "000-001-222-334",
        "should": "0",
        "to_have": "70000",
        "balance": "70000"
      }            
    ]
    
    this.customerUnPaidBills = [
      {
        "un_paid_bill_id": "1",
        "invoice_no": "000-001-01-00000335",
        "date": "2019-11-09",
        "balance": "15029"
      },
      {
        "un_paid_bill_id": "2",
        "invoice_no": "000-001-01-00000335",
        "date": "2019-11-09",
        "balance": "16029"
      }           
    ]
    
    this.customerIssuedInvoiceBills = [
      {        
        "issued_invoice_bill_id": "1",
        "invoice_no": "000-001-01-00000335",
        "date": "2019-11-09",
        "total": "15029",
        "balance": "15029",
        "payment_method": "Otros",
        "payment_state": "1"
      },
      {
        "issued_invoice_bill_id": "2",
        "invoice_no": "000-001-01-00000336",
        "date": "2019-11-09",
        "total": "15029",
        "balance": "15029",
        "payment_method": "Otros",
        "payment_state": "0"
      }           
    ]    
  }

  quotationData() {
    this.quotationDatas = [
      {
        quotation_id: '1',
        quotation_code: '000-001-01-001',
        invoice_id: '1',
        customer_id: '11',
        customer_firstname: 'Roy',
        customer_lastname: 'King',
        quotation_provider_name: 'Provider 1',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '2'
      }, 
      {
        quotation_id: '2',        
        quotation_code: '000-001-01-002',
        invoice_id: '2',
        customer_id: '22',
        customer_firstname: 'Jennifer',
        customer_lastname: 'Hart',        
        quotation_provider_name: 'Provider 2',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '2'
      },
      {
        quotation_id: '3',
        quotation_code: '000-001-01-003',
        invoice_id: '3',
        customer_id: '33',
        customer_firstname: 'Olivia',
        customer_lastname: 'Carr',        
        quotation_provider_name: 'Provider 3',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '1'
      }, 
      {
        quotation_id: '4',
        quotation_code: '000-001-01-004',
        invoice_id: '4',
        customer_id: '33',
        customer_firstname: 'Jean',
        customer_lastname: 'Shaw',        
        quotation_provider_name: 'Provider 1',
        quotation_total: '104',
        quotation_balance: '104',
        quotation_date: '2018-12-23',
        quotation_state: '0'
      }                      
    ]
  }
  
  invoiceData() {
    this.invoiceDatas = [
      {
        invoice_id: '1',
        invoice_code: '000-001-01-001',
        customer_id: '11',
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '2',
        invoice_code: '000-001-01-002',
        customer_id: '22',
        invoice_provider_name: 'Provider 2',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '0'
      },
      {
        invoice_id: '3',
        invoice_code: '000-001-01-003',
        customer_id: '33',
        invoice_provider_name: 'Provider 3',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }, 
      {
        invoice_id: '4',
        invoice_code: '000-001-01-004',
        customer_id: '33',
        invoice_provider_name: 'Provider 1',
        invoice_total: '104',
        invoice_balance: '104',
        invoice_date: '2018-12-23',
        invoice_state: '1'
      }                       
    ]
  }  

  purchaseData() {
    this.purchaseDatas = [
      {
        purchase_id: '1',
        purchase_code: '000-001-01-001',
        purchase_supplier_name: 'Provider 1',
        purchase_total: '104',
        purchase_balance: '107',
        purchase_date: '2018-12-23',
        purchase_state: '1',
        purchase_active_status: '1'
      }, 
      {
        purchase_id: '2',
        purchase_code: '000-001-01-002',
        purchase_supplier_name: 'Provider 2',
        purchase_total: '105',
        purchase_balance: '108',
        purchase_date: '2018-12-24',
        purchase_state: '0',
        purchase_active_status: '1'
      },
      {
        purchase_id: '3',
        purchase_code: '000-001-01-003',
        purchase_supplier_name: 'Provider 3',
        purchase_total: '106',
        purchase_balance: '109',
        purchase_date: '2018-12-25',
        purchase_state: '1',
        purchase_active_status: '0'
      }, 
      {
        purchase_id: '4',
        purchase_code: '000-001-01-004',
        purchase_supplier_name: 'Provider 4',
        purchase_total: '107',
        purchase_balance: '110',
        purchase_date: '2018-12-26',
        purchase_state: '1',
        purchase_active_status: '1'
      }                       
    ]
  }
  
  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
