import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-infotab2',
  templateUrl: './infotab2.component.html',
  styleUrls: ['./infotab2.component.scss']
})
export class Infotab2Component implements OnInit {

  isVisible = false

  customerAccountStatusFilter: FormGroup

  accountStatusEmail: FormGroup

  @Input() customerInfoData: any
  @Input() customerAccountData: any

  currentDate:any = new Date()

  emailModalFooter: any = null

  constructor(
    private fb: FormBuilder
  ) { 
    
  }

  ngOnInit() {
    this.customerAccount()
    this.formVariablesInit()

    this.accountEmailInit()
  }

  formVariablesInit() {
    this.customerAccountStatusFilter = this.fb.group({
      date_from: [null],
      date_to: [null]
    })      
  }      

  accountEmailInit() {
    this.accountStatusEmail = this.fb.group({
      email1: [null, [Validators.required]],
      email2: [null],
      email3: [null]
    })          
  }

  customerAccount() {

    // console.log('this.customerInfoData', this.customerInfoData)
    // console.log('this.customerAccountData', this.customerAccountData)
  }

  accountStatusFilter() {
    // console.log('this.customerAccountStatusFilter', this.customerAccountStatusFilter.value)
  }

  clearFilter() {
    this.customerAccountStatusFilter = this.fb.group({
      date_from: [null],
      date_to: [null]
    })
  }

  accountStatusPrint() {

  }

  accountStatusSendMail() {    
    this.isVisible = true
  }

  actMailSubmit() {
    for (const i in this.accountStatusEmail.controls) {
      this.accountStatusEmail.controls[i].markAsDirty()
      this.accountStatusEmail.controls[i].updateValueAndValidity()
    }    
    if(this.accountStatusEmail.value.email1 != null && this.accountStatusEmail.value.email1 != '') {
      this.isVisible = false
      this.accountStatusEmail.reset()
    }
  }

  handleCancel() {
    this.isVisible = false
  }   

}
