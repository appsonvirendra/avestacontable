import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-unpaid-bills',
  templateUrl: './unpaid-bills.component.html',
  styleUrls: ['./unpaid-bills.component.scss']
})
export class UnpaidBillsComponent implements OnInit {

  @Input() customerUnPaidBills: any

  constructor() { }

  ngOnInit() {
  }

}
