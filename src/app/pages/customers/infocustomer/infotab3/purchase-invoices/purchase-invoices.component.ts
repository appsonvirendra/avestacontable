import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-purchase-invoices',
  templateUrl: './purchase-invoices.component.html',
  styleUrls: ['./purchase-invoices.component.scss']
})
export class PurchaseInvoicesComponent implements OnInit {

  @Input() invoiceDatas: any

  constructor() { }

  ngOnInit() {
  }

}
