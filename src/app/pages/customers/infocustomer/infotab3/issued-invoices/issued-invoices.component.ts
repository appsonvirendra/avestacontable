import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-issued-invoices',
  templateUrl: './issued-invoices.component.html',
  styleUrls: ['./issued-invoices.component.scss']
})
export class IssuedInvoicesComponent implements OnInit {

  @Input() customerIssuedInvoiceBills: any

  constructor() { }

  ngOnInit() {
  }

}
