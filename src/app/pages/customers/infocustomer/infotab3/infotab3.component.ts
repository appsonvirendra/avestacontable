import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-infotab3',
  templateUrl: './infotab3.component.html',
  styleUrls: ['./infotab3.component.scss']
})
export class Infotab3Component implements OnInit {

  @Input() customerUnPaidBills: any
  @Input() customerIssuedInvoiceBills: any
  @Input() quotationDatas: any
  @Input() invoiceDatas: any
  @Input() purchaseDatas: any

  constructor() { }

  ngOnInit() {
  }

}
