import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CustomersService } from 'src/app/pages/customers/customers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-infotab4',
  templateUrl: './infotab4.component.html',
  styleUrls: ['./infotab4.component.scss']
})
export class Infotab4Component implements OnInit {

  customerEditform: FormGroup

  @Input() customerInfoData: any

  btnLoading: boolean = false  

  personalRequired: boolean = true 
  companyRequired: boolean = false

  personalDetailShow: boolean = true  

  @Output("populateGetSingleCustomer") populateGetSingleCustomer: EventEmitter<any> = new EventEmitter()

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _customersService: CustomersService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.formVariablesInit()
  }

  formVariablesInit() {
    if(this.customerInfoData.type == 'P') {
      this.personalDetailShow = true
    } else {
      this.personalDetailShow = false
    }
    this.customerEditform = this.fb.group({
      customer_id: [this.customerInfoData.customer_id],
      type: [this.customerInfoData.type],
      tax_exempt: [this.customerInfoData.tax_exempt],
      assign_seller: [this.customerInfoData.assign_seller],
      id_card: [this.customerInfoData.id_card],
      name: [this.customerInfoData.name],
      last_name: [this.customerInfoData.last_name],
      birth_date: [this.customerInfoData.birth_date],
      sex: [this.customerInfoData.sex],
      company: [this.customerInfoData.company],
      identification: [this.customerInfoData.identification],
      direction: [this.customerInfoData.direction],
      city: [this.customerInfoData.city],
      country: [this.customerInfoData.country],
      phone: [this.customerInfoData.phone],
      fax: [this.customerInfoData.fax],
      mobile: [this.customerInfoData.mobile],
      postal_code: [this.customerInfoData.postal_code],
      email: [this.customerInfoData.email],
      name1: [this.customerInfoData.name1],
      last_name1: [this.customerInfoData.last_name1],
      mobile1: [this.customerInfoData.mobile1],
      email1: [this.customerInfoData.email1],
      name2: [this.customerInfoData.name2],
      last_name2: [this.customerInfoData.last_name2],
      mobile2: [this.customerInfoData.mobile2],
      email2: [this.customerInfoData.email2],
      name3: [this.customerInfoData.name3],
      last_name3: [this.customerInfoData.last_name3],
      mobile3: [this.customerInfoData.mobile3],
      email3: [this.customerInfoData.email3]
    })      
  }  

  customerTypeSelection(value) {
    if(value == 'P') {
      this.personalRequired = true
      this.companyRequired = false
    } else {
      this.personalRequired = false
      this.companyRequired = true        
    }

    // Timeout for detail section hide/show
    setTimeout(() => {
      if(value == 'P') {
        this.personalDetailShow = true
      } else {      
        this.personalDetailShow = false     
      }
    }, 300)    
  }  

  updateCustomer() {
    this.btnLoading = true
    let params
    if(this.customerEditform.value.type == 'P') {

      params = {
        type: this.customerEditform.value.type, 
        tax_exempt: this.customerEditform.value.tax_exempt, 
        assign_seller: this.customerEditform.value.assign_seller,
        id_card: this.customerEditform.value.id_card,
        name: this.customerEditform.value.name,
        last_name: this.customerEditform.value.last_name,
        birth_date: this.customerEditform.value.birth_date,
        sex: this.customerEditform.value.sex,
        identification: this.customerEditform.value.identification,
        postal_code: this.customerEditform.value.postal_code,
        direction: this.customerEditform.value.direction,
        city: this.customerEditform.value.city,
        country: this.customerEditform.value.country,
        phone: this.customerEditform.value.phone,
        fax: this.customerEditform.value.fax,
        mobile: this.customerEditform.value.mobile,
        email: this.customerEditform.value.email,
      }      

    } else {

      params = {
        type: this.customerEditform.value.type, 
        tax_exempt: this.customerEditform.value.tax_exempt, 
        assign_seller: this.customerEditform.value.assign_seller,        
        company: this.customerEditform.value.company,
        identification: this.customerEditform.value.identification,
        postal_code: this.customerEditform.value.postal_code,
        direction: this.customerEditform.value.direction,
        city: this.customerEditform.value.city,
        country: this.customerEditform.value.country,
        phone: this.customerEditform.value.phone,
        fax: this.customerEditform.value.fax,
        mobile: this.customerEditform.value.mobile,
        email: this.customerEditform.value.email,
        name1: this.customerEditform.value.name1,
        last_name1: this.customerEditform.value.last_name1,
        mobile1: this.customerEditform.value.mobile1,
        email1: this.customerEditform.value.email1,
        name2: this.customerEditform.value.name2,
        last_name2: this.customerEditform.value.last_name2,
        mobile2: this.customerEditform.value.mobile2,
        email2: this.customerEditform.value.email2,        
        name3: this.customerEditform.value.name3,
        last_name3: this.customerEditform.value.last_name3,
        mobile3: this.customerEditform.value.mobile3,
        email3: this.customerEditform.value.email3
      }      

    }

    this._customersService.putCustomer(this.customerEditform.value.customer_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        this.populateGetSingleCustomer.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateCustomer')      
    })       
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
