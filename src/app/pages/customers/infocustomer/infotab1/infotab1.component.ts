import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-infotab1',
  templateUrl: './infotab1.component.html',
  styleUrls: ['./infotab1.component.scss']
})
export class Infotab1Component implements OnInit {

  @Input() customerInfoData: any

  constructor() { }

  ngOnInit() {
  }

}
