import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getCustomerList(bodyparams) {
    return this.apiService.post(`/customerList`, bodyparams)
      .pipe(map(data => data.data))
  }

  postCustomer(bodyparams) {
    return this.apiService.post(`/customerNew`, bodyparams)
      .pipe(map(data => data))
  }      

  getSingleCustomer(bodyparams) {
    return this.apiService.post(`/customerData`, bodyparams)
      .pipe(map(data => data.data[0]))
  }
  
  putCustomer(customer_id, bodyparams) {
    return this.apiService.post(`/customerUpdate/`+customer_id, bodyparams)
      .pipe(map(data => data))
  }  

  deleteCustomer(customer_id) {
    return this.apiService.post(`/customerDelete/`+customer_id)
      .pipe(map(data => data))
  }  

}
