import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerslistComponent } from './customerslist/customerslist.component';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

// mdi icon set up
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry, MatIconModule } from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerstableComponent } from './customerslist/customerstable/customerstable.component';
import { NewcustomerComponent } from './newcustomer/newcustomer.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { InfocustomerComponent } from './infocustomer/infocustomer.component';
import { Infotab1Component } from './infocustomer/infotab1/infotab1.component';
import { Infotab2Component } from './infocustomer/infotab2/infotab2.component';
import { Infotab3Component } from './infocustomer/infotab3/infotab3.component';
import { Infotab4Component } from './infocustomer/infotab4/infotab4.component';
import { UnpaidBillsComponent } from './infocustomer/infotab3/unpaid-bills/unpaid-bills.component';
import { IssuedInvoicesComponent } from './infocustomer/infotab3/issued-invoices/issued-invoices.component';
import { QuotationComponent } from './infocustomer/infotab3/quotation/quotation.component';
import { PurchaseInvoicesComponent } from './infocustomer/infotab3/purchase-invoices/purchase-invoices.component';
import { PurchaseOrdersComponent } from './infocustomer/infotab3/purchase-orders/purchase-orders.component';


@NgModule({
  declarations: [
    CustomerslistComponent, 
    CustomerstableComponent,     
    NewcustomerComponent, 
    InfocustomerComponent, 
    Infotab1Component, Infotab2Component, Infotab3Component, Infotab4Component, UnpaidBillsComponent, IssuedInvoicesComponent, QuotationComponent, PurchaseInvoicesComponent, PurchaseOrdersComponent
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class CustomersModule { 
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
  }    
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
