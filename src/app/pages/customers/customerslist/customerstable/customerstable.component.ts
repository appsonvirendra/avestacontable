import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { CustomersService } from 'src/app/pages/customers/customers.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-customerstable',
  templateUrl: './customerstable.component.html',
  styleUrls: ['./customerstable.component.scss']
})
export class CustomerstableComponent implements OnInit {

  @Input() customerDatas: any
  
  // search
  search_title = ''
  customerDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false  

  deleteCustomerId: any

  @Output("populateGetCustomerList") populateGetCustomerList: EventEmitter<any> = new EventEmitter();  

  constructor(
    private translate: TranslateService,
    private _customersService: CustomersService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.customerData()
  }

  customerData() {
    this.customerDisplayDatas = [...this.customerDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { name: string; phone: string; identification: string; email: string }) => {
      return (
        item.name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.phone.toLowerCase().indexOf(searchLower) !== -1 ||
        item.identification.toLowerCase().indexOf(searchLower) !== -1 ||
        item.email.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.customerDatas.filter((item: { name: string; phone: string; identification: string; email: string }) => filterFunc(item))

    this.customerDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.customerDisplayDatas = [...this.customerDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteCustomerId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }
  
  deleteCustomer() {         
    this.deleteBtnLoading = true

    this._customersService.deleteCustomer(this.deleteCustomerId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get customer list api
        this.populateGetCustomerList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteCustomer')
      this.deleteConfirmClose()
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
