import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CustomersService } from 'src/app/pages/customers/customers.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newcustomer',
  templateUrl: './newcustomer.component.html',
  styleUrls: ['./newcustomer.component.scss']
})
export class NewcustomerComponent implements OnInit {

  customerAddform: FormGroup

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  btnLoading: boolean = false  

  personalRequired: boolean = true 
  companyRequired: boolean = false

  personalDetailShow: boolean = true

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _customersService: CustomersService,
    private message: NzMessageService,
    private _router: Router        
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerAddform = this.fb.group({
      type: ['P'],
      tax_exempt: ['N'],
      assign_seller: [null],
      id_card: [null],
      name: [null],
      last_name: [null],
      birth_date: [null],
      sex: [null],
      company: [null],
      identification: [null],
      direction: [null],
      city: [null],
      country: [null],
      phone: [null],
      fax: [null],
      mobile: [null],
      postal_code: [null],
      email: [null],
      name1: [null],
      last_name1: [null],
      mobile1: [null],
      email1: [null],
      name2: [null],
      last_name2: [null],
      mobile2: [null],
      email2: [null],
      name3: [null],
      last_name3: [null],
      mobile3: [null],
      email3: [null]
    })      
  }

  customerTypeSelection(value) {
    if(value == 'P') {
      this.personalRequired = true
      this.companyRequired = false
    } else {
      this.personalRequired = false
      this.companyRequired = true        
    }

    // Timeout for detail section hide/show
    setTimeout(() => {
      if(value == 'P') {
        this.personalDetailShow = true
      } else {      
        this.personalDetailShow = false     
      }
    }, 300)    
  }

  insertCustomer() {
    this.btnLoading = true
    let params
    if(this.customerAddform.value.type == 'P') {

      params = {
        company_id: this.companyData.id,
        branch_id: this.companyBranchId,
        type: this.customerAddform.value.type, 
        tax_exempt: this.customerAddform.value.tax_exempt, 
        assign_seller: this.customerAddform.value.assign_seller,
        id_card: this.customerAddform.value.id_card,
        name: this.customerAddform.value.name,
        last_name: this.customerAddform.value.last_name,
        birth_date: this.customerAddform.value.birth_date,
        sex: this.customerAddform.value.sex,
        identification: this.customerAddform.value.identification,
        postal_code: this.customerAddform.value.postal_code,
        direction: this.customerAddform.value.direction,
        city: this.customerAddform.value.city,
        country: this.customerAddform.value.country,
        phone: this.customerAddform.value.phone,
        fax: this.customerAddform.value.fax,
        mobile: this.customerAddform.value.mobile,
        email: this.customerAddform.value.email,
      }      

    } else {

      params = {
        company_id: this.companyData.id,
        branch_id: this.companyBranchId,
        type: this.customerAddform.value.type, 
        tax_exempt: this.customerAddform.value.tax_exempt, 
        assign_seller: this.customerAddform.value.assign_seller,        
        company: this.customerAddform.value.company,
        identification: this.customerAddform.value.identification,
        postal_code: this.customerAddform.value.postal_code,
        direction: this.customerAddform.value.direction,
        city: this.customerAddform.value.city,
        country: this.customerAddform.value.country,
        phone: this.customerAddform.value.phone,
        fax: this.customerAddform.value.fax,
        mobile: this.customerAddform.value.mobile,
        email: this.customerAddform.value.email,
        name1: this.customerAddform.value.name1,
        last_name1: this.customerAddform.value.last_name1,
        mobile1: this.customerAddform.value.mobile1,
        email1: this.customerAddform.value.email1,
        name2: this.customerAddform.value.name2,
        last_name2: this.customerAddform.value.last_name2,
        mobile2: this.customerAddform.value.mobile2,
        email2: this.customerAddform.value.email2,        
        name3: this.customerAddform.value.name3,
        last_name3: this.customerAddform.value.last_name3,
        mobile3: this.customerAddform.value.mobile3,
        email3: this.customerAddform.value.email3
      }      

    }

    this._customersService.postCustomer(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to customer list page
        this.gotoCustomerList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCustomer')      
    })       
  }
  
  gotoCustomerList() {
    this._router.navigate(['/user/customers/list'], {})
  }  

  clearFilter() {
    this.customerAddform.reset()
    this.customerAddform.patchValue({
      type: ['P'],
      tax_exempt: ['N']
    })
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  } 

}
