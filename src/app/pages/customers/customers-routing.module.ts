import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerslistComponent } from './customerslist/customerslist.component';
import { NewcustomerComponent } from './newcustomer/newcustomer.component';
import { InfocustomerComponent } from './infocustomer/infocustomer.component';


const routes: Routes = [
  {
    path: 'list',
    component: CustomerslistComponent
  },
  {
    path: 'new',
    component: NewcustomerComponent
  },
  {
    path: 'info_customer',
    component: InfocustomerComponent
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
