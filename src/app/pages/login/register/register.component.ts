import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { CompanyService } from 'src/app/pages/login/company.service';

import { NzMessageService } from 'ng-zorro-antd/message';

import { TranslateService } from '@ngx-translate/core';

import { Router } from '@angular/router';

import { JwtService } from 'src/app/services/jwt.service';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loader: boolean = false

  validateForm: FormGroup

  // Button click disabled and loader on/off
  btnLoading = false

  companydata: any

  companyId: any
  wineryId: any

  planDatas: any
  countryDatas: any

  constructor(
    private fb: FormBuilder,
    private _companyService: CompanyService,
    private message: NzMessageService,
    private translate: TranslateService,
    private router: Router,
    private _jwtService: JwtService,
    private _wineriesService: WineriesService,
    private _branchService: BranchService
  ) { 

  }

  ngOnInit() {
    // Remove older local storage data
    this.removeOldLocalStorage()

    this.populateGetPlanList()
  }

  removeOldLocalStorage() {
    localStorage.removeItem('companyBranchId')
    localStorage.removeItem('avtjwtToken')
    localStorage.removeItem('companyRes')
  }

  registerFormInit() {
    this.validateForm = this.fb.group({
      plan: ['1'],
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      company_name: [null, [Validators.required]],
      contact: [null, [Validators.required]],
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confirm: [null, [this.confirmValidator]],
      country: ['COL']
    })    

  }

  populateGetPlanList() {
    this.loader = true

    this._companyService.getPlanList().subscribe((response) => {
      this.planDatas = response

      this.planDatas.forEach(value => {
        value.plan_id = value.plan_id.toString()

        // plan name and plan description add into one string
        if(value.plan_description != '') {
          value.plan_name_description = value.plan_name + ' (' + value.plan_description + ')'
        } else {
          value.plan_name_description = value.plan_name
        }
      })

      this.openMessageBar(this.translate.instant('PLAN_LIST_RECEIVED_SUCCESSFUL'))

      this.populateGetCountryList()   
    }, (error) => {
      this.populateGetCountryList()

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getPlanList')
    })     
  }
  
  populateGetCountryList() {
    this._companyService.getCountryList().subscribe((response) => {
      this.countryDatas = response

      this.openMessageBar(this.translate.instant('COUNTRY_LIST_RECEIVED_SUCCESSFUL'))

      this.registerFormInit()

      this.loader = false      
    }, (error) => {

      this.registerFormInit()

      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCountryList')
    })     
  }  

  validateConfirmPassword(): void {
    setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
  }  

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true }
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true }
    }
    return {}
  } 

  // Insert company
  insertCompany(companyRowData) {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty()
      this.validateForm.controls[i].updateValueAndValidity()
    }    
    
    if(this.validateForm.valid) {
      this.btnLoading = true
      let params = companyRowData    
      this._companyService.postRegister(params).subscribe((response) => {
        if(response.success) {

          // Check token exist and add local storage and redirect
          if(response.token) {
            this.openMessageBar(this.translate.instant('STEP_1_IS_DONE'))

            this.companydata = {
              id: response.id,
              token: response.token,
              role: 'company',
              email: companyRowData.email
            }
            
            // Save token to localstorage
            this._jwtService.saveToken(response.token)   
            
            // insert winery based on companyId
            this.companyId = response.id
            this.insertWinery()     
          }

        } else {   
          this.btnLoading = false       
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
      }, (error) => {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCompany')
      }) 
    }
  }

  // Auto insert first winery
  insertWinery() {
    let params = {
      company_id: this.companyId,
      name: 'Winery 1',
      address: 'Address', 
      city: 'City', 
      description: 'Description',
      default_winery: 1
    }      

    this._wineriesService.postWinery(params).subscribe((response) => {
      if(response.success) {
        this.openMessageBar(this.translate.instant('STEP_2_IS_DONE'))

        // Insert branch based on companyId and wineryId
        this.wineryId = response.data.wineries_id
        this.insertBranch()
      } else {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertWinery')
    })      
  }  

  // Auto insert first branch
  insertBranch() {       
    let params = {
      company_id: this.companyId,
      branch_name: 'Branch 1',
      cai: '', 
      billing_range: '', 
      address: '',
      city: '',
      email: this.companydata.email,
      invoice_prefix: '',
      printer_type: '1',
      cost_center: '1',
      description: '',
      wineries: this.wineryId
    }

    this._branchService.postBranch(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {        
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(this.translate.instant('STEP_3_IS_DONE'))

        // Insert company data to local storage and redirect to dashboard based on 
        if(this.companydata.role == 'company') {
          localStorage.setItem('companyRes', JSON.stringify(this.companydata))
          this.router.navigate(['/user/start'])
        }

        if(this.companydata.role == 'employee') {
          localStorage.setItem('posRes', JSON.stringify(this.companydata))
          this.router.navigate(['/pos'])
        }        

      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertBranch')
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }     

}