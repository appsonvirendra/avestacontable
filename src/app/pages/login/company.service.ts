import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(
    private apiService: ApiService
  ) {     
  }

  postRegister(bodyParams) {
    return this.apiService.post(`/register`, bodyParams)
      .pipe(map(data => data))
  }
  
  postLogin(bodyParams) {
    return this.apiService.post(`/login`, bodyParams)
      .pipe(map(data => data))
  }
  
  getPlanList() {
    return this.apiService.get(`/planList`)
      .pipe(map(data => data.data))
  }
  
  getCountryList() {
    return this.apiService.get(`/countryList`)
      .pipe(map(data => data.data))
  }
  
  getCurrencyList() {
    return this.apiService.get(`/currencyList`)
      .pipe(map(data => data.data))
  }
  
  getUtilConstantsList(bodyParams) {
    return this.apiService.get(`/utilConstants`, bodyParams)
      .pipe(map(data => data.data))
  }  

  // postRegister(bodyparams) {
  //   return this.http.post(this.storeUrl+`/register`, bodyparams)
  //     .pipe(map(data => data))
  // }

  // login() {
  //   return this.http.get(apiUrl.url.API_URL)
  // }  

}
