import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TranslateService } from '@ngx-translate/core';

import { CompanyService } from '../company.service';

import { NzMessageService } from 'ng-zorro-antd/message';

import { JwtService } from 'src/app/services/jwt.service';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class loginComponent implements OnInit {

  validateForm: FormGroup

  passwordVisible = false
  password: string

  loginData

  // Button click disabled and loader on/off
  btnLoading = false  

  companydata: any

  companyId: any
  wineryId: any  

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private router: Router,
    private authService: AuthenticationService,
    private translate: TranslateService,
    private _companyService: CompanyService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private _wineriesService: WineriesService,
    private _branchService: BranchService    
  ) { 

  }  

  ngOnInit() {
    // Remove older local storage data
    this.removeOldLocalStorage()

    this.languageTranslate()
    
    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })  
  }

  removeOldLocalStorage() {
    localStorage.removeItem('companyBranchId')
    localStorage.removeItem('avtjwtToken')
    localStorage.removeItem('companyRes')
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  loginCompany(formData) {    
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty()
      this.validateForm.controls[i].updateValueAndValidity()
    }
    
    if(this.validateForm.valid) {
      this.btnLoading = true
      let params = formData    
      this._companyService.postLogin(params).subscribe((response) => {
        if(response.success) {

          this.openMessageBar(this.translate.instant('STEP_1_IS_DONE'))

          // Check token exist and add local storage and redirect
          if(response.token) {
            this.companydata = {
              id: response.id,
              token: response.token,
              role: 'company',
              email: formData.email
            }

            // Save token to localstorage
            this._jwtService.saveToken(response.token)

            // insert winery based on companyId
            this.companyId = response.id

            this.populateGetWineryList()                      
          }          

        } else {  
          this.btnLoading = false        
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
      }, (error) => {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' login')
      }) 
    }    
  }  

  populateGetWineryList() {    
    let params = {
      company_id: this.companydata.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      if(response.length > 0) {

        this.openMessageBar(this.translate.instant('STEP_2_IS_DONE'))

        this.btnLoading = false
        // Insert company data to local storage and redirect to dashboard based on 
        if(this.companydata.role == 'company') {
          localStorage.setItem('companyRes', JSON.stringify(this.companydata))
          this.router.navigate(['/user/start'])
        }

        if(this.companydata.role == 'employee') {
          localStorage.setItem('posRes', JSON.stringify(this.companydata))
          this.router.navigate(['/pos'])
        }
      } else {
        this.insertWinery() 
      }  
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }    
  
  // Auto insert first winery
  insertWinery() {
    let params = {
      company_id: this.companyId,
      name: 'Winery 1',
      address: 'Address', 
      city: 'City', 
      description: 'Description',
      default_winery: 1
    }      

    this._wineriesService.postWinery(params).subscribe((response) => {
      if(response.success) {
        this.openMessageBar(this.translate.instant('STEP_2_IS_DONE'))        

        // Insert branch based on companyId and wineryId
        this.wineryId = response.data.wineries_id
        this.insertBranch()
      } else {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('FAILED'))        
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertWinery')
    })      
  }  

  // Auto insert first branch
  insertBranch() {       
    let params = {
      company_id: this.companyId,
      branch_name: 'Branch 1',
      cai: '', 
      billing_range: '', 
      address: '',
      city: '',
      email: this.companydata.email,
      invoice_prefix: '',
      printer_type: '1',
      cost_center: '1',
      description: '',
      wineries: this.wineryId
    }

    this._branchService.postBranch(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(this.translate.instant('STEP_3_IS_DONE'))

        // Insert company data to local storage and redirect to dashboard based on 
        if(this.companydata.role == 'company') {
          localStorage.setItem('companyRes', JSON.stringify(this.companydata))
          this.router.navigate(['/user/start'])
        }

        if(this.companydata.role == 'employee') {
          localStorage.setItem('posRes', JSON.stringify(this.companydata))
          this.router.navigate(['/pos'])
        }        

      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertBranch')
    })      
  }  

  errorMsg() {
    const modal = this.modalService.error({
      nzTitle: 'Wrong credentials'
    });

    setTimeout(() => modal.destroy(), 2000);
  }  
  
  addLocalStorage() {
    localStorage.setItem('dataSource', 'hello')
  }

  getLocalStorage() {
    console.log('getLocalStorage', localStorage.getItem('dataSource'))
  }

  clearLocalStorage() {
    localStorage.removeItem('dataSource')
  }
  
  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }   

}
