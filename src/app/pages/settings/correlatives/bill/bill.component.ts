import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {

   // current Correlative
  currentCorrelativeForm: FormGroup  
  currentCorrelativeVisible = false

  billform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.currentCorrelativeFormInit()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.billform = this.fb.group({
      branch: [null, [Validators.required]],
      current_correlative: [null, [Validators.required]]

    })      
  }

  saveBill() {
    for (const i in this.billform.controls) {
      this.billform.controls[i].markAsDirty()
      this.billform.controls[i].updateValueAndValidity()
    }
    if(this.billform.valid) {
      // console.log('this.billform.value', this.billform.value)
    }
  }

  clearFilter() {
    this.billform.reset()  
  }

    // current Correlative Form 
  currentCorrelativeFormInit() {
    this.currentCorrelativeForm = this.fb.group({
      current_correlative: [null, [Validators.required]]
    })      
  }  

  currentCorrelativeOpen() {
    this.currentCorrelativeVisible = true
  }

  currentCorrelativeClose() {
    this.currentCorrelativeFormInit()
    this.currentCorrelativeVisible = false
  }      

  currentCorrelativeCalculate() {    
    this.billform.patchValue({
      current_correlative: this.currentCorrelativeForm.value.current_correlative
    })    

    this.currentCorrelativeClose()
  }

}

