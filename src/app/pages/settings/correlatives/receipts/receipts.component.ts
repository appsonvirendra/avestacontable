import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-receipts',
  templateUrl: './receipts.component.html',
  styleUrls: ['./receipts.component.scss']
})
export class ReceiptsComponent implements OnInit {

   // current Correlative
  currentCorrelativeForm: FormGroup  
  currentCorrelativeVisible = false

  receiptsform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.currentCorrelativeFormInit()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.receiptsform = this.fb.group({
      branch: [null, [Validators.required]],
      current_correlative: [null, [Validators.required]]

    })      
  }

  saveBill() {
    for (const i in this.receiptsform.controls) {
      this.receiptsform.controls[i].markAsDirty()
      this.receiptsform.controls[i].updateValueAndValidity()
    }
    if(this.receiptsform.valid) {
      // console.log('this.receiptsform.value', this.receiptsform.value)
    }
  }

  clearFilter() {
    this.receiptsform.reset()  
  }

    // current Correlative Form 
  currentCorrelativeFormInit() {
    this.currentCorrelativeForm = this.fb.group({
      current_correlative: [null, [Validators.required]]
    })      
  }  

  currentCorrelativeOpen() {
    this.currentCorrelativeVisible = true
  }

  currentCorrelativeClose() {
    this.currentCorrelativeFormInit()
    this.currentCorrelativeVisible = false
  }      

  currentCorrelativeCalculate() {    
    this.receiptsform.patchValue({
      current_correlative: this.currentCorrelativeForm.value.current_correlative
    })    

    this.currentCorrelativeClose()
  }

}


