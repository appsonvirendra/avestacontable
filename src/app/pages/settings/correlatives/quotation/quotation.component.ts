import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {

   // current Correlative
  currentCorrelativeForm: FormGroup  
  currentCorrelativeVisible = false

  quotationform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.currentCorrelativeFormInit()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.quotationform = this.fb.group({
      branch: [null, [Validators.required]],
      current_correlative: [null, [Validators.required]]

    })      
  }

  saveBill() {
    for (const i in this.quotationform.controls) {
      this.quotationform.controls[i].markAsDirty()
      this.quotationform.controls[i].updateValueAndValidity()
    }
    if(this.quotationform.valid) {
      // console.log('this.quotationform.value', this.quotationform.value)
    }
  }

  clearFilter() {
    this.quotationform.reset()  
  }

    // current Correlative Form 
  currentCorrelativeFormInit() {
    this.currentCorrelativeForm = this.fb.group({
      current_correlative: [null, [Validators.required]]
    })      
  }  

  currentCorrelativeOpen() {
    this.currentCorrelativeVisible = true
  }

  currentCorrelativeClose() {
    this.currentCorrelativeFormInit()
    this.currentCorrelativeVisible = false
  }      

  currentCorrelativeCalculate() {    
    this.quotationform.patchValue({
      current_correlative: this.currentCorrelativeForm.value.current_correlative
    })    

    this.currentCorrelativeClose()
  }

}

