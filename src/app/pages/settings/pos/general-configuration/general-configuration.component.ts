import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-general-configuration',
  templateUrl: './general-configuration.component.html',
  styleUrls: ['./general-configuration.component.scss']
})
export class GeneralConfigurationComponent implements OnInit {

 generalconfigurationform: FormGroup
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  this.languageTranslate()
  this.generalconfigurationFormInit()
  }
   languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  } 
    generalconfigurationFormInit() {
    this.generalconfigurationform = this.fb.group({
      product_discription: [true],
      price_of_products: [true],
      enable: [true]
    })    
  }

  configurationSave() {
    for (const i in this.generalconfigurationform.controls) {
      this.generalconfigurationform.controls[i].markAsDirty()
      this.generalconfigurationform.controls[i].updateValueAndValidity()
    }
    if(this.generalconfigurationform.valid) {
      // console.log('this.generalconfigurationform.value', this.generalconfigurationform.value)
    }    
  }

}
