import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';

// basic load start
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
// basic load end

import { SettingsComponent } from './settings.component';
import { BranchListComponent } from './business/branch/branch-list/branch-list.component';
import { NewBranchComponent } from './business/branch/new-branch/new-branch.component';
import { InvoiceComponent } from './business/invoice/invoice.component';
import { WineriesComponent } from './inventory/wineries/wineries.component';
import { WineriestableComponent } from './inventory/wineries/wineriestable/wineriestable.component';
import { CategoriesComponent } from './inventory/categories/categories.component';
import { CreateCategoriesComponent } from './inventory/categories/create-categories/create-categories.component';
import { GeneralConfigurationComponent } from './pos/general-configuration/general-configuration.component';
import { AccountingComponent } from './business/accounting/accounting.component';
import { AccountingTab1Component } from './business/accounting/accounting-tab1/accounting-tab1.component';
import { AccountingTab2Component } from './business/accounting/accounting-tab2/accounting-tab2.component';
import { AccountingTab3Component } from './business/accounting/accounting-tab3/accounting-tab3.component';
import { TransactionsComponent } from './inventory/transactions/transactions.component';
import { CreateTransactionsComponent } from './inventory/transactions/create-transactions/create-transactions.component';
import { InventoriesComponent } from './inventory/inventories/inventories.component';
import { CustomersComponent } from './contacts/customers/customers.component';
import { CreateCustomersComponent } from './contacts/customers/create-customers/create-customers.component';
import { UsersComponent } from './contacts/users/users.component';
import { UsersFilterComponent } from './contacts/users/users-filter/users-filter.component';
import { UsersTableComponent } from './contacts/users/users-table/users-table.component';
import { CreateUsersComponent } from './contacts/users/create-users/create-users.component';
import { EditUsersComponent } from './contacts/users/edit-users/edit-users.component';
import { BillComponent } from './correlatives/bill/bill.component';
import { QuotationComponent } from './correlatives/quotation/quotation.component';
import { WorkOrderComponent } from './correlatives/work-order/work-order.component';
import { ReceiptsComponent } from './correlatives/receipts/receipts.component';
import { InvoiceUploadLogoComponent } from './business/invoice/invoice-upload-logo/invoice-upload-logo.component';
import { InvoiceUploadSealComponent } from './business/invoice/invoice-upload-seal/invoice-upload-seal.component';
import { DiscountsComponent } from './other-settings/discounts/discounts.component';
import { PermanentDiscountsComponent } from './other-settings/permanent-discounts/permanent-discounts.component';
import { ReasonComponent } from './inventory/reason/reason.component';
import { ChargeComponent } from './inventory/charge/charge.component';
import { CurrencyComponent } from './other-settings/currency/currency.component';



@NgModule({
  declarations: [SettingsComponent, BranchListComponent, NewBranchComponent, InvoiceComponent, WineriesComponent, WineriestableComponent, CategoriesComponent, CreateCategoriesComponent, GeneralConfigurationComponent, AccountingComponent, AccountingTab1Component, AccountingTab2Component, AccountingTab3Component, TransactionsComponent, CreateTransactionsComponent, InventoriesComponent, CustomersComponent, CreateCustomersComponent, UsersComponent, UsersFilterComponent, UsersTableComponent, CreateUsersComponent, EditUsersComponent, BillComponent, QuotationComponent, WorkOrderComponent, ReceiptsComponent, InvoiceUploadLogoComponent, InvoiceUploadSealComponent, DiscountsComponent, PermanentDiscountsComponent, ReasonComponent, ChargeComponent, CurrencyComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule    
  ]
})
export class SettingsModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
