import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { BranchListComponent } from './business/branch/branch-list/branch-list.component';
import { NewBranchComponent } from './business/branch/new-branch/new-branch.component';
import { InvoiceComponent } from './business/invoice/invoice.component';
import { WineriesComponent } from './inventory/wineries/wineries.component';
import { CategoriesComponent } from './inventory/categories/categories.component';
import { CreateCategoriesComponent } from './inventory/categories/create-categories/create-categories.component';
import { ReasonComponent } from './inventory/reason/reason.component';
import { ChargeComponent } from './inventory/charge/charge.component';
import { GeneralConfigurationComponent } from './pos/general-configuration/general-configuration.component';
import { AccountingComponent } from './business/accounting/accounting.component';
import { TransactionsComponent } from './inventory/transactions/transactions.component';
import { CreateTransactionsComponent } from './inventory/transactions/create-transactions/create-transactions.component';
import { InventoriesComponent } from './inventory/inventories/inventories.component';
import { CustomersComponent } from './contacts/customers/customers.component';
import { CreateCustomersComponent } from './contacts/customers/create-customers/create-customers.component';
import { UsersComponent } from './contacts/users/users.component';
import { CreateUsersComponent } from './contacts/users/create-users/create-users.component';
import { EditUsersComponent } from './contacts/users/edit-users/edit-users.component';
import { BillComponent } from './correlatives/bill/bill.component';
import { QuotationComponent } from './correlatives/quotation/quotation.component';
import { WorkOrderComponent } from './correlatives/work-order/work-order.component';
import { ReceiptsComponent } from './correlatives/receipts/receipts.component';
import { InvoiceUploadLogoComponent } from './business/invoice/invoice-upload-logo/invoice-upload-logo.component';
import { InvoiceUploadSealComponent } from './business/invoice/invoice-upload-seal/invoice-upload-seal.component';
import { DiscountsComponent } from './other-settings/discounts/discounts.component';
import { PermanentDiscountsComponent } from './other-settings/permanent-discounts/permanent-discounts.component';
import { CurrencyComponent } from './other-settings/currency/currency.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent    
  },
  {
    path: 'business/branch/list',
    component: BranchListComponent    
  },
  {
    path: 'business/branch/new',
    component: NewBranchComponent    
  },
  {
    path: 'business/invoice',
    component: InvoiceComponent    
  },
  {
    path: 'business/invoice/upload-logo',
    component: InvoiceUploadLogoComponent    
  },
  {
    path: 'business/invoice/upload-seal',
    component: InvoiceUploadSealComponent    
  },
  {
    path: 'business/accounting',
    component: AccountingComponent    
  },
  {
    path: 'inventory/inventories',
    component: InventoriesComponent   
  },
  {
    path: 'inventory/transactions',
    component: TransactionsComponent   
  },
  {
    path: 'inventory/transactions/create-transactions',
    component: CreateTransactionsComponent   
  },
  {
    path: 'inventory/wineries',
    component: WineriesComponent   
  },
  {
    path: 'inventory/categories',
    component: CategoriesComponent   
  },
  {
    path: 'inventory/categories/create-categories',
    component: CreateCategoriesComponent   
  },
  {
    path: 'inventory/reason',
    component: ReasonComponent   
  },
  {
    path: 'inventory/employee-position',
    component: ChargeComponent   
  },  
  {
    path: 'contacts/customers',
    component: CustomersComponent   
  },
  {
    path: 'contacts/customers/create-customers',
    component: CreateCustomersComponent   
  },
  {
    path: 'contacts/users',
    component: UsersComponent   
  },
  {
    path: 'contacts/users/create-users',
    component: CreateUsersComponent   
  },
  {
    path: 'contacts/users/edit',
    component: EditUsersComponent   
  },
  {
    path: 'correlatives/bill',
    component: BillComponent   
  },
  {
    path: 'correlatives/work-order',
    component: WorkOrderComponent   
  },
  {
    path: 'correlatives/receipts',
    component: ReceiptsComponent   
  },
  {
    path: 'correlatives/quotation',
    component: QuotationComponent   
  },
  {
    path: 'other-settings/discounts',
    component: DiscountsComponent   
  },
  {
    path: 'other-settings/permanent-discounts',
    component: PermanentDiscountsComponent   
  },
  {
    path: 'other-settings/currency',
    component: CurrencyComponent   
  },  
  {
    path: 'pos/general-configuration',
    component: GeneralConfigurationComponent   
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
