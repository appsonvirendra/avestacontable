import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PermanentDiscountsService } from 'src/app/pages/settings/other-settings/permanent-discounts/permanent-discounts.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-permanent-discounts',
  templateUrl: './permanent-discounts.component.html',
  styleUrls: ['./permanent-discounts.component.scss']
})
export class PermanentDiscountsComponent implements OnInit {

  // permanentdiscounts Form
  permanentDiscountsForm: FormGroup  

  // edit permanent Discounts Form
  editPermanentDiscountsForm: FormGroup  
  editPermanentDiscountsVisible = false

  deleteConfirmVisible = false

  permanentDiscountsDatas:any

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false
  
  deletePermanentDiscountsId: any     

  btnLoading2: boolean = false  
 
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _permanentDiscountsService: PermanentDiscountsService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService        
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.permanentDiscountsFormInit()

    this.populateGetPermanentDiscountsList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  populateGetPermanentDiscountsList() {
    this.loader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._permanentDiscountsService.getPermanentDiscountsList(params).subscribe((response) => {
      this.permanentDiscountsDatas = response

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getPermanentDiscountsList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  // permanentdiscounts Form
  permanentDiscountsFormInit() {
    this.permanentDiscountsForm = this.fb.group({ 
      discount: [null]
    })      
  } 

  insertPermanentDiscounts() {
    this.btnLoading2 = true
    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      discount: this.permanentDiscountsForm.value.discount
    }

    this._permanentDiscountsService.postPermanentDiscounts(params).subscribe((response) => {
      this.btnLoading2 = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get discounts list api
        this.populateGetPermanentDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.clearPermanentDiscounts()
    }, (error) => {
      this.btnLoading2 = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertPermanentDiscounts')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })       
  }

  clearPermanentDiscounts() {
    this.permanentDiscountsForm.reset()
  }

  // edit permanentDiscounts Form
  editPermanentDiscountsFormInit(data) {
    this.editPermanentDiscountsForm = this.fb.group({
      discount_id: [data.discount_id],
      discount: [data.discount]
    })      
  }  

  editPermanentDiscountsOpen(data) {
    this.editPermanentDiscountsVisible = true
    this.editPermanentDiscountsFormInit(data)
  }

  editPermanentDiscountsClose() {
    this.editPermanentDiscountsForm.reset()
    this.editPermanentDiscountsVisible = false
  }      

  updatePermanentDiscounts() {
    this.btnLoading = true

    let params = {
      discount: this.editPermanentDiscountsForm.value.discount
    }

    this._permanentDiscountsService.putPermanentDiscounts(this.companyBranchId, this.editPermanentDiscountsForm.value.discount_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get discounts list api
        this.populateGetPermanentDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.editPermanentDiscountsClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateDiscounts')
      this.editPermanentDiscountsClose()
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deletePermanentDiscountsId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }
  
  deletePermanentDiscounts() {         
    this.deleteBtnLoading = true

    this._permanentDiscountsService.deletePermanentDiscounts(this.deletePermanentDiscountsId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)
        
        // Populate get permanent discount list api
        this.populateGetPermanentDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deletePermanentDiscounts')
      this.deleteConfirmClose()
    })      
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  
  

}
