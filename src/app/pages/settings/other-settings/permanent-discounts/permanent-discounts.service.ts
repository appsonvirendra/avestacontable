import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PermanentDiscountsService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getPermanentDiscountsList(bodyparams) {
    return this.apiService.get(`/discounts?discount_type=2`, bodyparams)
      .pipe(map(data => data.data))
  } 
  
  postPermanentDiscounts(bodyparams) {
    return this.apiService.post(`/discount?page=2`, bodyparams)
      .pipe(map(data => data))
  }   

  putPermanentDiscounts(branch_id, discounts_id, bodyparams) {
    return this.apiService.put(`/discount/`+branch_id+`/`+discounts_id+`?discount_type=2`, bodyparams)
      .pipe(map(data => data))
  }  

  deletePermanentDiscounts(discount_id) {
    return this.apiService.delete(`/discount/`+discount_id+`?discount_type=2`)
      .pipe(map(data => data))
  }  

}
