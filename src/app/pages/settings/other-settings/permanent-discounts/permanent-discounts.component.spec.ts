import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermanentDiscountsComponent } from './permanent-discounts.component';

describe('PermanentDiscountsComponent', () => {
  let component: PermanentDiscountsComponent;
  let fixture: ComponentFixture<PermanentDiscountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermanentDiscountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentDiscountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
