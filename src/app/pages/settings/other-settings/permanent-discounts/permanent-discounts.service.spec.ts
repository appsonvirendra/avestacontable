import { TestBed } from '@angular/core/testing';

import { PermanentDiscountsService } from './permanent-discounts.service';

describe('PermanentDiscountsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermanentDiscountsService = TestBed.get(PermanentDiscountsService);
    expect(service).toBeTruthy();
  });
});
