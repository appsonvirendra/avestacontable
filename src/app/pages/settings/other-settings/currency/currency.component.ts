import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { CurrencyService } from 'src/app/pages/settings/other-settings/currency/currency.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {

  companyCurrencyDatas: any = []

  search_title = ''

  companyCurrencyDisplayDatas: any = []

  sortName: string | null = null
  sortValue: string | null = null  

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))  

  // Currency form
  currencyForm: FormGroup  
  currencyVisible = false  

  currency_id: any

  btnLoading: boolean = false

  deleteConfirmVisible = false
  deleteBtnLoading: boolean = false  

  allCurrencyDatas: any = []
  allCurrencyLoader: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _currencyService: CurrencyService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.populateGetCurrencyList()
    this.populateGetAllCurrencyList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  populateGetCurrencyList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._currencyService.getCompanyCurrencyList(params).subscribe((response) => {
      this.companyCurrencyDatas = response

      this.companyCurrencyDisplayDatas = [...this.companyCurrencyDatas]

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCompanyCurrencyList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  populateGetAllCurrencyList() {
    this.allCurrencyLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._currencyService.getAllCurrencyList(params).subscribe((response) => {
      this.allCurrencyDatas = response

      this.allCurrencyLoader = false      
    }, (error) => {
      this.allCurrencyLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getAllCurrencyList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  newCurrencyFormInit() {
    this.currencyForm = this.fb.group({
      currency_id: [null]
    })    
  }  

  currencyOpen() {
    this.currencyVisible = true
    this.newCurrencyFormInit()
  }

  currencyClose() {
    this.currencyVisible = false
    this.currency_id = null
  }  

  insertCurrency() {
    this.btnLoading = true
    let params = {
      company_id: this.companyData.id,
      currency_id: this.currencyForm.value.currency_id
    }

    this._currencyService.postCurrency(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate company currency list
        this.populateGetCurrencyList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.currencyClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCurrency')
    })
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()  
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
