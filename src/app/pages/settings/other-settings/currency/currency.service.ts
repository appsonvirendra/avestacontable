import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getCompanyCurrencyList(bodyparams) {
    return this.apiService.get(`/companyCurrencyList`, bodyparams)
      .pipe(map(data => data.data))
  }

  getAllCurrencyList(bodyparams) {
    return this.apiService.get(`/allCurrenciesList`, bodyparams)
      .pipe(map(data => data.data))
  }  

  postCurrency(bodyparams) {
    return this.apiService.post(`/companyCurrency`, bodyparams)
      .pipe(map(data => data))
  }  
  

}
