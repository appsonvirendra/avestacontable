import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DiscountsService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getDiscountsList(bodyparams) {
    return this.apiService.get(`/discounts?discount_type=1`, bodyparams)
      .pipe(map(data => data.data))
  } 
  
  postDiscounts(bodyparams) {
    return this.apiService.post(`/discount?page=1`, bodyparams)
      .pipe(map(data => data))
  }   

  putDiscounts(branch_id, discounts_id, bodyparams) {
    return this.apiService.put(`/discount/`+branch_id+`/`+discounts_id+`?discount_type=1`, bodyparams)
      .pipe(map(data => data))
  }  

  deleteDiscounts(discount_id) {
    return this.apiService.delete(`/discount/`+discount_id+`?discount_type=1`)
      .pipe(map(data => data))
  }  

}
