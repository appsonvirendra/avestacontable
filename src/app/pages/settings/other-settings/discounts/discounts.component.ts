import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { DiscountsService } from 'src/app/pages/settings/other-settings/discounts/discounts.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
   selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.scss']
})
export class DiscountsComponent implements OnInit {

  // discounts Form
  discountsForm: FormGroup  

  // edit dialog form
  editDiscountsForm: FormGroup  
  editDiscountsVisible = false

  deleteConfirmVisible = false

  discountsDatas:any

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false
  
  deleteDiscountsId: any     

  btnLoading2: boolean = false
 
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _discountsService: DiscountsService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService        
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.discountsFormInit()

    this.populateGetDiscountsList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  populateGetDiscountsList() {
    this.loader = true
    let params = {
      branch_id: this.companyBranchId
    }
    this._discountsService.getDiscountsList(params).subscribe((response) => {
      this.discountsDatas = response

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getDiscountsList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  // discounts Form 
  discountsFormInit() {
    this.discountsForm = this.fb.group({ 
      discount: [null]
    })      
  } 

  insertDiscounts() {
    this.btnLoading2 = true
    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,
      discount: this.discountsForm.value.discount
    }

    this._discountsService.postDiscounts(params).subscribe((response) => {
      this.btnLoading2 = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get discounts list api
        this.populateGetDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.clearDiscounts()
    }, (error) => {
      this.btnLoading2 = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertDiscounts')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })       
  }

  clearDiscounts() {
    this.discountsForm.reset()
  }

  // edit Discounts Form
  editDiscountsFormInit(data) {
    this.editDiscountsForm = this.fb.group({
      discount_id: [data.discount_id],
      discount: [data.discount]
    })      
  }  

  editDiscountsOpen(data) {
    this.editDiscountsVisible = true
    this.editDiscountsFormInit(data)
  }

  editDiscountsClose() {
    this.editDiscountsForm.reset()
    this.editDiscountsVisible = false
  }    
  
  updateDiscounts() {
    this.btnLoading = true

    let params = {
      discount: this.editDiscountsForm.value.discount
    }

    this._discountsService.putDiscounts(this.companyBranchId, this.editDiscountsForm.value.discount_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get discounts list api
        this.populateGetDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.editDiscountsClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateDiscounts')
      this.editDiscountsClose()
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteDiscountsId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteDiscounts() {         
    this.deleteBtnLoading = true

    this._discountsService.deleteDiscounts(this.deleteDiscountsId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)
        
        // Populate get discount list api
        this.populateGetDiscountsList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteDiscounts')
      this.deleteConfirmClose()
    })      
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    
  

}