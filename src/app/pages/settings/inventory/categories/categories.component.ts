import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup } from '@angular/forms';

import { CategoriesService } from 'src/app/pages/settings/inventory/categories/categories.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-categories',
   templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categoriesDatas: any

  search_title = ''

  categoriesDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  // edit categories account form
  editCategoriesForm: FormGroup  
  editCategoriesVisible = false  

  // sub categories account form
  subCategoriesForm: FormGroup  
  subCategoriesVisible = false  

  deleteConfirmVisible = false

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes')) 

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false
  
  deleteCategoriesId: any   

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _categoriesService: CategoriesService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
       
    // this.subCategoriesFormInit()

    this.populateGetCategoriesList()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  populateGetCategoriesList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._categoriesService.getCategoriesList(params).subscribe((response) => {
      this.categoriesDatas = response

      this.categoriesDisplayDatas = [...this.categoriesDatas]

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getCategoriesList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }   

  keyUpSearch() {
    let categoriesSearchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { category_code: string; category_name: string }) => {
      return (
       item.category_code.toLowerCase().indexOf(categoriesSearchLower) !== -1 || 
       item.category_name.toLowerCase().indexOf(categoriesSearchLower) !== -1
      )
    }

    const data = this.categoriesDatas.filter((item: { category_code: string; category_name: string }) => filterFunc(item))

    this.categoriesDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )   
  } 
  
  // edit categories form
  editCategoriesFormInit(data) {
    this.editCategoriesForm = this.fb.group({
      category_id: [data.category_id],
      category_code: [data.category_code],
      category_name: [data.category_name],
      category_description: [data.category_description]
    })      
  }  

  editCategoriesOpen(data) {
    this.editCategoriesVisible = true
    this.editCategoriesFormInit(data)
  }

  editCategoriesClose() {
    this.editCategoriesForm.reset()
    this.editCategoriesVisible = false
  }    
  
  updateCategories() {
    this.btnLoading = true

    let params = {
      category_code: this.editCategoriesForm.value.category_code, 
      category_name: this.editCategoriesForm.value.category_name, 
      category_description: this.editCategoriesForm.value.category_description
    }

    this._categoriesService.putCategories(this.companyData.id, this.editCategoriesForm.value.category_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get categories list api
        this.populateGetCategoriesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.editCategoriesClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateCategories')
      this.editCategoriesClose()
    })      
  }   

  // subCategoriesOpen() {
  //   this.subCategoriesVisible = true
  // }

  // subCategoriesClose() {
  //   this.subCategoriesForm.reset()
  //   this.subCategoriesVisible = false
  // }      

  // subCategoriesSave() {    
  //   for (const i in this.subCategoriesForm.controls) {
  //     this.subCategoriesForm.controls[i].markAsDirty()
  //     this.subCategoriesForm.controls[i].updateValueAndValidity()
  //   }          

  //   if(this.subCategoriesForm.valid) {      
  //     this.subCategoriesClose()
  //   }
  // }

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteCategoriesId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteCategories() {         
    this.deleteBtnLoading = true

    this._categoriesService.deleteCategories(this.deleteCategoriesId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get categories list api
        this.populateGetCategoriesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteCategories')
      this.deleteConfirmClose()
    })      
  }  

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
  
      localStorage.clear()
  
      this.authService.logout()
      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}

