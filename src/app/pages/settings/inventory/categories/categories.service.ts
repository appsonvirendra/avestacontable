import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getCategoriesList(bodyparams) {
    return this.apiService.get(`/categories`, bodyparams)
      .pipe(map(data => data.data))
  }  

  postCategories(bodyparams) {
    return this.apiService.post(`/category`, bodyparams)
      .pipe(map(data => data))
  } 
  
  getSingleCategories(bodyparams) {
    return this.apiService.get(`/category`, bodyparams)
      .pipe(map(data => data.data[0]))
  }
  
  putCategories(company_id, categories_id, bodyparams) {
    return this.apiService.put(`/category/`+company_id+`/`+categories_id, bodyparams)
      .pipe(map(data => data))
  }
  
  deleteCategories(categories_id) {
    return this.apiService.delete(`/category/`+categories_id)
      .pipe(map(data => data))
  }     

}
