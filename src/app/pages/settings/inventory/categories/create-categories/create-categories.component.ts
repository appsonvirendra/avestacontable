import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup } from '@angular/forms';

import { CategoriesService } from 'src/app/pages/settings/inventory/categories/categories.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-categories',
  templateUrl: './create-categories.component.html',
  styleUrls: ['./create-categories.component.scss']
})
export class CreateCategoriesComponent implements OnInit {

  categoriesAddform: FormGroup

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  btnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _categoriesService: CategoriesService,
    private message: NzMessageService,
    private _router: Router    
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.categoriesAddform = this.fb.group({
      category_code: [null],
      category_name: [null],
      category_description: [null]
    })      
  }

  insertCategories() {
    this.btnLoading = true
    let params = {
      company_id: this.companyData.id,
      category_code: this.categoriesAddform.value.category_code, 
      category_name: this.categoriesAddform.value.category_name, 
      category_description: this.categoriesAddform.value.category_description
    }

    this._categoriesService.postCategories(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to categories list page
        this.gotoCategoriesList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCategories')
    })       
  }  

  gotoCategoriesList() {
    this._router.navigate(['/user/settings/inventory/categories'], {})
  }  

  clearFilter() {
    this.categoriesAddform.reset()  
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}

