import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ChargeService } from 'src/app/pages/settings/inventory/charge/charge.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.scss']
})
export class ChargeComponent implements OnInit {

  chargeDatas: any

  search_title = ''

  chargeDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))  

  // Charge form
  chargeForm: FormGroup  
  chargeVisible = false  

  charge_id: any

  btnLoading: boolean = false

  deleteConfirmVisible = false
  deleteBtnLoading: boolean = false  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _chargeService: ChargeService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.populateGetChargeList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  populateGetChargeList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._chargeService.getChargeList(params).subscribe((response) => {
      this.chargeDatas = response

      this.chargeDisplayDatas = [...this.chargeDatas]

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getChargeList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { charge_name: string; charge_description: string }) => {
      return (
       item.charge_name.toLowerCase().indexOf(searchLower) !== -1 ||
       item.charge_description.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.chargeDatas.filter((item: { charge_name: string; charge_description: string }) => filterFunc(item))

    this.chargeDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )   
  }  

  // Charge form
  editChargeFormInit(data) {
    this.charge_id = data.charge_id
    this.chargeForm = this.fb.group({
      charge_name: [data.charge_name],
      charge_description: [data.charge_description]
    })      
  }
  
  newChargeFormInit() {
    this.chargeForm = this.fb.group({
      charge_name: [null],
      charge_description: [null]
    })    
  }

  chargeOpen(data) {
    this.chargeVisible = true
    if(data == '1') {
      this.newChargeFormInit()
    } else {
      this.editChargeFormInit(data)
    }
  }  

  chargeClose() {
    this.chargeForm.reset()
    this.chargeVisible = false
    this.charge_id = null
  }   

  updateCharge() {
    this.btnLoading = true

    let params = {
      charge_name: this.chargeForm.value.charge_name, 
      charge_description: this.chargeForm.value.charge_description 
    }

    this._chargeService.putCharge(this.charge_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get charge list api
        this.populateGetChargeList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.chargeClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateCharge')
      this.chargeClose()
    })
  }

  insertCharge() {
    this.btnLoading = true
    let params = {
      company_id: this.companyData.id,
      charge_name: this.chargeForm.value.charge_name,
      charge_description: this.chargeForm.value.charge_description
    }

    this._chargeService.postCharge(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate charge list page
        this.populateGetChargeList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.chargeClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertCharge')
    })
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.charge_id = elementId
  }

  deleteConfirmClose() {
    this.charge_id = null
    this.deleteConfirmVisible = false
  }  

  deleteCharge() {    
    this.deleteBtnLoading = true

    let params = {
      charge_id: this.charge_id
    }

    this._chargeService.deleteCharge(params).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get charge list api
        this.populateGetChargeList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteCharge')
      this.deleteConfirmClose()
    })      
  }  
  
  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()  
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
