import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChargeService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getChargeList(bodyparams) {
    return this.apiService.post(`/chargeList`, bodyparams)
      .pipe(map(data => data.data))
  }
  
  postCharge(bodyparams) {
    return this.apiService.post(`/chargeNew`, bodyparams)
      .pipe(map(data => data))
  }

  putCharge(charge_id, bodyparams) {
    return this.apiService.post(`/chargeUpdate/`+charge_id, bodyparams)
      .pipe(map(data => data))
  }
  
  deleteCharge(bodyparams) {
    return this.apiService.post(`/chargeDelete`, bodyparams)
      .pipe(map(data => data))
  }  

}
