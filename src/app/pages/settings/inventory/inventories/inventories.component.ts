import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.scss']
})
export class InventoriesComponent implements OnInit {

  inputDisabled = false

  // inventories Form
  inventoriesForm: FormGroup   

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.InventoriesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // Inventories form
  InventoriesInit() {
    this.inventoriesForm = this.fb.group({ 
      checkbox_1: [null],
      checkbox_2: [null],
      checkbox_3: [true],
      item_latest: ['1']
    })      
  }

  saveinventories() {
    for (const i in this.inventoriesForm.controls) {
      this.inventoriesForm.controls[i].markAsDirty()
      this.inventoriesForm.controls[i].updateValueAndValidity()
    }          

    if(this.inventoriesForm.valid) {      
      // console.log('this.inventoriesForm.value', this.inventoriesForm.value)
    }
  }

  clearinventories() {
    this.inventoriesForm = this.fb.group({
      checkbox_1: [null], 
      checkbox_2: [null],
      checkbox_3: [null],
      item_latest: ['0']
    })
  }

  onChangeCheck(value, position) {
    if(value == true && position == 1) {
      this.inventoriesForm.patchValue({
        checkbox_2: [false],
        checkbox_3: [false]
      })
      this.inputDisabled = true
    }

    if(value == true && position == 2) {
      this.inventoriesForm.patchValue({
        checkbox_1: [false],
        checkbox_3: [false]
      })
      this.inputDisabled = true
    }
    
    if(value == true && position == 3) {
      this.inventoriesForm.patchValue({
        checkbox_1: [false],
        checkbox_2: [false]
      })
      this.inputDisabled = false
    }    
  } 


}