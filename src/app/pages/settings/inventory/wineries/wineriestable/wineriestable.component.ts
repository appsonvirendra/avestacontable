import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-wineriestable',
  templateUrl: './wineriestable.component.html',
 styleUrls: ['./wineriestable.component.scss']
})
export class WineriestableComponent implements OnInit {

  // default wineries form
  defaultWineriesForm: FormGroup  
  defaultWineriesVisible = false  

  @Input() wineryDatas: any

  // edit wineries form
  editWineriesForm: FormGroup  
  editWineriesVisible = false

  // search
  search_title = ''

  wineryDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  btnLoading: boolean = false
  deleteBtnLoading: boolean = false

  deleteWineryId: any

  @Output("populateGetWineryList") populateGetWineryList: EventEmitter<any> = new EventEmitter();  

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  btnLoadingDefaultWinery: boolean = false

  defaultWinerySelected: any

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private _wineriesService: WineriesService,
    private message: NzMessageService    
  ) { 

  }

  ngOnInit() {
    this.wineriesData()
  }

  // default wineries form
  defaultWineriesFormInit() {
    this.defaultWineriesForm = this.fb.group({
      wineries_id: [this.defaultWinerySelected]
    })      
  }    

  defaultWineriesOpen() {
    this.defaultWineriesVisible = true
    this.defaultWineriesFormInit()
  }

  defaultWineriesClose() {
    this.defaultWineriesForm.reset()
    this.defaultWineriesVisible = false
  } 
  
  updateDefaultWinery() {
    this.btnLoadingDefaultWinery = true

    this._wineriesService.putDefaultWinery(this.companyData.id, this.defaultWineriesForm.value.wineries_id).subscribe((response) => {
      this.btnLoadingDefaultWinery = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get winery list api
        this.populateGetWineryList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.defaultWineriesClose()
    }, (error) => {
      this.btnLoadingDefaultWinery = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateDefaultWinery')
      this.defaultWineriesClose()
    })      
  }

  // edit wineries form
  editWineriesFormInit(data) {
    this.editWineriesForm = this.fb.group({
      wineries_id: [data.wineries_id, [Validators.required]],
      name: [data.name, [Validators.required]],
      city: [data.city, [Validators.required]],
      address: [data.address, [Validators.required]],
      description: [data.description, [Validators.required]]
    })      
  }  

  editWineriesOpen(data) {
    this.editWineriesVisible = true
    this.editWineriesFormInit(data)
  }

  editWineriesClose() {
    this.editWineriesForm.reset()
    this.editWineriesVisible = false
  }

  wineriesData() {
    // Set default winery value
    this.wineryDatas.forEach(element => {
      if(element.default_winery) {
        this.defaultWinerySelected = element.wineries_id.toString()
      }
    })    
    this.wineryDisplayData = [...this.wineryDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { name: string; description: string; city: string }) => {
      return (
        item.name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.description.toLowerCase().indexOf(searchLower) !== -1 ||
        item.city.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.wineryDisplayData.filter((item: {name: string; description: string; city: string }) => filterFunc(item))

    this.wineryDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.wineryDisplayData = [...this.wineryDatas]
    }    
  }  

  updateWinery() {
    for (const i in this.editWineriesForm.controls) {
      this.editWineriesForm.controls[i].markAsDirty()
      this.editWineriesForm.controls[i].updateValueAndValidity()
    }          

    if(this.editWineriesForm.valid) {
      this.btnLoading = true

      let params = {
        name: this.editWineriesForm.value.name,
        address: this.editWineriesForm.value.address, 
        city: this.editWineriesForm.value.city, 
        description: this.editWineriesForm.value.description
      }

      this._wineriesService.putWinery(this.editWineriesForm.value.wineries_id, params).subscribe((response) => {
        this.btnLoading = false
        if(response.success) {
          this.openMessageBar(this.translate.instant('SUCCESSFUL'))
          this.openMessageBar(response.message)

          // Populate get winery list api
          this.populateGetWineryList.emit()
        } else {          
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
        this.editWineriesClose()
      }, (error) => {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateWinery')
        this.editWineriesClose()
      })      
    }
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteWineryId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

  deleteWinery() {         
    this.deleteBtnLoading = true

    let params = {
      company_id: this.companyData.id,
      wineries_id: this.deleteWineryId
    }

    this._wineriesService.deleteWinery(params).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get winery list api
        this.populateGetWineryList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteWinery')
      this.deleteConfirmClose()
    })      
  }
    

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }    

}

