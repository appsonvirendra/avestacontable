import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WineriestableComponent } from './wineriestable.component';

describe('WineriestableComponent', () => {
  let component: WineriestableComponent;
  let fixture: ComponentFixture<WineriestableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WineriestableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WineriestableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
