import { TestBed } from '@angular/core/testing';

import { WineriesService } from './wineries.service';

describe('WineriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WineriesService = TestBed.get(WineriesService);
    expect(service).toBeTruthy();
  });
});
