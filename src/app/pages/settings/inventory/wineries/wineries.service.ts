import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WineriesService {

  constructor(
    private apiService: ApiService
  ) {     
  } 
  
  getWineryList(bodyparams) {
    return this.apiService.post(`/wineryList`, bodyparams)
      .pipe(map(data => data.data))
  }

  postWinery(bodyparams) {
    return this.apiService.post(`/wineryNew`, bodyparams)
      .pipe(map(data => data))
  }
  
  putWinery(wineries_id, bodyparams) {
    return this.apiService.post(`/wineryUpdate/`+wineries_id, bodyparams)
      .pipe(map(data => data))
  }
  
  deleteWinery(bodyparams) {
    return this.apiService.post(`/wineryDelete`, bodyparams)
      .pipe(map(data => data))
  }  

  putDefaultWinery(company_id, wineries_id) {
    return this.apiService.put(`/changeDefaultWinery/`+company_id+`/`+wineries_id)
      .pipe(map(data => data))
  }  

}
