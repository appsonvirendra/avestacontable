import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-wineries',
  templateUrl: './wineries.component.html',
  styleUrls: ['./wineries.component.scss']
})
export class WineriesComponent implements OnInit {

  // add wineries form
  addWineriesForm: FormGroup  
  addWineriesVisible = false
  activeCollapse = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  loader: boolean = false
  wineryDatas: any

  btnLoading: boolean = false

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private _wineriesService: WineriesService,
    private message: NzMessageService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.populateGetWineryList()

    this.addWineriesFormInit()
  }

  populateGetWineryList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      this.wineryDatas = response
      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }   

  // add wineries form
  addWineriesFormInit() {
    this.addWineriesForm = this.fb.group({
      name: [null, [Validators.required]],
      description: [null, [Validators.required]],
      city: [null, [Validators.required]],
      address: [null, [Validators.required]]
    })      
  }  

  addWineriesOpen() {
    this.addWineriesVisible = true
  }

  addWineriesClose() {
    this.addWineriesForm.reset()
    this.addWineriesVisible = false
  }   
  
  insertWinery() {
    for (const i in this.addWineriesForm.controls) {
      this.addWineriesForm.controls[i].markAsDirty()
      this.addWineriesForm.controls[i].updateValueAndValidity()
    }          

    if(this.addWineriesForm.valid) {
      this.btnLoading = true

      let params = {
        company_id: this.companyData.id,
        name: this.addWineriesForm.value.name,
        address: this.addWineriesForm.value.address, 
        city: this.addWineriesForm.value.city, 
        description: this.addWineriesForm.value.description,
        default_winery: 0
      }      

      this._wineriesService.postWinery(params).subscribe((response) => {
        this.btnLoading = false
        if(response.success) {
          this.openMessageBar(this.translate.instant('SUCCESSFUL'))
          this.openMessageBar(response.message)

          this.populateGetWineryList()
        } else {          
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
        this.addWineriesClose()
      }, (error) => {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertWinery')
        this.addWineriesClose()
      })      
    }
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  
  
}
