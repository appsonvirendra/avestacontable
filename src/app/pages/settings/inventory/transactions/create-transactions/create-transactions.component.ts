import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-transactions',
  templateUrl: './create-transactions.component.html',
  styleUrls: ['./create-transactions.component.scss']
})
export class CreateTransactionsComponent implements OnInit {

  createtransactionsAddform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.createtransactionsAddform = this.fb.group({
      transactions_type: [null, [Validators.required]],
      transactions_name: [null, [Validators.required]],
      transactions_description: [null]
    })      
  }

  savecreatetransactions() {
    for (const i in this.createtransactionsAddform.controls) {
      this.createtransactionsAddform.controls[i].markAsDirty()
      this.createtransactionsAddform.controls[i].updateValueAndValidity()
    }
    if(this.createtransactionsAddform.valid) {
      // console.log('this.createtransactionsAddform.value', this.createtransactionsAddform.value)
    }
  }

  clearFilter() {
    this.createtransactionsAddform.reset()  
  }

}
