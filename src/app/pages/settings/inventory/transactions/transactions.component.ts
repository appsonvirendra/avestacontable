import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  transationsDatas: any

  search_title = ''

  transationsDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  // edit Transations Form
  editTransationsForm: FormGroup  
  editTransationsVisible = false  


  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.categoriesData()
    this.editTransationsFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.transationsDatas = []
  }
  
  categoriesData() {
    this.transationsDatas = [
      {
        transaction_name: 'DEPARTURE',
        transaction_type: 'Departures'
      },
      {
        transaction_name: 'ENTRY',
        transaction_type: 'Tickets'
      }                      
    ]

    this.transationsDisplayDatas = [...this.transationsDatas]
  }

  keyUpSearch() {
    let categoriesSearchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { transaction_type: string}) => {
      return (
       item.transaction_type.toLowerCase().indexOf(categoriesSearchLower) !== -1
      )
    }

    const data = this.transationsDatas.filter((item: { transaction_type: string}) => filterFunc(item))

    this.transationsDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )   
  } 
  
  // edit transations form
  editTransationsFormInit() {
    this.editTransationsForm = this.fb.group({
      category_name: [null, [Validators.required]],
      category_description: [null],
      category_type: ['1']
    })      
  }  

  editTransationsOpen() {
    this.editTransationsVisible = true
  }

  editTransationsClose() {
    this.editTransationsForm.reset()
    this.editTransationsVisible = false
  }      

  editTransationsSave() {    
    for (const i in this.editTransationsForm.controls) {
      this.editTransationsForm.controls[i].markAsDirty()
      this.editTransationsForm.controls[i].updateValueAndValidity()
    }          

    if(this.editTransationsForm.valid) {      
      this.editTransationsClose()
    }
  }

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}