import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ReasonService } from 'src/app/pages/settings/inventory/reason/reason.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-reason',
  templateUrl: './reason.component.html',
  styleUrls: ['./reason.component.scss']
})
export class ReasonComponent implements OnInit {

  reasonDatas: any

  search_title = ''

  reasonDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  loader: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))  

  // Reason form
  reasonForm: FormGroup  
  reasonVisible = false  

  reason_id: any

  btnLoading: boolean = false

  deleteConfirmVisible = false
  deleteBtnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _reasonService: ReasonService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.populateGetReasonList()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  populateGetReasonList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._reasonService.getReasonList(params).subscribe((response) => {
      this.reasonDatas = response

      this.reasonDisplayDatas = [...this.reasonDatas]

      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getReasonList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { reason: string }) => {
      return (
       item.reason.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.reasonDatas.filter((item: { reason: string }) => filterFunc(item))

    this.reasonDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )   
  }  

  // Reason form
  editReasonFormInit(data) {
    this.reason_id = data.reason_id
    this.reasonForm = this.fb.group({
      reason: [data.reason]
    })      
  }
  
  newReasonFormInit() {
    this.reasonForm = this.fb.group({
      reason: [null]
    })    
  }

  reasonOpen(data) {
    this.reasonVisible = true
    if(data == '1') {
      this.newReasonFormInit()
    } else {
      this.editReasonFormInit(data)
    }
  }

  reasonClose() {
    this.reasonForm.reset()
    this.reasonVisible = false
    this.reason_id = null
  }   

  updateReason() {
    this.btnLoading = true

    let params = {
      reason: this.reasonForm.value.reason 
    }

    this._reasonService.putReason(this.reason_id, params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get reason list api
        this.populateGetReasonList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.reasonClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateReason')
      this.reasonClose()
    })
  }

  insertReason() {
    this.btnLoading = true
    let params = {
      company_id: this.companyData.id,
      reason: this.reasonForm.value.reason
    }

    this._reasonService.postReason(params).subscribe((response) => {
      this.btnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate reason list page
        this.populateGetReasonList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }

      this.reasonClose()
    }, (error) => {
      this.btnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertReason')
    })
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.reason_id = elementId
  }

  deleteConfirmClose() {
    this.reason_id = null
    this.deleteConfirmVisible = false
  }  

  deleteReason() {    
    this.deleteBtnLoading = true

    this._reasonService.deleteReason(this.reason_id).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get reason list api
        this.populateGetReasonList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteReason')
      this.deleteConfirmClose()
    })      
  }  
  
  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()  
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
