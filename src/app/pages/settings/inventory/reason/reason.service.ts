import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReasonService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getReasonList(bodyparams) {
    return this.apiService.get(`/reasonList`, bodyparams)
      .pipe(map(data => data.data))
  }
  
  postReason(bodyparams) {
    return this.apiService.post(`/reason`, bodyparams)
      .pipe(map(data => data))
  }

  putReason(reason_id, bodyparams) {
    return this.apiService.put(`/reason/`+reason_id, bodyparams)
      .pipe(map(data => data))
  }
  
  deleteReason(reason_id) {
    return this.apiService.delete(`/reason/`+reason_id)
      .pipe(map(data => data))
  }  

}
