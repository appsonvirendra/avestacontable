import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.scss']
})
export class EditUsersComponent implements OnInit {


  // edit user Form
  edituserForm: FormGroup 

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
   
    this.edituserFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // edit user Form
  edituserFormInit() {
    this.edituserForm = this.fb.group({
      purchase_id: '1',
      seller_id: [null, [Validators.required]],
      seller_name: [null, [Validators.required]],
      role: [null, [Validators.required]],
      branch: [null, [Validators.required]],
      item_wineries: [null],
      user: [null, [Validators.required]],
      password: [null, [Validators.required]],
      repeat_password: [null, [Validators.required]]
    })      
  }
  
  saveedituser() {
    for (const i in this.edituserForm.controls) {
      this.edituserForm.controls[i].markAsDirty()
      this.edituserForm.controls[i].updateValueAndValidity()
    }          

    if(this.edituserForm.valid) {      
      // console.log('this.edituserForm.value', this.edituserForm.value)
    }
  }

  clearFilter() {
    this.edituserForm.reset()
  }
}


