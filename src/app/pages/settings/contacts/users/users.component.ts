import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  usersDatas: any

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.usersData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
    // this.usersData()
  }
  
  submitForm() {
    this.usersDatas = []
  }
  
  usersData() {
    this.usersDatas = [
      {
        user_id: '1',
        users_user: 'ventas@inversioneslogisticas.com',
        users_name: 'Manuel Santos',
        users_role: 'Administrator',
        users_branch: 'Branch 1',
      }
    ]
  }

}
