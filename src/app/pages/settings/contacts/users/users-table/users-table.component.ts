import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  @Input() usersData: any

  search_title = ''

  usersDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null  


  currentDate:any = new Date().getMonth().toString() + 1

  constructor() { }

  ngOnInit() {
    this.UsersData()
  }

  UsersData() {
    this.usersDisplayData = [...this.usersData]
  }

  searchUsersbyuser() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { users_user: string; users_name: string}) => {
      return (
        item.users_user.toLowerCase().indexOf(searchLower) !== -1 || 
        item.users_name.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.usersData.filter((item: { users_user: string; users_name: string }) => filterFunc(item))

    this.usersDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )  
    
    if(searchLower.length == 0) {
      this.usersDisplayData = [...this.usersData]
    }
  }

}
