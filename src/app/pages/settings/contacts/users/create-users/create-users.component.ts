import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.scss']
})
export class CreateUsersComponent implements OnInit {


  // create users Form
  createusersForm: FormGroup 

    // search seller dialog
  searchSellerVisible = false
  sellerDatas = [] 

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.sellerData()
    this.createusersFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // create users Form
  createusersFormInit() {
    this.createusersForm = this.fb.group({
      seller_id: [null, [Validators.required]],
      seller_name: [null, [Validators.required]],
      role: [null, [Validators.required]],
      branch: [null, [Validators.required]],
      item_wineries: [null],
      user: [null, [Validators.required]],
      password: [null, [Validators.required]],
      repeat_password: [null, [Validators.required]]
    })      
  }
  
  saveCreateuser() {
    for (const i in this.createusersForm.controls) {
      this.createusersForm.controls[i].markAsDirty()
      this.createusersForm.controls[i].updateValueAndValidity()
    }          

    if(this.createusersForm.valid) {      
      // console.log('this.createusersForm.value', this.createusersForm.value)
    }
  }

  clearFilter() {
    this.createusersForm.reset()
  }
    // search seller dialog
  searchSellerOpen() {
    this.searchSellerVisible = true
  }

  searchSellerClose() {
    this.searchSellerVisible = false
  }   

  sellerData() {
    this.sellerDatas = [
      {
        seller_id: '1',
        seller_name: 'John Brown 1',
        seller_phone: "",
        seller_mobile: "",
        seller_email: "",
        seller_type: "1"
      },
      {
        seller_id: '2',
        seller_name: 'John Brown 2',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '3',
        seller_name: 'John Brown 3',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      },
      {
        seller_id: '4',
        seller_name: 'John Brown 4',
        seller_phone: "0731525374",
        seller_mobile: "1213141516",
        seller_email: "john@gmail.com",
        seller_type: "1"
      }               
    ]
  }

  sellerAddInput($event) {
    this.createusersForm.patchValue({
      seller_id: $event.seller_id,
      seller_name: $event.seller_name
    })

    this.searchSellerClose()    
  }
}

