import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-customers',
  templateUrl: './create-customers.component.html',
  styleUrls: ['./create-customers.component.scss']
})
export class CreateCustomersComponent implements OnInit {

  createcustomersAddform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.createcustomersAddform = this.fb.group({
      column_name: [null, [Validators.required]],
      column_impression: [null, [Validators.required]],
      field_type: [null, [Validators.required]],
      client_type: ['0']
    })      
  }

  savecreatecustomers() {
    for (const i in this.createcustomersAddform.controls) {
      this.createcustomersAddform.controls[i].markAsDirty()
      this.createcustomersAddform.controls[i].updateValueAndValidity()
    }
    if(this.createcustomersAddform.valid) {
      // console.log('this.createcustomersAddform.value', this.createcustomersAddform.value)
    }
  }

  clearFilter() {
    this.createcustomersAddform.reset()  
  }

}
