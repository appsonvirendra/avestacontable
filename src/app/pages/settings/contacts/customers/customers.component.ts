import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  customersDatas: any

  customer_field = true

  search_title = ''

  customersDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  // edit Customers Form
  editCustomersForm: FormGroup  
  editCustomersVisible = false  


  deleteConfirmVisible = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.categoriesData()
    this.editCustomersFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.customersDatas = []
  }
  
  categoriesData() {
    this.customersDatas = [
      {
        customer_column_name: 'Business',
        customer_printing_column: 'Business'
      },
      {
        customer_column_name: 'Business',
        customer_printing_column: 'Business'
      }                      
    ]

    this.customersDisplayDatas = [...this.customersDatas]
  }

  keyUpSearch() {
    let categoriesSearchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { customer_printing_column: string}) => {
      return (
       item.customer_printing_column.toLowerCase().indexOf(categoriesSearchLower) !== -1
      )
    }

    const data = this.customersDatas.filter((item: { customer_printing_column: string}) => filterFunc(item))

    this.customersDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )   
  } 
  
  // edit Customers Form
  editCustomersFormInit() {
    this.editCustomersForm = this.fb.group({
      category_column_name: [null],
      category_print_column: [null, [Validators.required]],
      category_field_type: ['0'],
      category_client_type: ['1', [Validators.required]],
    })      
  }  

  editCustomersOpen() {
    this.editCustomersVisible = true
  }

  editCustomersClose() {
    this.editCustomersForm.reset()
    this.editCustomersVisible = false
  }      

  editCustomersSave() {    
    for (const i in this.editCustomersForm.controls) {
      this.editCustomersForm.controls[i].markAsDirty()
      this.editCustomersForm.controls[i].updateValueAndValidity()
    }          

    if(this.editCustomersForm.valid) {      
      this.editCustomersClose()
    }
  }

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}