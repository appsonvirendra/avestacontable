import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceUploadSealComponent } from './invoice-upload-seal.component';

describe('InvoiceUploadSealComponent', () => {
  let component: InvoiceUploadSealComponent;
  let fixture: ComponentFixture<InvoiceUploadSealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceUploadSealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceUploadSealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
