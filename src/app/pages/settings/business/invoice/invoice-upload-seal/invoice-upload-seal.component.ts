import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invoice-upload-seal',
  templateUrl: './invoice-upload-seal.component.html',
  styleUrls: ['./invoice-upload-seal.component.scss']
})
export class InvoiceUploadSealComponent implements OnInit {

  invoiceuploadsealform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.invoiceuploadsealform = this.fb.group({
      image: [null]
    })      
  }

  saveinvoiceuploadseal() {
    for (const i in this.invoiceuploadsealform.controls) {
      this.invoiceuploadsealform.controls[i].markAsDirty()
      this.invoiceuploadsealform.controls[i].updateValueAndValidity()
    }
    if(this.invoiceuploadsealform.valid) {
      // console.log('this.invoiceuploadsealform.value', this.invoiceuploadsealform.value)
    }
  }

  clearFilter() {
    this.invoiceuploadsealform.reset()  
  }

}


