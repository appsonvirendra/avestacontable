import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceUploadLogoComponent } from './invoice-upload-logo.component';

describe('InvoiceUploadLogoComponent', () => {
  let component: InvoiceUploadLogoComponent;
  let fixture: ComponentFixture<InvoiceUploadLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceUploadLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceUploadLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
