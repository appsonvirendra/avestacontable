import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invoice-upload-logo',
  templateUrl: './invoice-upload-logo.component.html',
  styleUrls: ['./invoice-upload-logo.component.scss']
})
export class InvoiceUploadLogoComponent implements OnInit {

  invoiceuploadlogoform: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.invoiceuploadlogoform = this.fb.group({
      image: [null]
    })      
  }

  saveinvoiceuploadlogo() {
    for (const i in this.invoiceuploadlogoform.controls) {
      this.invoiceuploadlogoform.controls[i].markAsDirty()
      this.invoiceuploadlogoform.controls[i].updateValueAndValidity()
    }
    if(this.invoiceuploadlogoform.valid) {
      // console.log('this.invoiceuploadlogoform.value', this.invoiceuploadlogoform.value)
    }
  }

  clearFilter() {
    this.invoiceuploadlogoform.reset()  
  }

}

