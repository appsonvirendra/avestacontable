import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  // invoice info form
  invoiceInfoForm: FormGroup  

  invoice_e_b_button = true
  invoice_branch_button = true

  // customer search dialog
  searchCustomerVisible = false
  customerDatas = []
  
  // search tax dialog
  searchTaxVisible = false
  taxDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.invoiceInfoFormInit()

    this.customerData()

    this.taxData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // add branch form
  invoiceInfoFormInit() {
    this.invoiceInfoForm = this.fb.group({
      invoice_company_name: [null, [Validators.required]],
      invoice_rtn: [null, [Validators.required]],
      invoice_sales_document: ['1'],
      invoice_phone: [null, [Validators.required]],
      invoice_address: [null, [Validators.required]],
      invoice_email: [null],
      invoice_legend: [null],
      invoice_web_page: [null],
      invoice_exchange_rate: [null, [Validators.required]],
      invoice_express_billing: ['0'],
      customer_id: [null],
      customer_name: [null],      
      invoice_branch_selection: ['1'],
      invoice_branch: [null]
    })      
  }
  
  saveInvoice() {
    for (const i in this.invoiceInfoForm.controls) {
      this.invoiceInfoForm.controls[i].markAsDirty()
      this.invoiceInfoForm.controls[i].updateValueAndValidity()
    }          

    if(this.invoiceInfoForm.valid) {      
      // console.log('this.invoiceInfoForm.value', this.invoiceInfoForm.value)
    }
  }

  clearFilter() {
    this.invoiceInfoForm = this.fb.group({
      invoice_company_name: [null, [Validators.required]],
      invoice_rtn: [null, [Validators.required]],
      invoice_sales_document: ['1'],
      invoice_phone: [null, [Validators.required]],
      invoice_address: [null, [Validators.required]],
      invoice_email: [null],
      invoice_legend: [null],
      invoice_web_page: [null],
      invoice_exchange_rate: [null, [Validators.required]],
      invoice_express_billing: ['0'],
      customer_id: [null],
      customer_name: [null],      
      invoice_branch_selection: ['0'],
      invoice_branch: [null]
    })
  }

  // search customer dialog
  searchCustomerOpen() {
    this.searchCustomerVisible = true
  }

  searchCustomerClose() {
    this.searchCustomerVisible = false
  }   

  customerData() {
    this.customerDatas = [
      {
        customer_id: '1',
        customer_name: 'John Brown 1',
        customer_phone: "",
        customer_mobile: "",
        customer_email: "",
        customer_type: "1"
      },
      {
        customer_id: '2',
        customer_name: 'John Brown 2',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '3',
        customer_name: 'John Brown 3',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      },
      {
        customer_id: '4',
        customer_name: 'John Brown 4',
        customer_phone: "0731525374",
        customer_mobile: "1213141516",
        customer_email: "john@gmail.com",
        customer_type: "1"
      }               
    ]
  }

  customerAddInput($event) {
    this.invoiceInfoForm.patchValue({
      customer_id: $event.customer_id,
      customer_name: $event.customer_name
    })

    this.searchCustomerClose()    
  }  

  expressBilling() {
    if(this.invoiceInfoForm.value.invoice_express_billing == '1') {
      this.invoice_e_b_button = false
    } else {
      this.invoice_e_b_button = true
    }    
  }

  branchSelection() {
    if(this.invoiceInfoForm.value.invoice_branch_selection == '1') {
      this.invoice_branch_button = true
    } else {
      this.invoice_branch_button = false
    }    
  }
  
  // search tax dialog
  searchTaxOpen() {
    this.searchTaxVisible = true
  }

  searchTaxClose() {
    this.searchTaxVisible = false
  }   

  taxData() {
    this.taxDatas = [
      {
        tax_id: '1',
        tax_name: 'ISV',
        tax_description: 'ISV 15%',
        tax_value: '15'
      }              
    ]
  }


}
