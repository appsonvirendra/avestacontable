import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(
    private apiService: ApiService
  ) {     
  } 
  
  getBranchList(bodyparams) {
    return this.apiService.post(`/branchList`, bodyparams)
      .pipe(map(data => data.data))
  }
  
  postBranch(bodyparams) {
    return this.apiService.post(`/branchNew`, bodyparams)
      .pipe(map(data => data))
  }

  putBranch(branch_id, bodyparams) {
    return this.apiService.post(`/branchUpdate/`+branch_id, bodyparams)
      .pipe(map(data => data))
  }  

  deleteBranch(bodyparams) {
    return this.apiService.post(`/branchDelete`, bodyparams)
      .pipe(map(data => data))
  }  

}
