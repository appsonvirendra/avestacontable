import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.scss']
})
export class BranchListComponent implements OnInit {

  search_title = ''

  branchDisplayDatas: any

  sortName: string | null = null
  sortValue: string | null = null  

  // edit bank account form
  editBranchForm: FormGroup  
  editBranchVisible = false  

  deleteConfirmVisible = false

  branchDatas: any
  loader: boolean = false
  
  wineryDatas: any
  wineryLoader: boolean = false
  companyData = JSON.parse(localStorage.getItem('companyRes'))  
  branchUpdateLoader: boolean = false

  deleteBranchId: any

  deleteBtnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _branchService: BranchService,
    private message: NzMessageService,
    private _wineriesService: WineriesService    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.populateGetBranchList()    

    this.populateGetWineryList()
  }

  populateGetWineryList() {
    this.wineryLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      this.wineryDatas = response
      this.wineryLoader = false      
    }, (error) => {
      this.wineryLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }   

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response
      this.branchDisplayDatas = [...this.branchDatas]
      this.loader = false
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('FAILED')+' getBranchList')
    })     
  }   
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }

  keyUpSearch() {
    let branchSearchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { branch_name: string; email: string}) => {
      return (
        item.branch_name.toLowerCase().indexOf(branchSearchLower) !== -1 || 
        item.email.toLowerCase().indexOf(branchSearchLower) !== -1
      )
    }

    const data = this.branchDatas.filter((item: { branch_name: string; email: string}) => filterFunc(item))

    this.branchDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    )
    
    if(branchSearchLower.length == 0) {
      this.branchDisplayDatas = [...this.branchDatas]
    }      
  } 
  
  // edit branch form
  editBranchFormInit(data) {
    let wineryArray = []
    data.wineries.forEach(element => {
      wineryArray.push(element.wineries_id)
    });
    this.editBranchForm = this.fb.group({
      branch_id: [data.branch_id, [Validators.required]],
      branch_name: [data.branch_name, [Validators.required]],
      cai: [data.CAI],
      billing_range: [data.billing_range],
      address: [data.address, [Validators.required]],
      city: [data.city, [Validators.required]],
      email: [data.email, [Validators.required]],
      invoice_prefix: [data.invoice_prefix, [Validators.required]],
      printer_type: [data.printer_type, [Validators.required]],
      cost_center: [data.cost_center],
      description: [data.description],
      wineries: [wineryArray, [Validators.required]]
    })      
  }  

  editBranchOpen(data) {
    this.editBranchVisible = true
    this.editBranchFormInit(data)
  }

  editBranchClose() {
    this.editBranchForm.reset()
    this.editBranchVisible = false
  }      

  updateBranch() {
    for (const i in this.editBranchForm.controls) {
      this.editBranchForm.controls[i].markAsDirty()
      this.editBranchForm.controls[i].updateValueAndValidity()
    }          

    if(this.editBranchForm.valid) {
      this.branchUpdateLoader = true

      let params = {
        branchDetails: {
          branch_name: this.editBranchForm.value.branch_name,
          cai: this.editBranchForm.value.cai,
          billing_range: this.editBranchForm.value.billing_range,
          address: this.editBranchForm.value.address,
          city: this.editBranchForm.value.city,
          email: this.editBranchForm.value.email,
          invoice_prefix: this.editBranchForm.value.invoice_prefix,
          printer_type: this.editBranchForm.value.printer_type,
          cost_center: this.editBranchForm.value.cost_center,
          description: this.editBranchForm.value.description,
        },
        wineries: this.editBranchForm.value.wineries.toString()
      }

      this._branchService.putBranch(this.editBranchForm.value.branch_id, params).subscribe((response) => {
        this.branchUpdateLoader = false
        if(response.success) {
          this.openMessageBar(this.translate.instant('SUCCESSFUL'))
          this.openMessageBar(response.message)

          // Populate get branch list api
          this.populateGetBranchList()
        } else {          
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
        this.editBranchClose()
        window.location.reload()
      }, (error) => {
        this.branchUpdateLoader = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateBranch')
        this.editBranchClose()
      })      
    }
  }  

  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteBranchId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }
  
  deleteBranch() {         
    this.deleteBtnLoading = true

    let params = {
      branch_id: this.deleteBranchId
    }

    this._branchService.deleteBranch(params).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get branch list api
        this.populateGetBranchList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      localStorage.removeItem('companyBranchId')
      this.deleteConfirmClose()
      window.location.reload()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteBranch')
      this.deleteConfirmClose()
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
