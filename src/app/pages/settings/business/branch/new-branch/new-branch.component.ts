import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { WineriesService } from 'src/app/pages/settings/inventory/wineries/wineries.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-new-branch',
  templateUrl: './new-branch.component.html',
  styleUrls: ['./new-branch.component.scss']
})
export class NewBranchComponent implements OnInit {

  // add bank account form
  addBranchForm: FormGroup  

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  loader: boolean = false
  wineryDatas: any  

  btnLoading: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _wineriesService: WineriesService,
    private message: NzMessageService,
    private _branchService: BranchService,
    private _router: Router        
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.populateGetWineryList()

    this.addBranchFormInit()
  }

  populateGetWineryList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._wineriesService.getWineryList(params).subscribe((response) => {
      this.wineryDatas = response
      this.loader = false      
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getWineryList')
    })     
  }  

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // add branch form
  addBranchFormInit() {
    this.addBranchForm = this.fb.group({
      branch_name: [null, [Validators.required]],
      cai: [null],
      billing_range: [null],
      address: [null, [Validators.required]],
      city: [null, [Validators.required]],
      email: [null, [Validators.required]],
      invoice_prefix: [null, [Validators.required]],
      printer_type: ['1'],
      cost_center: ['1'],
      description: [null],
      wineries: [null, [Validators.required]]
    })      
  }

  insertBranch() {
    for (const i in this.addBranchForm.controls) {
      this.addBranchForm.controls[i].markAsDirty()
      this.addBranchForm.controls[i].updateValueAndValidity()
    }          

    if(this.addBranchForm.valid) {
      this.btnLoading = true

      let params = {
        company_id: this.companyData.id,
        branch_name: this.addBranchForm.value.branch_name,
        cai: this.addBranchForm.value.cai, 
        billing_range: this.addBranchForm.value.billing_range, 
        address: this.addBranchForm.value.address,
        city: this.addBranchForm.value.city,
        email: this.addBranchForm.value.email,
        invoice_prefix: this.addBranchForm.value.invoice_prefix,
        printer_type: this.addBranchForm.value.printer_type,
        cost_center: this.addBranchForm.value.cost_center,
        description: this.addBranchForm.value.description,
        wineries: this.addBranchForm.value.wineries.toString()
      }

      this._branchService.postBranch(params).subscribe((response) => {
        this.btnLoading = false
        if(response.success) {
          this.openMessageBar(this.translate.instant('SUCCESSFUL'))
          this.openMessageBar(response.message)
          // Redirect to branch list page
          this.gotoBranchList()
        } else {          
          this.openMessageBar(this.translate.instant('FAILED'))
          this.openMessageBar(response.message)
        }
      }, (error) => {
        this.btnLoading = false
        this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertBranch')
      })      
    }
  }  

  gotoBranchList() {
    this._router.navigate(['/user/settings/business/branch/list'], {})
    // Reload after 0.5 sec
    setTimeout(() => {
      window.location.reload()
    }, 500)    
  }

  clearFilter() {
    this.addBranchForm.reset()
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }   

}
