import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingTab3Component } from './accounting-tab3.component';

describe('AccountingTab3Component', () => {
  let component: AccountingTab3Component;
  let fixture: ComponentFixture<AccountingTab3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingTab3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingTab3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
