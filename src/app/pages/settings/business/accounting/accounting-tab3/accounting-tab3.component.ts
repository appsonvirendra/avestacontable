import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Person {
  key: string;
  costcenter_code: string;
  costcenter_name: string;
  costcenter_description: string;
}

@Component({
  selector: 'app-accounting-tab3',
  templateUrl: './accounting-tab3.component.html',
  styleUrls: ['./accounting-tab3.component.scss']
})
export class AccountingTab3Component implements OnInit {

  // costcenter Form
  costcenterForm: FormGroup  

  // edit dialog form
  editCostCenterForm: FormGroup  
  editCostCenterVisible = false

  deleteConfirmVisible = false
 
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.CostCenterFormInit()
    this.editCostCenterFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // CostCenter Form
  CostCenterFormInit() {
    this.costcenterForm = this.fb.group({ 
      code: [null, [Validators.required]],
      name: [null, [Validators.required]],
      description: [null, [Validators.required]]
    })      
  } 

    savecostcenter() {
    for (const i in this.costcenterForm.controls) {
      this.costcenterForm.controls[i].markAsDirty()
      this.costcenterForm.controls[i].updateValueAndValidity()
    }          

    if(this.costcenterForm.valid) {      
      // console.log('this.costcenterForm.value', this.costcenterForm.value)
    }
  }

    clearcostcenter() {
    this.costcenterForm.reset()
    this.costcenterForm.patchValue({
      code: [null],
      name: [null],
      description: [null]
    })
  }

    listOfData: Person[] = [
    {
      key: '1',
      costcenter_code: 'Logistic Investments',
      costcenter_name: 'Inversiones Logisticas',
      costcenter_description: 'Item 1'
    },
    {
      key: '2',
      costcenter_code: 'Logistic Investments',
      costcenter_name: 'Purchases',
      costcenter_description: 'Item 2'
    }
  ];

   // editCostCenterForm 
  editCostCenterFormInit() {
    this.editCostCenterForm = this.fb.group({
      costcenter_code: [null, [Validators.required]],
      costcenter_name: [null, [Validators.required]],
      costcenter_description: [null, [Validators.required]]
    })      
  }  

  editCostCenterOpen() {
    this.editCostCenterVisible = true
  }

  editCostCenterClose() {
    this.editCostCenterForm.reset()
    this.editCostCenterVisible = false
  }      

  editCostCenterSave() {    
    for (const i in this.editCostCenterForm.controls) {
      this.editCostCenterForm.controls[i].markAsDirty()
      this.editCostCenterForm.controls[i].updateValueAndValidity()
    }          
    if(this.editCostCenterForm.valid) {
      this.editCostCenterClose()
    }
  }

   // delete costcenter
  deleteCostCenterOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}