import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingTab2Component } from './accounting-tab2.component';

describe('AccountingTab2Component', () => {
  let component: AccountingTab2Component;
  let fixture: ComponentFixture<AccountingTab2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingTab2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingTab2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
