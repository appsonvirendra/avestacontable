import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Person {
  key: string;
  sources_name: string;
}

@Component({
   selector: 'app-accounting-tab2',
  templateUrl: './accounting-tab2.component.html',
  styleUrls: ['./accounting-tab2.component.scss']
})
export class AccountingTab2Component implements OnInit {

  // sourcesForm
  sourcesForm: FormGroup  

  // edit dialog form
  editSourcesForm: FormGroup  
  editSourcesVisible = false

  deleteConfirmVisible = false
 
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.SourcesFormInit()
    this.editSourcesFormInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // sourcesForm Form
  SourcesFormInit() {
    this.sourcesForm = this.fb.group({ 
      source: [null, [Validators.required]]
    })      
  } 

    savesources() {
    for (const i in this.sourcesForm.controls) {
      this.sourcesForm.controls[i].markAsDirty()
      this.sourcesForm.controls[i].updateValueAndValidity()
    }          

    if(this.sourcesForm.valid) {      
      // console.log('this.sourcesForm.value', this.sourcesForm.value)
    }
  }

    clearsources() {
    this.sourcesForm.reset()
    this.sourcesForm.patchValue({
      source: [null]
    })
  }

    listOfData: Person[] = [
    {
      key: '1',
      sources_name: 'Heritage'
    },
    {
      key: '2',
      sources_name: 'Passive'
    },
    {
      key: '3',
      sources_name: 'Assets'
    }
  ];

   // editSourcesForm 
  editSourcesFormInit() {
    this.editSourcesForm = this.fb.group({
      source: [null, [Validators.required]],
    })      
  }  

  editSourcesOpen() {
    this.editSourcesVisible = true
  }

  editSourcesClose() {
    this.editSourcesForm.reset()
    this.editSourcesVisible = false
  }      

  editSourcesSave() {    
    for (const i in this.editSourcesForm.controls) {
      this.editSourcesForm.controls[i].markAsDirty()
      this.editSourcesForm.controls[i].updateValueAndValidity()
    }          
    if(this.editSourcesForm.valid) {
      this.editSourcesClose()
    }
  }

   // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}