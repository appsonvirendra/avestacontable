import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-accounting-tab1',
  templateUrl: './accounting-tab1.component.html',
  styleUrls: ['./accounting-tab1.component.scss']
})
export class AccountingTab1Component implements OnInit {

  // correlativeinvoice form
  correlativeinvoiceForm: FormGroup  

  // seatcorrelativeForm
  seatcorrelativeForm: FormGroup  

  invoice_branch_button = true
   

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.seatCorrelativeFormInit()
    this.CorrelativeInvoiceInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  // Correlative Invoice form
  CorrelativeInvoiceInit() {
    this.correlativeinvoiceForm = this.fb.group({ 
      invoice_branch_selection: ['1'],
      invoice_branch: [null]
    })      
  }

  saveCorrelativeInvoice() {
    for (const i in this.correlativeinvoiceForm.controls) {
      this.correlativeinvoiceForm.controls[i].markAsDirty()
      this.correlativeinvoiceForm.controls[i].updateValueAndValidity()
    }          

    if(this.correlativeinvoiceForm.valid) {      
      // console.log('this.correlativeinvoiceForm.value', this.correlativeinvoiceForm.value)
    }
  }

  clearCorrelativeInvoiceFilter() {
    this.correlativeinvoiceForm = this.fb.group({
      invoice_phone: [null, [Validators.required]], 
      invoice_branch_selection: ['0'],
      invoice_branch: [null]
    })
  }

  // seatcorrelative Form
  seatCorrelativeFormInit() {
    this.seatcorrelativeForm = this.fb.group({ 
      current_correlative: [null, [Validators.required]],
      branch: [null, [Validators.required]],
    })      
  } 

    saveSeatCorrelative() {
    for (const i in this.seatcorrelativeForm.controls) {
      this.seatcorrelativeForm.controls[i].markAsDirty()
      this.seatcorrelativeForm.controls[i].updateValueAndValidity()
    }          

    if(this.seatcorrelativeForm.valid) {      
      // console.log('this.seatcorrelativeForm.value', this.seatcorrelativeForm.value)
    }
  }

    clearSeatCorrelative() {
    this.seatcorrelativeForm.reset()
    this.seatcorrelativeForm.patchValue({
      current_correlative: [null],
      branch: ['1'],
    })
  }

  branchSelection() {
    if(this.correlativeinvoiceForm.value.invoice_branch_selection == '1') {
      this.invoice_branch_button = true
    } else {
      this.invoice_branch_button = false
    }    
  }


}