import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingTab1Component } from './accounting-tab1.component';

describe('AccountingTab1Component', () => {
  let component: AccountingTab1Component;
  let fixture: ComponentFixture<AccountingTab1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingTab1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingTab1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
