import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private apiService: ApiService
  ) { 

  }

  getEmployeeList(bodyparams) {
    return this.apiService.post(`/employeeList`, bodyparams)
      .pipe(map(data => data.data))
  }  

  postEmployee(bodyparams) {
    return this.apiService.post(`/employeeNew`, bodyparams)
      .pipe(map(data => data))
  }  

  getSingleEmployee(bodyparams) {
    return this.apiService.post(`/employeeData`, bodyparams)
      .pipe(map(data => data.data[0]))
  }  

  putEmployee(employee_id, bodyparams) {
    return this.apiService.post(`/employeeUpdate/`+employee_id, bodyparams)
      .pipe(map(data => data))
  }  

  deleteEmployee(employee_id) {
    return this.apiService.post(`/employeeDelete/`+employee_id)
      .pipe(map(data => data))
  }  

}
