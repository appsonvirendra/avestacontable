import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// basic load start
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
// basic load end

import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { NewEmployeeComponent } from './new-employee/new-employee.component';
import { InfoEmployeeComponent } from './info-employee/info-employee.component';
import { EmployeeTableComponent } from './employee-list/employee-table/employee-table.component';
import { InfoEmployeeTab1Component } from './info-employee/info-employee-tab1/info-employee-tab1.component';

@NgModule({
  declarations: [EmployeeListComponent, NewEmployeeComponent, InfoEmployeeComponent, EmployeeTableComponent, InfoEmployeeTab1Component],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule    
  ]
})
export class EmployeesModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
