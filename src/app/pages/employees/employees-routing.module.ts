import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { NewEmployeeComponent } from './new-employee/new-employee.component';
import { InfoEmployeeComponent } from './info-employee/info-employee.component';

const routes: Routes = [
  {
    path: 'list',
    component: EmployeeListComponent    
  },
  {
    path: ':_id',
    component: NewEmployeeComponent
  },
  {
    path: 'emp/info',
    component: InfoEmployeeComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
