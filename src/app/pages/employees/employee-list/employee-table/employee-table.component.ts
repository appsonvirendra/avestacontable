import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EmployeeService } from 'src/app/pages/employees/employee.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.scss']
})
export class EmployeeTableComponent implements OnInit {

  @Input() employeeDatas: any

  // search
  search_title = ''  
  employeeDisplayDatas: any
  sortName: string | null = null
  sortValue: string | null = null  

  deleteConfirmVisible = false

  deleteEmployeeId: any

  deleteBtnLoading: boolean = false  
  
  @Output("populateGetEmployeeList") populateGetEmployeeList: EventEmitter<any> = new EventEmitter()  

  constructor(
    private translate: TranslateService,
    private _employeeService: EmployeeService,
    private message: NzMessageService    
  ) { 
    
  }

  ngOnInit() {
    this.employeeData()
  }

  employeeData() {
    this.employeeDatas.forEach(value => {
      value.full_name = value.first_name + " " + value.last_name
    })
    this.employeeDisplayDatas = [...this.employeeDatas]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { first_name: string; identification: string; phone: string }) => {
      return (
        item.first_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.identification.toLowerCase().indexOf(searchLower) !== -1 ||
        item.phone.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.employeeDatas.filter((item: { first_name: string; identification: string; phone: string }) => filterFunc(item))

    this.employeeDisplayDatas = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? -1
          : 1
        : b[this.sortName!] > a[this.sortName!]
        ? -1
        : 1
    ) 
    
    if(searchLower.length == 0) {
      this.employeeDisplayDatas = [...this.employeeDatas]
    }    
  } 
  
  // delete confirm
  deleteConfirmOpen(elementId) {
    this.deleteConfirmVisible = true
    this.deleteEmployeeId = elementId
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }    

  deleteEmployee() {         
    this.deleteBtnLoading = true

    this._employeeService.deleteEmployee(this.deleteEmployeeId).subscribe((response) => {
      this.deleteBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Populate get employee list api
        this.populateGetEmployeeList.emit()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
      this.deleteConfirmClose()
    }, (error) => {
      this.deleteBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' deleteEmployee')
      this.deleteConfirmClose()
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
