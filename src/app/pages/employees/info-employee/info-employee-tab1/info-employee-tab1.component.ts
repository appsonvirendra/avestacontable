import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-employee-tab1',
  templateUrl: './info-employee-tab1.component.html',
  styleUrls: ['./info-employee-tab1.component.scss']
})
export class InfoEmployeeTab1Component implements OnInit {

  @Input() employeeDatas: any

  constructor() { }

  ngOnInit() {
  }

}
