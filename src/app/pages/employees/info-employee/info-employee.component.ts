import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { EmployeeService } from 'src/app/pages/employees/employee.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-info-employee',
  templateUrl: './info-employee.component.html',
  styleUrls: ['./info-employee.component.scss']
})
export class InfoEmployeeComponent implements OnInit {

  employee_id: any

  employeeDatas: any

  employeeDataLoader: boolean = false

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private _employeeService: EmployeeService,
    private message: NzMessageService        
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getQueryParams()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  getQueryParams() {
    this.route.queryParams.subscribe(params => {
      this.employee_id = params.employee_id
      this.populateGetEmployee()
    })    
  }

  populateGetEmployee() {
    this.employeeDataLoader = true
    let params = {
      employee_id: this.employee_id
    }
    this._employeeService.getSingleEmployee(params).subscribe((response) => {
      this.employeeDatas = response

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.employeeDataLoader = false     
    }, (error) => {
      this.employeeDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleEmployee')
    })      
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
