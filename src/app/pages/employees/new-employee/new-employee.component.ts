import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BranchService } from 'src/app/pages/settings/business/branch/branch.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { JwtService } from 'src/app/services/jwt.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { EmployeeService } from 'src/app/pages/employees/employee.service';

import { Router, ActivatedRoute } from '@angular/router';

import * as _moment from 'moment';
const moment = _moment

import { ChargeService } from 'src/app/pages/settings/inventory/charge/charge.service';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.scss']
})
export class NewEmployeeComponent implements OnInit {

  employee_id: any
  pageType: any  

  employeeForm: FormGroup

  saveBtnLoading: boolean = false

  companyData = JSON.parse(localStorage.getItem('companyRes'))

  companyBranchId = localStorage.getItem('companyBranchId')  

  branchDatas: any = []
  
  base64String:String = null

  // Format date
  format = 'YYYY-MM-DD'
  
  loader: boolean = false

  employeeDatas: any
  employeeDataLoader: any
  employeeDataVisible: any  
  chargeDatas: any

  chargeDataLoader: boolean = false

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private _branchService: BranchService,
    private message: NzMessageService,
    private _jwtService: JwtService,
    private authService: AuthenticationService,
    private _employeeService: EmployeeService,        
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _chargeService: ChargeService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    if(this.companyBranchId) {
      this.populateGetChargeList()      
    } else {
      this.populateGetBranchList()
    }    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  } 
  
  populateGetBranchList() {
    this.loader = true
    let params = {
      company_id: this.companyData.id
    }
    this._branchService.getBranchList(params).subscribe((response) => {
      this.branchDatas = response

      // branch id store in local storage and asign to select box
      if(!!localStorage.getItem('companyBranchId')) {
        this.companyBranchId = localStorage.getItem('companyBranchId')
      } else {
        localStorage.setItem('companyBranchId', this.branchDatas[0].branch_id)
        this.companyBranchId = this.branchDatas[0].branch_id
      }      

      this.loader = false

      // Populate get charge list api
      this.populateGetChargeList()
    }, (error) => {
      this.loader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getBranchList')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })     
  }
  
  populateGetChargeList() {
    this.chargeDataLoader = true
    let params = {
      company_id: this.companyData.id
    }
    this._chargeService.getChargeList(params).subscribe((response) => {
      this.chargeDatas = response
      this.openMessageBar(this.translate.instant('EMPLOYEE_POSITION_LIST_RECEIVED_SUCCESSFUL'))

      this.getQueryParams()

      this.chargeDataLoader = false      
    }, (error) => {
      this.chargeDataLoader = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getChargeList')    
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }
    })     
  }  
  
  // Get current query params
  getQueryParams() {
    this._activatedRoute.queryParams.subscribe(params => { 
      this.employee_id = params.employee_id  
      if(this.employee_id) {
        this.pageType = 'edit'
        this.populateGetEmployee()
      } else {        
        this.pageType = 'new'
        this.employeeFormInitNew()
        this.employeeDataVisible = true
      }
    })    
  }   

  populateGetEmployee() {
    this.employeeDataLoader = true
    let params = {
      employee_id: this.employee_id
    }
    this._employeeService.getSingleEmployee(params).subscribe((response) => {
      this.employeeDatas = response

      // Int to string convert for select box auto selection
      this.employeeDatas.sex = this.employeeDatas.sex.toString()

      this.openMessageBar(this.translate.instant('DETAIL'))

      this.employeeFormInitEdit()

      this.employeeDataLoader = false     
      this.employeeDataVisible = true 
    }, (error) => {
      this.employeeDataLoader = false
      this.employeeDataVisible = false

      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' getSingleEmployee')
      // if token expire user should be logout
      if(error.message == 'token_invalid' || error.message == 'token_expired') {
        this.userLogout()
      }      
    })      
  }  

  // edit employee form
  employeeFormInitEdit() {
    this.employeeForm = this.fb.group({
      identification: [this.employeeDatas.identification],
      first_name: [this.employeeDatas.first_name],
      last_name: [this.employeeDatas.last_name],
      birth_date: [this.employeeDatas.birth_date],
      sex: [this.employeeDatas.sex],
      phone: [this.employeeDatas.phone],
      mobile: [this.employeeDatas.mobile],
      email: [this.employeeDatas.email],
      direction: [this.employeeDatas.direction],
      admission_date: [this.employeeDatas.admission_date],
      salary: [this.employeeDatas.salary],
      charge_id: [this.employeeDatas.charge_id],
      image: [null]
    })    
  }  

  employeeFormInitNew() {
    this.employeeForm = this.fb.group({
      identification: [null],
      first_name: [null],
      last_name: [null],
      birth_date: [null],
      sex: [null],
      phone: [null],
      mobile: [null],
      email: [null],
      direction: [null],
      admission_date: [null],
      salary: [null],
      charge_id: [null],
      image: [null]
    })      
  }

  handleFileSelect(evt) {
    var files = evt.target.files
    var file = files[0]

    if (files && file) {
      var reader = new FileReader()
      reader.onload =this._handleReaderLoaded.bind(this)
      reader.readAsBinaryString(file)
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result
    this.base64String = btoa(binaryString)
  }  

  saveEmployee() {

    // Change date format
    let birthDateFormat = '', admissionDateFormat = ''
    if(this.employeeForm.value.birth_date) {
      birthDateFormat = moment(this.employeeForm.value.birth_date).format(this.format)
    }
    
    if(this.employeeForm.value.admission_date) {
      admissionDateFormat = moment(this.employeeForm.value.admission_date).format(this.format)
    }    

    let params = {
      company_id: this.companyData.id,
      branch_id: this.companyBranchId,        
      identification: this.employeeForm.value.identification,
      first_name: this.employeeForm.value.first_name,
      last_name: this.employeeForm.value.last_name,
      birth_date: birthDateFormat,
      sex: this.employeeForm.value.sex,
      phone: this.employeeForm.value.phone,
      mobile: this.employeeForm.value.mobile,
      email: this.employeeForm.value.email,
      direction: this.employeeForm.value.direction,
      admission_date: admissionDateFormat,
      salary: this.employeeForm.value.salary,
      charge_id: this.employeeForm.value.charge_id,
      image: this.base64String
    }    

    // Remove image key if encoded string not persent
    if(params.image == null) {
      delete params.image
    }     

    // Call update method or insert method based on page type
    if (this.pageType == 'edit') {
      this.updateEmployee(params)    
    } else {   
      this.insertEmployee(params)
    }     
  }
  
  updateEmployee(itemRowData) {
    this.saveBtnLoading = true
    this._employeeService.putEmployee(this.employee_id, itemRowData).subscribe((response) => {
      this.saveBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to employee list page
        this.gotoEmployeeList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.saveBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' updateEmployee')
    })  
  }
  
  insertEmployee(itemRowData) {
    this.saveBtnLoading = true
    this._employeeService.postEmployee(itemRowData).subscribe((response) => {
      this.saveBtnLoading = false
      if(response.success) {
        this.openMessageBar(this.translate.instant('SUCCESSFUL'))
        this.openMessageBar(response.message)

        // Redirect to employee list page
        this.gotoEmployeeList()
      } else {          
        this.openMessageBar(this.translate.instant('FAILED'))
        this.openMessageBar(response.message)
      }
    }, (error) => {
      this.saveBtnLoading = false
      this.openMessageBar(this.translate.instant('UNABLE_TO_CONNECT')+' insertEmployee')
    })    
  }  

  gotoEmployeeList() {
    this._router.navigate(['/user/employees/list'], {})
  }  

  clearFilter() {
    this.employeeForm.reset()
  }

  userLogout() {
    // Remove token from localstorage
    setTimeout(() => {
      this._jwtService.destroyToken()
      localStorage.clear()
      this.authService.logout()      
    }, 2000)    
  }  

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}
