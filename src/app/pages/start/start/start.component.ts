import { Component, OnInit, NgZone } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as Highcharts from 'highcharts';

import { StartService } from '../start.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})

export class StartComponent implements OnInit {

isShowshortcut = false;

supportDialogVisible = false
supportForm: FormGroup

  deleteConfirmVisible = false
  
  chartWidth: any = '90%'

  windowWidth: any

  // branchData: any

  // companyData = JSON.parse(localStorage.getItem('companyRes'))

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private ngZone: NgZone,
    private _startService: StartService,
    private message: NzMessageService
  ) { 
   window.onload = (e) =>
   {
      this.ngZone.run(() => {
        this.windowWidth = window.innerWidth
         // console.log("Width: " + window.innerWidth);
         // console.log("Height: " + window.innerHeight);
      });
   };
   
    window.setTimeout(() => {
      this.chartWidth = '99%'
    }, 2000)
  } 

  ngOnInit() {
   this.supportFormInit()
    this.languageTranslate()

   //  window.dispatchEvent(new Event('resize'))
  }
  
  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }   

  dataChart: typeof Highcharts = Highcharts;
  dataChartOptions: any = {   
    chart: {        
      type: "column",
    },
    title: {
      text: "Ingresos / Gastos"
    },
    subtitle: {
      enabled: false
    },
    xAxis:{
      categories:["Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    },
    yAxis: {          
      title:{
        text:""
      },
      labels: {
        formatter: function() {
          return this.value + "k"
        }
      }       
    },
    tooltip: {
      valueSuffix:""
    },
    colors: [
      '#90ED7D',
      '#F7A35C'
    ],     
    series: [
      {
        name: 'Ingresos',
        data: [100, 200, 300, 400, 550, 300]
      },
      {
        name: 'Gastos',
        data: [200, 100, 300, 400, 600, 200]
      }
    ],
    credits: {
      enabled: false
    }
  }; 

  pieChart: typeof Highcharts = Highcharts;
  pieChartOptions = {   
    chart: {              
      type: "pie",
    },
    title: {
      text: "Gastos"
    },
    subtitle: {
      enabled: false
    },
    xAxis:{
      categories:[]
    },
    yAxis: {          
      title:{
        text:""
      } 
    },
    tooltip: {
      valueSuffix:""
    },
    colors: [
      '#90ED7D',
      '#F7A35C'
    ],    
    series: [{
      name: '',
      colorByPoint: true,
      data: [
        {
          name: 'Proveedores',
          y: 30
        },
        {
          name: 'Cuentas contables',
          y: 70,
        }        
      ]
    }],
    credits: {
      enabled: false
    }
  };   
  
  lineChart: typeof Highcharts = Highcharts;
  lineChartOptions: any = {   
    chart: {        
      type: "column",
    },
    title: {
      text: "Cuentas por cobrar"
    },
    subtitle: {
      enabled: false
    },
    xAxis:{
      categories:["Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    },
    yAxis: {          
      title:{
        text:""
      },
      labels: {
        formatter: function() {
          return this.value + "k"
        }
      }       
    },
    tooltip: {
      valueSuffix:""
    },
    colors: [
      '#90ED7D',
      '#5B5B5F'
    ],    
    series: [
      {
        name: 'Ingresos',
        data: [1000, 200, 300, 400, 550, 1000]
      },
      {
        name: 'Gastos',
        data: [100, 100, 300, 400, 600, 200]
      }
    ],
    credits: {
      enabled: false
    }
  };   

  salesPieChart: typeof Highcharts = Highcharts;
  salesPieChartOptions = {   
    chart: {              
      type: "pie",
    },
    title: {
      text: "Ventas"
    },
    subtitle: {
      enabled: false
    },
    xAxis:{
      categories:[]
    },
    yAxis: {          
      title:{
        text:""
      } 
    },
    tooltip: {
      valueSuffix:""
    },
    colors: [
      '#90ED7D',
      '#5B5B5F'
    ],    
    series: [{
      name: '',
      colorByPoint: true,
      data: [
        {
          name: 'Mejores clientes',
          y: 60
        },
        {
          name: 'Items más vendidos',
          y: 40,
        }        
      ]
    }],
    credits: {
      enabled: false
    }
  };   
 // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  } 
  ConfirmClose() {
    this.deleteConfirmVisible = false;
    this.isShowshortcut = !this.isShowshortcut;
  } 

     // supportForm
     supportFormInit() {
    this.supportForm = this.fb.group({
      question: [null, [Validators.required]],
      details: [null, [Validators.required]],
      your_name: [null, [Validators.required]]
    })   
  }

  openSupportDialog() {
    this.supportDialogVisible = true
  }

  closeSupportDialog() {
    this.supportDialogVisible = false
  }

  saveSupport() {
    for (const i in this.supportForm.controls) {
      this.supportForm.controls[i].markAsDirty()
      this.supportForm.controls[i].updateValueAndValidity()
    }  
    // console.log('this.supportForm.value --', this.supportForm.value)
    if(this.supportForm.value.category_name != null && this.supportForm.value.category_name != '')  {
      this.supportDialogVisible = false
      this.supportFormInit()
    }    
  }

  openMessageBar(msg) {
    this.message.info(msg, {
      nzDuration: 5000
    })
  }  

}


