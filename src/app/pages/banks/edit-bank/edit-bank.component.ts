// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-edit-bank',
//   templateUrl: './edit-bank.component.html',
//   styleUrls: ['./edit-bank.component.scss']
// })
// export class EditBankComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-bank',
  templateUrl: './edit-bank.component.html',
  styleUrls: ['./edit-bank.component.scss']
})
export class EditBankComponent implements OnInit {

  transactionAddForm: FormGroup

  currentDate:any = new Date()  

  // transaction table data 
  transactionTableDatas = []  

  // search account dialog
  searchAccountVisible = false
  accountDatas = []

  // bankaccount search dialog
  searchBankaccountVisible = false
  bankaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.transactionAddFormInit()

    this.transactionTableData()  
    
    this.accountData()

    this.bankaccountData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  transactionAddFormInit() {
    this.transactionAddForm = this.fb.group({
      date: [null, [Validators.required]],
      transaction: ['0', [Validators.required]],
      bankaccount_id: [null],
      bankaccount_no: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      description: [null, [Validators.required]],
      reference: [null, [Validators.required]],
      voucher: [null, [Validators.required]],
      source: [null, [Validators.required]],
      cost_center: [null, [Validators.required]],
      commentary: [null],
      debit: [null],
      credit: [null],
      transactionTableDatas: []
    })      
  }
  
  saveTransaction() {
    for (const i in this.transactionAddForm.controls) {
      this.transactionAddForm.controls[i].markAsDirty()
      this.transactionAddForm.controls[i].updateValueAndValidity()
    }
    if(this.transactionAddForm.valid) {
      console.log('this.transactionAddForm.value', this.transactionAddForm.value)
    }
  }

  transactionTableData() {
    this.transactionTableDatas = []    
  }

  transactionTableDataRemove(item_index) {
    this.transactionTableDatas.splice(item_index, 1)
    this.transactionAddForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  

  // search account dialog
  searchAccountOpen() {
    this.searchAccountVisible = true
  }

  searchAccountClose() {
    this.searchAccountVisible = false
  }   

  accountData() {
    this.accountDatas = [
      {
        account_id: '1',
        account_code: '1-01-01-01',
        account_description: "CAJA GENERAL",
        belonging_to: 'Activo',
        account_is: 'Cuenta de Grupo'
      }           
    ]
  }

  accountAddInput($event) {
    this.transactionTableDatas.push(
      $event
    )

    this.transactionAddForm.patchValue({
      transactionTableDatas: $event
    })     

    this.searchAccountClose()    
  }

  // search bankaccount dialog
  searchBankaccountOpen() {
    this.searchBankaccountVisible = true
  }

  searchBankaccountClose() {
    this.searchBankaccountVisible = false
  }   

  bankaccountData() {
    this.bankaccountDatas = [
      {
        bankaccount_id: '1',
        bankaccount_no: '00001111',
        bank_name: 'Ficohsa',
        bank_id: '1',
        bank: 'Banco Financiera Comercial',
        bank_address: 'Avenida'
      }               
    ]
  }

  bankaccountAddInput($event) {
    this.transactionAddForm.patchValue({
      bankaccount_id: $event.bankaccount_id,
      bankaccount_no: $event.bankaccount_no
    })

    this.searchBankaccountClose()    
  }  

}

