import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bank-account-new',
  templateUrl: './bank-account-new.component.html',
  styleUrls: ['./bank-account-new.component.scss']
})
export class BankAccountNewComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()

  accountCodingVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      bankaccount_no: [null, [Validators.required]],
      bankaccount_initial_balance: [null, [Validators.required]],
      bank_name: [null, [Validators.required]],
      bank: [null, [Validators.required]],
      bank_address: [null, [Validators.required]],
      bank_correlative_checks: [null, [Validators.required]]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm.reset() 
  }  
  
  accountCodingOpen() {
    this.accountCodingVisible = true
  }

  accountCodingClose() {
    this.accountCodingVisible = false
  }

}
