import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bank-account-table',
  templateUrl: './bank-account-table.component.html',
  styleUrls: ['./bank-account-table.component.scss']
})
export class BankAccountTableComponent implements OnInit {

  @Input() bankaccountData: any

  // edit bank account form
  editBankAccountForm: FormGroup  
  editBankAccountVisible = false

  // search
  search_title = ''

  bankaccountDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.editBankAccountFormInit()

    this.bankAccountData()
  }

  // edit bank account form
  editBankAccountFormInit() {
    this.editBankAccountForm = this.fb.group({
      bankaccount_no: [null, [Validators.required]],
      bank_name: [null, [Validators.required]],
      bank: [null, [Validators.required]],
      bank_address: [null, [Validators.required]],
      bank_correlative_checks: [null, [Validators.required]]
    })      
  }  

  editBankAccountOpen() {
    this.editBankAccountVisible = true
  }

  editBankAccountClose() {
    this.editBankAccountForm.reset()
    this.editBankAccountVisible = false
  }      

  editBankAccountSave() {    
    for (const i in this.editBankAccountForm.controls) {
      this.editBankAccountForm.controls[i].markAsDirty()
      this.editBankAccountForm.controls[i].updateValueAndValidity()
    }          

    if(this.editBankAccountForm.valid) {
      this.editBankAccountClose()
    }
  }

  bankAccountData() {
    this.bankaccountDisplayData = [...this.bankaccountData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { bankaccount_no: string; bank_name: string; bank: string; bank_address: string }) => {
      return (
        item.bankaccount_no.toLowerCase().indexOf(searchLower) !== -1 ||
        item.bank_name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.bank.toLowerCase().indexOf(searchLower) !== -1 ||
        item.bank_address.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.bankaccountData.filter((item: { bankaccount_no: string; bank_name: string; bank: string; bank_address: string }) => filterFunc(item))

    this.bankaccountDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.bankaccountDisplayData = [...this.bankaccountData]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}
