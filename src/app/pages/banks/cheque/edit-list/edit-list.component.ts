import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
 styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit {

  chequeAddForm: FormGroup

  currentDate:any = new Date()  

  // cheque table data 
  chequeTableDatas = []  

  // search account dialog
  searchAccountVisible = false
  accountDatas = []

  // bankaccount search dialog
  searchBankaccountVisible = false
  bankaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.chequeAddFormInit()

    this.chequeTableData()  
    
    this.accountData()

    this.bankaccountData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  chequeAddFormInit() {
    this.chequeAddForm = this.fb.group({
      date: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      amount_in_letters: [null, [Validators.required]],
      pay_to: [null, [Validators.required]],
      bankaccount_id: [null],
      bankaccount_no: [null, [Validators.required]],
      cheque_no: [null, [Validators.required]],
      reference: [null, [Validators.required]],
      voucher: [null, [Validators.required]],
      source: [null, [Validators.required]],
      cost_center: [null, [Validators.required]],
      commentary: [null],
      debit: [null],
      credit: [null],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.chequeAddForm.controls) {
      this.chequeAddForm.controls[i].markAsDirty()
      this.chequeAddForm.controls[i].updateValueAndValidity()
    }
    if(this.chequeAddForm.valid) {
      console.log('this.chequeAddForm.value', this.chequeAddForm.value)
    }
  }

  chequeTableData() {
    this.chequeTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.chequeTableDatas.splice(item_index, 1)
    this.chequeAddForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  

  // search account dialog
  searchAccountOpen() {
    this.searchAccountVisible = true
  }

  searchAccountClose() {
    this.searchAccountVisible = false
  }   

  accountData() {
    this.accountDatas = [
      {
        account_id: '1',
        account_code: '1-01-01-01',
        account_description: "CAJA GENERAL",
        belonging_to: 'Activo',
        account_is: 'Cuenta de Grupo'
      }           
    ]
  }

  accountAddInput($event) {
    this.chequeTableDatas.push(
      $event
    )

    this.chequeAddForm.patchValue({
      chequeTableDatas: $event
    })     

    this.searchAccountClose()    
  }

  // search bankaccount dialog
  searchBankaccountOpen() {
    this.searchBankaccountVisible = true
  }

  searchBankaccountClose() {
    this.searchBankaccountVisible = false
  }   

  bankaccountData() {
    this.bankaccountDatas = [
      {
        bankaccount_id: '1',
        bankaccount_no: '00001111',
        bank_name: 'Ficohsa',
        bank_id: '1',
        bank: 'Banco Financiera Comercial',
        bank_address: 'Avenida',
        cheque_no: '112233'
      }               
    ]
  }

  bankaccountAddInput($event) {
    this.chequeAddForm.patchValue({
      bankaccount_id: $event.bankaccount_id,
      bankaccount_no: $event.bankaccount_no,
      cheque_no: $event.cheque_no
    })

    this.searchBankaccountClose()    
  }

}

