import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-cheque-list',
  templateUrl: './cheque-list.component.html',
  styleUrls: ['./cheque-list.component.scss']
})
export class ChequeListComponent implements OnInit {

  chequeDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.chequeData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.chequeDatas = []
  }
  
  chequeData() {
    this.chequeDatas = [
      {
        cheque_id: '1',
        cheque_date: '2018-12-23',
        cheque_reference: '00001',
        cheque_pay_to: 'al portador',
        cheque_bank: 'Banco Financiera',
        cheque_account_no: '00112233',
        cheque_amount: '300',
        cheque_state: '2'
      }, 
      {
        cheque_id: '2',
        cheque_date: '2018-12-23',
        cheque_reference: '00002',
        cheque_pay_to: 'al portador',
        cheque_bank: 'Banco Financiera',
        cheque_account_no: '00112233',
        cheque_amount: '300',
        cheque_state: '1'
      },
      {
        cheque_id: '3',
        cheque_date: '2018-12-23',
        cheque_reference: '00003',
        cheque_pay_to: 'al portador',
        cheque_bank: 'Banco Financiera',
        cheque_account_no: '00112233',
        cheque_amount: '300',
        cheque_state: '1'
      }, 
      {
        cheque_id: '4',
        cheque_date: '2018-12-23',
        cheque_reference: '00004',
        cheque_pay_to: 'al portador',
        cheque_bank: 'Banco Financiera',
        cheque_account_no: '00112233',
        cheque_amount: '300',
        cheque_state: '1'
      }                      
    ]
  }

}
