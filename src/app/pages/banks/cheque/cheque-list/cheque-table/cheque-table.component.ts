import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cheque-table',
  templateUrl: './cheque-table.component.html',
  styleUrls: ['./cheque-table.component.scss']
})
export class ChequeTableComponent implements OnInit {
   // modal loading
   isVisible = false;
   isConfirmLoading = false;
  @Input() chequeData: any

  constructor() { }
   // modal confirm
  deleteConfirmOpen(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnInit() {
  }

}
