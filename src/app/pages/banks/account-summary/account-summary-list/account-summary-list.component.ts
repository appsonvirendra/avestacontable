import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-summary-list',
  templateUrl: './account-summary-list.component.html',
  styleUrls: ['./account-summary-list.component.scss']
})
export class AccountSummaryListComponent implements OnInit {

  accountsummaryDatas: any

  activeCollapse = true  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accountsummaryData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.accountsummaryDatas = []
  }
  
  accountsummaryData() {
    this.accountsummaryDatas = [
      {
        accountsummary_id: '1',
        accountsummary_bankname: 'Banco Financiera Comercial',
        accountsummary_account_no: '000111222',
        accountsummary_balance: '5000'
      }                   
    ]
  }

}
