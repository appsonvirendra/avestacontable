import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-account-summary-table',
  templateUrl: './account-summary-table.component.html',
  styleUrls: ['./account-summary-table.component.scss']
})
export class AccountSummaryTableComponent implements OnInit {

  @Input() accountsummaryData: any

  constructor() { }

  ngOnInit() {
  }

}
