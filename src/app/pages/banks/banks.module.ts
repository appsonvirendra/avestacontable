import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';

import { BanksRoutingModule } from './banks-routing.module';
import { BankAccountComponent } from './bank-account/bank-account.component';
import { BankAccountNewComponent } from './bank-account/bank-account-new/bank-account-new.component';
import { BankAccountTableComponent } from './bank-account/bank-account-table/bank-account-table.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { NewTransactionComponent } from './transaction/new-transaction/new-transaction.component';
import { InfoTransactionComponent } from './transaction/info-transaction/info-transaction.component';
import { TransactionFilterComponent } from './transaction/transaction-list/transaction-filter/transaction-filter.component';
import { TransactionTableComponent } from './transaction/transaction-list/transaction-table/transaction-table.component';
import { ChequeListComponent } from './cheque/cheque-list/cheque-list.component';
import { ChequeFilterComponent } from './cheque/cheque-list/cheque-filter/cheque-filter.component';
import { ChequeTableComponent } from './cheque/cheque-list/cheque-table/cheque-table.component';
import { NewChequeComponent } from './cheque/new-cheque/new-cheque.component';
import { InfoChequeComponent } from './cheque/info-cheque/info-cheque.component';
import { AccountStatusListComponent } from './account-status/account-status-list/account-status-list.component';
import { AccountStatusFilterComponent } from './account-status/account-status-list/account-status-filter/account-status-filter.component';
import { AccountStatusTableComponent } from './account-status/account-status-list/account-status-table/account-status-table.component';
import { AccountSummaryListComponent } from './account-summary/account-summary-list/account-summary-list.component';
import { AccountSummaryFilterComponent } from './account-summary/account-summary-list/account-summary-filter/account-summary-filter.component';
import { AccountSummaryTableComponent } from './account-summary/account-summary-list/account-summary-table/account-summary-table.component';
import { EditBankComponent } from './edit-bank/edit-bank.component';
import { EditListComponent } from './cheque/edit-list/edit-list.component';


@NgModule({
  declarations: [BankAccountComponent, BankAccountNewComponent, BankAccountTableComponent, TransactionListComponent, NewTransactionComponent, InfoTransactionComponent, TransactionFilterComponent, TransactionTableComponent, ChequeListComponent, ChequeFilterComponent, ChequeTableComponent, NewChequeComponent, InfoChequeComponent, AccountStatusListComponent, AccountStatusFilterComponent, AccountStatusTableComponent, AccountSummaryListComponent, AccountSummaryFilterComponent, AccountSummaryTableComponent, EditBankComponent, EditListComponent],
  imports: [
    CommonModule,
    BanksRoutingModule,
    NgZorroAntdModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class BanksModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
