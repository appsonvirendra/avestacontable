import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-transaction-filter',
  templateUrl: './transaction-filter.component.html',
  styleUrls: ['./transaction-filter.component.scss']
})
export class TransactionFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  // bankaccount search dialog
  searchBankaccountVisible = false
  bankaccountDatas = []

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.bankaccountData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      bank: ['0'],
      bankaccount_id: [null],
      bankaccount_no: [null],
      bankaccount_name: [null]
    })  
  }

  searchFilter() {
    this.searchEvent.emit(this.filterForm.value)
  }

  clearFilter() {
    this.filterForm = this.fb.group({
      date_from: [null],
      date_to: [null],
      source: ['0'],
      state: ['0'],
      reference_no: [null],
      bank: ['0'],
      bankaccount_id: [null],
      bankaccount_no: [null],
      bankaccount_name: [null]
    }) 
  }   

  // search bankaccount dialog
  searchBankaccountOpen() {
    this.searchBankaccountVisible = true
  }

  searchBankaccountClose() {
    this.searchBankaccountVisible = false
  }   

  bankaccountData() {
    this.bankaccountDatas = [
      {
        bankaccount_id: '1',
        bankaccount_no: '00001111',
        bank_name: 'Ficohsa',
        bank_id: '1',
        bank: 'Banco Financiera Comercial',
        bank_address: 'Avenida'
      }               
    ]
  }

  bankaccountAddInput($event) {
    this.filterForm.patchValue({
      bankaccount_id: $event.bankaccount_id,
      bankaccount_no: $event.bankaccount_no
    })

    this.searchBankaccountClose()    
  }  

}
