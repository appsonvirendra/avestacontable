import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {

  transactionDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.transactionData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.transactionDatas = []
  }
  
  transactionData() {
    this.transactionDatas = [
      {
        transaction_id: '1',
        transaction_date: '2018-12-23',
        transaction_reference: '00001',
        transaction_description: 'numero de transacion',
        transaction_bank: 'Banco Financiera',
        transaction_account_no: '00112233',
        transaction_amount: '300',
        transaction_state: '2'
      }, 
      {
        transaction_id: '2',
        transaction_date: '2018-12-23',
        transaction_reference: '00002',
        transaction_description: 'numero de transacion',
        transaction_bank: 'Banco Financiera',
        transaction_account_no: '00112233',
        transaction_amount: '300',
        transaction_state: '1'
      },
      {
        transaction_id: '3',
        transaction_date: '2018-12-23',
        transaction_reference: '00003',
        transaction_description: 'numero de transacion',
        transaction_bank: 'Banco Financiera',
        transaction_account_no: '00112233',
        transaction_amount: '300',
        transaction_state: '1'
      }, 
      {
        transaction_id: '4',
        transaction_date: '2018-12-23',
        transaction_reference: '00004',
        transaction_description: 'numero de transacion',
        transaction_bank: 'Banco Financiera',
        transaction_account_no: '00112233',
        transaction_amount: '300',
        transaction_state: '1'
      }                      
    ]
  }

}
