import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-info-transaction',
  templateUrl: './info-transaction.component.html',
  styleUrls: ['./info-transaction.component.scss']
})
export class InfoTransactionComponent implements OnInit {

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

}
