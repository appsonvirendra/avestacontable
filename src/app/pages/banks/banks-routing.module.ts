import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankAccountComponent } from './bank-account/bank-account.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { NewTransactionComponent } from './transaction/new-transaction/new-transaction.component';
import { InfoTransactionComponent } from './transaction/info-transaction/info-transaction.component';
import { ChequeListComponent } from './cheque/cheque-list/cheque-list.component';
import { NewChequeComponent } from './cheque/new-cheque/new-cheque.component';
import { InfoChequeComponent } from './cheque/info-cheque/info-cheque.component';
import { AccountStatusListComponent } from './account-status/account-status-list/account-status-list.component';
import { AccountSummaryListComponent } from './account-summary/account-summary-list/account-summary-list.component';
import { EditBankComponent } from './edit-bank/edit-bank.component';
import { EditListComponent } from './cheque/edit-list/edit-list.component';


const routes: Routes = [
  {
    path: 'bank-account',
    component: BankAccountComponent
  },
  {
    path: 'transaction/list',
    component: TransactionListComponent    
  },
  {
    path: 'transaction/new',
    component: NewTransactionComponent
  },
  {
    path: 'transaction/edit',
    component: EditBankComponent
  },
  {
    path: 'transaction/info',
    component: InfoTransactionComponent
  },
  {
    path: 'cheque/list',
    component: ChequeListComponent    
  },
  {
    path: 'cheque/new',
    component: NewChequeComponent
  },
  {
    path: 'cheque/info',
    component: InfoChequeComponent
  },
  {
    path: 'cheque/edit',
    component: EditListComponent
  },
  {
    path: 'account-status/list',
    component: AccountStatusListComponent    
  },
  {
    path: 'account-summary/list',
    component: AccountSummaryListComponent    
  }   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BanksRoutingModule { }
