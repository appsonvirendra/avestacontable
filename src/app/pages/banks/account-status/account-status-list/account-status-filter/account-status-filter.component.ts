import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-account-status-filter',
  templateUrl: './account-status-filter.component.html',
  styleUrls: ['./account-status-filter.component.scss']
})
export class AccountStatusFilterComponent implements OnInit {

  filterForm: FormGroup

  @Output() searchEvent = new EventEmitter<string>()  

  // bankaccount search dialog
  searchBankaccountVisible = false
  bankaccountDatas = []

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.filterFormInit()

    this.bankaccountData()
  }

  filterFormInit() {
    this.filterForm = this.fb.group({
      date_from: [null, [Validators.required]],
      date_to: [null],
      bankaccount_id: [null],
      bankaccount_no: [null, [Validators.required]]
    })  
  }

  searchFilter() {
    for (const i in this.filterForm.controls) {
      this.filterForm.controls[i].markAsDirty()
      this.filterForm.controls[i].updateValueAndValidity()
    }
    if(this.filterForm.valid) {
      this.searchEvent.emit(this.filterForm.value)
    }    
  }

  clearFilter() {
    this.filterForm.reset()
  }   

  // search bankaccount dialog
  searchBankaccountOpen() {
    this.searchBankaccountVisible = true
  }

  searchBankaccountClose() {
    this.searchBankaccountVisible = false
  }   

  bankaccountData() {
    this.bankaccountDatas = [
      {
        bankaccount_id: '1',
        bankaccount_no: '00001111',
        bank_name: 'Ficohsa',
        bank_id: '1',
        bank: 'Banco Financiera Comercial',
        bank_address: 'Avenida'
      }               
    ]
  }

  bankaccountAddInput($event) {
    this.filterForm.patchValue({
      bankaccount_id: $event.bankaccount_id,
      bankaccount_no: $event.bankaccount_no
    })

    this.searchBankaccountClose()    
  }

}
