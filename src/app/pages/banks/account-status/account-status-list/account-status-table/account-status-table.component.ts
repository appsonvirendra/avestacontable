import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-account-status-table',
  templateUrl: './account-status-table.component.html',
  styleUrls: ['./account-status-table.component.scss']
})
export class AccountStatusTableComponent implements OnInit {

  @Input() accountstatusData: any

  constructor() { }

  ngOnInit() {
  }

}
