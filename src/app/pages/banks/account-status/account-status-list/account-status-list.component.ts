import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-status-list',
  templateUrl: './account-status-list.component.html',
  styleUrls: ['./account-status-list.component.scss']
})
export class AccountStatusListComponent implements OnInit {

  accountstatusDatas: any

  activeCollapse = true  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.accountstatusData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.accountstatusDatas = []
  }
  
  accountstatusData() {
    this.accountstatusDatas = [
      {
        accountstatus_id: '1',
        accountstatus_initial_balance: '5000'
      }                   
    ]
  }

}
