export const url = {
  DEMO_URL: "https://jsonplaceholder.typicode.com/todos/1",
  All_TODOS_URL: "https://jsonplaceholder.typicode.com/todos?_limit=10",
  DUMMY_URL: "https://reqres.in",
  API_URL: "https://avestacontable-api.herokuapp.com/api"
}